#!/usr/bin/env bash

set -e

# remove all entities found at the path given as the only parameter
function remove() {
    path="$1"
    # database:remove is broken in firebase-tools 8.x with Node 10.x
    echo "{}" | firebase database:set "$path" --confirm
}

function log(){
  message="$1"
  echo "==========================================="
  echo "$message"
  echo "==========================================="
}

function wipeDatabase(){
  log "Removing functions since they may block data deletion"
  firebase functions:delete updateLeaderboard totalsUpdatedFromDay userDailyPoints --force || true
  log "Deleting all data"
  remove "/"
}

function runNpmScript(){
  script="$1"
  npm run "$script" --prefix ../functions
}

function deployData(){
  log "Deploying database rules"
  runNpmScript deploy:database

  log "Deploying foundational idempotent data"
  runNpmScript deploy:data

  log "Deploying identities"
  runNpmScript deploy:data:identities
}

# Make sure you authenticate with Google to create a user first.
function authorizeStationManager(){
    log "Authorizing station manager"
    runNpmScript config:auth:station
}

function configurePubSub(){
    log "Authorizing particle to publish scans to 'stations-scans' pubsub..."
    log "Authorizing publishing publish messages to school-tag project to associate scans"
    log "Deploy the functions using Bitbucket Pipelines ..."
    log "https://bitbucket.org/school-tag/school-tag-cloud/addon/pipelines/home"
    read -p "Press any key to continue..." -n 1 -r
    runNpmScript config:pubsub
}

function useProject(){
  read -p "Which environment?" -r -e
  env="$REPLY"
  if [[ "$env" == "sandbox" || "$env" == "playground" ]];then
    runNpmScript "env:$env"
  else
    export ENV="$env"
    runNpmScript "env:prod"
  fi
}

useProject
log "Displaying system that will be modified"
firebase use

read -p "This will delete everything!!! Are you sure?" -n 1 -r
echo    # (optional) move to a new line
if [[  $REPLY =~ ^[Yy]$ ]];then
  wipeDatabase
  deployData
  configurePubSub
  authorizeStationManager
  log "Now trigger a Pipeline to redeploy functions and test the system"
else
  log "Aborted"
  exit 1
fi

