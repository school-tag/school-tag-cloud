#!/bin/bash
# Exports the version JSON generated during a bitbucket pipelines build
# all $BITBUCKET_* come from environment variables
# https://confluence.atlassian.com/bitbucket/environment-variables-794502608.html

# see validation for explanation
_ENV=$1
_PROJECT=$2
_STATION_MANAGER_EMAIL=$3

if [ -z "$_ENV" ]; then
    echo "First parameter is the environment (sandbox, playground, production)"
    exit -1
fi

if [ -z "$_PROJECT" ]; then
    echo "Second parameter is the google cloud / firebase project hosting the system"
    exit -1
fi

if [ -z "$_STATION_MANAGER_EMAIL" ]; then
    echo "Third parameter is the Google Account email that is authorized to grant rewards."
    exit -1
fi

# dev is a very special case.  It expects a local web server, but uses the sandbox cloud
if [ "$_ENV" == "dev" ]; then
    webApplicationUrl="http://localhost:3000"
    cloudEnvironment="sandbox"
else
    cloudEnvironment="$_ENV"
    webApplicationUrl="https://$cloudEnvironment.schooltag.org"
fi


project="$_PROJECT"
commit="$BITBUCKET_COMMIT"
branch="$BITBUCKET_BRANCH"
buildNumber="$BITBUCKET_BUILD_NUMBER"
buildTime=$(date +%Y-%m-%dT%H:%M:%S%z)
baseUrl="https://bitbucket.org/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG"
branchUrl="$baseUrl/branch/$branch"
commitUrl="$baseUrl/commits/$commit"
buildUrl="$baseUrl/addon/pipelines/home#!/results/$buildNumber"
functionsUrl="https://us-central1-$project.cloudfunctions.net"
selfUrl="$functionsUrl/environments"
environment="$cloudEnvironment"

# using javascript, rather than JSON so require can simply load without extra utility
echo -e "//generated during the build by pipelines-env-generation.sh"
echo -e "module.exports.variables = {"
echo -e "  selfUrl: \"$selfUrl\","
echo -e "  branchUrl: \"$branchUrl\","
echo -e "  branch: \"$branch\","
echo -e "  commitUrl: \"$commitUrl\","
echo -e "  commit: \"$commit\","
echo -e "  buildUrl: \"$buildUrl\","
echo -e "  buildNumber: \"$buildNumber\","
echo -e "  buildTime: \"$buildTime\","
echo -e "  webApplicationUrl: \"$webApplicationUrl\","
echo -e "  functionsUrl: \"$functionsUrl\","
echo -e "  environment: \"$cloudEnvironment\","
echo -e "  project: \"$project\","
echo -e "  stationManagerEmail: \"$_STATION_MANAGER_EMAIL\","
echo -e "}"







