#!/usr/bin/env bash

# installs and runs tests and deploys to given firebase project if all goes well.
_FIREBASE_PROJECT=$1

_PROJECT_ROOT_DIR=$(pwd)
_FUNCTIONS_DIR=${_PROJECT_ROOT_DIR}/functions
_NODE_MODULES_DIR=${_FUNCTIONS_DIR}/node_modules
_NODE_MODULES_BIN_DIR=${_NODE_MODULES_DIR}/.bin
_FIREBASE_BIN=${_NODE_MODULES_BIN_DIR}/firebase


if [ -z ${_FIREBASE_PROJECT} ]; then
    echo "provide the firebase project id";
    exit 1;
fi

# fail script if anything fails
set -e

echo "cleaning test data: ---------------- $(date) ----------------"

${_FIREBASE_BIN} use ${_FIREBASE_PROJECT}
${_FIREBASE_BIN} database:remove /_test --token=$FIREBASE_TOKEN --confirm

exit 0
