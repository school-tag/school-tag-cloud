#!/usr/bin/env bash


# the release tag, in the form of release-${environment}-${version}
TAG=$1

IFS='-' read -ra TAG_PARTS <<< "$TAG"

export RELEASE_KEYWORD=${TAG_PARTS[0]}
if [ "$RELEASE_KEYWORD" == "release" ]; then
  export ENV=${TAG_PARTS[1]}
  export VERSION=${TAG_PARTS[2]}
  echo "releasing version ${VERSION} to ${ENV}"
  # env:prod expectes $ENV variable
  npm run env:prod
else
  echo "incorrect tag provided: '$TAG'"
  exit 1;
fi
