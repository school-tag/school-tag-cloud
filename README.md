## School Tag Cloud
_Gamify School Commutes_

Cloud logic applying the rules to the [School Tag Game][2]. 
Uses database triggers and http endpoints from [Firebase Functions][1] 
to reward users for scanning NFC tags.


### Game Play

A student arrives at school and scans an NFC sticker at a 
[School Tag Station][18]. The student is awarded points and the parent
is notified of the students arrival by using the [progressive web app][21]. 
Watch game play [at Willow Creek Academy][22], testing [on the playground][20] 
and development [in the sandbox][19].

<img src="https://docs.google.com/drawings/d/e/2PACX-1vRO1eCZLGSdDI55PygZUNrUdw9AwDS9WopiRWsP7RyXaz4vR1VXz4CC-I7BZWQIEUoaB6sHW8GEjSWW/pub?w=1032&amp;h=840">

### Developer Setup

Written in Node.js (Javascript) for [Firebase Functions][1].

**Requirements:** 

* Node Package Manager (npm)
* WebStorm (recommended)
* Firebase tools
* NFC Tag (for testing)


```bash
cd functions
```

All commands are executed in [functions](functions) directory.

```npm
npm install
```

Downloads and installs the dependencies in [package.json][3]

```npm
npm test
```

Runs the build:

* Unit Tests
* Cloud Tests
* Enforces 100% code coverage
* Enforces ESLint Code Quality 
  * Automatically corrects errors (if possible)

```bash
npm run test:coverage
```  
Views detailed code coverage in a web browser. Useful to pass coverage requirements.

  
```npm
npm run test:unit
```

Runs only unit tests that have no dependency on the cloud 
located in [test][4].

```npm
npm run test:cloud
```
Runs only tests that depend on the cloud (Firebase database, etc) 
located in [test-cloud][5].  Test data found [in the Sandbox database][7].

```npm
npm run test:deploy
```
Runs only tests that depend on the Functions deployed to the 
cloud. Validates code a triggers are working correctly in the cloud.
Located in [test-cloud-deploy][6].



### Developer Contributions

Improvements are always welcome. Please follow the required process.

[Create an Issue][8]
* Assign issue to yourself
* Change status to open
* Take note of issue #

[Create an Issue Branch][9]
* name the branch starting with issue number, followed by hyphen, then short description
  * 23-short-description
  * Only commit code related to the issue
  
**Make and Test changes**
* Make changes in the branch
* Run the build locally to ensure it will pass
* Commit and push code to Bitbucket
  * `issue #` is required in commit comments
  * so are useful comments explaining what you did
  * Merge _master_ into _issue branch_ to ensure all is up-to-date
* [Ensure Pipeline][10] build passes
* Manually test using the app _In the Sandbox_

[Make a pull request][11]
* Aaron Roller will approve and merge into master or request modifications
* Master build will deploy, if tests are successful, to the [Playground] 

### Environments

[The Sandbox][7] is where anything goes. 
* Tests run in _issue branches_ and _master_ always go against the sandbox. 
* Test data is removed periodically and often.

[The Playground][14] is where code plays well together.
* Stable(ish) and tested
* Issues have been accepted as relevant
* Code reviewed for quality and conventions
* Passed Tests _In the Sandbox_

[Demo][15] shows the features working well.
* Released code only
* Demonstrates all features in a stable environment
* Data rarely deleted
* Not used in any real game
* Planned: Currently not in use

[Live - WCA][16] where [Willow Creek Academy][17] game is played.
* A real game being played at a real school
* Released code only
* Data never deleted

**Live - _Your School_** where your school game could be played
* Easy to setup and administer
* Low costs paid directly to Google Cloud
* Assistance provided by School Tag developers

### Contributors? ###

* [Aaron Roller][12] - Creator, Developer
* [Lillie Roller][13] - Fun Consultant, Student Leader

[1]: https://firebase.google.com/docs/functions
[2]: https://schooltag.org
[3]: functions/package.json
[4]: functions/test
[5]: functions/test-cloud
[6]: functions/test-cloud-deploy
[7]: https://console.firebase.google.com/project/school-tag-sbox/database/data/_test
[8]: https://bitbucket.org/school-tag/school-tag-cloud/addon/bitbucket-trello-addon/trello-board
[9]: https://bitbucket.org/school-tag/school-tag-cloud/branches/
[10]: https://bitbucket.org/school-tag/school-tag-cloud/addon/pipelines/home
[11]: https://bitbucket.org/school-tag/school-tag-cloud/pull-requests/
[12]: https://bitbucket.org/aroller
[13]: https://bitbucket.org/lillie-the-best
[14]: https://console.firebase.google.com/project/school-tag-playground
[15]: https://console.firebase.google.com/project/school-tag
[16]: https://console.firebase.google.com/project/wca-school-tag
[17]: https://willowcreekacademy.org
[18]: https://bitbucket.org/aawhere/school-tag-station-android
[19]: https://sandbox.schooltag.org
[20]: https://playground.schooltag.org
[21]: https://bitbucket.org/school-tag/school-tag-app
[22]: https://wca.schooltag.org 