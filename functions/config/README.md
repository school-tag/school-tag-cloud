# Authorize a Station Manager

Assign the role `schoolStationManager` to a user in the system by targeting the email address.
This instructs the system that the user authenticated with such an email is authorized to create tags and identities.
The tags will be created when the user scans a tag that is not yet associated.

## Prerequisites:
1. Authenticate into application using the Google Account you wish to authorize as the manager
1. Ensure generated/application-env.js is targeting the environment you wish to modify.
   1. pipelines/bin/pipelines-env-generation.sh will generate
1. Ensure the email that you wish to authorize is declared in that application-env.js


## Steps to Run

1. Get New Private Key so the firebase-admin will be authorized.
   1. https://console.firebase.google.com/project/school-tag-lf/settings/serviceaccounts/adminsdk
   1. save in .firebase/school-tag-{school code}.json (see README in .firebase for more info)
1. firebase use school-tag-{school code}
1. npm run-script config:auth:station

Expect something similar to:
```text
 authorizing schooltagstation@gmail.com to be schoolStationManager
 schooltagstation@gmail.com previously had custom claims undefined
 schooltagstation@gmail.com(DQqqRWEaJDO6SMX26fn8siCxdP13) is now schoolStationManager
```

