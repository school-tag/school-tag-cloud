#!/usr/bin/env bash

#the project name (school-tag-playground)
project=$1

if [[ -z "$project" ]];then
  echo "Project ID is required as first parameter"
  exit -1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

gcloud pubsub topics add-iam-policy-binding projects/"$project"/topics/stations-scans --member=serviceAccount:particle-public@particle-public.iam.gserviceaccount.com --role=roles/pubsub.publisher

# grant permissions to the main school tag project to associate a tag with the new project
gcloud pubsub topics add-iam-policy-binding projects/school-tag/topics/associate-tag --member=serviceAccount:"$project"@appspot.gserviceaccount.com --role=roles/pubsub.publisher
