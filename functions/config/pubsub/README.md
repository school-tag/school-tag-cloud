# Particle - Gcloud Pubsub

Allow particle to publish messages on the google cloud pubsub
so a station scan can be forwarded to the corresponding Firebae function.

The `stations-scans` function is sent with regular deployment of the app. 
Authorization must be given after the function is deployed.


## Associate Tag Publisher

See [school-tag project](https://bitbucket.org/school-tag/school-tag-marketing) to understand how tags are associated to a school. This is done by
publishing a message to a pubsub topic that will persist the association. 

Grant access from the school project to the `school-tag` project's [pubsub topic](https://console.cloud.google.com/cloudpubsub/topic/list?project=school-tag).

