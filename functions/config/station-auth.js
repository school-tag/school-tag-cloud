/**
 *
 * Authorize an authenticated user to be the station manager for the school.
 * https://trello.com/c/wfgLah61
 *
 * See README.md in this directory.
 *
 * WARNING: No Code should reference this. Only intended for command line run
 * NOTE: This script is idempotent and may be run multiple times with the same effect.
 */
const env = require("env-var");

const admin = require("firebase-admin");
const project = env("npm_package_config_project").asString();
const firebaseCredsDir = env("npm_package_config_firebaseCredsDir").asString();
const serviceAccount = require(`../${firebaseCredsDir}/${project}`);
const schoolStationManagerEmail = env("npm_package_config_schoolStationManagerEmail").asString();
const SchoolGameRules = require("../main/game/school-game-rules");


admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: `https://${project}.firebaseio.com`,
});


//this is used in school-game-rule.json, but not elsewhere in the code
const role = SchoolGameRules.schoolStationManagerRoleName();

console.log(`${project} being modified.`);
console.log(`authorizing ${schoolStationManagerEmail} to be ${role} `);
admin.auth().getUserByEmail(schoolStationManagerEmail)
    .then(function (userRecord) {
        const uid = userRecord.uid;
        console.log(`${schoolStationManagerEmail} previously had custom claims`, userRecord.customClaims);
        const customClaims = {};
        customClaims[role] = true;
        admin.auth().setCustomUserClaims(uid, customClaims).then(() => {
            console.log(`${schoolStationManagerEmail}(${uid}) is now ${role}`);
        });
        //now create new user to identify the school tag station manager
        //id and authUid are the same making it easy to find
        const schoolStationManagerUserData = {
            authUid: userRecord.uid,
            avatar: "/school-tag-logo.png",
            nickname: "School Tag",
            id: userRecord.uid,
        };
        const userPath = `/users/${userRecord.uid}`;
        admin.database().ref(userPath).set(schoolStationManagerUserData)
            .then((result)=>{
            console.log(`Set ${userPath} to:`, schoolStationManagerUserData);
            process.exit(0);
        }).catch((error)=>{
            console.log(`Failed to create user ${userPath} .`, error);
            process.exit(1);
        });
    })
    .catch(function (error) {
        console.log("Error fetching user data:", error);
        process.exit(2);
    });

