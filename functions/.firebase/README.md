
# Firebase Admin SDK Setup

* Follow the instructions from [firebase admin setup](https://firebase.google.com/docs/admin/setup)
* Download the service file from [cloud console](https://console.cloud.google.com/iam-admin/serviceaccounts/project?project=school-tag-sbox&authuser=0&consoleReturnUrl=https:%2F%2Fconsole.firebase.google.com%2Fproject%2Fschool-tag-sbox%2Fanalytics%2Fapp%2Fandroid:org.schooltag.station.sandbox%2Fuserproperty&consoleUI=FIREBASE).
* Save the file as school-tag-sbox.json (or which project is declared in package json for the sandbox environment)
* run the cloud tests 
```
npm run-script test:cloud --prefix=functions
```

This directory may look something like this for an advanced programmer using multiple environments. 
 
```
.firebase
- README.md
- school-tag.json
- school-tag-playground.json
- school-tag-sbox.json
- school-tag-wca.json
```

The json files in this directory are ignored in git so these files exist locally only to avoid sharing credentials.
