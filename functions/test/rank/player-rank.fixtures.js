
const UserRewards = require("../../main/game/user-rewards");
const User = require("../../main/user/user");
const PlayerRank = require("../../main/rank/player-rank");
const UserFixtures = require("../../test/user/user-fixtures");
const highScore = 15;
const middleScore = 10;
const lowScore = 5;
const lowerScore = 3;
const a = "a";
module.exports.highestReward1= UserRewards.builder().points(highScore).userId(a).build();
const b = "b";
module.exports.highestReward2 = UserRewards.builder().points(highScore).userId(b).build();
const c = "c";
module.exports.middleReward1 = UserRewards.builder().points(middleScore).userId(c).build();
const d = "d";
module.exports.lowReward1 = UserRewards.builder().points(lowScore).userId(d).build();
const e = "e";
module.exports.lowReward2 = UserRewards.builder().points(lowScore).userId(e).build();
const f = "f";
module.exports.rewardWithoutPoints = UserRewards.builder().userId(f).build();
const g = "g";
module.exports.lowerReward1 = UserRewards.builder().points(lowerScore).userId(g).build();


module.exports.userA = User.builder().avatar("/avatar/a.png").nickname("A is for Apple").id(a).build();
module.exports.userB = User.builder().avatar("/avatar/b.png").nickname("Bi is for Boy").id(b).build();
module.exports.userC = User.builder().avatar("/avatar/c.png").nickname("C is for Cat").id(c).build();
module.exports.userD = User.builder().avatar("/avatar/d.png").nickname("D is for Dog").id(d).build();
module.exports.userE = User.builder().avatar("/avatar/e.png").nickname("E is for Egg").id(e).build();
module.exports.userF = User.builder().avatar("/avatar/f.png").nickname("F is for Fun").id(f).build();
module.exports.userG = User.builder().avatar("/avatar/g.png").nickname("G is for Good").id(g).build();

module.exports.users = {};
module.exports.users[a]=module.exports.userA.portable();
module.exports.users[b]=module.exports.userB.portable();
module.exports.users[c]=module.exports.userC.portable();
module.exports.users[d]=module.exports.userD.portable();
module.exports.users[e]=module.exports.userE.portable();
module.exports.users[f]=module.exports.userF.portable();
module.exports.users[g]=module.exports.userG.portable();

module.exports.playerRank1 = ()=>{
    const playerRank = new PlayerRank();
    playerRank.rank = 1;
    const user = UserFixtures.user1();
    playerRank.nickname = user.nickname();
    playerRank.userId = user.id().toString();
    playerRank.avatar = user.avatar();
    playerRank.points = 10;
    return playerRank;

};