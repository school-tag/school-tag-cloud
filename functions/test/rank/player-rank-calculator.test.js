const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const PlayerRankCalculator = require("../../main/rank/player-rank-calculator");
const UserRewardsUtils = require("../../main/game/user-rewards-utils");
const highestReward1 = require("../../test/rank/player-rank.fixtures").highestReward1;
const highestReward2 = require("../../test/rank/player-rank.fixtures").highestReward2;
const middleReward1 = require("../../test/rank/player-rank.fixtures").middleReward1;
const lowReward1 = require("../../test/rank/player-rank.fixtures").lowReward1;
const lowReward2 = require("../../test/rank/player-rank.fixtures").lowReward2;
const lowerReward1 = require("../../test/rank/player-rank.fixtures").lowerReward1;
const rewardWithoutPoints = require("../../test/rank/player-rank.fixtures").rewardWithoutPoints;
const users = require("../../test/rank/player-rank.fixtures").users;
const userA = require("../../test/rank/player-rank.fixtures").userA;
const userB = require("../../test/rank/player-rank.fixtures").userB;
const userC = require("../../test/rank/player-rank.fixtures").userC;
const userD = require("../../test/rank/player-rank.fixtures").userD;
const userE = require("../../test/rank/player-rank.fixtures").userE;
const userG = require("../../test/rank/player-rank.fixtures").userG;

const calculator = new PlayerRankCalculator(UserRewardsUtils.pointsValueFunction());


//the calculator is stateful and each step builds the story that adds more rewards to test the scenarios
//adding the lowest first, then middle, then lowest again, then highest forces proof or sorting regarless of order added
//adding duplicates shows points rewarded
describe("PlayerRankCalculator", () => {
    describe("with no rewards", () => {
        const groupRanks = calculator.calculate();
        it("group ranks should not be null", () => {
            expect(groupRanks).to.not.equal(null);
        });
        it("should report an no ranks", () => {
            expect(groupRanks.length).to.equal(0);
        });
    });
    describe("with the first lowest reward", () => {
        calculator.pointsRewarded(lowReward1);
        const rankingGroups = calculator.calculate();
        it("has a single rank", () => {
            expect(rankingGroups.length).to.equal(1);
        });
        describe("which creates the first ranking group", () => {
            const firstRankingGroup = rankingGroups[0];
            const expectedReward = lowReward1;
            it("in first place", () => {
                expect(firstRankingGroup.rank).to.equal(1);
            });
            it("with the correct points", () => {
                expect(firstRankingGroup.points).to.equal(expectedReward.points());
            });
            it("and the right id", () => {
                expect(firstRankingGroup.entities[0].userId()).to.deep.equal(expectedReward.userId());
            });
        });
    });
    describe("then a middle reward", () => {
        calculator.pointsRewarded(middleReward1);
        const rankingGroups = calculator.calculate();
        it("now has 2 ranks", () => {
            expect(rankingGroups.length).to.equal(2);
        });
        describe("with the first ranking group for middle group", () => {
            const rankingGroup = rankingGroups[0];
            const expectedReward = middleReward1;
            it("in first place", () => {
                expect(rankingGroup.rank).to.equal(1);
            });
            it("with the correct points", () => {
                expect(rankingGroup.points).to.equal(expectedReward.points());
            });
            it("and an entity with the expected id", () => {
                expect(rankingGroup.entities[0].userId()).to.deep.equal(expectedReward.userId());
            });
        });
        describe("and second ranking group for lowest group", () => {
            const rankingGroup = rankingGroups[1];
            const expectedReward = lowReward1;
            it("in second place", () => {
                expect(rankingGroup.rank).to.equal(2);
            });
            it("with the correct points", () => {
                expect(rankingGroup.points).to.equal(expectedReward.points());
            });
            it("and an entity with the expected id", () => {
                expect(rankingGroup.entities[0].userId()).to.deep.equal(expectedReward.userId());
            });
        });
    });
    describe("then the first highest reward", () => {
        calculator.pointsRewarded(highestReward1);
        const rankingGroups = calculator.calculate();
        it("now has 3 ranks", () => {
            expect(rankingGroups.length).to.equal(3);
        });
        describe("with the first ranking group for highest group", () => {
            const rankingGroup = rankingGroups[0];
            const expectedReward = highestReward1;
            it("in first place", () => {
                expect(rankingGroup.rank).to.equal(1);
            });
            it("with the correct points", () => {
                expect(rankingGroup.points).to.equal(expectedReward.points());
            });
            it("and an entity with the expected id", () => {
                expect(rankingGroup.entities[0].userId()).to.deep.equal(expectedReward.userId());
            });
        });
        describe("and second ranking group for middle group", () => {
            const rankingGroup = rankingGroups[1];
            const expectedReward = middleReward1;
            it("in second place", () => {
                expect(rankingGroup.rank).to.equal(2);
            });
            it("with the correct points", () => {
                expect(rankingGroup.points).to.equal(expectedReward.points());
            });
            it("and an entity with the expected id", () => {
                expect(rankingGroup.entities[0].userId()).to.deep.equal(expectedReward.userId());
            });
        });
        describe("and third ranking group for lowest group", () => {
            const rankingGroup = rankingGroups[2];
            const expectedReward = lowReward1;
            it("in third place", () => {
                expect(rankingGroup.rank).to.equal(3);
            });
            it("with the correct points", () => {
                expect(rankingGroup.points).to.equal(expectedReward.points());
            });
            it("and an entity with the expected id", () => {
                expect(rankingGroup.entities[0].userId()).to.deep.equal(expectedReward.userId());
            });
        });
    });
    describe("then the second highest reward", () => {
        calculator.pointsRewarded(highestReward2);
        const rankingGroups = calculator.calculate();
        it("still has 3 ranks since", () => {
            expect(rankingGroups.length).to.equal(3);
        });
        describe("first ranking group has a tie", () => {
            const rankingGroup = rankingGroups[0];
            const expectedReward = highestReward1;
            it("in first place", () => {
                expect(rankingGroup.rank).to.equal(1);
            });
            it("with the correct points", () => {
                expect(rankingGroup.points).to.equal(expectedReward.points());
            });
            it("and an entity with the expected id", () => {
                expect(rankingGroup.entities[0].userId()).to.deep.equal(expectedReward.userId());
            });
            it("and is tied with the highestReward2 ", () => {
                expect(rankingGroup.entities[1].userId()).to.deep.equal(highestReward2.userId());
            });
        });
        describe("and second ranking group for middle group", () => {
            const rankingGroup = rankingGroups[1];
            const expectedReward = middleReward1;
            it("in third place because of the first place tie", () => {
                expect(rankingGroup.rank).to.equal(3);
            });
            it("with the correct points", () => {
                expect(rankingGroup.points).to.equal(expectedReward.points());
            });
            it("and an entity with the expected id", () => {
                expect(rankingGroup.entities[0].userId()).to.deep.equal(expectedReward.userId());
            });
        });
        describe("and third ranking group for lowest group", () => {
            const rankingGroup = rankingGroups[2];
            const expectedReward = lowReward1;
            it("in fourth place", () => {
                expect(rankingGroup.rank).to.equal(4);
            });
            it("with the correct points", () => {
                expect(rankingGroup.points).to.equal(expectedReward.points());
            });
            it("and an entity with the expected id", () => {
                expect(rankingGroup.entities[0].userId()).to.deep.equal(expectedReward.userId());
            });
        });
    });

    describe("then the second lowest reward", () => {
        calculator.pointsRewarded(lowReward2);
        const rankingGroups = calculator.calculate();
        it("still has 3 ranks since", () => {
            expect(rankingGroups.length).to.equal(3);
        });
        describe("first place still has a tie", () => {
            const rankingGroup = rankingGroups[0];
            const expectedReward = highestReward1;
            it("in first place", () => {
                expect(rankingGroup.rank).to.equal(1);
            });
            it("with the correct points", () => {
                expect(rankingGroup.points).to.equal(expectedReward.points());
            });
            it("and an entity with the expected id", () => {
                expect(rankingGroup.entities[0].userId()).to.deep.equal(expectedReward.userId());
            });
            it("and is tied with the highestReward2 ", () => {
                expect(rankingGroup.entities[1].userId()).to.deep.equal(highestReward2.userId());
            });
        });
        describe("and second ranking group for middle group", () => {
            const rankingGroup = rankingGroups[1];
            const expectedReward = middleReward1;
            it("in third place because of the first place tie", () => {
                expect(rankingGroup.rank).to.equal(3);
            });
            it("with the correct points", () => {
                expect(rankingGroup.points).to.equal(expectedReward.points());
            });
            it("and an entity with the expected id", () => {
                expect(rankingGroup.entities[0].userId()).to.deep.equal(expectedReward.userId());
            });
        });
        describe("and third ranking group for lowest group", () => {
            const rankingGroup = rankingGroups[2];
            const expectedReward = lowReward1;
            it("in fourth place", () => {
                expect(rankingGroup.rank).to.equal(4);
            });
            it("with the correct points", () => {
                expect(rankingGroup.points).to.equal(expectedReward.points());
            });
            it("and an entity with the expected id", () => {
                expect(rankingGroup.entities[0].userId()).to.deep.equal(expectedReward.userId());
            });
            it("and tied with lowReward2", () => {
                expect(rankingGroup.entities[1].userId()).to.deep.equal(lowReward2.userId());
            });
        });
        describe("player ranks", () => {
            let assertPlayerRank = function (playerRank, reward, expectedRank, expectedUser) {
                expect(playerRank.userId).to.equal(reward.userId().toString());
                expect(playerRank.points).to.equal(reward.points());
                expect(playerRank.rank).to.equal(expectedRank);
                expect(playerRank.nickname).to.equal(expectedUser.nickname());
                expect(playerRank.avatar).to.equal(expectedUser.avatar());
            };
            const playerRanks = calculator.playerRanks(users);
            it("has proper length", () => {
                expect(playerRanks.length).to.equal(5);
            });
            it("first is highest reward 1", () => {
               assertPlayerRank(playerRanks[0], highestReward1, 1, userA);
            });
            it("second is highest reward 2", () => {
                assertPlayerRank(playerRanks[1], highestReward2, 1, userB);
            });
            it("third is middle reward", () => {
                assertPlayerRank(playerRanks[2], middleReward1, 3, userC);
            });
            it("fourth is lowest reward 1", () => {
                assertPlayerRank(playerRanks[3], lowReward1, 4, userD);
            });
            it("fifth is lowest reward 2", () => {
                assertPlayerRank(playerRanks[4], lowReward2, 4, userE);
            });
        });
    });
    describe("then a reward without points", () => {
        calculator.pointsRewarded(rewardWithoutPoints);
        const rankingGroups = calculator.calculate();
        it("still has 3 ranking groups", () => {
            expect(rankingGroups.length).to.equal(3);
        });
        describe("and third ranking group", () => {
            const rankingGroup = rankingGroups[2];
            it("is unchanged with two in the group", () => {
                expect(rankingGroup.entities.length).to.equal(2);
            });
        });
    });
    describe("then a lower score", () => {
        calculator.pointsRewarded(lowerReward1);
        const rankingGroups = calculator.calculate();
        it("now has 4 ranking groups", () => {
            expect(rankingGroups.length).to.equal(4);
        });
        describe("and the fourth ranking group", () => {
            const rankingGroup = rankingGroups[3];
            it("has one added", () => {
                expect(rankingGroup.entities.length).to.equal(1);
            });
            describe("player ranks", () => {
                let assertPlayerRank = function (playerRank, reward, expectedRank, expectedUser) {
                    expect(playerRank.userId).to.equal(reward.userId().toString());
                    expect(playerRank.points).to.equal(reward.points());
                    expect(playerRank.rank).to.equal(expectedRank);
                    expect(playerRank.nickname).to.equal(expectedUser.nickname());
                    expect(playerRank.avatar).to.equal(expectedUser.avatar());
                };
                const playerRanks = calculator.playerRanks(users);
                it("has proper length", () => {
                    expect(playerRanks.length).to.equal(6);
                });
                it("first is highest reward 1", () => {
                    assertPlayerRank(playerRanks[0], highestReward1, 1, userA);
                });
                it("second is highest reward 2", () => {
                    assertPlayerRank(playerRanks[1], highestReward2, 1, userB);
                });
                it("third is middle reward", () => {
                    assertPlayerRank(playerRanks[2], middleReward1, 3, userC);
                });
                it("fourth is lowest reward 1", () => {
                    assertPlayerRank(playerRanks[3], lowReward1, 4, userD);
                });
                it("fifth is lowest reward 2", () => {
                    assertPlayerRank(playerRanks[4], lowReward2, 4, userE);
                });
                //notice f is missing since it had no points
                it("sixth is lower reward 1 ranked in a new group", () => {
                    assertPlayerRank(playerRanks[5], lowerReward1, 6, userG);
                });
            });

        });
    });
});