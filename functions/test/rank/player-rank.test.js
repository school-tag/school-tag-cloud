const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const PlayerRankFixtures = require("../../test/rank/player-rank.fixtures");
describe("PlayerRank", () => {
    describe("portable", () => {
        it("returns self", () => {
            const rank1 = PlayerRankFixtures.playerRank1();
            expect(rank1.portable()).to.deep.equal(rank1);
        });
    });
});