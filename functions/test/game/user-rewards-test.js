const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));

const UserRewards = require("../../main/game/user-rewards.js");
const UserId = require("../../main/user/user.js").UserId;
const DateUtils = require("../../main/lang/date-utils.js");
const DateTestUtils = require("../../test/lang/date-test-utils.js");
const UserRewardsFixtures = require("../../test/game/user-rewards.fixtures");

describe("UserRewards", function () {
    describe("builder", function () {
        it("should't build without time interval", function () {
            expect(UserRewards.builder().build).to.throw();
        });
    });
    describe("timeInterval", function () {
        it("should retain local time zone", function () {
            const iso = DateTestUtils.rangeString1();
            const range = DateUtils.momentRange(iso);
            expect(range.start.utcOffset()).to.equal(-2 * 60);
            const rewards = UserRewards.builder().timeInterval(iso).build();
            expect(rewards.timeInterval().toString()).to.equal(iso);
            expect(rewards.hasTimeInterval()).to.equal(true);
        });
    });
    describe("coins", function () {
        it("should not have any by default", function () {
            const rewards = UserRewardsFixtures.userRewards1();
            expect(rewards.hasCoins()).to.equal(false);
        });
        it("should equal what is set", function () {
            const coins = 3;
            const rewards = UserRewardsFixtures.minimalBuilder().coins(coins).build();
            expect(rewards.hasCoins()).to.equal(true);
            expect(rewards.coins()).to.equal(coins);
        });
    });
    describe("metersToSchool", function () {
        it("should not have any by default", function () {
            const rewards = UserRewardsFixtures.userRewards1();
            expect(rewards.hasMetersToSchool()).to.equal(false);
        });
        it("should equal what is set", function () {
            const metersToSchool = 123;
            const rewards = UserRewardsFixtures.minimalBuilder().metersToSchool(metersToSchool).build();
            expect(rewards.hasMetersToSchool()).to.equal(true);
            expect(rewards.metersToSchool()).to.equal(metersToSchool);
        });
    });

    describe("id", function () {
        it("should be combination of interval and user", function () {
            const isoTimeInterval = DateTestUtils.rangeString1();
            const userId = new UserId("1235-abc");
            const rewards = UserRewards.builder()
                .timeInterval(isoTimeInterval)
                .userId(userId).build();
            expect(rewards.id()).to.contain(userId);
            expect(rewards.id()).to.contain(isoTimeInterval);
        });
        it("should be the time interval if no user provided", function () {
            const isoTimeInterval = DateTestUtils.rangeString1();
            const rewards = UserRewards.builder().timeInterval(isoTimeInterval).build();
            expect(rewards.id()).to.equal(isoTimeInterval);
        });
    });
    describe("pointsForRules", function () {
        it("should allow a map of rules to points", function () {
            const ruleId1 = "rule1";
            const ruleId2 = "rule2";
            const points1 = 10;
            const points2 = 22;

            const pointsForRules = {};
            pointsForRules[ruleId1] =points1;
            pointsForRules[ruleId2]= points2;
            const rewards = UserRewardsFixtures.minimalBuilder().pointsForRules(pointsForRules).build();
            expect(rewards.pointsForRules()[ruleId1]).to.equal(points1);
            expect(rewards.pointsForRules()[ruleId2]).to.equal(points2);
        });
    });
});
