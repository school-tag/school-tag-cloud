const UserRewards = require("../../main/game/user-rewards.js");
const UserId = require("../../main/user/user.js").UserId;
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const DateFixtures = require("../../test/lang/date-test-utils.js");

/**
 * Helper for {UserRewards} during testing
 */
class UserRewardsFixtures {
    /**@return {UserRewards} with for timestamp 1*/
    static userRewards1() {
        return this.minimalBuilder().build();
    }

    /**
     * @return {UserRewardsBuilder} ready to build
     */
    static minimalBuilder() {
        return UserRewardsFixtures.withoutTimeIntervalBuilder().timeIntervalForDay(DateFixtures.timestamp1());
    }

    /**
     * @return {UserRewardsBuilder} without a time interval
     */
    static withoutTimeIntervalBuilder() {
        return UserRewards.builder().id("user-rewards-1")
            .userId("user-1");
    }
}

module.exports = UserRewardsFixtures;
