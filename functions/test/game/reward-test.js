const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const RewardFixtures = require("../../test/game/reward-fixtures");
const Reward = require("../../main/game/reward");
const UserId = require("../../main/user/user").UserId;

describe("Reward", function () {
    describe("builder", function () {
        it("shouldn't build without required properties", function () {
            expect(Reward.builder().build).to.throw();
        });
        it("should build with minimal providing defaults", function () {
            /**@type {Reward}*/
            const reward = RewardFixtures.builder1WithMinimal().build();
            expect(reward.timeReceived()).not.to.equal(null, "because it assigns now");
            expect(reward.scanId()).to.equal(RewardFixtures.scanId1());
            expect(reward.ruleId()).to.equal(RewardFixtures.ruleId1());
            expect(reward._property(Reward.prototype.userId.name)).not.to.be.instanceOf(UserId);
            expect(reward.userId()).to.deep.equal(RewardFixtures.recipientUserId1());
        });
        it("should set the id from the combine ids", function () {
            const reward = RewardFixtures.builder1WithMinimal().build();
            expect(reward.id()).to.not.equal(null);
            expect(reward.id()).to.contain(reward.userId().toString());
            expect(reward.id()).to.contain(reward.scanId());
            expect(reward.id()).to.contain(reward.ruleId());
        });
    });
    describe("coins", function () {
        it("should not have any by default", function () {
            const reward = RewardFixtures.reward1();
            expect(reward.coins()).to.be.undefined;
            expect(reward.hasCoins()).to.equal(false);
        });
        it("should have coins even if zero", function () {
            const reward = RewardFixtures.builder1WithMinimal().coins(0).build();
            expect(reward.coins()).to.equal(0);
            expect(reward.hasCoins()).to.equal(true);
        });
        it("undefined should be ignored", function () {
            let coins;
            const reward = RewardFixtures.builder1WithMinimal().coins(coins).build();
            expect(reward.hasCoins()).to.equal(false);
        });
    });
});
