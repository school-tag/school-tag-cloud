const Moment = require("moment");
const MomentRange = require("moment-range");
const moment = MomentRange.extendMoment(Moment);
const Firebase = require("firebase");
const Database = require("firebase").database.Database;
const Reference = require("firebase").database.Reference;

const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));

const UserRewards = require("../../main/game/user-rewards.js");
const Reward = require("../../main/game/reward.js");
const Scan = require("../../main/scan/scan.js");
const UserRewardsRepository = require("../../main/game/user-rewards-repository.js");
const GameCalculator = require("../../main/game/game-calculator.js");
const RuleRepository = require("../../main/rule/rule-repository.js");
const UserId = require("../../main/user/user.js").UserId;
const DateUtils = require("../../main/lang/date-utils.js");
const DateFixtures = require("../../test/lang/date-test-utils.js");
const RewardTestUtils = require("./reward-fixtures.js");
const UserRewardTestUtils = require("../../test/game/user-rewards.fixtures");
const FireTestUtils = require("../../test/firebase/firebase-test-utils.js");

describe("UserRewardsRepository", function () {
    describe("userDailyTotalsRef", function () {
        it("should provide year/month/day/userId ref", function () {
            const repository = new UserRewardsRepository(FireTestUtils.mockReference());
            const userId = new UserId("foo");
            const moment = DateFixtures.timestamp1();
            const ref = repository.userDailyTotalsRef(userId, moment);
            const userRewardsDayRef = "/rewards/users/foo/totals/dailies/years/2017/weeks/23/days/4";
            expect(ref.path).to.equal(userRewardsDayRef);
        });
    });
    describe("weekRef", function () {
        /** https://bitbucket.org/school-tag/school-tag-cloud/issues/4 */
        it("should return iso week for sunday", function () {
            const ref = UserRewardsRepository.weekRef(DateFixtures.timestampSunday1(), FireTestUtils.mockReference());
            expect(ref.path).to.equal("/years/2017/weeks/" + DateFixtures.timestampSunday1IsoWeek());
        });
    });
    describe("setRewardsForScan", function () {
        it("set the rewards to the userId/scans path", function (done) {
            const userId = new UserId("foo");
            const scanId = "scan-id-1";
            const ruleId = "rule-1";
            const expectedChildren = [userId.toString(), scanId];
            const database = {
                child: function (pathSegment) {
                    return this;
                },
                set: function (rewards) {
                    expect(Object.keys(rewards)).to.be.lengthOf(1);
                    expect(rewards).to.have.property(ruleId);
                    //this is the money
                    done();
                },
            };
            const repository = new UserRewardsRepository(database);
            repository.userDailyTotalsRef = function () {
                //tested elswhere
                return database;
            };
            const moment = DateFixtures.timestamp1();
            const scan = Scan.builder().id(scanId).timestamp(moment).tagId("foo").build();
            const userRewards = UserRewards.builder().userId(userId).timeIntervalForDay(moment).build();

            const rewardsForScan = [Reward.builder()
                .ruleId(ruleId)
                .userId(userId)
                .points(10)
                .scanId(scan.id())
                .timeReceived(DateFixtures.timestamp1())
                .build(),
            ];
            const promise = repository.setRewardsForScan(userRewards, scan, rewardsForScan).then(
                function onSuccess() {
                    done();
                });

            expect(ref.path).to.equal("/rewards/years/2017/months/6/days/8/users/foo");
        });
    });
    describe("rewardsFromScans", () => {
        it("should return the rewards from multiple scans", function () {

            /**@type Reward*/
            const reward1 = RewardTestUtils.builder1WithMinimal().build();
            //match the structure in firebase console
            //.../users/{userId}/scans/{scanId}/rules/everyScanCounts
            const scans = {
                "scan-id-doesnt-matter-1": {
                    "rules": {
                        "rule-id-doesnt-matter-1-1": {
                            points: 1,
                        },
                        "rule-id-doesnt-matter-1-2": {
                            points: 2,
                        },
                    },

                },
                "scan-id-doesnt-matter-2": {
                    "rules": {
                        "rule-id-doesnt-matter-2-1": {
                            points: 3,
                        },
                        "rule-id-doesnt-matter-2-2": {
                            points: 4,
                        },
                    },
                },
            };

            const repository = new UserRewardsRepository({});
            const rewards = repository.rewardsFromScans(reward1.userId(), scans);
            expect(rewards).to.have.lengthOf(4);
            const actualReward1 = rewards[0];
            expect(actualReward1.ruleId()).to.equal("rule-id-doesnt-matter-1-1");
            expect(actualReward1.points()).to.equal(1);

            const actualReward2 = rewards[3];
            expect(actualReward2.ruleId()).to.equal("rule-id-doesnt-matter-2-2");
            expect(actualReward2.points()).to.equal(4);

            //this test could be improved by proving each 4 were retrieved.
        });
    });
    describe("userRewardsForDay", () => {
        let assertUserRewards = function (expected) {
            const reference = FireTestUtils.mockReference();
            const repository = new UserRewardsRepository(reference);
            repository._snapshot = (ref) => {
                return Promise.resolve({
                    key: expected.id(),
                    hasChildren: () => {
                        return true;
                    },
                    val: () => {
                        return expected.portable();
                    },
                    exists: () => {
                        return true;
                    },
                });
            };
            return repository.userRewardsForDay(expected.userId(), DateFixtures.timestamp1()).then(
                /** @param {UserRewards} userRewards*/
                (userRewards) => {
                    expect(userRewards).to.not.equal(null);
                    //expect(userRewards.id()).to.equal(expected.id());
                    expect(userRewards.points()).to.equal(expected.points());
                    expect(userRewards.userId().toString()).to.equal(expected.userId().toString());
                    expect(userRewards.userId()).to.be.instanceOf(UserId);
                    expect(userRewards.hasTimeInterval()).to.equal(true);
                });
        };
        it("should build entity from properties", function () {
            const expected = UserRewardTestUtils.userRewards1();
            return assertUserRewards(expected);
        });
        it("should assign a time interval if not provided", function () {
            const expected = UserRewardTestUtils.withoutTimeIntervalBuilder().build();
            return assertUserRewards(expected);
        });
    });
    describe("dayRef", () => {
        it("should should return time units with verbose", function () {
            const moment = DateFixtures.timestamp1();
            const dayRef = UserRewardsRepository.dayRef(moment, FireTestUtils.mockReference());
            expect(dayRef.path).to.equal("/years/" + moment.year() + "/weeks/" + moment.isoWeek() + "/days/" + moment.isoWeekday());
        });
    });
    describe("momentFromDayRef", () => {
        it("should return the moment produced by dayRef", function () {
            const moment = DateFixtures.timestamp1();
            const dayRef = UserRewardsRepository.dayRef(moment, FireTestUtils.mockReference());
            const momentFromRef = UserRewardsRepository.momentFromDayRef(dayRef);
            expect(momentFromRef.year()).to.equal(moment.year());
            expect(momentFromRef.isoWeek()).to.equal(moment.isoWeek());
            expect(momentFromRef.isoWeekday()).to.equal(moment.isoWeekday());
        });
        it("should return the month and year", function () {
            const expectedMoment = DateFixtures.timestamp1();
            const dayRef = UserRewardsRepository.dayRef(expectedMoment, FireTestUtils.mockReference());
            const monthRef = dayRef.parent.parent;
            const actualMoment = UserRewardsRepository.momentFromDayRef(monthRef);
            expect(actualMoment.isoWeek()).to.equal(actualMoment.isoWeek());
            expect(actualMoment.year()).to.equal(actualMoment.year());
        });
        it("should return the year", function () {
            const expectedMoment = DateFixtures.timestamp1();
            const dayRef = UserRewardsRepository.dayRef(expectedMoment, FireTestUtils.mockReference());
            const yearRef = dayRef.parent.parent.parent.parent;
            const actualMoment = UserRewardsRepository.momentFromDayRef(yearRef);
            expect(actualMoment.year()).to.equal(actualMoment.year());
        });
        it("should recognize sunday as the end of the week", function () {
            const expectedMoment = DateFixtures.timestampSunday1();
            const dayRef = UserRewardsRepository.dayRef(expectedMoment, FireTestUtils.mockReference());
            const actualMoment = UserRewardsRepository.momentFromDayRef(dayRef);
            expect(actualMoment.isoWeekday()).to.equal(DateFixtures.timestampSunday1IsoWeekday());
            expect(actualMoment.isoWeek()).to.equal(DateFixtures.timestampSunday1IsoWeek());
        });

    });
    describe("userIdFromRef", () => {
        it("should return the user id for a valid path ", function () {
            const expectedUserId = new UserId("foo");
            //add a prefix to prove it isn't null parent stopping the search
            //
            const prefixRef = FireTestUtils.mockReference().child("before").child("is").child("something");
            const ref = UserRewardsRepository.userRef(expectedUserId, prefixRef)
                .child("anything").child("goes").child("here");

            const actualUserId = UserRewardsRepository.userIdFromRef(ref);
            expect(actualUserId.toString()).to.equal(expectedUserId.toString());
        });
        it("should throw when missing users ", function () {
            const ref = FireTestUtils.mockReference().child("junk");
            expect(function () {
                UserRewardsRepository.userIdFromRef(ref);
            }).to.throw();
        });
    });
    describe("setUserRewardsForPeriodFromCalculator", () => {
        it("should return nothing for a calculator with no rewards", function () {
            //empty rewards is the key to this test
            const calculator = GameCalculator.builder().rewards([]).build();
            //actual ref doesn't matter
            let ref = {};
            const userRewardsRepository = new UserRewardsRepository(ref);
            //ok, nothing really to test besides that it returns
            //just covering a branch
            return userRewardsRepository.setUserRewardsForPeriodFromCalculator(ref, calculator);
        });
        it("should throw when missing users ", function () {
            const ref = FireTestUtils.mockReference().child("junk");
            expect(function () {
                UserRewardsRepository.userIdFromRef(ref);
            }).to.throw();
        });
    });
    describe("userRewardsForPeriod", () => {
        const userId = new UserId("foo");
        const moment = DateFixtures.timestamp1();
        const dayMomentRangeFunction = DateUtils.weekdayMomentRangeFunction(moment);
        it("should return empty array for empty children ", function () {
            const monthWithNoDays = {};

            const dailyUserRewards = UserRewardsRepository.userRewardsForPeriod(userId, monthWithNoDays);
            expect(dailyUserRewards).to.have.lengthOf(0);
        });
        it("month with three days of rewards should have points", function () {
            const points1 = 11;
            const points2 = 22;
            const points3 = 33;
            const monthWithThreeDays = {
                1: {
                    points: points1,
                },
                2: {
                    points: points2,
                },
                3: {
                    points: points3,
                },
            };
            const dailyUserRewards = UserRewardsRepository.userRewardsForPeriod(
                userId,
                monthWithThreeDays,
                dayMomentRangeFunction);
            expect(dailyUserRewards).to.have.lengthOf(3);
            expect(dailyUserRewards[0].points()).to.equal(points1);
            expect(dailyUserRewards[1].points()).to.equal(points2);
            expect(dailyUserRewards[2].points()).to.equal(points3);
        });
    });
    describe("calculatorTotals", function () {
        describe("minimalReward", function () {
            const reward = RewardTestUtils.reward1();
            const calculator = GameCalculator.builder().rewards([reward]).build();
            const totals = UserRewardsRepository.totals(calculator);
            it("should include points", function () {
                expect(totals.points).to.equal(reward.points());
            });
            it("should include points for rules", function () {
                expect(Object.keys(totals.pointsForRules)).to.have.lengthOf(1);
                expect(totals.pointsForRules[reward.ruleId()]).to.equal(reward.points());
            });
            it("should have zero coins", function () {
                expect(totals.coins).to.equal(0);
                //would be better as undefined
            });
            it("should include user id", function () {
                expect(totals.userId).to.equal(reward.userId().toString());
            });
            it("should not include distance", function () {
                expect(totals.metersToSchool).to.be.undefined;
            });
        });
        it("should include coins", function () {
            const reward = RewardTestUtils.builder1WithMinimal().coins(3).build();
            const calculator = GameCalculator.builder().rewards([reward]).build();
            const totals = UserRewardsRepository.totals(calculator);
            expect(totals.coins).to.equal(reward.coins());
        });
        it("should include distance", function () {
            const reward = RewardTestUtils.builder1WithMinimal().metersToSchool(30).build();
            const calculator = GameCalculator.builder().rewards([reward]).build();
            const totals = UserRewardsRepository.totals(calculator);
            expect(totals.metersToSchool).to.equal(reward.metersToSchool());
        });
    });
});
