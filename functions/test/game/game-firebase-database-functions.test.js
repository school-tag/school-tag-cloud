const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const GameFirebaseDatabaseFunctions = require("../../main/game/game-firebase-database-functions.js");
const RewardFixtures = require("../../test/game/reward-fixtures");
const ScanFixtures = require("../../test/scan/scan-fixtures.js");

describe("GameFirebaseDatabaseFunctions", function () {
    describe("rewards for scan", function () {
        it("should notify recipients", function () {
            //basic call validation of mocks
            //relies on cloud testing of services to do the heavy lifting
            let notificationCalled = false;
            const expectedReward = RewardFixtures.reward1();

            const bootstrap = {
                userRewardsService: {
                    notifyFollowersOfReward: (actualReward) => {
                        notificationCalled = true;
                        expect(actualReward.ruleId()).to.equal(expectedReward.ruleId());
                        return Promise.resolve();
                    },
                },
            };
            const functions = new GameFirebaseDatabaseFunctions(bootstrap);
            const snapshot = {
                val: () => {
                    return expectedReward;
                },
            };
            return functions.notifyOfRuleReward(snapshot)
                .then(() => {
                    expect(notificationCalled).to.equal(true);
                });
        });
    });
    describe("updateLeaderboard", function () {
        it("passes it on to the rank service", function () {
            const token = "yay";
            const bootstrap = {
                playerRankService: {
                    updateTotalRanks: () => {
                        return Promise.resolve(token);
                    },
                },
            };
            return new GameFirebaseDatabaseFunctions(bootstrap).updateLeaderboard().then((result) => {
                expect(result).to.equal(token);
            });
        });
    });
    describe("scansCreated", function () {
        it("should return zero rewards for zero scans", function () {
            const functions = new GameFirebaseDatabaseFunctions({});
            return functions.scansCreated([]).then((calculator)=>{
                   expect(calculator.hasRewards()).to.be.false;
            });
        });
        it("should log an error when tag service fails to associate a tag", function () {
            const scan1 = ScanFixtures.scanWithoutId();
            const scans = [scan1];
            let errorReported = false;
            const errorMessage = "forced failure";
            const bootstrap = {
                tagService: {
                    associateToSchool: function () {
                        return Promise.reject(errorMessage);
                    },
                },
                scanService: {
                    tagForScan: function () {
                        return Promise.reject("not testing this part");
                    },
                },
            };
            const functions = new GameFirebaseDatabaseFunctions(bootstrap);
            functions.reportTagAssociationError = (tagId, error) => {
                expect(tagId).to.equal(scan1.tagId());
                expect(error).to.equal(errorMessage);
                errorReported = true;
            };
            functions.scanCreated = () => {
                return Promise.resolve(ScanFixtures.scan1());
            };
            return functions.scansCreated(scans).then((scansCreated) => {
                fail("mock should reject");
            }).catch(()=>{
                expect(errorReported).to.equal(true);
            });
        });
    });
    describe("reportTagAssociationError", function () {
        it("just logs an error", function () {
            const functions = new GameFirebaseDatabaseFunctions();
            functions.reportTagAssociationError("some-id","ignore since this is a test");
            //forces required test coverage so passing test coverage is the indication of success.
        });
    });
});
