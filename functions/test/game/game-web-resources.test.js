const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const UserFixtures = require("../../test/user/user-fixtures");
const GameWebResources = require("../../main/game/game-web-resources");
const SystemBootstrap = require("../../main/bootstrap.js");

describe("GameWebResources", function () {
    describe("messageBuilder", function () {
        const gameWebResources = new GameWebResources(SystemBootstrap.applicationVersion());
        const user = UserFixtures.user1();
        const message = gameWebResources.messageBuilderForUserNotifications(user).build();
        it("should assign user url", function () {
            expect(message.hasMoreInfoUrl()).to.equal(true);
            expect(message.moreInfoUrl()).to.equal(`${gameWebResources.webApplicationHomeUrl()}/users/${user.nickname().toString()}`);
        });
        it("should assign icon url", function () {
            expect(message.hasIconUrl()).to.equal(true);
            expect(message.iconUrl()).to.equal(`${gameWebResources.webApplicationHomeUrl()}/assets/icon/favicon-32x32.png`);
        });
        it("should assign about user", function () {
            expect(message.hasAboutUser()).to.equal(true);
            expect(message.aboutUser()).to.deep.equal(user);
        });
    });
});
