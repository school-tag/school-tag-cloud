const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const moment = require("moment");
const describe = require("mocha").describe;
const beforeEach = require("mocha").beforeEach;

const ScanFixtures = require("../../test/scan/scan-fixtures.js");
const DateUtils = require("../../main/lang/date-utils.js");
const UserRewardsTestUtils = require("../../test/game/user-rewards.fixtures");
const RuleService = require("../../main/rule/rule-service.js");
const SchoolGameRules = require("../../main/game/school-game-rules.js");
const GameService = require("../../main/game/game-service.js");
const RuleRepository = require("../../main/rule/rule-repository.js");
const TagFixtures = require("../../test/tag/tag-fixtures.js");
const rules = require("../../data/rules/rules-sandbox.json");
const SystemBootstrap = require("../../main/bootstrap");
const DateFixtures = require("../../test/lang/date-test-utils");
const UserFixtures = require("../../test/user/user-fixtures");
const AuthService = require("../../main/user/auth-service");
const Station = require("../../main/station/station");
const StationService = require("../../main/station/station.service");

const GameTestUtils = require("../../test/game/game-test-utils");

let scanWithMoment = function (scan, moment) {
    return scan.toBuilder().timestamp(moment).build();
};

//represents the station to indicate arrivals.
const schoolStation = Station.builder().name("School").type(SchoolGameRules.stationTypeArrival()).id("school").build();

/**
 * tests the rules in an isolated way, but does not handle state to ensure multiple scans will be blocked leaving that
 * up to the end to end tests.
 */
describe("school game rules", function () {

    describe("environment", function () {
        it("should be sandbox", function () {
            expect(SystemBootstrap.applicationVersion().environment()).to.equal(GameTestUtils.testEnvironmentName());
        });
    });

    //mocking out connected resources with test data
    /**@type {UserRewardsService}*/
    const userRewardsService = {
        userRuleRewardCountForDayOfScan: () => {
            //for happy path testing, the scan not already made
            return 0;
        },
        userIdFromScan: () => {
            return Promise.resolve(UserFixtures.user1Id());
        },
        totalPointsForUserFromScanForRule: () => {
            return Promise.resolve(0);
        },
    };

    /**@type {StationService}*/
    const stationService = {
        stationById: (id) => {
            switch (id) {
                case schoolStation.id():
                    return Promise.resolve(schoolStation);
                default:
                    return Promise.reject(Errors.notFound("stations/fixture"));
            }
        },
    };
    const authIdWithSchoolStationManagerClaims = "auth-id-1";
    const authService = new AuthService({
        getUser: (authUserId) => {
            const customClaims = {};
            switch (authUserId) {
                case authIdWithSchoolStationManagerClaims:
                    customClaims[SchoolGameRules.schoolStationManagerRoleName()] = true;
                    break;
                default:
                    throw new Error(`authUid='${authUserId}' not handled`);
            }
            const userRecord = {customClaims: customClaims};
            return Promise.resolve(userRecord);
        },
    });

    const tag1 = TagFixtures.tag1();
    const bikeTag = TagFixtures.tag1().toBuilder().category(SchoolGameRules.tagCategoryBike()).build();
    const walkStationTag = TagFixtures.tag1().toBuilder()
        .category(SchoolGameRules.tagCategoryStation())
        .type(SchoolGameRules.tagTypeTag()).build();
    //most scanning is done by the station manager. Consider a new test class for testing non-station
    const scan1 = ScanFixtures.scan1().toBuilder()
        .tagId(tag1.id())
        .station(schoolStation.id())
        .authUid(authIdWithSchoolStationManagerClaims).build();
    const busScan = ScanFixtures.scan1().toBuilder().tagId(tag1.id()).authUid(authIdWithSchoolStationManagerClaims)
        .category(SchoolGameRules.tagCategoryBus()).build();
    const bikeScan = ScanFixtures.scanSunday1().toBuilder().tagId(bikeTag.id()).build();
    //run the rules engine
    const userRewards = UserRewardsTestUtils.userRewards1();
    const fridayMarch3rd0730 = "2017-03-03T07:30:00-02:00";
    const early = moment.parseZone(fridayMarch3rd0730);
    const onTime = moment(early).add(40, "minutes");
    //no rewards for being too early before the game starts
    const tooEarly = moment(early).subtract(1, "second");
    //8:15 is late
    const lateStartTime = moment(early).add(45, "minutes");
    //no points given for being too lateStartTime after the game ends
    const tooLate = moment(lateStartTime).add(2, "hours");

    /**adds 1 day to one of the times above that would be rewarded on the friday, but not on saturday at the same time.
     *
     * @param {moment.Moment} rewardedMoment the time on friday the reward would pass
     * @param {RuleService} ruleService
     * @return {Promise.<TResult>}
     */
    const notRewardedOnWeekends = function (rewardedMoment, ruleService) {
        //notice it is in the time window, just wrong day
        const saturday = moment(rewardedMoment).add(1, DateUtils.UnitOfTime.day());
        const scan = scan1.toBuilder().timestamp(saturday).build();

        return gameServiceForRules(ruleService).rewardsForScan(scan, tag1).then(function (rewards) {
            expect(rewards).to.have.lengthOf(0);
        });
    };

    let ruleIncludedInAll = function (targetRuleId) {
        let included = false;
        SchoolGameRules.allRules().forEach((rule) => {
            if (rule.event.type == targetRuleId) {
                included = true;
            }
        });
        expect(included).to.equal(true);
    };

    let gameServiceForRules = function (ruleService, userRewardsServiceParam) {
        if (!userRewardsServiceParam) {
            userRewardsServiceParam = userRewardsService;
        }
        return GameService.builder()
            .ruleService(ruleService)
            .momentService(DateFixtures.momentService())
            .userRewardsService(userRewardsServiceParam)
            .stationService(stationService)
            .build();
    };
    /**
     *
     * @param {moment.Moment} moment
     * @param {RuleService} ruleService
     * @param {UserRewardService?} userRewardsServiceParam
     * @return {Promise<Reward[]>}
     */
    let notRewarded = function (moment, ruleService, userRewardsServiceParam) {
        const scan = scan1.toBuilder().timestamp(moment).build();
        const gameService = gameServiceForRules(ruleService, userRewardsServiceParam);
        return gameService.rewardsForScan(scan, tag1).then(function (rewards) {
            expect(rewards).to.have.lengthOf(0);
        });
    };
    let shouldBeRewardedForScanAndTag = function (scan, tag, expectedPoints, expectedRuleId, ruleService) {
        const gameService = gameServiceForRules(ruleService);
        return gameService.rewardsForScan(scan, tag).then(function (rewards) {
            expect(rewards).to.not.equal(null);
            expect(rewards, JSON.stringify(rewards)).to.have.lengthOf(1);
            const reward = rewards[0];
            expect(reward.points()).to.equal(expectedPoints);
            expect(reward.ruleId()).to.equal(expectedRuleId);
            return rewards;
        });
    };
    let shouldBeRewarded = function (moment, expectedRuleId, expectedPoints, ruleService) {
        const scan = scanWithMoment(scan1, moment);
        return shouldBeRewardedForScanAndTag(scan, tag1, expectedPoints, expectedRuleId, ruleService);
    };
    let shouldBeRewardedWithCoinsToo = function (moment, expectedRuleId, expectedPoints, expectedCoins, ruleService) {
        return shouldBeRewarded(moment, expectedRuleId, expectedPoints, ruleService).then((rewards) => {
            expect(rewards[0].coins()).to.equal(expectedCoins);
            return rewards;
        });
    };
    let rulesRepository = function (rules, userRewardsServiceParam) {
        if (!userRewardsServiceParam) {
            userRewardsServiceParam = userRewardsService;
        }
        const repository = new RuleRepository(rules,
            SchoolGameRules.schoolGameOperators(),
            SystemBootstrap._facts(userRewardsServiceParam, authService));
        return repository;
    };

    describe("morningArrivalOnTimeRule", function () {
        const onTimeRuleService = new RuleService(rulesRepository([SchoolGameRules.morningArrivalOnTimeRule()]));
        describe("for the first on time scan on a school day", function () {
            const expectedPoints = SchoolGameRules.morningArrivalOnTimePoints();
            const moment = onTime;
            const expectedRuleId = SchoolGameRules.morningArrivalOnTimeRuleId();
            it("should be rewarded by the school station manager", function () {
                return shouldBeRewarded(moment, expectedRuleId, expectedPoints, onTimeRuleService);
            });
            it("should not be rewarded by anyone else", function () {
                //write a custom negative case to prove manage auth is working
                const scan = scan1.toBuilder().authUid(null).timestamp(moment).build();
                const gameService = gameServiceForRules(onTimeRuleService);
                return gameService.rewardsForScan(scan, tag1).then(function (rewards) {
                    expect(rewards).to.have.lengthOf(0);
                });
            });
        });

        it("should not be rewarded for being late", function () {
            return notRewarded(lateStartTime, onTimeRuleService);
        });
        it("should not be rewarded for being too early", function () {
            return notRewarded(tooEarly, onTimeRuleService);
        });
        it("should not be rewarded for a weekend", function () {
            return notRewardedOnWeekends(early, onTimeRuleService);
        });
        it("should be included in all rules", function () {
            ruleIncludedInAll(SchoolGameRules.morningArrivalOnTimeRuleId());
        });

    });
    describe("morningArrivalLateRule", function () {
        const lateRuleService = new RuleService(rulesRepository([SchoolGameRules.morningArrivalLateRule()]));
        it("should be rewarded for late start", function () {
            return shouldBeRewarded(
                lateStartTime,
                SchoolGameRules.morningArrivalLateRuleId(),
                SchoolGameRules.morningArrivalLatePoints(),
                lateRuleService);
        });
        it("should not be rewarded for being on time", function () {
            return notRewarded(early, lateRuleService);
        });
        it("should not be rewarded for a weekend", function () {
            return notRewardedOnWeekends(lateStartTime, lateRuleService);
        });
        it("should be included in all rules", function () {
            ruleIncludedInAll(SchoolGameRules.morningArrivalLateRuleId());
        });

    });
    describe("morningArrivalEarlyRule", function () {
        const earlyRuleService = new RuleService(rulesRepository([SchoolGameRules.morningArrivalEarlyRule()]));
        it("should be rewarded for early start", function () {
            return shouldBeRewarded(early,
                SchoolGameRules.morningArrivalEarlyRuleId(),
                SchoolGameRules.morningArrivalEarlyPoints(),
                earlyRuleService);
        });
        it("should not be rewarded for a weekend", function () {
            return notRewardedOnWeekends(lateStartTime, earlyRuleService);
        });
        it("should be included in all rules", function () {
            ruleIncludedInAll(SchoolGameRules.morningArrivalEarlyRuleId());
        });
        it("should not be rewarded for being too early", function () {
            return notRewarded(tooEarly, earlyRuleService);
        });
    });
    describe("everyScanCounts", function () {
        const everyScanCountsService = new RuleService(rulesRepository([SchoolGameRules.everyScanCountsRule()]));
        it("should be rewarded for every scan", function () {
            return shouldBeRewardedWithCoinsToo(
                tooLate,
                SchoolGameRules.everyScanCountsRuleId(),
                SchoolGameRules.everyScanCountsPoints(),
                SchoolGameRules.everyScanCountsCoins(),
                everyScanCountsService);
        });
        it("should be not be rewarded when not in the sandbox", function () {
            const notTheSandbox = "test";
            const notSandboxRepository = new RuleRepository(
                [SchoolGameRules.everyScanCountsRule()],
                SchoolGameRules.schoolGameOperators(),
                [SystemBootstrap.environmentFact(notTheSandbox)]);
            return notRewarded(onTime, new RuleService(notSandboxRepository));
        });
    });
    describe("firstScan", function () {
        const firstScanService = new RuleService(rulesRepository([SchoolGameRules.firstScanRule()]));
        it("should be rewarded for for the first scan at any time", function () {
            const pointsRewarded = 10;
            return shouldBeRewarded(
                tooLate,
                SchoolGameRules.firstScanRuleId(),
                pointsRewarded,
                firstScanService).then(() => {
                const userRewardServiceReturnsPoints = Object.assign({}, userRewardsService);
                userRewardServiceReturnsPoints.totalPointsForUserFromScanForRule = () => {
                    return pointsRewarded;
                };
                const rulesServiceWithPoints = new RuleService(rulesRepository([SchoolGameRules.firstScanRule()],
                    userRewardServiceReturnsPoints));
                return notRewarded(moment(), rulesServiceWithPoints, userRewardServiceReturnsPoints);
            });
        });
    });
    const arrivedByBikeRuleId = "arrivedByBike";
    describe(arrivedByBikeRuleId, function () {
        const rule = rules["arrivedByBike"];
        const bikeRuleService = new RuleService(rulesRepository([rule]));
        it("should be rewarded when a bike tag is scanned by school station mgr before 9", function () {
            return shouldBeRewardedForScanAndTag(
                scanWithMoment(scan1, early),
                bikeTag,
                SchoolGameRules.pointsForGreenTransport(),
                arrivedByBikeRuleId,
                bikeRuleService);
        });
        it("should not be rewarded for a non-bike tag", function () {
            return notRewarded(early, bikeRuleService);
        });
    });
    const arrivedByBusRuleId = "arrivedByBus";
    describe(arrivedByBikeRuleId, function () {
        const rule = rules[arrivedByBusRuleId];
        const ruleService = new RuleService(rulesRepository([rule]));
        it("should be rewarded by station manager for a bus scan using backpack tag any time", function () {
            return shouldBeRewardedForScanAndTag(
                scanWithMoment(busScan, early),
                tag1,
                SchoolGameRules.pointsForGreenTransport(),
                arrivedByBusRuleId,
                ruleService);
        });
        it("should not be rewarded when not scanned by station manager", function () {
            const gameService = gameServiceForRules(ruleService);
            const busScanByNonManager = busScan.toBuilder().authUid(null).build();
            return gameService.rewardsForScan(busScanByNonManager, tag1).then(function (rewards) {
                expect(rewards.length).to.equal(0);
                return rewards;
            });
        });
        it("should not be rewarded using a bike tag", function () {
            const gameService = gameServiceForRules(ruleService);
            const busScanByNonManager = busScan.toBuilder().build();
            return gameService.rewardsForScan(busScanByNonManager, bikeTag).then(function (rewards) {
                expect(rewards.length).to.equal(0);
                return rewards;
            });
        });
    });
    const scannedWalkStationRuleId = "scannedWalkStation";
    describe(scannedWalkStationRuleId, function () {
        const rule = rules[scannedWalkStationRuleId];
        const walkRuleService = new RuleService(rulesRepository([rule]));
        it("rule should be available", function () {
            expect(rule.id).to.not.equal(null);
        });
        it("should be rewarded for scanning during arrival times", function () {
            return shouldBeRewardedForScanAndTag(
                scan1.toBuilder().timestamp(onTime).build(),
                walkStationTag,
                SchoolGameRules.pointsForBonusRewards(),
                scannedWalkStationRuleId,
                walkRuleService);
        });
        it("should not be rewarded for a non-walk tag", function () {
            return notRewarded(early, walkRuleService);
        });
    });
    describe("authUserHasRole", function () {
        describe("with an admin", function () {
            //authService is only called if preconditions are met
            //setup/cleanup authService before those tests that will use authService
            const authService = new AuthService({});
            const fact = SchoolGameRules.authUserHasRole(authService);
            describe("and no role param", function () {
                const emptyParams = {};
                it("throws an error", function () {
                    try {
                        fact.calculationMethod(emptyParams);
                        expect("should throw error").to.equal(null);
                    } catch (error) {
                        expect(error.message).to.contain("role");
                    }
                });
            });
            describe("and a role in the params", function () {
                const targetRole = "test-role";
                const params = {
                    role: targetRole,
                };
                describe("but no scan in the almanac", function () {
                    const almanac = {};
                    almanac.factValue = () => {
                        //simulates a scan not found
                        return Promise.resolve(null);
                    };
                    it("throws an error", function () {
                        return fact.calculationMethod(params, almanac).then(() => {
                            return Promise.reject("should throw error");
                        }, (error) => {
                            expect(error).to.not.equal(null);
                            expect(error.message).to.not.equal(null);
                            expect(error.message).to.contain("scan");
                        });
                    });
                });
                describe("and a scan in the almanac", function () {

                    describe("with no authUid", function () {
                        const almanac = {};
                        almanac.factValue = () => {
                            //a scan without an authUid
                            return Promise.resolve(ScanFixtures.scan1());
                        };
                        it("returns false", function () {
                            return fact.calculationMethod(params, almanac).then((value) => {
                                expect(value).to.equal(false);
                            });
                        });
                    });
                    describe("with an authUid", function () {
                        const almanac = {};
                        almanac.factValue = () => {
                            //simulates a scan not found
                            return Promise.resolve(ScanFixtures.scan1WithAuthUid1());
                        };
                        describe("but getUser does not find a user", function () {
                            beforeEach("setup authService.getUser() to indicate user not found", function () {
                                authService._adminAuth = {
                                    getUser: () => {
                                        return Promise.reject("mock rejection of user not found");
                                    },
                                };
                            });
                            afterEach("reset the authService", function () {
                                authService._adminAuth = undefined;
                            });
                            it("returns false", function () {

                                return fact.calculationMethod(params, almanac).then((value) => {
                                    expect(value).to.equal(false);
                                });
                            });
                        });
                        describe("and getUser returns a user", function () {
                            describe("with customClaims", function () {
                                let customClaims;
                                let authRecord;
                                beforeEach("setup authService.getUser() to return mutable claims", function () {
                                    customClaims = {};
                                    authRecord = {
                                        customClaims: customClaims,
                                    };
                                    authService._adminAuth = {
                                        getUser: () => {

                                            return Promise.resolve(authRecord);
                                        },
                                    };
                                });
                                afterEach("reset the authService", function () {
                                    authService._adminAuth = undefined;
                                });
                                describe("that is null", function () {
                                    it("returns false", function () {
                                        authRecord = {};
                                        return fact.calculationMethod(params, almanac).then((value) => {
                                            expect(value).to.equal(false);
                                        });
                                    });
                                });
                                describe("having the target role equal to true", function () {
                                    it("returns true", function () {
                                        customClaims[targetRole] = true;

                                        return fact.calculationMethod(params, almanac).then((value) => {
                                            expect(value).to.equal(true);
                                        });
                                    });
                                });
                                describe("having the target role equal to false", function () {
                                    it("returns false", function () {
                                        customClaims[targetRole] = false;
                                        return fact.calculationMethod(params, almanac).then((value) => {
                                            expect(value).to.equal(false);
                                        });
                                    });
                                });
                            });
                            describe("with customClaims without the target role", function () {
                                beforeEach("setup authService.getUser() to return empty claims", function () {
                                    authService._adminAuth = {
                                        getUser: () => {
                                            return Promise.resolve({
                                                customClaims: {},
                                            });
                                        },
                                    };
                                });
                                afterEach("reset the authService", function () {
                                    authService._adminAuth = undefined;
                                });
                                it("returns false", function () {
                                    return fact.calculationMethod(params, almanac).then((value) => {
                                        expect(value).to.equal(false);
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});