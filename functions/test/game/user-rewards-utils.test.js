const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));

const UserRewardsUtils = require("../../main/game/user-rewards-utils.js");
const UserRewards = require("../../main/game/user-rewards.js");
const UserId = require("../../main/user/user.js").UserId;
const DateUtils = require("../../main/lang/date-utils.js");
const UnitOfTime = DateUtils.UnitOfTime;

const DateTestUtils = require("../../test/lang/date-test-utils.js");
const UserRewardTestUtils = require("../../test/game/user-rewards.fixtures");
const RewardFixtures = require("../../test/game/reward-fixtures");
const User = require("../../main/user/user");
const SchoolGameRules = require("../../main/game/school-game-rules");

describe("UserRewardsUtils", function () {
    describe("filter", function () {
        const rewards1 = UserRewardTestUtils.userRewards1();
        const moment = DateTestUtils.timestamp1();
        describe("matchingWeek", function () {
            it("should return nothing for nothing", function () {
                const actual = UserRewardsUtils.filter([]).matchingWeek(moment);
                expect(actual).to.have.lengthOf(0);
            });
            it("should keep a reward that starts with the moment", function () {
                const actual = UserRewardsUtils.filter([rewards1]).matchingWeek(rewards1.timeInterval().start);
                expect(actual).to.have.lengthOf(1);
            });
            it("should not keep a reward that starts before", function () {
                const beforeStartMoment = rewards1.timeInterval().start.subtract(7, UnitOfTime.day());
                const actual = UserRewardsUtils.filter([rewards1])
                    .matchingWeek(beforeStartMoment);
                expect(actual).to.have.lengthOf(0);
            });
            it("should not keep a reward that starts before", function () {
                const afterStartMoment = rewards1.timeInterval().start.add(7, UnitOfTime.day());
                const actual = UserRewardsUtils.filter([rewards1])
                    .matchingWeek(afterStartMoment);
                expect(actual).to.have.lengthOf(0);
            });
        });
    });
    describe("formatRewardMessage", function () {
        const userWithoutNickname = User.builder().build();
        const nickname = "fozzy";
        const userWithNickname = User.builder().nickname(nickname).build();
        let assertRuleText = function (ruleId, lookingFor) {
            const reward = RewardFixtures.builder1WithMinimal().ruleId(ruleId).build();
            it("and should have nickname when provided", function () {
                const text = UserRewardsUtils.formatRewardMessage(reward, userWithNickname);
                expect(text).to.contain(nickname);
                expect(text).to.contain(lookingFor);
                expect(text).to.not.contain(reward.ruleId());
            });
            it("and should not have nickname when missing", function () {
                const text = UserRewardsUtils.formatRewardMessage(reward, userWithoutNickname);
                expect(text).to.not.contain(nickname);
                expect(text).to.contain(lookingFor);
                expect(text).to.not.contain(reward.ruleId());
            });
        };
        describe("for every scan counts rule id should contain explanation", function () {
            const ruleId = SchoolGameRules.everyScanCountsRuleId();
            const lookingFor = "tagging";
            assertRuleText(ruleId, lookingFor);
        });
        describe("for on time arrival rule it should contain explanation", function () {
            assertRuleText(SchoolGameRules.morningArrivalOnTimeRuleId(), "on time");
        });
        describe("for late arrival rule it should contain explanation", function () {
            assertRuleText(SchoolGameRules.morningArrivalLateRuleId(), " arriving late");
        });
        describe("for bike", function () {
            assertRuleText(SchoolGameRules.arrivedByBike(), "biking");
        });
        describe("for bus", function () {
            assertRuleText(SchoolGameRules.arrivedByBus(), "bus");
        });
        describe("for walk", function () {
            assertRuleText(SchoolGameRules.arrivedByFoot(), "walk");
        });
        describe("for scanning a walk station", function () {
            assertRuleText(SchoolGameRules.scannedWalkStation(), "walking");
        });
        describe("for early rule it should contain explanation", function () {
            assertRuleText(SchoolGameRules.morningArrivalEarlyRuleId(), " early");
        });
        describe("for unknown rule id should contain message with rule id", function () {
            const reward = RewardFixtures.builder1WithMinimal().ruleId("whatever").build();
            it("and should have nickname when provided", function () {
                const text = UserRewardsUtils.formatRewardMessage(reward, userWithNickname);
                expect(text).to.contain(nickname);
                expect(text).to.contain(reward.ruleId());
            });
            it("and should not have nickname when missing", function () {
                const text = UserRewardsUtils.formatRewardMessage(reward, userWithoutNickname);
                expect(text).to.not.contain(nickname);
                expect(text).to.contain(reward.ruleId());
            });
        });

    });
});
