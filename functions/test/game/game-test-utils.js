const expect = require("chai").expect;

const RuleRepository = require("../../main/rule/rule-repository.js");
const SchoolGameRuleRepository = require("../../main/game/school-game-rules.js");
const ScanTestUtils = require("../../test/scan/scan-fixtures.js");
const SystemBootstrap = require("../../main/bootstrap");
const schoolStationManagerEmail = require("../../package").config.schoolStationManagerEmail;


/**
 * static utility class useful for testing the game without connectivity.
 */
class GameTestUtils {

    /**the number of points awarded for everyScanCountsRule().
     *
     * @return {number}
     */
    static everyScanCountsPoints() {
        return SchoolGameRuleRepository.everyScanCountsPoints();
    }

    /**
     * @return {Rule} the rule that matches the UserRewards daily summary as a fact
     */
    static everyScanCountsRule() {
        return SchoolGameRuleRepository.everyScanCountsRule();
    }

    /**
     * @return {RuleRepository} repository with one rule .. every scan counts
     */
    static everyScanCountsRepository() {
        return this.singleRuleRepository(GameTestUtils.everyScanCountsRule());
    }

    /**
     * @return {string} naming the environment that is ok to run tests.
     */
    static testEnvironmentName() {
        return "sandbox";
    }
    /**
     *
     * @return {Fact} that contains the environment where tests run
     */
    static testEnvironmentFact() {
        return SystemBootstrap.environmentFact(GameTestUtils.testEnvironmentName());
    }

    /**
     * @param {Rule} rule
     * @return {RuleRepository} containing the given rule
     */
    static singleRuleRepository(rule) {
        const ruleRepository = new RuleRepository([rule], [], [GameTestUtils.testEnvironmentFact()]);
        return ruleRepository;
    }

    /**
     * @param {AuthService} authService
     * @return {Promise<string>} given the uid or rejecting if not found
     */
    static schoolStationManagerAuthUid(authService) {
        return authService.authUidByEmail(schoolStationManagerEmail);
    }

    /**
     *  creates a scan in the system that has the authUid of the station manager for creation and rewarding
     *
     * @param {SystemBootstrap} bootstrap
     * @param {Scan} optionalScan will be used if provided, otherwise ScanTestUtils
     * @return {Promise<Scan>}
     */
    static authorizedScanCreated(bootstrap, optionalScan) {
        return GameTestUtils.schoolStationManagerAuthUid(bootstrap.authService()).then((stationManagerUid) => {
            //create the scan as if it were a client phone
            let scanFixture;
            if (optionalScan) {
                scanFixture = optionalScan;
            } else {
                scanFixture = ScanTestUtils.scanWithoutId();
            }
            const incomingScan = scanFixture.toBuilder().authUid(stationManagerUid).build();
            return bootstrap.scanService.scanCreated(incomingScan);
        });
    }

    /**End to end procedures for scanning a tag and being rewarded for it.
     *
     * @param {SystemBootstrap} bootstrap setup to target the database of interest.
     * @return {Promise.<Reward[]>} to be returned to the test runner
     */
    static everyScanShouldBeRewarded(bootstrap) {
        return GameTestUtils.authorizedScanCreated(bootstrap).then((scan) => {
            //process the scan to ensure points are given
            return bootstrap.gameService.scanReceived(scan).then((rewards) => {
                expect(rewards).to.have.lengthOf(1);
                const reward = rewards[0];
                expect(reward.points()).to.be.above(0);

                return bootstrap.gameService.scanReceived(scan).then((rewardsAgain) => {
                    //second attempt should find the existing
                    const rewardAgain = rewardsAgain[0];
                    expect(rewardAgain.scanId()).to.equal(reward.scanId());
                    expect(rewardAgain.points()).to.equal(reward.points());
                    return rewardAgain;
                });
            });
        });
    }
}

module.exports = GameTestUtils;
