const Reward = require("../../main/game/reward");
const UserId = require("../../main/user/user").UserId;
const DateFixtures = require("../../test/lang/date-test-utils");

/**
 * useful constants for testing
 */
class RewardFixtures {
    /**@return {int}*/
    static points1() {
        return 10;
    }

    /**@return {UserId}*/
    static recipientUserId1() {
        return new UserId("reward-recipient-1");
    }

    /**@return {string}*/
    static ruleId1() {
        return "reward-rule-1";
    }

    /**@return {string}*/
    static scanId1() {
        return "reward-scan-1";
    }

    /**
     * @return {RewardBuilder} with #1 constants which is enough to build
     */
    static builder1WithMinimal() {
        return Reward.builder()
            .points(this.points1())
            .userId(this.recipientUserId1())
            .ruleId(this.ruleId1())
            .scanId(this.scanId1())
            .timeReceived(DateFixtures.timestamp1());
    }

    /**
     *
     * @return {Reward} built with values from methods ending in 1
     */
    static reward1() {
        return RewardFixtures.builder1WithMinimal().build();
    }
}

module.exports = RewardFixtures;
