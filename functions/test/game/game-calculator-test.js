const UserRewards = require("../../main/game/user-rewards.js");
const UserId = require("../../main/user/user.js").UserId;
const Reward = require("../../main/game/reward.js");
const RewardFixtures = require("../../test/game/reward-fixtures");
const GameCalculator = require("../../main/game/game-calculator.js");
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;

describe("GameCalculator", function () {
    const oneRewards = [RewardFixtures.builder1WithMinimal().build()];
    const gameCalculatorWithOneRewards = GameCalculator.builder().rewards(oneRewards).build();
    const gameCalculatorWithNoRewards = GameCalculator.builder().rewards([]).build();
    describe("rewards", function () {
        it("should be zero with no rewards", () => {
            const calculator = GameCalculator.builder().build();
            expect(calculator.points()).to.equal(0);
            expect(calculator.coins()).to.equal(0);
            expect(calculator.pointsForRules()).to.not.equal(null);
            expect(calculator.metersToSchool()).to.equal(undefined);
            expect(calculator.hasMetersToSchool()).to.equal(false);
            expect(Object.keys(calculator.pointsForRules())).to.have.lengthOf(0);
        });
        it("should equal to the same points for a single reward", () => {
            const numberOfCoins = 1;
            const reward = RewardFixtures.builder1WithMinimal().coins(numberOfCoins).build();
            const calculator = GameCalculator.builder().rewards([reward]).build();
            expect(calculator.points()).to.equal(reward.points());
            expect(calculator.coins()).to.equal(numberOfCoins);
            const pointsForRules = calculator.pointsForRules();
            expect(Object.keys(pointsForRules)).to.have.lengthOf(1);
            expect(pointsForRules[reward.ruleId()]).to.equal(reward.points());
        });
        it("should equal to the sum of points for two rewards of same rule", () => {
            const reward1 = RewardFixtures.builder1WithMinimal().coins(2).build();
            const reward2 = RewardFixtures.builder1WithMinimal().coins(3).build();
            const calculator = GameCalculator.builder().rewards([reward1, reward2]).build();
            const pointsSum = reward1.points() + reward2.points();
            const coinsSum = reward1.coins() + reward2.coins();
            expect(calculator.points()).to.equal(pointsSum);
            expect(calculator.coins()).to.equal(coinsSum);
            const pointsForRules = calculator.pointsForRules();
            expect(Object.keys(pointsForRules)).to.have.lengthOf(1);
            expect(pointsForRules[reward1.ruleId()]).to.equal(pointsSum);
            expect(pointsForRules[reward2.ruleId()]).to.equal(pointsSum);
        });
        it("should equal to the sum of points for two rewards of different rules", () => {
            const reward1 = RewardFixtures.builder1WithMinimal().ruleId("first").points(3).build();
            const reward2 = RewardFixtures.builder1WithMinimal().ruleId("second").points(4).build();
            const calculator = GameCalculator.builder().rewards([reward1, reward2]).build();
            const sum = reward1.points() + reward2.points();
            expect(calculator.points()).to.equal(sum);
            const pointsForRules = calculator.pointsForRules();
            expect(Object.keys(pointsForRules)).to.have.lengthOf(2);
            expect(pointsForRules[reward1.ruleId()]).to.equal(reward1.points());
            expect(pointsForRules[reward2.ruleId()]).to.equal(reward2.points());

        });
        describe("metersToSchool", function () {
            const maxMeters = 100;
            const minMeters = 20;
            const reward1 = RewardFixtures.builder1WithMinimal().metersToSchool(maxMeters).build();
            const reward2 = RewardFixtures.builder1WithMinimal().metersToSchool(minMeters).build();
            it("should sum distances from two rewards", () => {
                const calculator = GameCalculator.builder().rewards([reward1, reward2]).build();
                expect(calculator.metersToSchool()).to.equal(minMeters + maxMeters);
                expect(calculator.hasMetersToSchool()).to.equal(true);
            });
            it("should keep max distance when requested", () => {
                const calculator = GameCalculator.builder().calculateMaxDistance().rewards([reward1, reward2]).build();
                expect(calculator.metersToSchool()).to.equal(maxMeters);
            });
            it("should ignore undefined distances", () => {
                //one rewards doesn't have distance...the others do.
                const rewards = oneRewards.concat([reward1, reward2]);
                const calculator = GameCalculator.builder().calculateMaxDistance().rewards(rewards).build();
                expect(calculator.metersToSchool()).to.equal(maxMeters);
            });
        });
    });
    describe("userId", function () {

        it("should be null from no rewards", () => {
            const calculator = GameCalculator.builder().rewards([]).build();
            expect(calculator.userId()).to.equal(null);
        });
        it("should be provided from rewards", () => {
            /**@type Reward*/
            const reward = RewardFixtures.builder1WithMinimal().build();
            const calculator = GameCalculator.builder().rewards([reward]).build();
            expect(calculator.userId().toString()).to.equal(reward.userId().toString());
        });
    });
    describe("hasRewards", function () {
        it("should return true if one or more rewards available", () => {
            expect(gameCalculatorWithOneRewards.hasRewards()).to.equal(true);
        });
        it("should return false if no rewards available", () => {
            expect(gameCalculatorWithNoRewards.hasRewards()).to.equal(false);
        });
    });
    describe("toString", function () {
        it("should return the properties with one reward", () => {
            expect(gameCalculatorWithOneRewards.toString()).to.not.equal(null);
        });
        it("should return the properties with no reward", () => {
            expect(gameCalculatorWithNoRewards.toString()).to.not.equal(null);
        });
    });
    describe("hasProperty", function () {
        it("should be true if property exists", () => {
            expect(gameCalculatorWithOneRewards._hasProperty(GameCalculator.prototype.points.name)).to.equal(true);
        });
        it("should be false if property doesn't exist", () => {
            expect(gameCalculatorWithNoRewards._hasProperty(GameCalculator.prototype.userId.name)).to.equal(false);
        });
    });

    describe("_pointsForRuleAssigned", function () {
        const ruleId = "rule-id-1";
        const pointsToAdd = 5;
        const coins = 1;
        const pointsForRules = {};
        it("should add to points up", () => {
            GameCalculator._pointsForRuleAssigned(ruleId, pointsToAdd, pointsForRules);
            expect(Object.keys(pointsForRules)).to.have.lengthOf(1);
            expect(pointsForRules[ruleId]).to.equal(pointsToAdd);
            const morePoints = 3;
            GameCalculator._pointsForRuleAssigned(ruleId, morePoints, pointsForRules);
            expect(pointsForRules[ruleId]).to.equal(pointsToAdd + morePoints);
        });
    });
    describe("_userRewardsPointsForRulesAssigned", function () {
        const ruleId = "rule-id-1";
        const pointsToAdd = 5;
        //these come with the reward
        const rewardPointsForRules = {};
        rewardPointsForRules[ruleId] = pointsToAdd;
        const pointsForRules = {};
        const reward = UserRewards.builder().pointsForRules(rewardPointsForRules).build();
        it("should add to points up", () => {
            GameCalculator._userRewardsPointsForRulesAssigned(reward, pointsForRules);
            expect(pointsForRules[ruleId]).to.equal(pointsToAdd);
        });
    });


});
