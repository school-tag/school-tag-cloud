
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const UserId = require("../../main/user/user.js").UserId;
const GameService = require("../../main/game/game-service.js");
const RuleService = require("../../main/rule/rule-service.js");
const DateFixtures = require("../../test/lang/date-test-utils");
const GameTestUtils = require("../../test/game/game-test-utils.js");
const UserRewardsTestUtils = require("../../test/game/user-rewards.fixtures");
const ScanFixtures = require("../../test/scan/scan-fixtures.js");
const StationFixtures = require("../../test/station/station.fixtures.js");
const TagFixtures = require("../../test/tag/tag-fixtures.js");
const TagService = require("../../main/tag/tag-service.js");
const UserFixtures = require("../../test/user/user-fixtures.js");
const SchoolGameRules = require("../../main/game/school-game-rules");
const Errors = require("../../main/lang/errors");
const RewardFixtures = require("../../test/game/reward-fixtures");

const momentService = DateFixtures.momentService();

describe("GameService", function () {
    describe("updateSummaryFromRewards", function () {
        it("should return the promise of the calculator with no rewards", function () {
            const userId = new UserId("blah");
            const dayMoment = DateFixtures.timestamp1();
            const rewards = [];
            GameService.builder().build().updateSummaryFromRewards(userId, dayMoment, rewards)
                .then((calculator) => {
                    expect(calculator.hasRewards()).to.equal(false);
                });
        });
        it("should calculate max distance for metersToSchool", function () {
            const userId = new UserId("blah");
            const dayMoment = DateFixtures.timestamp1();
            const minReward = RewardFixtures.builder1WithMinimal().metersToSchool(20).build();
            const maxReward = RewardFixtures.builder1WithMinimal().metersToSchool(30).build();
            const rewards = [maxReward, minReward];
            const userRewardsRepository = {
                setUserRewardsForDayFromCalculator: () => Promise.resolve(),
            };
            const userRewardsService = {
                _userRewardsRepository: userRewardsRepository,
            };
            GameService.builder().userRewardsService(userRewardsService).build().updateSummaryFromRewards(userId, dayMoment, rewards)
                .then((calculator) => {
                    expect(calculator.hasMetersToSchool()).to.equal(true);
                    expect(calculator.metersToSchool()).to.equal(maxReward.metersToSchool());
                });
        });
    });
    describe("everyScanCounts", function () {
        it("should find a match with every scan", function () {
            //setup the service with a single rule repository
            const repository = GameTestUtils.everyScanCountsRepository();
            const ruleService = new RuleService(repository);
            const gameService = GameService.builder()
                .ruleService(ruleService)
                .momentService(momentService)
                .userRewardsService({
                    userIdFromScan: () => {
                        return Promise.resolve(UserFixtures.user1Id());
                    },
                }).build();
            //run the rules engine
            const scan = ScanFixtures.scan1();
            return gameService.rewardsForScan(scan, TagFixtures.tag1()).then(function (rewards) {
                expect(rewards).to.not.equal(null);
                expect(rewards).to.have.lengthOf(1);
                const reward = rewards[0];
                expect(reward.points()).to.equal(GameTestUtils.everyScanCountsPoints());
                expect(reward.ruleId()).to.equal(GameTestUtils.everyScanCountsRule().event.type);
                expect(reward.hasMetersToSchool()).to.equal(false);
            });
        });
    });
    describe("rewardsForScan", function () {
        it("should reject if station is not found", function () {
            const gameService = GameService.builder()
                .stationService({
                    stationById() {
                        return Promise.reject(Errors.notFound("stations"));
                    },
                }).build();
            //run the rules engine
            const scan = ScanFixtures.scan1WithStation1();
            const tag = TagFixtures.tag1();
            return gameService.rewardsForScan(scan, tag).then(function () {
                expect(false).to.equal(true);
            }, (error) => {
                expect(error).not.to.equal(null);
                expect(Errors.hasHttpStatusCodeOf(error, Errors.notFoundStatusCode()));
            });
        });
        it("should include the station in the facts when the station is found", function () {
            const station = StationFixtures.station1();
            const gameService = GameService.builder()
                .stationService({
                    stationById() {
                        return Promise.resolve(station);
                    },
                }).userRewardsService({
                    userIdFromScan: () => {
                        return Promise.resolve(UserFixtures.user1Id());
                    },
                }).ruleService({
                    run: (facts) => {
                        expect(facts.station.id).to.equal(station.id());
                        return Promise.resolve([]);
                    },
                }).build();
            //run the rules engine
            const scan = ScanFixtures.scan1WithStation1();
            const tag = TagFixtures.tag1();
            return gameService.rewardsForScan(scan, tag).then(function (rewards) {
                //confirm the result comes from the mock
                expect(rewards.length).to.equal(0);
            });
        });
        it("should include rewards from the station distance", function () {
            const station = StationFixtures.station1WithType1Distance1();
            const repository = GameTestUtils.everyScanCountsRepository();
            const ruleService = new RuleService(repository);
            const gameService = GameService.builder()
                .stationService({
                    stationById() {
                        return Promise.resolve(station);
                    },
                }).userRewardsService({
                    userIdFromScan: () => {
                        return Promise.resolve(UserFixtures.user1Id());
                    },
                }).ruleService(ruleService).momentService(momentService).build();
            //run the rules engine
            const scan = ScanFixtures.scan1WithStation1();
            const tag = TagFixtures.tag1();
            return gameService.rewardsForScan(scan, tag).then(function (rewards) {
                //confirm the result comes from the mock
                expect(rewards.length).to.equal(1);
                const reward1 = rewards[0];
                expect(reward1.hasMetersToSchool()).to.equal(true);
                expect(reward1.metersToSchool()).to.equal(StationFixtures.distance1());
            });
        });
    });
    describe("applyRewardsForScan", function () {
        it("should create a default tag if no tag is found", function () {
            const scan = ScanFixtures.scan1();
            const userRewards = UserRewardsTestUtils.userRewards1();
            const scanService = {
                tagForScan: (scan) => {
                    return Promise.reject();
                },
            };
            const gameService = GameService.builder()
                .scanService(scanService)
                .momentService(momentService)
                .build();
            let called = false;
            gameService.userRewardsForScanAndTag = (scan, tag, userRewards) => {
                called = true;
                expect(tag).to.not.equal(null);
                expect(tag.id()).to.equal(scan.tagId());
            };
            return gameService.applyRewardsForScan(scan, userRewards).then((rewards) => {
                expect(called).to.equal(true);
            });
        });
        it("should find a tag already created", function () {
            const scan = ScanFixtures.scan1();
            const userRewards = UserRewardsTestUtils.userRewards1();
            const scanService = {
                tagForScan: (scan) => {
                    return Promise.resolve(TagFixtures.tag1());
                },
            };
            const gameService = GameService.builder()
                .scanService(scanService)
                .momentService(momentService)
                .build();
            let called = false;
            gameService.userRewardsForScanAndTag = (scan, tag, userRewards) => {
                called = true;
                expect(tag).to.not.equal(null);
                expect(tag.id()).to.equal(scan.tagId());
            };
            return gameService.applyRewardsForScan(scan, userRewards).then((rewards) => {
                expect(called).to.equal(true);
            });
        });
    });
    describe("tagOwnerOption", function () {
        it("should assign from the scan if provided", function () {
            const scan = ScanFixtures.withUserIdOfTagOwner(ScanFixtures.scan1());
            const options = {};
            GameService.tagOwnerOption(scan, options);
            expect(scan.hasUserIdOfTagOwner()).to.equal(true);
            expect(TagService.associateTagToUser(options)).to.equal(scan.userIdOfTagOwner());
        });
        it("should not assign anything if scan didn't provide", function () {
            const scan = ScanFixtures.scan1();
            const options = {};
            GameService.tagOwnerOption(scan, options);
            expect(scan.hasUserIdOfTagOwner()).to.equal(false);
            expect(TagService.associateTagToUser(options)).to.equal(undefined);
        });
    });
    describe("shouldNotCreateAssociatedUserOption", function () {
        it("should be true if tagCategory is token", function () {
            const scan = ScanFixtures.scan1().toBuilder().tagCategory(SchoolGameRules.tagCategoryToken()).build();
            const options = {};
            GameService.userCreateOption(scan, options);
            expect(TagService.associatedUserShouldNotBeCreated(options)).to.equal(true);
        });
        it("should not be included by default", function () {
            const scan = ScanFixtures.scan1();
            const options = {};
            GameService.userCreateOption(scan, options);
            expect(TagService.associatedUserShouldNotBeCreated(options)).to.equal(undefined);
        });
    });
    describe("tagCategoryOption", function () {
        it("should assign from the scan if provided and valid", function () {
            const category = SchoolGameRules.tagCategoryBike();
            const scan = ScanFixtures.scan1().toBuilder().tagCategory(category).build();
            const options = {};
            GameService.tagCategoryOption(scan, options);
            expect(scan.hasTagCategory()).to.equal(true);
            expect(TagService.categoryOption(options)).to.equal(scan.tagCategory());
        });
        it("should assign the default if scan didn't provide", function () {
            const scan = ScanFixtures.scan1();
            const options = {};
            GameService.tagCategoryOption(scan, options);
            expect(scan.hasTagCategory()).to.equal(false);
            expect(TagService.categoryOption(options)).to.equal(SchoolGameRules.tagCategoryBackpack());
        });
        it("should throw when given bad category", function (done) {
            const scan = ScanFixtures.scan1().toBuilder().tagCategory('junk').build();
            const options = {};
            try {
                GameService.tagCategoryOption(scan, options);
                done("should have thrown error");
            } catch (error) {
                done();
            }

        });
    });
    describe("tagTypeOption", function () {
        it("should assign from the scan if provided and valid", function () {
            const type = SchoolGameRules.tagTypeQuickReadCode();
            const scan = ScanFixtures.scan1().toBuilder().tagType(type).build();
            const options = {};
            GameService.tagTypeOption(scan, options);
            expect(scan.hasTagType()).to.equal(true);
            expect(TagService.typeOption(options)).to.equal(scan.tagType());
        });
        it("should assign the default if scan didn't provide", function () {
            const scan = ScanFixtures.scan1();
            const options = {};
            GameService.tagTypeOption(scan, options);
            expect(scan.hasTagType()).to.equal(false);
            expect(TagService.typeOption(options)).to.equal(SchoolGameRules.tagTypeTag());
        });
        it("should throw when given bad type", function (done) {
            const scan = ScanFixtures.scan1().toBuilder().tagType('junk').build();
            const options = {};
            try {
                GameService.tagTypeOption(scan, options);
                done("should have thrown error");
            } catch (error) {
                done();
            }

        });
    });
    describe("pointsFromTagOrDefault", function () {
        const tagPoints = 10;
        const defaultPoints = 22;
        const tagWithoutPoints = TagFixtures.tag1();
        const tagWithPoints = tagWithoutPoints.toBuilder().points(tagPoints).build();
        it("should come from tag if given", function () {
            expect(GameService.pointsFromTagOrDefault(tagWithPoints, defaultPoints)).to.equal(tagPoints);
        });
        it("should provide the default if tag has none", function () {
            expect(GameService.pointsFromTagOrDefault(tagWithoutPoints, defaultPoints)).to.equal(defaultPoints);

        });
        it("should provide undefined if default is not defined", function () {
            expect(GameService.pointsFromTagOrDefault(tagWithoutPoints)).to.equal(undefined);
        });
    });
});
