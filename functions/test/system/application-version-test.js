const ApplicationVersion = require("../../main/system/application-version.js");
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));

const json = {
    "branchUrl": "https://bitbucket.org/aawhere/school-tag-cloud/branch/master",
    "commitUrl": "https://bitbucket.org/aawhere/school-tag-cloud/commits/de94ee8c4cf6e40d7314fd5c3a9e1670a056b743",
    "buildUrl": "https://bitbucket.org/aawhere/school-tag-cloud/addon/pipelines/home#!/results/8",
    "buildNumber": "8",
    "commit": "de94ee8c4cf6e40d7314fd5c3a9e1670a056b743",
    "webApplicationUrl": "https://sandbox.schooltag.org",
    "functionsUrl": "https://us-central1-school-tag-sbox.cloudfunctions.net",
    "environment": "test",
};

describe("ApplicationVersion", function () {
    const applicationVersion = new ApplicationVersion(json);
    const applicationVersionFromNull = new ApplicationVersion({});
    describe("commitUrl", function () {
        it("should match expected", function () {
            expect(applicationVersion.commitUrl()).to.equal(json.commitUrl);
        });
    });
    describe("branchUrl", function () {
        it("should match expected", function () {
            expect(applicationVersion.branchUrl()).to.equal(json.branchUrl);
        });
    });

    describe("buildNumber", function () {
        it("should match expected", function () {
            expect(applicationVersion.buildNumber()).to.equal(Number.parseInt(json.buildNumber));
        });
        it("should return null for null", function () {
            expect(applicationVersionFromNull.buildNumber()).to.equal(null);
        });
    });
    describe("buildUrl", function () {
        it("should equal expected", function () {
            expect(applicationVersion.buildUrl()).to.equal(json.buildUrl);
        });
    });
    describe("selfUrl", function () {
        it("should equal expected", function () {
            expect(applicationVersion.selfUrl()).to.equal(json.selfUrl);
        });
    });
    describe("webApplicationUrl", function () {
        it("should equal expected", function () {
            expect(applicationVersion.webApplicationUrl()).to.equal(json.webApplicationUrl);
        });
    });
    describe("functionsUrl", function () {
        it("should equal expected", function () {
            expect(applicationVersion.functionsUrl()).to.equal(json.functionsUrl);
        });
    });
    describe("environment", function () {
        it("should equal expected", function () {
            expect(applicationVersion.environment()).to.equal(json.environment);
        });
    });
});


