const Assert = require("assert");
const expect = require("chai").expect;

const appRootDir = require("app-root-dir").get();
const FirebaseUtils = require(appRootDir + "/main/firebase/firebase-utils");

describe("FirebaseUtils", function () {
    describe("QueryEventType", function () {
        it("should equal itself", function () {
            Assert.equal(FirebaseUtils.QueryEventType.VALUE, FirebaseUtils.QueryEventType.VALUE);
        });
    });
    describe("parallelEntityRef", function () {
        it("should insert the element as the new root", function () {
            const existingRootPath = "http://example.com/a/b/c";
            const entitySuffix = "/d/e/f";
            const entityPath = existingRootPath + entitySuffix;
            const newRootElement = "c1";
            const existingRootRef = FirebaseUtils.mockReference(existingRootPath);
            const entityRef = FirebaseUtils.mockReference(entityPath);
            const parallelEntityRef = FirebaseUtils.parallelEntityRef(existingRootRef, entityPath, newRootElement);
            expect(parallelEntityRef.toString()).to.equal(existingRootPath + "/" + newRootElement + entitySuffix);
        });
    });

});
