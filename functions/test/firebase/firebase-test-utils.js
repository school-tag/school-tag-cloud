const console = require("console");
const Test = require("mocha").it;
const Firebase = require("firebase");
const admin = require("firebase-admin");
const Reference = require("firebase").database.Reference;
const env = require("env-var");
const simpleGit = require("simple-git")();
const moment = require("moment");

const FirebaseUtils = require("../../main/firebase/firebase-utils.js");
const Audit = require("../../main/entity/repository.js").Audit;

const DEVELOPER = env("DEVELOPER").asString();
/** default env variable https://nodejs.org/api/process.html#process_process_env */
const USER = env("USER").asString();
const NODE_USER_AGENT = env("npm_config_user_agent").asString();
//pointing to the default project ensures the testing environment will be used
const project = require("../../package").config.project;
const INTELLIJ_USER_AGENT = env("XPC_SERVICE_NAME").asString();
const COMMIT = env("BITBUCKET_COMMIT").asString();
const BUILD_NUMBER = env("BITBUCKET_BUILD_NUMBER").asString();
// setup the service account using https://firebase.google.com/docs/admin/setup
// the service credentials are ignored by git so every developer needs to download their own service creds
const serviceAccount = require(`../../.firebase/${project}`);

let instance = null;

/**
 * Singleton that must be run during testing if connections to firebase are to happen.
 */
class FirebaseTestUtils {

    /**
     * @private use instance();
     */
    constructor() {
    }

    /**
     * Static method providing the singleton pattern
     * @return {FirebaseTestUtils}
     */
    static instance() {
        if (instance == null) {
            instance = new FirebaseTestUtils();
            let config = {
                credential: admin.credential.cert(serviceAccount),
                databaseURL: `https://${project}.firebaseio.com`,
            };
            //this can only be done once
            admin.initializeApp(config);
            //create a unique reference for this test suite execution
            //so the database residue will be clear
            const testRef = admin.database().ref().child("_test");
            const all = env();
            console.log(JSON.stringify(all));
            const developerKey = (DEVELOPER != null) ? DEVELOPER : (USER != null) ? USER : "unknown";
            const developerRef = testRef.child(developerKey);
            const nowPath = new Date().getTime();
            instance.rootReference = developerRef.child(nowPath);
            const where = (NODE_USER_AGENT != null) ? NODE_USER_AGENT : (INTELLIJ_USER_AGENT != null) ? INTELLIJ_USER_AGENT : "test";
            const which = (COMMIT != null) ? COMMIT + ":" + BUILD_NUMBER : nowPath;
            instance.auditTemplate = new Audit(developerKey, where, which);
            console.log(instance.rootReference.toString() + ".json   -> (view test data) ");
            //add branch when development gets busy
            // simpleGit.branch((error, branchSummary) => {
            //     const branchRef = developerRef.child(branchSummary.current);
            // });
        }
        return instance;
    }

    /**
     * @return {Reference}
     * */
    database() {
        return this.rootReference;
    }

    /** provides a reference using the test root given by database(), but appends the
     * description of the test in the same manner as mocha tests describe -> describe -> it.
     *
     * @param {Test} test? mocha "describe -> describe -> it" test which provides where. null indicates root should be used.
     * @return {Reference} to the te
     */
    testReference(test) {
        const titles = [];
        let testRef;
        if (test) {
            let method = test;
            testRef = this.database();

            //append all of the titles until the root "describe"
            while (method != null && method.title != null && method.title != "") {
                titles.push(method.title);
                method = method.parent;
            }
        } else {
            testRef = this.database().getRoot();
            console.warn("a83D: no test provided...using root reference: " + testRef);
        }

        //now add the children start with highest ancestor first.
        titles.reverse().forEach((title) => {
            testRef = testRef.child(title);
        });
        return testRef;
    }

    /**
     * @param {string} [givenPath] prefix to any child being added.
     * @return {Reference} with limited function
     */
    static mockReference(givenPath) {
        return FirebaseUtils.mockReference();
    }
}

module.exports = FirebaseTestUtils;

