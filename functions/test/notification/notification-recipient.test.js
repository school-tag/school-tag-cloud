const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const NotificationRecipient = require("../../main/notification/notification-recipient");
const DateTestUtils = require("../../test/lang/date-test-utils.js");

describe("NotificationRecipient", () => {
    const expectedRecipientId = "recipient-id-1";
    const expectedSubscriptionTime = DateTestUtils.timestamp1();
    const recipient = NotificationRecipient.builder()
        .recipientId(expectedRecipientId)
        .subscribedAtTime(expectedSubscriptionTime).build();
    it("should have recipient id", () => {
        expect(recipient.recipientId()).to.equal(expectedRecipientId);
    });
    it("should have subscription time", () => {
        expect(recipient.subscribedAtTime().toISOString()).to.equal(expectedSubscriptionTime.toISOString());
    });
    it("to builder constucts", () => {
        const clone = recipient.toBuilder().recipientId("something else").build();
        expect(clone.recipientId()).to.not.equal(expectedRecipientId);
    });
    describe("hasRecipientId()", () => {
        it("should return true when recipient is available", () => {
            expect(recipient.hasRecipientId()).to.equal(true);
        });
        it("should return true false when no recipient", () => {
            const clone = recipient.toBuilder().recipientId(null).build();
            expect(clone.hasRecipientId()).to.equal(false);
        });
    });

});