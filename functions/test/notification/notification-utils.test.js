const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const NotificationUtils = require("../../main/notification/notification-utils");
const Message = require("../../main/message/message");
const MessageFixtures = require("../../test/message/message.fixtures");

describe("NotificationUtils", function () {
    describe("messageToNotificationPayload", function () {
        const title = "Elemer's Capade";
        const text = "I like wabbits.";
        const iconUrl = "http://cool-icons.com/bunny.png";
        const linkUrl = "http://my-site.com/fudd";

        it("should handle all properties", function () {
            const message = MessageFixtures.message1Builder()
                .title(title)
                .text(text)
                .moreInfoUrl(linkUrl)
                .iconUrl(iconUrl).build();
            const payload = NotificationUtils.messageToNotificationPayload(message);
            const notification =payload.notification;
            expect(notification.title).to.equal(title);
            expect(notification.body).to.equal(text);
            expect(notification.click_action).to.equal(linkUrl);
            expect(notification.icon).to.equal(iconUrl);
        });
    });
});