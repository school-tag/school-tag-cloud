const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const Notification = require("../../main/notification/notification-result.js");
const Message = require("../../main/message/message.js");

describe("NotificationResult", function () {
    it("should be successful with a recordId", function () {
        const expected = "stuff";
        const notification = Notification.builder().senderRecordId(expected).build();
        expect(notification.senderRecordId()).to.equal(expected);
        expect(notification.isSuccessful()).to.equal(true);
        expect(notification.isErroneous()).to.equal(false);
    });
    it("should be erroneous with an error code", function () {
        const expected = "stuff";
        const code = "ABC";
        const message = "ouch";
        const error = {code: code, message: message};
        const notification = Notification.builder().error(error).build();
        expect(notification.errorCode()).to.equal(code);
        expect(notification.errorMessage()).to.equal(message);
        expect(notification.isErroneous()).to.equal(true);
        expect(notification.isSuccessful()).to.equal(false);
    });
    it("should return a message ", function () {
        const expected = Message.builder().text("whatever").build();
        const notification = Notification.builder().message(expected).build();
        expect(notification.message().text()).to.equal(expected.text());
    });
    it("should return a clone from toBuilder ", function () {
        const notification = Notification.builder().build();
        expect(notification).to.equal(notification);
        const clone = notification.toBuilder().build();
        expect(clone).to.not.equal(notification);
    });
});