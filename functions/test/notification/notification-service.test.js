const FirebaseError = require("firebase").FirebaseError;

const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const NotificationService = require("../../main/notification/notification-service");
const NotificationResult = require("../../main/notification/notification-result");
const Message = require("../../main/message/message.js");
const MessageTestUtils = require("../message/message.fixtures.js");

describe("NotificationService", () => {
    describe("_notificationFromResult", () => {
        const service = new NotificationService();
        const message = MessageTestUtils.message1();
        const messageId = "message-id";
        const recipientId = "fcm-token";
        it("should convert a result to notification", () => {
            const result = {
                messageId: messageId,
                canonicalRegistrationToken: recipientId,
            };
            const notification = service._notificationFromResult(result, message);
            expect(notification).to.not.equal(null);
            expect(notification.senderRecordId()).to.equal(messageId);
            expect(notification.isSuccessful()).to.equal(true);
        });
        it("should be erroneous if error provided ", () => {
            const error = {
                code: 333,
            };
            const result = {
                error: error,
            };
            const notification = service._notificationFromResult(result, message);
            expect(notification.isErroneous()).to.equal(true);
            expect(notification.errorCode()).to.equal(error.code);
        });
        it("should succeed when no canonical token is provided", () => {
            const result = {
                messageId: messageId,
            };
            const notification = service._notificationFromResult(result, message);
            expect(notification.isSuccessful()).to.equal(true);
        });

    });
    describe("saveNotification", () => {
        const service = new NotificationService();
        //these tests depend on validation steps remaining in their current order (annoying, but...?)
        it("should not allow notification id ", () => {
            return service.saveNotification(NotificationResult.builder().id("anything").build())
                .catch((error) => {
                    expect(error.message).to.contain("id");
                    return Promise.resolve();
                });
        });
        it("should require message id ", () => {
            const messageWithoutId = Message.builder().build();
            const notification = NotificationResult.builder().message(messageWithoutId).build();
            return service.saveNotification(notification)
                .catch((error) => {
                    expect(error.message).to.contain("message");
                    return Promise.resolve();
                });
        });
    });
});