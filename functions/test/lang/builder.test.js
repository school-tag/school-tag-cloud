const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const Builder = require("../../main/lang/builder");

/**
 * just for testing
 */
class A {

}

/**
 * juust for testing
 */
class B {

}

/**
 *
 */
class TestForBuilder {

    /**
     *
     * @param {A} a
     * @param {B} b
     * @param {B} c
     */
    constructor(a, b, c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    /**
     * @return {Builder.<TestForBuilder>}
     */
    static builder() {
        return new class extends Builder {
            /**
             * @return {TestForBuilder}
             */
            build() {
                return new TestForBuilder(
                    this.get(A),
                    this.get(B),
                    this.get("c")
                );
            }
        };
    }
}

describe("Builder", function () {

    const a = new A();
    const b = new B();
    const c = new B();

    const result = TestForBuilder.builder().use(a).and(b).and(c, "c").build();
    it("should set the single property for type to be assigned", function () {
        expect(result.a).to.equal(a);
    });
    it("should set the property by the type when not given a name", function () {
        expect(result.b).to.equal(b);
    });
    it("should set the property by name when given", function () {
        expect(result.c).to.equal(c);
    });
    it("should throw an exception when build not overridden", function () {
        try {
            new Builder().build();
            expect("shouln't make it here").to.equal(null);
        } catch (error) {
            //expected
        }

    });

});