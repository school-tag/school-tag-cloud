const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const Errors = require("../../main/lang/errors");

describe("Errors", function () {
    const message = "this is a test";
    const code = "k29z";
    const error = Errors.of(message, code);
    const httpStatusCode = 410;
    const httpStatusCodeWrong = 411;
    const errorWithHttpStatusCode = Errors.of(message, code, httpStatusCode);
    it("should accept message and code", function () {
        expect(error).to.not.equal(null);
        expect(error.message).to.equal(message);
        expect(error.code).to.equal(code);
    });
    it("should transform json", function () {
        const json = Errors.json(error);
        expect(json.message).to.contain(message);
        expect(json.code).to.contain(code);
    });
    it("should transform json with null error", function () {
        const json = Errors.json();
        expect(json).to.not.equal(null);
    });

    describe("hasHttpStatus", function () {
        it("should return true if set", function () {
            expect(Errors.hasHttpStatusCode(errorWithHttpStatusCode)).to.equal(true);
        });
        it("should return false if not set", function () {
            expect(Errors.hasHttpStatusCode(error)).to.equal(false);
        });
        it("should return false null error", function () {
            expect(Errors.hasHttpStatusCode(null)).to.equal(false);
        });
    });
    describe("httpStatusCode", function () {
        it("should return status code if set", function () {
            expect(Errors.httpStatusCode(errorWithHttpStatusCode)).to.equal(httpStatusCode);
        });
        it("should not return status code if not set", function () {
            expect(Errors.httpStatusCode(error)).to.not.equal(httpStatusCode);
        });

        describe("json", function () {
            it("should have status code if set", function () {
                expect(JSON.stringify(Errors.json(errorWithHttpStatusCode))).to.contain(`${httpStatusCode}`);
            });
            it("should not have status code if set", function () {
                expect(JSON.stringify(Errors.json(error))).to.not.contain(`${httpStatusCode}`);
            });
        });
    });
    describe("hasHttpStatusCodeOf", function () {
        it("should return true if set", function () {
            expect(Errors.hasHttpStatusCodeOf(errorWithHttpStatusCode, httpStatusCode)).to.equal(true);
        });
        it("should return false if not set", function () {
            expect(Errors.hasHttpStatusCodeOf(error, httpStatusCode)).to.equal(false);
        });
        it("should return false if error is undefined", function () {
            expect(Errors.hasHttpStatusCodeOf(undefined, httpStatusCode)).to.equal(false);
        });
        it("should return false if wrong error code", function () {
            expect(Errors.hasHttpStatusCodeOf(errorWithHttpStatusCode, httpStatusCodeWrong)).to.equal(false);
        });

        describe("json", function () {
            it("should have status code if set", function () {
                expect(JSON.stringify(Errors.json(errorWithHttpStatusCode))).to.contain(`${httpStatusCode}`);
            });
            it("should not have status code if set", function () {
                expect(JSON.stringify(Errors.json(error))).to.not.contain(`${httpStatusCode}`);
            });
        });
    });

});