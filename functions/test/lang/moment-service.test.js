const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const MomentService = require("../../main/lang/moment-service");
const moment = require("moment-timezone");
const DateFixtures = require("../../test/lang/date-test-utils.js");

const tapaeiZone = "Asia/Taipei";
const tapaeiOffset = 8 * 60;
describe("MomentService", function () {
    it("should use the given time zone", function () {
        const zone = tapaeiZone;
        const service = new MomentService(zone);
        expect(service.now().utcOffset()).to.equal(tapaeiOffset);
    });
    it("should convert utc to local time", function () {
        const zone = "Asia/Taipei";
        const service = new MomentService(zone);
        const utcMoment = moment.parseZone("2013-11-18T03:55:00Z");
        const local = service.toLocal(utcMoment);
        expect(local.format()).to.equal("2013-11-18T11:55:00+08:00");
        expect(local.utcOffset()).to.equal(tapaeiOffset);
    });
    it("should see utc tomorrow as local today", function () {
        const actual = DateFixtures.momentService().toLocal(DateFixtures.timestampUtcTomorrow());
        expect(actual.format()).to.equal(DateFixtures.timestampLocalTodayMatchingUtcTomorrowString());
    });
    it("should understand time zones", function () {
        //examples from the docs
        const a = moment.tz("2013-11-18 11:55", "Asia/Taipei");
        const b = moment.tz("2013-11-18 11:55", "America/Toronto");
        expect(a.format()).to.equal("2013-11-18T11:55:00+08:00");
        expect(b.format()).to.equal("2013-11-18T11:55:00-05:00");
        expect(a.utc().format()).to.equal("2013-11-18T03:55:00Z");
        expect(b.utc().format()).to.equal("2013-11-18T16:55:00Z");
    });
    it("should throw when not given a time zone", function (done) {
        try {
            new MomentService();
            done("the zone should be required");
        } catch (error) {
            done();
        }
    });
});