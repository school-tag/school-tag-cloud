const moment = require("moment");
const DateUtils = require("../../main/lang/date-utils.js");
const MomentService = require("../../main/lang/moment-service");

/**
 *Useful items during testing.
 */
class DateFixtures {
    /**
     * Example string with a time zone unlikely for any developer
     * (useful for providing tz consistency during transformation)
     * Don't think about changing anything of the value since a lot of tests are relying on this value to be constant.
     * @return {string}
     */
    static timestampString1() {
        return "2017-06-08T20:43:59-02:00";
    };

    /**
     * @return {string} after timestampString1
     */
    static timestampString2() {
        return "2017-06-08T20:50:12-02:00";
    };

    /**
     * @return {string} only the date part of timestamp1String
     */
    static timestamp1DateString1() {
        return "2017-06-08";
    };

    /**
     * last second of the 7th day of iso week 36.
     *
     * @return {string}
     */
    static timestampSunday1String() {
        return "2017-09-10T23:59:59-02:00";
    };

    /**
     * @return {number} the week corresponding to the timestamp
     */
    static timestampSunday1IsoWeek() {
        return 36;
    }

    /**
     * @return {number} the day of the week corresponding to the timestamp
     */
    static timestampSunday1IsoWeekday() {
        return 7;
    }

    /**
     *
     * @return {moment.Moment} matching timestampString1
     */
    static timestamp1() {
        return DateUtils.timestampMoment(DateFixtures.timestampString1());
    };

    /**
     * @return {moment.Moment} after timestamp1 matching timestampString2
     */
    static timestamp2() {
        return DateUtils.timestampMoment(DateFixtures.timestampString2());
    };

    /**
     * @see timestampSunday1String
     *
     * @return {moment.Moment|null}
     */
    static timestampSunday1() {
        return DateUtils.timestampMoment(DateFixtures.timestampSunday1String());
    };

    /**  Wednesday morning
     * @return {string} time spanning different days
     */
    static timestampUtcTomorrowString() {
        return "2021-05-05T00:33:19Z";
    }

    /** Tuesday Night
     * @return {string} matching timestamp for UTC tomorrow
     */
    static timestampLocalTodayMatchingUtcTomorrowString() {
        return "2021-05-04T22:33:19-02:00";
    }

    /**
     *
     * @return {moment.Moment} time that shows up as tomorrow in UTC but today in local time
     */
    static timestampUtcTomorrow() {
        return DateUtils.timestampMoment(DateFixtures.timestampUtcTomorrowString());
    }

    /**
     * @return {moment.Moment} same time at UtcTomorrow, but in local time
     */
    static timestampLocalTodayMatchingUtcTomorrow() {
        return DateUtils.timestampMoment(DateFixtures.timestampLocalTodayMatchingUtcTomorrowString());
    }

    /** @return {String} an is range in string form that is unlikely to reproduce by other means. */
    static rangeString1() {
        return "2017-06-17T03:10:08-02:00/2017-06-17T13:29:57-02:00";
    }

    /**@return {MomentRange} a range produced from rangeString1()*/
    static range1() {
        return DateUtils.momentRange(DateFixtures.rangeString1());
    }

    /**
     *
     * @return {string} parseable by moment.utcOffset() to be used for testing
     */
    static utcOffset() {
        return "+02:00";
    }

    /**
     *
     * @return {string} the name of a time zone where nobody lives in the atlantic
     */
    static timeZone() {
        return "America/Noronha";
    }

    /**
     * @return {MomentService} to be used to create moments in testing (considers time zone)
     */
    static momentService() {
        return new MomentService(DateFixtures.timeZone());
    }
}

module.exports = DateFixtures;
