const appRootDir = require("app-root-dir").get();
const Assert = require("assert");
const DateUtils = require(appRootDir + "/main/lang/date-utils.js");
const DateFixtures = require(appRootDir + "/test/lang/date-test-utils.js");
const describe = require("mocha").describe;
const it = require("mocha").it;
const Moment = require("moment");
const MomentRange = require("moment-range");
const moment = MomentRange.extendMoment(Moment);
const should = require("chai").should();
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
describe("DateUtils", function () {
    describe("asDate", function () {
        it("should return a date object unmodified", function () {
            const before = new Date();
            const after = DateUtils.asDate(before);
            before.should.equal(after);
        });
        it("should return a date from an iso string", function () {
            const date = DateUtils.asDate("2017-06-08T20:43:59-07:00");
            date.should.be.an.instanceOf(Date);
        });
        it("should return a null from a null", function () {
            const date = DateUtils.asDate(null);
            expect(date).to.be.null;
        });
        it("should return an equal date from its own toString", function () {
            const now = new Date();
            const date = DateUtils.asDate(now.toISOString());
            date.should.be.instanceOf(Date);
            // date.should.equal(now); // fails for some reason
            date.toISOString().should.equal(now.toISOString());
        });
        it("should throw an error when not a validate date", function () {
            try {
                expect(DateUtils.asDate("junk")).to.equal(null);
            } catch (error) {
                error.should.be.instanceOf(Assert.AssertionError);
            }
        });
    });
    describe("momentRange", function () {
        it("should allow moments to keep given time zone", function () {
            const start = "2015-01-17T09:50:04+03:00";
            const end = "2015-04-17T08:29:55-04:00";
            const timeInterval = start + "/" + end;
            const range = DateUtils.momentRange(timeInterval);
            expect(range.toString()).equal(timeInterval);
        });
    });
    describe("timestampMoment", function () {
        it("should be portable to its equal", function () {
            const expected = DateFixtures.timestampString1();
            const moment = DateUtils.timestampMoment(expected);
            expect(DateUtils.timestampMomentPortable(moment)).to.equal(expected);
        });
        it("should return null if given null", function () {
            expect(DateUtils.timestampMoment(null)).to.equal(null);
        });
    });
    describe("timestampMomentPortable", function () {
        it("should return null if given null", function () {
            expect(DateUtils.timestampMomentPortable(null)).to.equal(null);
        });
    });
    describe("dayMomentRange", function () {
        it("should return midnight to midnight in same time zone", function () {
            const timestampFormat = "2015-01-17T09:50:04+02:00";
            const moment = DateUtils.timestampMoment(timestampFormat);
            const range = DateUtils.dayMomentRange(moment);
            expect(DateUtils.timestampMomentPortable(range.start)).to.equal("2015-01-17T00:00:00+02:00");
            expect(DateUtils.timestampMomentPortable(range.end)).to.equal("2015-01-17T23:59:59+02:00");
        });
        it("should provide today for utc tomorrow", function () {
            const today = DateFixtures.momentService().toLocal(DateFixtures.timestampUtcTomorrow());
            const range = DateUtils.dayMomentRange(today);
            expect(range.toString()).equal("2021-05-04T00:00:00-02:00/2021-05-04T23:59:59-02:00");
        });
    });
    describe("weekMomentRange", function () {
        it("should return Monday to Sunday at midnight in same time zone", function () {
            const someWednesday = "2017-10-25T09:50:04+02:00";
            const moment = DateUtils.timestampMoment(someWednesday);
            const range = DateUtils.weekMomentRange(moment);
            expect(DateUtils.timestampMomentPortable(range.start)).to.equal("2017-10-23T00:00:00+02:00");
            expect(DateUtils.timestampMomentPortable(range.end)).to.equal("2017-10-29T23:59:59+02:00");
        });
    });
    describe("isoMonth", function () {
        it("should work for a round trip", function () {
            const momentMonth = 0;
            const isoMonth = DateUtils.isoMonth(momentMonth);
            expect(isoMonth).to.equal(momentMonth + 1);
            expect(DateUtils.momentMonthFromIso(isoMonth)).to.equal(momentMonth);
        });
        it("should return the string unchanged", function () {
            const expected = "junk";
            const momentMonth = {
                month: () => {
                    return expected;
                },
            };
            const isoMonth = DateUtils.isoMonth(momentMonth);
            expect(isoMonth).to.equal(expected);
        });
    });
    describe("dayOfMonth", function () {
        it("should work for a round trip", function () {
            const momentMonth = 0;
            const isoMonth = DateUtils.isoMonth(momentMonth);
            expect(isoMonth).to.equal(momentMonth + 1);
            expect(DateUtils.momentMonthFromIso(isoMonth)).to.equal(momentMonth);
        });
    });
    describe("UnitOfTime", function () {
        it("should have iso week for week", function () {
            expect(DateUtils.UnitOfTime.week()).to.equal("isoWeek");
        });
        it("should be day for day", function () {
            expect(DateUtils.UnitOfTime.day()).to.equal("day");
        });
        it("should be hour for hour", function () {
            expect(DateUtils.UnitOfTime.hour()).to.equal("hour");
        });
        it("should be month for month", function () {
            expect(DateUtils.UnitOfTime.month()).to.equal("month");
        });
        it("should be year for year", function () {
            expect(DateUtils.UnitOfTime.year()).to.equal("year");
        });
    });
    describe("weekdayMomentRangeFunction", () => {
        const weekdayMomentRangeFunction = DateUtils.weekdayMomentRangeFunction(DateFixtures.timestamp1());
        it("should return a function", () => {
            expect(typeof weekdayMomentRangeFunction).to.equal("function");
        });
        it("should return a monday start when called with a monday", () => {
            const monday = DateUtils.DayOfWeek.monday();
            const range = weekdayMomentRangeFunction(monday);
            expect(range.start.isoWeekday()).to.equal(monday);
            expect(range.end.isoWeekday()).to.equal(monday);
        });
        /** https://bitbucket.org/school-tag/school-tag-cloud/issues/4 */
        it("should not mutate the given moment", () => {
            const original = DateFixtures.timestamp1();
            const expected = original.toISOString();
            const fn = DateUtils.weekdayMomentRangeFunction(original);
            fn(DateUtils.DayOfWeek.monday());
            expect(original.toISOString()).to.equal(expected);
        });
    });
    describe("dayOfMonth", () => {
        const dayOfMonth = DateUtils.dayOfMonth(DateFixtures.timestamp1());
        it("should return the iso day of the month", () => {
            expect(dayOfMonth).to.equal(8);
            expect(dayOfMonth).to.equal(DateFixtures.timestamp1().date());
        });
    });
    describe("datePortable", () => {
        const dateString = DateUtils.datePortable(DateFixtures.timestamp1());
        it("should return only the date", () => {
            expect(dateString).to.equal(DateFixtures.timestamp1DateString1());
        });
    });
    //FIXME: moment range does not support isoweek, it supports month and week though
    // describe("weekMomentRange", () => {
    //     it("should return the week range of the given moment", () => {
    //       const range = DateUtils.weekMomentRange(DateFixtures.timestamp1());
    //       expect(range.start.isoWeekday()).to.equal(DateUtils.DayOfWeek.monday());
    //       expect(range.start.isoWeek()).to.equal(23);
    //     });
    // });
});

