const Station = require("../../main/station/station");

/**
 *
 */
class StationFixtures {

    /**
     * @return {string} the fixture id of a station
     */
    static stationId1() {
        return "station-fixture-1";
    }

    /**
     *
     * @return {string} some arbitrary station type
     */
    static type1() {
        return "station-type-1";
    }

    /**
     *
     * @return {number} number of meters to school
     */
    static distance1() {
        return 750;
    }
    /**
     *
     * @return {Station} with stationId1 and some descriptive name
     */
    static station1() {
        return Station.builder().name("Station 1 Fixture").id(this.stationId1()).build();
    }

    /**
     *
     * @return {Station} station1() with type1() assigned to type.
     */
    static station1WithType1() {
        return this.station1().toBuilder().type(this.type1()).build();
    }

    /**
     * @return {Station}
     */
    static station1WithType1Distance1() {
        return this.station1WithType1().toBuilder().metersToSchool(this.distance1()).build();
    }
}

module.exports = StationFixtures;