const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const StationFixtures = require("../../test/station/station.fixtures");
describe("Station", () => {
    describe("fixtures", () => {
        it("have an id", () => {
            const station = StationFixtures.station1WithType1();
            expect(station.hasId()).to.equal(true);
            expect(station.id()).to.equal(StationFixtures.stationId1());
        });
        it("will provide a name", () => {
            const station = StationFixtures.station1WithType1();
            expect(station.name()).to.not.equal(null);
        });
        it("will provide a type", () => {
            const station = StationFixtures.station1WithType1();
            expect(station.type()).to.equal(StationFixtures.type1());
        });
        it("will provide a distance", () => {
            const station = StationFixtures.station1WithType1Distance1();
            expect(station.metersToSchool()).to.equal(StationFixtures.distance1());
        });
    });
});

