const appRootDir = require("app-root-dir").get();
const Assert = require("assert");
const Scan = require(appRootDir + "/main/scan/scan.js");
const DateTestUtil = require(appRootDir + "/test/lang/date-test-utils.js");
const ScanFixtures = require(appRootDir + "/test/scan/scan-fixtures.js");
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const moment = require("moment");

describe("Scan", function () {
    describe("tagId", function () {
        it("should be set using mutator", function () {
            const expected = "a234";
            const tag = Scan.builder().tagId(expected).build();
            expect(expected).to.equal(tag.tagId());
        });
        it("should be in toString results", function () {
            const tagId = "junk";
            const string = Scan.builder().tagId(tagId).build().toString();
            expect(string).to.containIgnoreSpaces(tagId);
        });
        it("should fail building if tagId is null", function () {
            try {
                Scan.builder().build();
            } catch (error) {
                expect(error.message).to.containIgnoreSpaces("null");
                expect(error.message).to.containIgnoreSpaces("tagId");
            }
        });
        it("should not fail building from toBuilder()", function () {
                const tagId = "dom";
                const scan = Scan.builder().tagId(tagId).build();
                const rebuilt = scan.toBuilder().build();
                expect(rebuilt.tagId()).to.equal(tagId);

        });
    });
    describe("timestamp", function () {
        it("should be set to now if not given", function () {
            const scan = ScanFixtures.scan1();
            expect(scan.timestamp()).to.not.equal(null);
        });
        it("should accept iso string", function () {
            const expected = ScanFixtures.timestamp1();
            const scan = ScanFixtures.scan1().toBuilder().timestamp(expected.format()).build();
            const timestampString1 = ScanFixtures.timestampString1();
            expect(expected.format()).to.equal(timestampString1);
            expect(scan.timestamp()).to.not.equal(null);
            expect(scan.timestamp().format()).to.equal(timestampString1);
            expect(expected.isSame(expected)).to.equal(true);
        });
        it("should throw exception if invalid", function () {
            try {
                expect(ScanFixtures.scan1().toBuilder().timestamp("junk").build()).to.equal(null);
            } catch (error) {
                expect(error).to.be.instanceOf(Assert.AssertionError);
            }
        });
    });
    describe("authUid", function () {
        it("should be assignable", function () {
            const authUserId = "given";
            const tag = ScanFixtures.scan1().toBuilder().authUid(authUserId).build();
            expect(tag.hasAuthUid()).to.equal(true);
            expect(tag.authUid()).to.equal(authUserId);
        });
        it("can be unassigned", function () {
            const tag = ScanFixtures.scan1().toBuilder().build();
            expect(tag.hasAuthUid()).to.equal(false);
        });
    });
    describe("tagType", function () {
        it("should be assignable", function () {
            const tagType = "given";
            const tag = ScanFixtures.scan1().toBuilder().tagType(tagType).build();
            expect(tag.hasTagType()).to.equal(true);
            expect(tag.tagType()).to.equal(tagType);
        });
        it("can be unassigned", function () {
            const tag = ScanFixtures.scan1().toBuilder().build();
            expect(tag.hasTagType()).to.equal(false);
        });
    });
    describe("tagCategory", function () {
        it("should be assignable", function () {
            const tagCategory = "given";
            const tag = ScanFixtures.scan1().toBuilder().tagCategory(tagCategory).build();
            expect(tag.hasTagCategory()).to.equal(true);
            expect(tag.tagCategory()).to.equal(tagCategory);
        });
        it("can be unassigned", function () {
            const tag = ScanFixtures.scan1().toBuilder().build();
            expect(tag.hasTagCategory()).to.equal(false);
        });
    });
    describe("category", function () {
        it("should be assignable", function () {
            const category = "given";
            const tag = ScanFixtures.scan1().toBuilder().category(category).build();
            expect(tag.hasCategory()).to.equal(true);
            expect(tag.category()).to.equal(category);
        });
        it("can be unassigned", function () {
            const tag = ScanFixtures.scan1().toBuilder().build();
            expect(tag.hasCategory()).to.equal(false);
        });
    });
    describe("batchId", function () {
        it("should be assignable", function () {
            const batchId = "batch-123";
            const tag = ScanFixtures.scan1().toBuilder().batchId(batchId).build();
            expect(tag.hasBatchId()).to.equal(true);
            expect(tag.batchId()).to.equal(batchId);
        });
        it("can be unassigned", function () {
            const tag = ScanFixtures.scan1().toBuilder().build();
            expect(tag.hasBatchId()).to.equal(false);
        });
    });
    describe("userIdOfRewardRecipient", function () {
        it("should be assignable", function () {
            const userId = "given";
            const tag = ScanFixtures.scan1().toBuilder().userIdOfRewardRecipient(userId).build();
            expect(tag.hasUserIdOfRewardRecipient()).to.equal(true);
            expect(tag.userIdOfRewardRecipient()).to.equal(userId);
        });
        it("can be unassigned", function () {
            const tag = ScanFixtures.scan1().toBuilder().build();
            expect(tag.hasUserIdOfRewardRecipient()).to.equal(false);
        });
    });
});
