const appRootDir = require("app-root-dir").get();
const Scan = require(appRootDir + "/main/scan/scan.js");
const ScanService = require(appRootDir + "/main/scan/scan-service.js");
const ScanTestUtils = require(appRootDir + "/test/scan/scan-fixtures.js");
const TagTestUtils = require(appRootDir + "/test/tag/tag-fixtures.js");
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const moment = require("moment");

describe("ScanService", function () {
    describe("tagForScan", function () {
        it("should provide a tag for the scan", function () {
            //relies heavily on the _tagService testing
            const scan = ScanTestUtils.scan1();
            const repository = null; //not used for this method
            const tagService = {
                tagFoundOrCreated: function (tagId) {
                    return Promise.resolve(TagTestUtils.tag(tagId));
                },
            };
            const scanService = new ScanService(repository, tagService);
            return scanService.tagForScan(scan).then((tag) => {
                expect(tag.id()).to.equal(scan.tagId());
            });
        });
        it("should provide a tag for the scanId", function () {
            //relies heavily on the _tagService testing
            const scan = ScanTestUtils.scan1();
            const repository = {
                get: ()=>{
                    return Promise.resolve(scan);
                },
            }; //not used for this method
            const tagService = {
                tagFoundOrCreated: function (tagId) {
                    return Promise.resolve(TagTestUtils.tag(tagId));
                },
            };
            const scanService = new ScanService(repository, tagService);
            return scanService.tagForScan(scan.id()).then((tag) => {
                expect(tag.id()).to.equal(scan.tagId());
            });
        });
    });
});
