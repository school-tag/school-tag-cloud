const appRootDir = require("app-root-dir").get();
const Scan = require(appRootDir + "/main/scan/scan.js");
const ScanRepository = require(appRootDir + "/main/scan/scan-repository.js");
const describe = require("mocha").describe;
const it = require("mocha").it;
const DateTestUtils = require("../../test/lang/date-test-utils.js");
const should = require("chai").should();
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));

describe("ScanRepository", function () {
    describe("constructor", function () {
        it("should succeed", function (done) {
            let database = function () {
            };
            database.child = function(path) {
                // scans is passed into ScanRepository constructor
                path.should.startsWith("scans/");
                path.should.contains("-");
                done();
            };

            let repository = new ScanRepository(database,null,DateTestUtils.momentService());
            let databaseRef = repository._ref();
        });
    });
});
