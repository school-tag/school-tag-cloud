const Scan = require("../../main/scan/scan.js");
const DateFixtures = require("../../test/lang/date-test-utils.js");
const TagFixtures = require("../../test/tag/tag-fixtures.js");
const UserFixtures = require("../../test/user/user-fixtures.js");
const StationFixtures = require("../../test/station/station.fixtures");

// ========== ScanTestUtil =============

/**
 *
 */
class ScanFixtures {
    /**
     *
     * @return {string}
     */
    static tagId1() {
        return TagFixtures.tag1Id();
    }


    /**
     * @return {string} the fixture id of a station
     */
    static station1() {
        return StationFixtures.stationId1();
    }


    /**
     * @return {string} set in scan.authUid
     */
    static authUid1() {
        return "auth-uid-1";
    }

    /**
     *
     * @return {*|string}
     */
    static timestampString1() {
        return DateFixtures.timestampString1();
    }

    /**
     *
     * @return {*|moment.Moment}
     */
    static timestamp1() {
        return DateFixtures.timestamp1();
    }

    /**
     * @return {moment.Moment}
     */
    static timestamp2() {
        return DateFixtures.timestamp2();
    }

    /** @return {Scan} a scan that uses other properties in this class named with 1.*/
    static scan1() {
        return ScanFixtures.scanWithoutId().toBuilder().id("scan-1").build();
    }

    /**
     * @return {Scan} scan1 with station1
     */
    static scan1WithStation1() {
        return this.scan1().toBuilder().station(this.station1()).build();
    }


    /** @return {Scan} scan1 with authUid1 assigned.*/
    static scan1WithAuthUid1() {
        return ScanFixtures.scan1().toBuilder().authUid(ScanFixtures.authUid1()).build();
    }

    /**
     * provides a scan, without id, assigned with timestamp of Sunday1
     * @return {Scan}
     */
    static scanSunday1() {
        return ScanFixtures.scanWithoutId().toBuilder().timestamp(DateFixtures.timestampSunday1()).build();
    }

    /**
     *
     * @return {Scan} scan with minimal properties and no id assigned
     */
    static scanWithoutId() {
        return Scan.builder().tagId(ScanFixtures.tagId1()).timestamp(ScanFixtures.timestamp1()).build();
    }

    /**
     * @return {Scan} similar to scanWithoutId but at timestamp2
     */
    static scan2WithoutId() {
        return this.scanWithoutId().toBuilder().timestamp(ScanFixtures.timestamp2()).build();
    }

    /**
     * @return {Scan} like it came in from the stations without id in UTC tomorrow that translates local today
     */
    static scanUtcTomorrow() {
        return this.scanWithoutId().toBuilder().timestamp(DateFixtures.timestampUtcTomorrow()).build();
    }
    /**
     * Give a scan and user 1 will be assigned to be associated to the tag.
     * @param {Scan} scan
     * @return {Scan}
     */
    static withUserIdOfTagOwner(scan) {
        return scan.toBuilder().userIdOfTagOwner(UserFixtures.user1Id().toString()).build();
    }
}

module.exports = ScanFixtures;
