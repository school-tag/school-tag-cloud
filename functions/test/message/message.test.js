const describe = require("mocha").describe;
const it = require("mocha").it;
let expect = require("chai").expect;

const Message = require("../../main/message/message.js");
const MessageFixtures = require("../../test/message/message.fixtures.js");

describe("Message", function () {
    describe("text", function () {
        it("is assignable", function () {
            expect(MessageFixtures.message1().text()).to.not.equal(null);
        });
    });
    describe("timestamp", function () {
        it("is not set if not provided", function () {
            //leave this up to the message service
            const message = MessageFixtures.message1().toBuilder().timestamp(null).build();
            expect(message.timestamp()).to.equal(null);
        });
    });
    describe("aboutUser", function () {
        it("is available if provided", function () {
            const message = MessageFixtures.message1WithUser();
            expect(message.hasAboutUser()).to.equal(true);
            expect(message.aboutUser()).to.deep.equal(MessageFixtures.message1User());
        });
        it("is not available if not provided", function () {
            const message = MessageFixtures.message1();
            expect(message.hasAboutUser()).to.equal(false);
        });
    });
    describe("aboutPayload", function () {
        const payload = {
            "foo": "bar",
            "quantity": 3,
        };

        it("is available if provided", function () {
            const message = MessageFixtures.message1Builder().aboutPayload(payload).build();
            expect(message.hasAboutPayload()).to.equal(true);
            expect(message.aboutPayload()).to.deep.equal(payload);
        });
        it("is not available if not provided", function () {
            const message = MessageFixtures.message1();
            expect(message.hasAboutPayload()).to.equal(false);
        });
    });
    describe("iconUrl", function () {
        const iconUrl = "http://test.me/icon.png";
        it("is available if provided", function () {
            const message = Message.builder().iconUrl(iconUrl).build();
            expect(message.hasIconUrl()).to.equal(true);
            expect(message.iconUrl()).to.equal(iconUrl);
        });
        it("is not available if not provided", function () {
            const message = MessageFixtures.message1();
            expect(message.hasIconUrl()).to.equal(false);
        });
    });
    describe("moreInfoUrl", function () {
        const moreInfoUrl = "http://test.me/more/info";
        it("is available if provided", function () {
            const message = Message.builder().moreInfoUrl(moreInfoUrl).build();
            expect(message.hasMoreInfoUrl()).to.equal(true);
            expect(message.moreInfoUrl()).to.equal(moreInfoUrl);
        });
        it("is not available if not provided", function () {
            const message = MessageFixtures.message1();
            expect(message.hasMoreInfoUrl()).to.equal(false);
        });
    });
    describe("code", function () {
        const code = "a93n";
        it("is available if provided", function () {
            const message = Message.builder().code(code).build();
            expect(message.hasCode()).to.equal(true);
            expect(message.code()).to.equal(code);
        });
        it("is not available if not provided", function () {
            const message = MessageFixtures.message1();
            expect(message.hasCode()).to.equal(false);
        });
    });
    describe("title", function () {
        const title = "This is Cool!";
        it("is available if provided", function () {
            const message = Message.builder().title(title).build();
            expect(message.hasTitle()).to.equal(true);
            expect(message.title()).to.equal(title);
        });
        it("is not available if not provided", function () {
            const message = MessageFixtures.message1();
            expect(message.hasTitle()).to.equal(false);
        });
    });
});

