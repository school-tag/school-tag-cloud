const appRootDir = require("app-root-dir").get();
const Message = require(appRootDir + "/main/message/message.js");
const DateTestUtils = require(appRootDir + "/test/lang/date-test-utils.js");
const UserFixtures = require("../../test/user/user-fixtures");

/**
 * Use this to generate your mock objects.
 */
class MessageFixtures {

    /**
     *
     * @return {string}text for message1
     */
    static text1() {
        return "Hello world";
    }

    /**
     *
     * @return {moment.Moment} for #message1
     */
    static timestamp1() {
        return DateTestUtils.timestamp1();
    }

    /**
     *
     * @return {Message} with the basics available in matching methods ending with 1.
     */
    static message1() {
        return this.message1Builder().build();
    }

    /**
     *
     * @return {MessageBuilder} with the text and timestamp
     */
    static message1Builder() {
        return Message.builder().text(this.text1()).timestamp(this.timestamp1());
    }

    /**
     * @return {User} about user for message 1
     */
    static message1User() {
        return UserFixtures.user1();
    }

    /**
     * @return {Message} message1 with user assigned.
     */
    static message1WithUser() {
        return MessageFixtures.message1Builder().aboutUser(MessageFixtures.message1User()).build();
    }
}

module.exports = MessageFixtures;