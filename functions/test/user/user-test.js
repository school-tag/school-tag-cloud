const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;

const User = require("../../main/user/user.js");
const DateTestUtils = require("../../test/lang/date-test-utils.js");
const DateUtils = require("../../main/lang/date-utils.js");
const UserProperties = require("../../main/user/user").UserProperties;

/**Unit tests for user.*/
describe("User", function () {
    describe("momentCreated", function () {
        it("should maintain timezone", function () {
            const expected = DateTestUtils.timestampString1();
            const user = User.builder().momentCreated(expected).build();
            expect(DateUtils.timestampMomentPortable(user.momentCreated())).to.equal(expected);
        });
        it("should be set automatically", function () {
            const user = User.builder().build();
            expect(user.momentCreated()).to.not.equal(null);
        });
    });
    describe("hasNickname", function () {
        it("is true if nickname is set", function () {
            const expected = "yabadabadoo";
            const withNickame = User.builder().nickname(expected).build();
            expect(withNickame.nickname()).to.equal(expected);
            expect(withNickame.hasNickname()).to.equal(true);
        });
        it("is false if nickname is not set", function () {
            const withoutNickname = User.builder().build();
            expect(withoutNickname.hasNickname()).to.equal(false);
        });
    });
    describe("avatar", function () {
        it("is true if avatar is set", function () {
            const expected = "/whatever/image.png";
            const withAvatar = User.builder().avatar(expected).build();
            expect(withAvatar.avatar()).to.equal(expected);
            expect(withAvatar.hasAvatar()).to.equal(true);
        });
        it("is false if avatar is not set", function () {
            const withoutNickname = User.builder().build();
            expect(withoutNickname.hasAvatar()).to.equal(false);
        });
    });
    describe("authUid", function () {
        it("should be assignable", function () {
            const authUserId = "given";
            const tag = User.builder().authUid(authUserId).build();
            expect(tag.hasAuthUid()).to.equal(true);
            expect(tag.authUid()).to.equal(authUserId);
        });
        it("can be unassigned", function () {
            const tag = User.builder().build();
            expect(tag.hasAuthUid()).to.equal(false);
        });
    });
    describe("UserProperties", function () {
        it("should be assignable", function () {
            const properties = new UserProperties();
            expect(properties.momentCreated).to.equal(undefined);
            expect(properties.nickname).to.equal(undefined);
            expect(properties.authUid).to.equal(undefined);
            expect(properties.avatar).to.equal(undefined);
            expect(properties.id).to.equal(undefined);
        });
    });
});
