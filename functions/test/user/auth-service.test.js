const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const AuthService = require("../../main/user/auth-service");


const nullAuthService = new AuthService(null);
const authUid = "abc";
const role = "xyz";
const matchingCustomClaims = [];
matchingCustomClaims[role] = true;
const notMatchingCustomClaims = [];

describe("AuthService", function () {
    describe("authUserHasRole", function () {
        it("should return true if the user is found and has the given role", function () {
            const adminAuth = {
                getUser: ()=>{
                    return Promise.resolve({
                        customClaims: matchingCustomClaims,
                    });
                },
            };
            const authService = new AuthService(adminAuth);
            return authService.authUserHasRole(authUid, role).then((result)=>{
                expect(result).to.equal(true);
            });
        });
        it("should return false if the user is found and does not have the given role", function () {
            const adminAuth = {
                getUser: ()=>{
                    return Promise.resolve({
                        customClaims: notMatchingCustomClaims,
                    });
                },
            };
            const authService = new AuthService(adminAuth);
            return authService.authUserHasRole(authUid, role).then((result)=>{
                expect(result).to.equal(false);
            });
        });
        it("should return false if user is not found ", function () {
            const adminAuth = {
                getUser: ()=>{
                    return Promise.reject("user not found");
                },
            };
            const authService = new AuthService(adminAuth);
            return authService.authUserHasRole(authUid, role).then((result)=>{
                expect(result).to.equal(false);
            });
        });
        it("should throw an error if a role is not given", function () {
            try {
                return nullAuthService.authUserHasRole("abc").then(() => {
                    return Promise.reject("error should be thrown");
                });
            } catch (error) {
                //this is expected
            }
        });
        it("should return false if the authUid is undefined", function () {
            return nullAuthService.authUserHasRole(undefined, "whatever").then((hasRole) => {
                expect(hasRole).to.equal(false);
            });
        });

    });
});
