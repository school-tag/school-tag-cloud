const appRootDir = require("app-root-dir").get();
const User = require(appRootDir + "/main/user/user.js");
const UserId = require(appRootDir + "/main/user/user.js").UserId;

/**
 * test fixtures for User
 */
class UserFixtures {

    /**
     *
     * @return {UserId} for user1
     */
    static user1Id() {
        return new UserId("user-1");
    }

    /**
     *
     * @return {User} with id and nickname 1
     */
    static user1() {
        return this.user1Builder().nickname("nickname-1").build();
    }

    /**
     * @return {UserBuilder}
     */
    static user1Builder() {
        return User.builder().id(this.user1Id());
    }

    /**
     *
     * @return {string} static id for validation
     */
    static authUid1() {
        return "auth-user-1";
    }
    /**
     *
     * @return {User} with authUid1 and no nickname.
     */
    static user1WithAuthId1() {
        return UserFixtures.user1Builder().authUid(UserFixtures.authUid1()).build();
    }
}

module.exports = UserFixtures;