const appRootDir = require("app-root-dir").get();
const EntityUtils = require(appRootDir + "/main/entity/entity-utils.js");
const Entity = require(appRootDir + "/main/entity/entity.js");
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));

describe("EntityUtils", function () {
    describe("portableArray", function () {
        const id1 = "foo";
        const key1 = "test";
        const value1 = "junk";
        const entity1 = new Entity.EntityBuilder(new Entity()).id(id1)
            ._property(key1, value1).build();
        //build the expected entity manually
        const expectedEntity1 = {};
        expectedEntity1[Entity.EntityProperties.id()] = id1;
        expectedEntity1[key1] = value1;
        //assign the entity to the root object
        const expected = {};
        expected[id1] = expectedEntity1;
        it("should return empty object for empty array", function () {
            expect(EntityUtils.portableArray([])).to.deep.equal({});
        });
        it("should find a single entity ", function () {
            expect(EntityUtils.portableArray([entity1])).to.deep.equal(expected);
        });
        it("should find a two entities ", function () {
            const id2 = "bar";
            const entity2 = new Entity.EntityBuilder(new Entity()).id(id2)
                ._property(key1, value1).build();
            const expectedEntity2 = {};
            expectedEntity2[Entity.EntityProperties.id()] = id2;
            expectedEntity2[key1] = value1;
            expected[id1] = expectedEntity1;
            expected[id2] = expectedEntity2;
            expect(EntityUtils.portableArray([entity1, entity2])).to.deep.equal(expected);
        });
        it("should use the id from the id extractor function ", function () {
            const key = key1;
            const extractorFunction = function (entity) {
                return key;
            };
            expect(Object.keys(EntityUtils.portableArray([entity1], extractorFunction))).to.contain(key);
        });
    });
    describe("deepClone", function () {
        it("should create a deep copy without references", () => {
            const beforeModification = 0;
            obj1 = {a: beforeModification, b: {c: beforeModification}};
            let obj3 = EntityUtils.deepClone(obj1);
            expect(obj3).to.deep.equal(obj1);
            //modifying obj1 shouldn't affect object 3
            const afterModification = 4;
            obj1.a = afterModification;
            obj1.b.c = afterModification;
            expect(obj3.a).to.equal(beforeModification);
            expect(obj3.b.c).to.equal(beforeModification);
        });
    });

});
