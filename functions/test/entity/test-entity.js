const Entity = require("../../main/entity/entity.js");

/**
 * Used only for testing
 * @private
 * @extends {Entity.<TestEntity>}
 */
class TestEntity extends Entity {
    /**
     * provides nothing
     * @param {*} properties to be stored as the properties of the entity.
     */
    constructor(properties) {
        super(properties);
    }

    /**
     * call this to get a test entity.
     * @param {TestEntity} [entity]
     * @return {EntityBuilder.<TestEntity>}
     */
    static builder(entity) {
        if (!(entity instanceof TestEntity)) {
            entity = new TestEntity(entity);
        }
        return new Entity.EntityBuilder(entity);
    }
}

module.exports = TestEntity;