const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const TestEntity = require("./test-entity.js");
const Repository = require("../../main/entity/repository.js");
const FirebaseUtils = require("../../main/firebase/firebase-utils.js");

describe("Repository", function () {
    const id = "repository-test-1";
    const domain = "tests";
    const path = `/repository/test/ref/${id}`;
    const entity = TestEntity.builder().id(id).build();
    const snapshot = {
        ref: path,
        key: id, //end of path
        exists: () => {
            return true;
        },
        val: () => {
            return entity.portable();
        },
    };
    const root = FirebaseUtils.mockReference(path);
    const fullPathIdRepository = Repository.builder()
        .hasFullPathId(true)
        .databaseReference(root)
        .entityType(TestEntity).build();
    const defaultRepository = Repository.builder()
        .databaseReference(root)
        .entityDomain(domain)
        .entityType(TestEntity).build();
    describe("hasFullPath", function () {
        it("should be false by default", function () {
            expect(defaultRepository._hasFullPathId).to.equal(false);
        });
        it("should be false if set to false", function () {
            const repositoryFullPathFalse = Repository.builder()
                .hasFullPathId(false)
                .entityType(TestEntity)
                .databaseReference(root).build();
            expect(repositoryFullPathFalse._hasFullPathId).to.equal(false);
        });
        it("should be true if set to true", function () {
            expect(fullPathIdRepository._hasFullPathId).to.equal(true);
        });
    });
    describe("entityFromSnapshot", function () {
        it("should return entity with id = key", function () {
            const actual = defaultRepository.entityFromSnapshot(snapshot);
            expect(actual).to.be.instanceOf(TestEntity);
            expect(actual.id()).to.equal(id);
        });
        it("should return entity with full path if requested", function () {
            const actual = fullPathIdRepository.entityFromSnapshot(snapshot);
            expect(actual).to.be.instanceOf(TestEntity);
            expect(actual.id()).to.equal(path);
        });
    });
    describe("userNotFoundCode", function () {
        it("should return the domain when available", function () {
            expect(defaultRepository.notFoundCode()).to.equal("tests/notFound");
        });
        it("should return the entity type when available", function () {
            expect(fullPathIdRepository.notFoundCode()).to.equal("TestEntity/notFound");
        });
    });
    describe("pathAppended", function () {
        it("returns the given appeneded to the path", function () {
            expect(defaultRepository.pathAppended("stuff")).to.equal("tests/stuff");
        });
    });

});