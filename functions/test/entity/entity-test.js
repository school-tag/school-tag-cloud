const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));

const TestEntity = require("./test-entity.js");


describe("Entity", function () {
    describe("hasId", function () {
        it("should return true if id is set", function () {
            expect(TestEntity.builder().id("foo").build().hasId()).to.equal(true);
        });
        it("should return false if id is not set", function () {
            expect(TestEntity.builder().build().hasId()).to.equal(false);

        });
        it("should return false if id is set to null", function () {
            expect(TestEntity.builder().id(null).build().hasId()).to.equal(false);
        });
    });
    describe("toBuilder", function () {
        it("should clone properties instead of mutate", function () {
            const original = TestEntity.builder().id("foo").build();
            const key = "yin";
            const value = "yang";
            const copy = original.toBuilder()._property(key, value).build();
            expect(original._property(key)).to.be.undefined;
            expect(copy._property(key)).to.equal(value);

        });
    });
});
