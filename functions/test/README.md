
#Testing
This project is heavily tested to ensure all works as prescribed.  

## File Naming / Directory Structure

All tests are located in subdirectories of the `test` directory, a sibling of `main`.
Each directory in main must have a corresponding directory in test where the files are organized.

### Unit Tests
Unit tests are quick tests that validate a function or class does what it promises.

`{domain}-test.js` where domain matches the subject being tested, generally matching the corresponding js file in `main`.

* quick
* never dependent on any external resource (even files)
* small and focused
* synchronous 

Mocks are encouraged to test the functionality of a connected service.
Such allows the validation of a Firebase Repository, for example, without relying on the internet 
or external service to be available.

###Integration Tests
Connected tests that validate the code works well with external systems.
See [test-integration](../test-cloud) for more information.

`{domain}-{system}-test.js` where the _system_ indicates the service for which the test depends (firebase for example). 

#### Firebase Integration Tests

To run the tests, one must have the Firebase credentials in .firebase.

See [.firebase/README.md](../.firebase/README.md).

###System Tests
Connected tests that depend on code being deployed to the system for validation of function.

System tests validate the code using the real service which requires internet.  

`{domain}-{system}-test.js` where the _system_ indicates the service for which the test depends (firebase-functions for example). 

See [test-system][../test-system].

## Mocha + Chai Tests

[Mocha](https://mochajs.org/) runs all tests with `npm test`.

[Chai](http://chaijs.com/) is used for assertions.  



```$javascript
const describe = require('mocha').describe;
const it = require('mocha').it;
const should = require('chai').should();
const expect = require('chai').expect;
const chai = require('chai');
chai.use(require('chai-string'));

describe('ClassName', function () {
    describe('member', function () {
        it('should do something', function () {
         const something = "hello";
         something.should.not.be.null;
        })
    })
})
```

Notice chai `should` is preferred over `expects`, but expect must be used in some situations.


### WebStorm Tests

Running tests in WebStorm or other IDEA products is easy and encouraged.  
Using the debugger works well. 

