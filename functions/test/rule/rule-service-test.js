const Fact = require("json-rules-engine").Fact;

const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));

const RuleService = require("../../main/rule/rule-service.js");
const RuleRepository = require("../../main/rule/rule-repository.js");
const RuleFixtures = require("./rule-fixtures");

describe("RuleService", function () {
    describe("constructor", function () {
        it("should handle constant facts", function () {
            //from https://github.com/CacheControl/json-rules-engine/blob/master/docs/facts.md
            const fact = new Fact("apiKey", "4feca34f9d67e99b8af2");
            const repository = new RuleRepository([], [], [fact]);
            const ruleService = new RuleService(repository);
            //it previously did not
            return ruleService._enginePromise;
        });
    });
});
