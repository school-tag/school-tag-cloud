/**
 * Useful for testing rules
 * https://github.com/CacheControl/json-rules-engine
 */
class RuleFixtures {

    /**
     *
     * @return {string} the id used in basicOptions()
     */
    static basicOptionsRuleId() {
        return "my-event";
    }

    /**
     * Sample options from https://github.com/CacheControl/json-rules-engine/blob/master/docs/rules.md
     *
     * @return {*}
     */
    static basicOptions() {
        return {
            conditions: {
                all: [
                    {
                        fact: "my-fact",
                        operator: "equal",
                        value: "some-value",
                    },
                ],
            },
            event: {
                type: RuleFixtures.basicOptionsRuleId(),
                params: {
                    customProperty: "customValue",
                },
            },
            // optional, default: 1
            priority: 1,
            onSuccess: function (event, almanac) {
            }, // optional
            onFailure: function (event, almanac) {
            }, // optional
        };
    }

    /** A untyped object, as if it came from firebase, with a key referencing the basic options.
     *
     * @return {*}
     */
    static singleRuleBasicOptions() {
        const rulesObject = {};
        const basicOptions = RuleFixtures.basicOptions();
        rulesObject[basicOptions.event.type] = basicOptions;
        return rulesObject;
    }

    /**
     * @return {[*]} the basic options rule in array form to be used with the repository constructor for default rules
     */
    static defaultRulesBasicOptionsArray() {
        return [RuleFixtures.basicOptions()];
    }
}

module.exports = RuleFixtures;