const Rule = require("json-rules-engine").Rule;
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;

const Rules = require("../../main/rule/rules.js");
const RuleTestUtils = require("../../test/rule/rule-fixtures");

describe("Rules", function () {
    describe("toArray", function () {
        it("should produce empty array for no rules given", function () {
            const rules = Rules.builder().build();
            expect(rules.toArray()).to.have.lengthOf(0);
        });
        it("should produce array with 1 rule", function () {
            const rulesJson = RuleTestUtils.singleRuleBasicOptions();
            const rules = Rules.builder(rulesJson).build();
            const rulesArray = rules.toArray();
            expect(rulesArray).to.have.lengthOf(1);
            const rule = rulesArray[0];
            expect(rule).to.be.instanceOf(Rule);
        });
    });
    describe("builder", function () {
        it("should clone existing", function () {
            const original = Rules.builder().build();
            const cloned = original.toBuilder().id("other").build();
            expect(original.id()).to.not.equal(cloned.id());
        });
    });
});