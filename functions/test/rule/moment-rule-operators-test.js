const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const moment = require("moment");

const appRootDir = require("app-root-dir").get();
const MomentRuleOperators = require(appRootDir + "/main/rule/moment-rule-operators.js");

/**
 * Inputs are strings, just like the rules declared in json.  Moments are the facts.
 */
describe("MomentRuleOperators", function () {
    const eightTimestamp = "2017-03-02T08:00:00";
    const eightTime = "08:00:00";
    const afterEightTime = "08:00:01";
    const beforeEightTime = "07:59:59";
    let assertOperator = function (operator, timestamp, timeOfDay, expectedBoolean) {
        const result = operator.cb(timestamp, timeOfDay);
        expect(result).to.equal(expectedBoolean);
    };
    describe("afterTimeOfDayInclusive", function () {
        let assertAfterInclusive = function (timestamp, timeOfDay, expected) {
            assertOperator(MomentRuleOperators.afterTimeOfDayInclusive(), timestamp, timeOfDay, expected);
        };
        it("should be true for equal times ", function () {
            assertAfterInclusive(eightTimestamp, eightTime, true);
        });
        it("should be false for early time ", function () {
            assertAfterInclusive(eightTimestamp, afterEightTime, false);
        });
        it("should be true for later time", function () {
            assertAfterInclusive(eightTimestamp, beforeEightTime, true);

        });
    });
    describe("afterTimeOfDayExclusive", function () {
        let assertAfterExclusive = function (timestamp, timeOfDay, expected) {
            assertOperator(MomentRuleOperators.afterTimeOfDayExclusive(), timestamp, timeOfDay, expected);
        };
        it("should be true for equal times ", function () {
            assertAfterExclusive(eightTimestamp, eightTime, false);
        });
        it("should be false for early time ", function () {
            assertAfterExclusive(eightTimestamp, afterEightTime, false);
        });
        it("should be true for later time", function () {
            assertAfterExclusive(eightTimestamp, beforeEightTime, true);

        });
    });
    describe("beforeTimeOfDayInclusive", function () {
        let assertBeforeInclusive = function (timestamp, timeOfDay, expected) {
            assertOperator(MomentRuleOperators.beforeTimeOfDayInclusive(), timestamp, timeOfDay, expected);
        };
        it("should be true for equal times ", function () {
            assertBeforeInclusive(eightTimestamp, eightTime, true);
        });
        it("should be true for early time ", function () {
            assertBeforeInclusive(eightTimestamp, afterEightTime, true);
        });
        it("should be false for later time", function () {
            assertBeforeInclusive(eightTimestamp, beforeEightTime, false);

        });
    });
    describe("beforeTimeOfDayInclusive", function () {
        let assertBeforeExclusive = function (timestamp, timeOfDay, expected) {
            assertOperator(MomentRuleOperators.beforeTimeOfDayExclusive(), timestamp, timeOfDay, expected);
        };
        it("should be true for equal times ", function () {
            assertBeforeExclusive(eightTimestamp, eightTime, false);
        });
        it("should be true for early time ", function () {
            assertBeforeExclusive(eightTimestamp, afterEightTime, true);
        });
        it("should be false for later time", function () {
            assertBeforeExclusive(eightTimestamp, beforeEightTime, false);

        });
    });
    describe("isWeekday", function () {
        let assertIsWeekday = function (timestamp, wantedValue, expected) {
            const result = MomentRuleOperators.isWeekday().cb(timestamp, wantedValue);
            expect(result).to.equal(expected);
        };
        //edge case since Z is actually a Sunday
        const monday = "2017-08-07T00:00:00+02:00";
        //edge case since Z is actually monday
        const sunday = "2017-08-06T23:00:00-02:00";
        //times are acquired by looking at a calendar
        //time zones are set to ensure local time is respected
        it("should be true for monday", function () {
            assertIsWeekday(monday, true, true);
        });
        it("should be false for monday expecting false", function () {
            //proves boolean checks work
            assertIsWeekday(monday, false, false);
        });
        it("should be false for sunday", function () {
            assertIsWeekday(sunday, true, false);
        });
        it("should be false for null", function () {
            assertIsWeekday(null, true, false);

        });
    });

});
