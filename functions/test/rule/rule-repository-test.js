const Operator = require("json-rules-engine").Operator;
const Fact = require("json-rules-engine").Fact;
const Rule = require("json-rules-engine").Rule;
const console = require("console");
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;

const Rules = require("../../main/rule/rules.js");
const RuleRepository = require("../../main/rule/rule-repository.js");
const RuleFixtures = require("./rule-fixtures");

describe("RuleRepository", function () {
    const rule1Properties = {
        conditions: {
            all: [
                {
                    fact: 'my-fact',
                    operator: 'equal',
                    value: 'some-value',
                },
            ],
        },
    };
    const rule1 = new Rule(rule1Properties);
    const oneRule = [rule1];
    describe("constructor", function () {

        it("should set facts", function () {
            const fact1 = new Fact("fact-1", 3);
            const repo = new RuleRepository([], [], [fact1]);
            expect(repo.facts()).to.have.lengthOf(1);
        });
        it("should set default rules", function () {

            const repo = new RuleRepository(oneRule, [], []);
            return repo.rules().then((rules) => {
                expect(rules).to.have.lengthOf(1);
            });
        });
        it("should use the repository if default is not provided", function () {
            let mockCalled = false;
            const database = {
                get: () => {
                    const rulesProperties = {
                        "rule1": rule1,
                    };
                    mockCalled = true;
                    return Promise.resolve(Rules.builder(rulesProperties).build());
                },
            };
            const repo = new RuleRepository(null, [], [], database);
            return repo.rules().then((rules) => {
                expect(mockCalled).to.equal(true);
                //leave parsing details up to the cloud tests
            });
        });

    });
    describe("rule by id", function () {
        const rules = RuleFixtures.defaultRulesBasicOptionsArray();
        const repo = new RuleRepository(rules, [], []);
        const expectedRuleId = RuleFixtures.basicOptionsRuleId();

        it("should find a rule by the id", function () {
            return repo.rule(expectedRuleId).then((rule) => {
                expect(rule).to.not.equal(null);
                expect(rule.event.type).to.equal(expectedRuleId);
            });
        });
        it("should should report error for rule not found", function () {
            return repo.rule("junk").then((rule) => {
                return Promise.reject("shouldn't have found the rule " + rule);
            }, (error) => {
                return Promise.resolve(error.message);
            });
        });

    });

});
