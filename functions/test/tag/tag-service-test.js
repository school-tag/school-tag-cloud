const appRootDir = require("app-root-dir").get();
const Tag = require(appRootDir + "/main/tag/tag.js");
const User = require(appRootDir + "/main/user/user.js");
const TagService = require(appRootDir + "/main/tag/tag-service.js");
const TagTestUtils = require(appRootDir + "/test/tag/tag-fixtures.js");
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
describe("TagService", function () {
    describe("tagAndUserCreated", function () {
        it("should indicate error if user creation fails", function (done) {
            const repository = {
                get: function () {
                    return Promise.reject();
                },
                /**@param {Tag} tag */
                save: function (tag) {
                    done("tag shouldn't be saved");
                },
            };
            const userService = {
                createUser: function () {
                    throw error("purposely throwing");
                },
            };
            const tagService = new TagService(repository, userService);
            tagService.tagFoundOrCreated("doesn't matter").then(
                function onSuccess(tag) {
                    done("expecting error");
                },
                function onError(error) {
                    done();
                }
            );
        });
    });
    describe("tagFoundOrCreated", function () {
        it("should find an existing tag", function () {
            const tag = TagTestUtils.tag1();
            const userService = null;
            const repository = {
                get: function (tagId) {
                    return Promise.resolve(tag);
                },
            };
            const tagService = new TagService(repository, userService);
            return tagService.tagFoundOrCreated(tag.id()).then(
                /** @param {Tag} found*/
                function (found) {
                    expect(found.id()).to.equal(tag.id());
                }
            );
        });
    });
    describe("associateTagToUser", function () {
        it("should return the given user", function () {
            const options = {};
            const userId = "user-1";
            TagService.associateTagToUser(options, userId);
            const extracted = TagService.associateTagToUser(options);
            expect(extracted).to.equal(userId);
        });
        it("should return nothing when given no options", function () {
            expect(TagService.associateTagToUser(null)).to.equal(null);
        });
        it("should return nothing when given options has no user", function () {
            const options = {};
            const assigned = TagService.associateTagToUser(options);
            expect(assigned).to.equal(undefined);
            const extracted = TagService.associateTagToUser(options);
            expect(extracted).to.equal(undefined);
        });
    });

});
