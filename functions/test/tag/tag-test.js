const describe = require("mocha").describe;
const it = require("mocha").it;
let expect = require("chai").expect;

const appRootDir = require("app-root-dir").get();
const Tag = require(appRootDir + "/main/tag/tag.js");
const UserId = require(appRootDir + "/main/user/user.js").UserId;
const TagTestUtils = require(appRootDir + "/test/tag/tag-fixtures.js");

describe("Tag", function () {
    describe("tagId", function () {
        it("builder mutator should set tagId", function () {
            const expected = TagTestUtils.tag1Id;
            const tag = Tag.builder().id(expected).build();
            expect(expected).to.equal(tag.tagId());
        });
    });
    describe("userId", function () {
        it("hasUserId", function () {
            const tag = Tag.builder().userId(new UserId("user")).build();
            expect(tag.hasUserId()).to.equal(true);
        });
    });
    describe("category", function () {
        it("should be available when set", function () {
            const junkCategory = "junk";
            const tagWithCategory = TagTestUtils.tag1().toBuilder().category(junkCategory).build();
            expect(tagWithCategory.hasCategory()).to.equal(true);
            expect(tagWithCategory.category()).to.equal(junkCategory);
        });
        it("should be unavailable when not set", function () {
            const tagWithoutCategory = TagTestUtils.withId().build();
            expect(tagWithoutCategory.hasCategory()).to.equal(false);
        });
    });
    describe("points", function () {
        it("should be available when set", function () {
            const points = 10;
            const tag = TagTestUtils.tag1().toBuilder().points(points).build();
            expect(tag.hasPoints()).to.equal(true);
            expect(tag.points()).to.equal(points);
        });
        it("should be unavailable when not set", function () {
            const tagWithoutPoints = TagTestUtils.withId().build();
            expect(tagWithoutPoints.hasPoints()).to.equal(false);
        });
    });
    describe("type", function () {
        it("should be available when set", function () {
            const testType = "junk";
            const tagWithCategory = TagTestUtils.tag1().toBuilder().type(testType).build();
            expect(tagWithCategory.hasType()).to.equal(true);
            expect(tagWithCategory.type()).to.equal(testType);
        });
        it("should be unavailable when not set", function () {
            const tagWithoutCategory = TagTestUtils.withId().build();
            expect(tagWithoutCategory.hasType()).to.equal(false);
        });
    });

    describe("builder", function () {
        it("builder should mutate given Tag", function () {
            const tag1 = TagTestUtils.tag1();
            const tag2 = Tag.builder(tag1).build();
            expect(tag1.tagId()).to.equal(tag2.tagId());
            expect(tag2).to.not.equal(null);
        });
    });
});


