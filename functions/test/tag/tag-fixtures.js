const Tag = require("../../main/tag/tag.js");
const TagService = require("../../main/tag/tag-service");
const UserFixtures = require("../../test/user/user-fixtures.js");
const SchoolGameRules = require("../../main/game/school-game-rules");
/**
 * Helper for testing {Tag}
 */
class TagFixtures {
    /**@return {string}*/
    static tag1Id() {
        return "tag-1";
    };

    /**@return {Tag}*/
    static tag1() {
        return TagFixtures.tag(TagFixtures.tag1Id());
    };


    /**
     * @param {string} tagId which will be assigned to ID
     * @return {Tag}
     */
    static tag(tagId) {
        return Tag.builder().id(tagId)
            .userId(UserFixtures.user1Id())
            .category(SchoolGameRules.tagCategoryBackpack()).build();
    }

    /**
     * @param {string} tagId
     * @return {TagBuilder} with only id already set
     */
    static withId() {
        return Tag.builder()
            .id(TagFixtures.tag1());
    }


    /**
     * Creates an options and assigns the authorization to create a tag using the Tag Service.
     * @return {{}}
     */
    static optionsAuthorizedToCreate() {
        const options = {};
        TagService.authorizedToCreateOption(options, true);
        return options;
    };
}

module.exports = TagFixtures;
