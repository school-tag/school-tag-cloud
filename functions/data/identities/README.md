## Identities

The Avatar (virtual identity) of a person interacting with the system.  

* Image
* Nickname


```json
   "Peru": {
      "nickname": "Peru",
      "avatar": "/square-flags/Peru-01.png",
      "category": "country",
      "type": "player",
      "level": 1
   },
```

### Identity Types

All identities are categorized into one of two types:

* player - those with a tag
* observer - those without a tag, but authenticated by a phone

These categories are used to deliver the appropriate identity to a new user.

### Identity Levels

Player identities have a numerical "level" that increases with value:

* 1 - The base level delivered to all new players
* 2+ - Premium identities that may be chosen earning achievements

Future enhancements will allow the player to choose a new identity when a prize is earned.

### Identity Categories

Help categorize images into subgroups like countries, country provinces, avengers, etc. 
Mostly available for database administration for filtering and data mining.

### Identity Assignment

Each identity is assigned only once per school project.  As an identity is chosen,
the identity entity is modified to reference the user for which it has been assigned.
The user also receives a copy of necessary attributes like icon and name. 

### Identity Images

Although any image would work, the best images are either square or cicular in nature.  

* Transparent backgrounds
* High resolution PNG or SVG

The images will be resized by the system to ensure a large image will be delivered smaller.

#### Image Storage and View

All images must be stored in the [Main School Tag project](https://console.firebase.google.com/project/school-tag/storage/school-tag.appspot.com/files~2Fimages~2Favatars).

Images delivery is up to the client app, since the cloud does not do any presentation.
The paths found in the JSON must reference images found on this server, using a relative path
from the `/images/avatars` directory.

### Identity Deployment

Writing json to the database using Firebase CLI.  Write permissions must be assigned
to the user for the project.  

1. Create a json containing all identity json to be offered for a school.
1. Save the json in the `functions/data/identities/env` directory 
   1. Name it to match the environment school code (i.e. `wca`, `lf`, `playground`, etc)
1. `ENV={school code} npm run deploy:data:identities:env`

You will be be prompted for confirmation.

:warning: All identities will be overwritten thereby allowing duplicate identities to be delivered.

Future enhancements should deliver only new identities.


### Identity Testing

The sandbox and playground environments receive `test.json` during continuous integration 
and testing. The tests actually reserve identities and will fail if not periodically deployed,
because no identities will be available.

CI runs `npm run deploy:data:identities` as part of the `deploy:data` calls.

