const functions = require("firebase-functions");
const admin = require("firebase-admin");

const env = require("env-var");
const NODE_USER_AGENT = env("npm_config_user_agent").asString();
const cors = require("cors")({origin: true});

//see /pipelines/pipelines-env-generation.sh
const ApplicationEnv = require("./generated/application-env");
const GameFirebaseDatabaseFunctions = require("./main/game/game-firebase-database-functions");
const GameFirebaseHttpFunctions = require("./main/game/game-firebase-http-functions");
const SystemBootstrap = require("./main/bootstrap");
const Audit = require("./main/entity/repository").Audit;
const UserRewardsRepository = require("./main/game/user-rewards-repository");
const {PubSub} = require("@google-cloud/pubsub");
//initialize the admin api using the functions config
admin.initializeApp(functions.config().firebase);

/**
 * Returns the versions of the application to identify changes to the app.
 * @type {HttpsFunction}
 */
exports.environments = functions.https.onRequest((request, response) => {
    cors(request, response, () => {
        //response.header("Access-Control-Allow-Origin", "*");
        const all = {};
        Object.assign(all, ApplicationEnv.variables, env);
        response.json(all);
    });
});

/**Registers the given fcm token to be notified when the user being followed achieves a reward.
 *
 * No authentication is required since the system first validates the notification works by sending
 * a welcome message. If the token is not valid, then it will not continue and the system resources
 * are not used.
 *
 * If the token succeeds, then the token will be saved and subsequently
 * notified by the notifyFollowersOfReward function when the participant achieves a reward.
 *
 * {
 *   "following": "${userId}",
 *   "notificationRecipient": {
 *   "recipientId": "${fcmToken}"
 *  }
 * }
 *
 * @method POST
 * @type {HttpsFunction}
 * @return {Promise<void>} indicating when it should be done, although it is done when response.send is called.
 */
exports.followUser = functions.https.onRequest((request, response) => {
    const httpBootstrap = exports.bootstrap(exports.followUser.name, admin.database().ref());
    const httpFunctions = new GameFirebaseHttpFunctions(httpBootstrap);
    return httpFunctions.followUserHttpFunction(request, response);
});

/**
 *
 * @param {string} resource indicating which function was called.
 * @param {Reference|admin.database.Reference} rootReference from the admin or database trigger
 * @return {SystemBootstrap}
 */
exports.bootstrap = (resource, rootReference) => {
    const audit = new Audit(resource, NODE_USER_AGENT, ApplicationEnv.variables.buildNumber);
    return SystemBootstrap.builder()
        .rootReference(rootReference)
        .auditTemplate(audit)
        //do not set rules ... use the database
        //FIXME: this needs to come from environment
        .timeZone("America/Los_Angeles")
        .messaging(admin.messaging())
        .admin(admin)
        .pubSub(new PubSub())
        .build();
};

/**
 * @param {DataSnapshot} data the snapshot that triggered this event
 * @param {EventContext} context information about the event
 * @return {GameFirebaseDatabaseFunctions}
 */
exports.gameDatabaseFunctions = function (data, context) {
    const bootstrap = exports.bootstrap(context.resource, data.ref.root);
    const gameFunctions = new GameFirebaseDatabaseFunctions(bootstrap);
    //provide the snapshot so the function can use specifics
    /** @type DataSnapshot */
    gameFunctions.snapshot = data;

    return gameFunctions;
};

const scansReceivedPath = "/scans/{date}/{scanId}";

/** Triggered when a scan is received, this calls the game service to
 *  provide any rewards that may be achieved for reaching rewards.
 *
 * @type {CloudFunction<DeltaSnapshot>}
 */
// exports.scanReceived = functions.database.ref(scansReceivedPath)
//     .onCreate((data, context) => {
//         const gameFunctions = exports.gameDatabaseFunctions(data, context);
//         return gameFunctions.scanReceived(gameFunctions.snapshot);
//     });

/**
 * The path where scans are written. Although not restful, the path is efficient
 * and clear in the console since homogeneous data lives within.
 * @type {string}
 */
const userScansPath = GameFirebaseDatabaseFunctions.userRewardsScansForDayPathTemplate();

/**Listens for scans and summarizes all points for a day.
 */
// exports.userDailyPoints = functions.database.ref(userScansPath)
//     .onWrite((change, context) => {
//         const gameFunctions = exports.gameDatabaseFunctions(change.after, context);
//         return gameFunctions.userDailyPoints(gameFunctions.snapshot);
//     });

const totalsUpdatedPath = GameFirebaseDatabaseFunctions.userRewardsTotalsForDayPathTemplate();
/**
 * Updates the totals for weeks, months, etc. once the daily total is updated.
 *
 * @type {CloudFunction<DeltaSnapshot>}
 */
exports.totalsUpdatedFromDay = functions.database.ref(totalsUpdatedPath)
    .onWrite((change, context) => {
        const gameFunctions = exports.gameDatabaseFunctions(change.after, context);
        return gameFunctions.totalsUpdatedFromDay(gameFunctions.snapshot);
    });

const rewardForScanPath =
    "rewards/users/{userId}/scans/years/{year}/weeks/{week}/days/{dayOfWeek}/{scanId}/rules/{ruleId}";
/**
 * When a reward is achieved, the followers of the user are notified.
 * @type {CloudFunction<DeltaSnapshot>}
 */
exports.notifyFollowersOfReward = functions.database.ref(rewardForScanPath)
    .onCreate((data, context) => {
        const gameFunctions = exports.gameDatabaseFunctions(data, context);
        return gameFunctions.notifyOfRuleReward(gameFunctions.snapshot);
    });


/**
 * Triggered when a grand total is updated, typically after ever reward is given from a scan,
 * this updates the ranks for all players of the game. This frequency can be a bit heavy handed,
 * but the calls are fairly efficient since all entities (totals and users) are loaded in bulk and
 * used by the PlayerRankCalculator.  This is likely sustainable until 1000 players.
 *
 * @type {CloudFunction<DeltaSnapshot>}
 */
exports.updateLeaderboard = functions.database.ref(UserRewardsRepository.grandTotalsPath())
    .onWrite((change, context) => {
        const gameFunctions = exports.gameDatabaseFunctions(change.after, context);
        return gameFunctions.updateLeaderboard();
    });

/** Pub/Sub message to work with connected stations that publish when a scan of a tag happens since they
 * do not have access to write directly to the datastore.
 *
 * The message must contain a scan json.
 *
 * {
 *    "timestamp": "2019-03-04T13:37:13-07:00",
 *    "tagId"    : "0475F41AEC4C80"
 * }
 *
 * Only creates a scan leaving the database trigger to do the processing.
 * @type {CloudFunction<Message>}
 */
exports.stationScan = functions.pubsub.topic("station-scan").onPublish((message) => {
    console.log(`message received for topic stations-scans: ${message.data}`);
    if (message.json) {
        const bootstrap = exports.bootstrap(exports.stationScan.name, admin.database().ref());
        const gameFunctions = new GameFirebaseDatabaseFunctions(bootstrap);
        return gameFunctions.scansCreated([message.json]);
    } else {
        return Promise.reject("Message body is not json");
    }
});

/** Pub/Sub message to work with connected school stations that read many visits from the tag
 * and persists the scans for possible reward.
 *
 * The message must contain a json containing an array of scans.
 * [
 *  {
 *    "timestamp": "2019-03-04T13:37:13-07:00",
 *    "tagId"    : "0475F41AEC4C80"
 *  },
 *  {
 *    "timestamp": "2019-03-04T13:37:13-07:00",
 *    "tagId"    : "0475F41AEC4C80"
 *  }
 * ]
 * Only creates a scan leaving the database trigger to do the processing.
 * @type {CloudFunction<Message>}
 */
exports.stationsScans = functions.pubsub.topic("stations-scans").onPublish((message) => {
    console.log(`message received for topic stations-scans: ${message.data}`);
    if (message.json) {
        const bootstrap = exports.bootstrap(exports.stationsScans.name, admin.database().ref());
        const gameFunctions = new GameFirebaseDatabaseFunctions(bootstrap);
        return gameFunctions.scansCreated(message.json);
    } else {
        return Promise.reject("Message body is not json");
    }
});
