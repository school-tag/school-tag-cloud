const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const UserId = require("../../main/user/user").UserId;

//test
const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils.js");

describe("Hitting the database on Sandbox", function () {
    describe("someService", function () {
        it("is used for debugging code", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrapWithDatabaseRules();
            expect(bootstrap).not.to.equal(null);
        });
    });
});
