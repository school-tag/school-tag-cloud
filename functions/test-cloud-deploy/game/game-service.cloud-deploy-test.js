const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));

//test
const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils.js");
const GameTestUtils = require("../../test/game/game-test-utils.js");
const ScanTestUtils = require("../../test/scan/scan-fixtures.js");

describe("GameService in the Sandbox", function () {
    describe("scanReceived", function () {
        it("should award points for every scan received using database rules", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrapWithDatabaseRules();
            return GameTestUtils.everyScanShouldBeRewarded(bootstrap);
        });
    });
    /** https://bitbucket.org/school-tag/school-tag-cloud/issues/4 */
    describe("scanReceived on sunday", function () {
        it("should award points for every scan received using database rules", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrapWithDatabaseRules();
            const scan = ScanTestUtils.scanSunday1();
            return bootstrap.scanService.scanCreated(scan).then((scanWithId) => {
                return bootstrap.gameService.scanReceived(scanWithId).then((rewards) => {
                    return bootstrap.scanService.tagForScan(scanWithId).then((tag) => {
                        return bootstrap.gameService.userRewardsWeekTotals(tag.userId(), scan.timestamp())
                            .then((usersRewards) => {
                                expect(usersRewards.points()).to.be.greaterThan(0);
                            });
                    });
                });
            });

        });
    });
});
