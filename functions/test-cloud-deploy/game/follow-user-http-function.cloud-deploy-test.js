const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const request = require("request-promise-native");
const SystemBootstrap = require("../../main/bootstrap");
const GameFirebaseHttpFunctions = require("../../main/game/game-firebase-http-functions");
const NotifiationRecipient = require("../../main/notification/notification-recipient");
const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils");
const Errors = require("../../main/lang/errors");

describe("followUserHttpFunction", function () {
    it("should return 403  for an invalid token", () => {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        const baseUrl = bootstrap.applicationVersion.functionsUrl();
        const functionName = GameFirebaseHttpFunctions.prototype.followUser.name;
        const followUserUrl = `${baseUrl}/${functionName}`;
        const notificationRecipient = NotifiationRecipient.builder().recipientId("bad-recipient-id").build();

        return bootstrap.userService.createUser().then((user) => {
            const options = {
                method: 'POST',
                uri: followUserUrl,
                body: {
                    following: user.id().toString(),
                    notificationRecipient: notificationRecipient.portable(),
                },
                json: true, // Automatically stringifies the body to JSON
                resolveWithFullResponse: true,
            };
            return request(options).then((response) => {
                return Promise.reject("should be an error");
            }, (error) => {
                expect(Errors.httpStatusCode(error)).to.equal(403);
                return Promise.resolve();
            });
        });
    });
});
