const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const Rule = require("json-rules-engine").Rule;

const appRootDir = require("app-root-dir").get();
const FirebaseCloudTestUtils = require(appRootDir + "/test-cloud/firebase-cloud-test-utils.js");

describe("Rule Repository", function () {
    describe("constructor", function () {
        it("should load rules from the sandbox database", function () {
            //use the root, not the test, so we can get the real rules.
            const bootstrap = FirebaseCloudTestUtils.bootstrapWithDatabaseRules();
            return bootstrap.ruleRepository.rules().then((rules) => {
                expect(rules.length).to.be.greaterThan(0);
                expect(rules[0]).to.be.instanceOf(Rule);
            });
        });
    });
});