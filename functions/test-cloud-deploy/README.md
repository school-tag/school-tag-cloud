## System Testing

Tests that validate cloud connected resources (using firebase), after deployment to the cloud,
to ensure the code deployed to the cloud works as expected.

```
1. run unit tests
2. run integration tests
3. deploy to the cloud
4. test against the cloud
```

These tests can take some time to complete because remote calls are being made.  Timeouts should be set to a few seconds.

These tests are typically run by Bitbucket Pipelines after the deployment phase.  See `pipelines-deploy.sh`.

Read more about [testing levels][1].


[1]: https://en.wikipedia.org/wiki/Software_testing#Testing_levels
