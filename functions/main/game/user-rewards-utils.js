const appRootDir = require("app-root-dir").get();

const UserId = require(appRootDir + "/main/user/user.js").UserId;
const Reward = require(appRootDir + "/main/game/reward.js");
const UserRewards = require(appRootDir + "/main/game/user-rewards.js");
const SchoolGameRules = require("./school-game-rules");

/**
 * static utility methods related to UserRewards
 */
class UserRewardsUtils {

    /**
     *
     * @param {UserRewards[]} userRewardsArray
     * @return {{matchingWeek: (function(number))}}
     */
    static filter(userRewardsArray) {
        return {
            /**Filters the given user rewards array keeping only those matching the
             *  iso week year of the given moment.
             *
             *  The user reward represents a range so the start and end must match the week to be kept.
             *
             * @param {moment.Moment} moment The week number, usually provided from moment().week()
             * @return {UserReward[]} containing only those user rewards that start/end within the week.
             */
            matchingWeek: (moment) => {
                return userRewardsArray.filter((userRewards) => {
                    const momentRange = userRewards.timeInterval();
                    const week = moment.isoWeek();
                    const year = moment.isoWeekYear();
                    return momentRange.start.isoWeek() == week &&
                        momentRange.start.isoWeekYear() == year &&
                        momentRange.end.isoWeek() == week &&
                        momentRange.end.isoWeekYear() == year;
                });
            },

        };
    };

    /**
     * @return {function(UserRewards)} that extracts the points from the userRewards given to it
     */
    static pointsValueFunction() {
        return (userRewards) => {
            return userRewards.points();
        };
    }

    /**
     * Function that extracts the points for a reward from the UserRewards#pointsForRules.
     * Returns the number or undefined if not
     * @param {string} ruleId the potential points were rewarded for
     * @return {function(UserRewards)}
     */
    static pointsForRuleValueFunction(ruleId) {
        return (userRewards) => {
            return userRewards.pointsForRules()[ruleId];
        };
    }

    /** Using the rule id, this produces a message that will communicate the achievement
     * for a user.
     *
     * @param {Reward} reward
     * @param {User} user
     * @return {string} formatted and ready to display in english
     */
    static formatRewardMessage(reward, user) {
        const points = reward.points();
        const nickname = user.hasNickname() ? user.nickname() : "a student";
        //FIXME: switch statement isn't sustainable. template must come from rule lookup.
        let text;
        switch (reward.ruleId()) {
            case SchoolGameRules.everyScanCountsRuleId():
                //FIXME: this text needs to be provided by a localizable library
                text = `${nickname} earned ${points} for tagging`;
                break;
            case SchoolGameRules.morningArrivalOnTimeRuleId():
                text = `${nickname} earned ${points} for arriving on time`;
                break;
            case SchoolGameRules.morningArrivalLateRuleId():
                text = `${nickname} earned ${points} for arriving late`;
                break;
            case SchoolGameRules.morningArrivalEarlyRuleId():
                text = `${nickname} earned ${points} for arriving early`;
                break;
            case SchoolGameRules.arrivedByBike():
                text = `${nickname} earned ${points} for biking`;
                break;
            case SchoolGameRules.scannedWalkStation():
                text = `${nickname} earned ${points} for walking`;
                break;
            case SchoolGameRules.arrivedByBus():
                text = `${nickname} earned ${points} for taking the bus`;
                break;
            case SchoolGameRules.arrivedByFoot():
                text = `${nickname} earned ${points} for walking`;
                break;
            default:
                const ruleId = reward.ruleId();
                text = `${nickname} earned ${points} points for ${ruleId}`;
        }
        return text;
    }
}

module.exports = UserRewardsUtils;
