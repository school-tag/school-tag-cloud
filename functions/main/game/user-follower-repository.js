const Reference = require("firebase").database.Reference;
const DataSnapshot = require("firebase").database.DataSnapshot;
const ThenableReference = require("firebase").database.ThenableReference;
const FirebaseError = require("firebase").FirebaseError;
const FirebaseUtils = require("../firebase/firebase-utils");
const NotificationRecipient = require("../../main/notification/notification-recipient");

/**
 * Persistent access to provide recipients of notifications for user achievements.
 */
class UserFollowerRepository {

    /**
     * @param {Repository} repository
     */
    constructor(repository) {
        this._repository = repository;
    }

    /**
     * @param {UserId} userId
     * @return {Promise<NotificationRecipient[]>} stream of notification recipient ids for contacting the followers.
     */
    recipientsFollowingUser(userId) {
        return this._repository.list(this.followersOfUserPath(userId));
    }


    /**
     * @param {UserId} userId
     * @param {NotificationRecipient} notificationRecipient
     * @return {Promise<void>}
     */
    followUser(userId, notificationRecipient) {
        const path = this.withRecipientPath(userId, notificationRecipient.recipientId());
        return this._repository.save(notificationRecipient, path);
    }

    /**
     *
     * @param {UserId} userId
     * @param {string} recipientId
     * @return {string}
     */
    withRecipientPath(userId, recipientId) {
        return this.followersOfUserPath(userId) + "/" + recipientId;
    }

    /**
     *
     * @param {UserId} userId
     * @return {string}
     */
    followersOfUserPath(userId) {
        return `/followersOfUser/${userId.toString()}/withRecipientId`;
    }
}

module.exports = UserFollowerRepository;