const Entity = require("../../main/entity/entity.js");
const EntityBuilder = require("../../main/entity/entity.js").EntityBuilder;
const UserId = require("../../main/user/user.js").UserId;
const DateUtils = require("../../main/lang/date-utils.js");
const moment = require("moment");
const Reward = require("../../main/game/reward.js");
const UserRewards = require("../../main/game/user-rewards.js");

/**
 * Given all necessary entities, this calculates summaries of rewards
 * given to users and other groups.
 *
 * The base entity is not important, but the fields should be as follows:
 *
 *  - Entity
 *  - - points
 *
 *  Rewards, Totals (From Year, Month, Day, etc) are known to follow such a pattern.
 */
class GameCalculator {

    /**
     * @param {*} properties
     * @private
     */
    constructor(properties) {
        this._props = properties;
        /**@type {Reward[]}*/
        let rewards = this.rewards();
        if (!rewards) {
            rewards = [];
        }
        let userId = null;
        //sum the points from the rewards
        let points = 0;
        let coins = 0;
        let distance = 0;
        const pointsForRules = {};
        rewards.forEach((reward) => {
            points += reward.points();
            if (reward.hasCoins()) {
                coins += reward.coins();
            }
            GameCalculator._pointsForRulesAssigned(reward, pointsForRules);

            if (reward.hasMetersToSchool()) {
                //distance function is assigned by builder and may be sum or max
                distance = properties.calculateDistance(distance, reward.metersToSchool());
            }

            //user id is retrieved from the rewards...which must be from the same user
            if (!userId) {
                userId = reward.userId();
            } else {
                console.assert(userId.equals(reward.userId()));
            }
        });
        this._props[GameCalculator.prototype.points.name] = points;
        this._props[GameCalculator.prototype.coins.name] = coins;
        if (distance > 0) {
            this._props[GameCalculator.prototype.metersToSchool.name] = distance;
        }
        this._props[GameCalculator.prototype.userId.name] = (userId) ? userId.toString() : null;
        this._props[GameCalculator.prototype.pointsForRules.name] = pointsForRules;
    }


    /** @return {Reward[]} */
    rewards() {
        return this._property(GameCalculator.prototype.rewards.name);
    }

    /**
     *
     * @param {Reward|UserRewards} reward providing the points for the rules
     * @param {Object.<string, number>} pointsForRules map of rules to points that is mutated
     * @private
     */
    static _pointsForRulesAssigned(reward, pointsForRules) {
        //UserRewards doesn't have individual rules
        if (reward instanceof Reward) {
            const ruleId = reward.ruleId();
            const points = reward.points();
            GameCalculator._pointsForRuleAssigned(ruleId, points, pointsForRules);
        } else if (reward instanceof UserRewards) {
            GameCalculator._userRewardsPointsForRulesAssigned(reward, pointsForRules);
        }
    }

    /**
     * @param {UserRewards} reward
     * @param {Object.<string, number>} pointsForRules map of rules to points that is mutated
     * @private
     */
    static _userRewardsPointsForRulesAssigned(reward, pointsForRules) {
        //UserRewards has rulesForPoints for a time period
        //add up all for the given time period
        const incomingPointsForRules = reward.pointsForRules();
        if (incomingPointsForRules) {
            Object.keys(incomingPointsForRules).forEach((ruleId) => {
                const points = incomingPointsForRules[ruleId];
                GameCalculator._pointsForRuleAssigned(ruleId, points, pointsForRules);
            });
        }
    }

    /**common method that adds the points to the ruleId in the map.
     * @param {string} ruleId
     * @param {number} points
     * @param  {Object.<string, number>} pointsForRules map of rules to points that is mutated
     * @private
     */
    static _pointsForRuleAssigned(ruleId, points, pointsForRules) {
        let pointsForRule = pointsForRules[ruleId];
        if (!pointsForRule) {
            pointsForRule = 0;
        }
        pointsForRule += points;
        pointsForRules[ruleId] = pointsForRule;
    }

    /**
     *
     * @return {number} total points from all rewards
     */
    points() {
        return this._property(GameCalculator.prototype.points.name);
    }

    /**
     * @return {number} total coins from all rewards. returns zero if no coins.
     */
    coins() {
        return this._property(GameCalculator.prototype.coins.name);
    }

    /**
     * Typically the cumulative distances calculated, but for some calculations this may be the max
     * distance recorded (for example, when looking at all rewards for the day.
     * @return {number} indicating the distance traveled to school
     */
    metersToSchool() {
        return this._property(GameCalculator.prototype.metersToSchool.name);
    }

    /**
     * @return {boolean} true if meters to school is available
     */
    hasMetersToSchool() {
        return this._hasProperty(GameCalculator.prototype.metersToSchool.name);
    }

    /**Provides total points for each rule.
     *
     * @return {Object.<string, number>} map of ruleId: points.  an empty object if nothing achieved
     */
    pointsForRules() {
        return this._property(GameCalculator.prototype.pointsForRules.name);
    }

    /**
     * @return {boolean} true if one or more points for rules exists.
     */
    hasPointsForRules() {
        return Object.keys(this.pointsForRules()).length > 0;
    }

    /**
     * @return {?UserId}
     */
    userId() {
        const property = this._property(GameCalculator.prototype.userId.name);
        return (property) ? new UserId(property) : null;
    }

    /**
     * @return {boolean} true if user id is assigned
     */
    hasUserId() {
        return this._hasProperty(GameCalculator.prototype.userId.name);
    }

    /**
     *
     * @return {boolean} true if at least one reward is given, even if there are no points
     */
    hasRewards() {
        const rewards = this.rewards();
        return rewards != null && rewards.length > 0;
    }

    /**
     * @return {String} showing the properties in JSON form
     */
    toString() {
        return JSON.stringify(this._props);
    }

    /**
     *
     * @param {string} key
     * @return {*}
     * @private
     */
    _property(key) {
        return this._props[key];
    }

    /** @param {string} key
     * @return {boolean} true if the property has been set.*/
    _hasProperty(key) {
        //!! coverts the presence of something to a boolean, I though && would do it but it does not
        return !!this._property(key) && this._props.hasOwnProperty(key);
    }

    /**
     * use this to create the GameCalculator instead of the constructor.
     * @return {GameCalculatorBuilder}
     */
    static builder() {
        return new GameCalculatorBuilder();
    }
}

/**
 *
 */
class GameCalculatorBuilder {

    /**
     * @private
     */
    constructor() {
        this._props = {
            calculateDistance: (distance1, distance2) => distance1 + distance2,
        };
    }

    /**
     * @param {string} key
     * @param {*} value
     * @return {GameCalculatorBuilder}
     * @protected
     */
    _property(key, value) {
        this._props[key] = value;
        return this;
    }

    /**
     *
     * @param {Reward[]|UserRewards[]} rewards
     * @return {GameCalculatorBuilder}
     */
    rewards(rewards) {
        return this._property(GameCalculator.prototype.rewards.name, rewards);

    }

    /**
     * Keep the max distance instead of summing the distances together.  Useful to determine the max
     * distance traveled to school when multiple sensors were reached.
     * @return {GameCalculatorBuilder}
     */
    calculateMaxDistance() {
        return this._property("calculateDistance", (distance1, distance2) => Math.max(distance1, distance2));
    }

    /**
     * Builds the calculator with the properties given.
     * @return {GameCalculator}
     */
    build() {
        return new GameCalculator(this._props);
    }
}

module.exports = GameCalculator;
module.exports.GameCalculatorBuilder = GameCalculatorBuilder;
