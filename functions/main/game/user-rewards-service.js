const ScanService = require("../../main/scan/scan-service");
const UserRewardsRepository = require("../../main/game/user-rewards-repository");
const GameCalculator = require("../../main/game/game-calculator");
const NotificationService = require("../../main/notification/notification-service");
const UserFollowerRepository = require("../../main/game/user-follower-repository");
const UserService = require("../../main/user/user-service");
const Builder = require("../../main/lang/builder");
const UserRewardsUtils = require("../../main/game/user-rewards-utils");
const Message = require("../../main/message/message.js");
const Errors = require("../../main/lang/errors");
const moment = require("moment");

/**
 * Acess to UserRewardsRepository and associated services
 */
class UserRewardsService {

    /**
     *
     * @param {UserRewardsRepository} userRewardsRepository
     * @param {UserFollowerRepository} userFollowerRepository
     * @param {ScanService} scanService
     * @param {NotificationService} notificationService
     * @param {UserService} userService
     */
    constructor(userRewardsRepository, userFollowerRepository, scanService, notificationService, userService) {
        this._scanService = scanService;
        this._userFollowerRepository = userFollowerRepository;
        this._userRewardsRepository = userRewardsRepository;
        this._notificationService = notificationService;
        this._userService = userService;
    }

    /**
     * @param {Scan} scan indicating when the tag was scanned
     * @param {String} ruleId indicating the rule to be counted
     * @return {Promise.<number>} promise of an integer indicating the number of times the rule was given a reward,
     *                  for the user, on the day fo the scan.
     */
    userRuleRewardCountForDayOfScan(scan, ruleId) {
        return this.userIdFromScan(scan).then((userId) => {
            return this._userRewardsRepository.ruleCountForUserOnDayOfScan(userId, scan, ruleId);
        });
    }

    /** Provides the rewards for the given user on the day matching the moment.
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @return {Promise.<UserRewards>} providing the result if found, otherwise an error.
     */
    userRewardsForDay(userId, moment) {
        return this._userRewardsRepository.userRewardsForDay(userId, moment);
    }

    /**
     * @param {Scan} scan identifying the tag which leads to the user.
     * @param {string} ruleId indicating which rule is of interest
     * @return {Promise<number>} total points rewarded all time for the user matching the rule.
     *                           0 return if rule never matched.
     */
    totalPointsForUserFromScanForRule(scan, ruleId) {
        return this.userIdFromScan(scan).then((userId) => {
            return this.totalPointsForUserForRule(userId, ruleId);
        });
    }

    /**
     * @param {UserId} userId
     * @param {string} ruleId
     * @return {Promise<number>} total points rewarded all time for the user matching the rule.
     *                           0 return if rule never matched.
     */
    totalPointsForUserForRule(userId, ruleId) {
        //FIXME:This should be all time, not just this year, but this year is all that is calculated
        return this.userRewardsYearTotals(userId, moment()).then((rewards) => {
            const rewardsForPoints = rewards.pointsForRules();
            let points;
            if (ruleId in rewardsForPoints) {
                points = rewardsForPoints[ruleId];
            } else {
                points = 0;
            }
            return points;
        }).catch(() => {
            return 0;
        });
    }

    /**Finds the user id given the tag and the scan. For backpack and bike tags, the user id is assigned.
     * For station tags, the user id comes from the authenticated user...provided in the scan.
     * For token tags, the user id comes from the
     * @param {Scan} scan
     * @param {Tag} [tag]
     * @return {Promise.<UserId>}
     */
    userIdFromScan(scan, tag) {
        const tagPromise = tag ? Promise.resolve(tag) : this._scanService.tagForScan(scan);
        return tagPromise.then((tag) => {
                if (tag.hasUserId()) {
                    return Promise.resolve(tag.userId());
                } else if (scan.hasUserIdOfRewardRecipient()) {
                    return Promise.resolve(scan.userIdOfRewardRecipient());
                } else if (scan.hasAuthUid()) {
                    return this._userService.userForAuthUid(scan.authUid()).then((user) => {
                        return user.id();
                    });
                } else {
                    const missingId = Errors.of("if tag doesn't have user, then scan must have auth user", "0sx8", 400);
                    return Promise.reject(missingId);
                }
            }
        );

    }

    /**Calculate the year and grand totals and persist them.
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @return {Promise.<GameCalculator>} the calculator used to determine the totals
     */
    updateYearTotals(userId, moment) {

        return this._userRewardsRepository.userRewardsWeeksForYear(userId, moment)
            .then((userRewardsForAllWeeks) => {
                const calculator = GameCalculator.builder().rewards(userRewardsForAllWeeks).build();
                return this._userRewardsRepository
                    .setUserRewardsForYearFromCalculator(userId, moment, calculator)
                    .then(() => {
                        return calculator;
                    });
            });
    }


    /**
     * The totals for the year of the moment given.
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @return {Promise.<UserRewards>}
     */
    userRewardsYearTotals(userId, moment) {
        return this._userRewardsRepository.userRewardsTotalsForYear(userId, moment);
    }

    /**
     *
     * @param {UserId} userId
     * @return {Promise.<UserRewards>}
     */
    userRewardsGrandTotals(userId) {
        return this._userRewardsRepository.userGrandTotals(userId);
    }

    /**
     * All the totals for all users.
     * @return {Promise<UserRewards[]>}
     */
    grandTotalsForAll() {
        return this._userRewardsRepository.grandTotals();
    }

    /** Provides the detailed rewards for the user on the day given in the moment.
     * Each reward is related ot it's origin scan.
     *
     * @param {UserId} userId
     * @param {moment.Moment} momentForDay
     * @return {Promise<Reward[]>} for the user on the given day
     */
    rewardsForUserForDay(userId, momentForDay) {
        const baseRef = this._userRewardsRepository._ref();
        const ref = UserRewardsRepository.userRewardsDayScansRef(userId, momentForDay, baseRef);
        return this._userRewardsRepository._snapshot(ref).then((snapshot) => {
            if (snapshot.exists()) {
                const userRewardsScans = snapshot.val();
                return this._userRewardsRepository.rewardsFromScans(userId, userRewardsScans);
            } else {
                return Promise.resolve([]);
            }
        });
    }

    /**
     *  People wishing to be notified when a user received a reward for achieving a rule.
     *
     * @param {UserId} userId
     * @return {Promise<NotificationRecipient[]>}
     */
    recipientsFollowingUser(userId) {
        return this._userFollowerRepository.recipientsFollowingUser(userId);
    }

    /**
     *
     * @param {UserId} userId being followed
     * @param {NotificationRecipient} notificationRecipient now to notify the recipient (fcm token)
     * @return {Promise<void>}
     */
    followUser(userId, notificationRecipient) {
        return this._userFollowerRepository.followUser(userId, notificationRecipient);
    }

    /**
     * @param {Reward} reward recently achieved
     * @return {Promise<Promise<NotificationResult>[]>}
     */
    notifyFollowersOfReward(reward) {
        const userId = reward.userId();
        const userPromise = this._userService.user(userId);
        const followerPromise = this.recipientsFollowingUser(userId);
        return Promise.all([userPromise, followerPromise])
            .then((values) => {
                const user = values[0];
                const followers = values[1];
                const ruleId = reward.ruleId();
                const messageText = UserRewardsUtils.formatRewardMessage(reward, user);
                const path = `/followingUser/${userId}/aboutRule/${ruleId}`;
                const message = Message.builder().text(messageText).id(path).build();
                return this._notificationService.notifyRecipients(followers, message);
            });
    }

    /**
     * @return {Builder<UserRewardsService>}
     */
    static builder() {
        return new class extends Builder {
            /** @return {UserRewardsService} */
            build() {
                return new UserRewardsService(
                    this.get(UserRewardsRepository),
                    this.get(UserFollowerRepository),
                    this.get(ScanService),
                    this.get(NotificationService),
                    this.get(UserService)
                );
            }
        };
    }
}

module.exports = UserRewardsService;

