const NotificationRecipient = require("../../main/notification/notification-recipient");
const Message = require("../../main/message/message");
const Errors = require("../../main/lang/errors");
const WebResources = require("./game-web-resources");

/**
 * Service dedicated to handling HTTP specifics and calling associated services to handle
 * the application objects. In other words, domain objects are extracted from http request and
 * resulting domain objects are translated into json, status code for the http response.
 *
 * Moving this here keeps the majority of the code out of index.js which is difficult to test and maintain.
 */
class GameFirebaseHttpFunctions {

    /**
     *
     * @param {SystemBootstrap} bootstrap
     */
    constructor(bootstrap) {
        this._bootstrap = bootstrap;
    }

    /**
     * @param {Request} request containing the user to be followed and recipient information
     * @param {Response} response with status and body explaining success/failure
     * @return {Promise<void>} indicating when complete
     */
    followUserHttpFunction(request, response) {
        const recipient = NotificationRecipient.builder(request.body.notificationRecipient)
            .subscribedAtTime(this._bootstrap.momentService.now()).build();
        const userKey = request.body.following;
        return this.followUser(userKey, recipient).then((successMessage) => {
            response.status(200).json(successMessage.portable());
        }, (error) => {
            const status = Errors.hasHttpStatusCode(error) ? Errors.httpStatusCode(error) : 400;
            response.status(status).json(Errors.json(error));
        });
    }

    /**
     *
     * @param {string} followingUserKey either id or nickname
     * @param {NotificationRecipient} recipient
     * @return {Promise<Message>}
     */
    followUser(followingUserKey, recipient) {
        if (followingUserKey) {
            return this._bootstrap.userService.userFromKey(followingUserKey).then((user) => {
                //purposely testing notifications before adding as follower to avoid unauth spammers
                return this.validateRecipientSubscription(recipient, user).then((notificationResult) => {
                    if (notificationResult.isSuccessful()) {
                        return this._bootstrap.userRewardsService.followUser(user.id(), recipient).then(() => {
                            return Message.builder()
                                .text(`Successfully subscribed to ${user.nickname()}.`)
                                .code("cn2d").build();
                        });
                    } else {
                        const notificationError = Errors.of(
                            notificationResult.errorMessage(),
                            notificationResult.errorCode(),
                            403);
                        return Promise.reject(notificationError);
                    }

                });
            });
        } else {
            return Promise.reject(Errors.of("'following' property identifies the user to be followed (nickname or id)",
                "kdn2", 400));
        }
    }

    /** Sends a welcome message an validates the recipient id can be used to notify the recipient.
     *
     * @param {NotificationRecipient} recipient
     * @param {User} user
     * @return {Promise<NotificationResult>}
     */
    validateRecipientSubscription(recipient, user) {
        if (recipient && recipient.hasRecipientId()) {
            const message = this._bootstrap.gameWebResources.messageBuilderForUserNotifications(user)
                .id(`/following/${user.id()}/withRecipientId/${recipient.recipientId()}`)
                //FIXME: i18n
                .text(`Receiving alerts about ${user.nickname()}`).build();
            return this._bootstrap.notificationService.notifyRecipients([recipient], message)
                .then((notificationResults) => {
                    //making assumption there is always at least one
                    return notificationResults[0];
                });
        } else {
            const error = Errors.of("Recipient ID (firebase cloud messaging token) is required. ", "k3dc", 400);
            return Promise.reject(error);
        }
    }

}

module.exports = GameFirebaseHttpFunctions;
