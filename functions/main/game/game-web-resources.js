const Message = require("../../main/message/message");
const ApplicationVersion = require("../../main/system/application-version");

/**
 * Provides references to resources found on the web.
 * Purposely not configurable at runtime, all related resources should be tested and built with a specific version
 * to allow for quality valiation and revertability.
 *
 * Static methods are used since the code must be available at build time.
 *
 * Resources should already be available on the web before deploying server changes relying on such resources.
 */
class GameWebResources {

    /**
     * @param {ApplicationVersion} applicationVersion used to retrieve web url
     */
    constructor(applicationVersion) {
        this._applicationVersion = applicationVersion;
    }

    /**
     * @return {string} the root of the web application
     */
    webApplicationHomeUrl() {
        return this._applicationVersion.webApplicationUrl();
    }

    /**
     * @return {string} a url reference to an icon that is suitable for web push notifications
     */
    notificationIconUrl() {
        return `${this.webApplicationHomeUrl()}/assets/icon/favicon-32x32.png`;
    }

    /**
     * @param {User} user
     * @return {string}
     */
    userPageUrl(user) {
        return `${this.webApplicationHomeUrl()}/users/${user.nickname()}`;
    }

    /**Common properties in messages when notifying about a user.
     *
     * @param {User} user
     * @return {MessageBuilder} new message builder with properties.
     */
    messageBuilderForUserNotifications(user) {
        return Message.builder()
            .title("School Tag")
            .aboutUser(user)
            .moreInfoUrl(this.userPageUrl(user))
            .iconUrl(this.notificationIconUrl());
    }
}

module.exports = GameWebResources;
