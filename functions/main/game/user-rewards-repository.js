const DateUtils = require("../../main/lang/date-utils.js");
const EntityUtils = require("../../main/entity/entity-utils.js");
const FirebaseError = require("firebase").FirebaseError;
const Database = require("firebase").database.Database;
const DataSnapshot = require("firebase").database.DataSnapshot;
const DeltaSnapshot = require("firebase-functions").database.DeltaSnapshot;
const FirebaseUtils = require("../../main/firebase/firebase-utils");
const Moment = require("moment");
const MomentRange = require("moment-range");
const moment = MomentRange.extendMoment(Moment);
const Reference = require("firebase").database.Reference;
const Repository = require("../../main/entity/repository.js");
const RuleRepository = require("../../main/rule/rule-repository.js");
const Scan = require("../../main/scan/scan.js");
const UserId = require("../../main/user/user.js").UserId;
const Reward = require("../../main/game/reward.js");
const ScanRepository = require("../../main/scan/scan-repository.js");
const UserRewards = require("../../main/game/user-rewards.js");
const GameCalculator = require("../../main/game/game-calculator.js");
const Audit = require("../../main/entity/repository.js").Audit;

const _usersPath = "users";
const _dailiesPath = "dailies";
const _yearliesPath = "yearlies";
const _weekliesPath = "weeklies";
const _yearsPath = "years";
const _monthsPath = "months";
const _weeksPath = "weeks";
const _daysPath = "days";
const _totalsPath = "totals";
const _rulesPath = "rules";

/**
 * @extends {Repository.<UserRewards>}
 */
class UserRewardsRepository extends Repository {

    /**
     *
     * @param {Database} database the database that matches the functions of Firebase database.
     * @param {Audit} auditTemplate for tracking modifications
     * @param {MomentService} momentService for timestamps
     * @constructor
     */
    constructor(database, auditTemplate, momentService) {
        super(database, UserRewardsRepository.pathName(), UserRewards, auditTemplate);
        this._momentService = momentService;
    }

    /**
     * @return {string} the root path element for this repository
     */
    static pathName() {
        return "rewards";
    }

    /** Given the reference for a day, this will turn the
     * data into user rewards.
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @return {Promise.<UserRewards>} providing the result if found, otherwise an error.
     */
    userRewardsForDay(userId, moment) {
        const dayRewardRef = this.userDailyTotalsRef(userId, moment);
        return this.entityFromRef(dayRewardRef).then(
            (userRewards) => {
                const builder = userRewards.toBuilder();
                if (!userRewards.hasTimeInterval()) {
                    builder.timeIntervalForDay(moment);
                }
                return builder.build();
            });
    }

    /**
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @return {Promise.<UserRewards>}
     */
    userRewardsTotalsForWeek(userId, moment) {
        const ref = this.userWeeklyTotalsRef(userId, moment);
        return this.entityFromRef(ref);
    }

    /**
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @return {Promise.<UserRewards>}
     */
    userRewardsTotalsForYear(userId, moment) {
        const ref = this.userYearlyTotalsRef(userId, moment);
        return this.entityFromRef(ref);
    }

    /**
     *
     * @param {UserId} userId
     * @return {Promise<UserRewards>}
     */
    userGrandTotals(userId) {
        return this.entityFromRef(this.userGrandTotalsRef(userId));
    }

    /**
     *
     * @return {Promise<UserRewards[]>} user rewards for all users
     */
    grandTotals() {
        //order by child will invoke the list.
        return this.listRef(this.grandTotalsRef());
    }

    /**
     * Provides the rewards for the days of the week for the user.
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @return {Promise.<UserRewards[]>}
     */
    userRewardsDaysForWeek(userId, moment) {
        //get the days for the week by getting the path of the specific day and up one
        const ref = this.userDailyTotalsRef(userId, moment).parent;
        const momentRangeFunction = DateUtils.weekdayMomentRangeFunction(moment);
        return this._snapshot(ref).then((snapshot) => {
            return UserRewardsRepository.userRewardsForPeriod(userId,
                snapshot.val(), momentRangeFunction);
        });
    }

    /** Gets the rewards for all the weeks that fall within a year.
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @return {Promise.<UserRewards[]>}
     */
    userRewardsWeeksForYear(userId, moment) {
        const ref = this.userWeeklyTotalsRef(userId, moment).parent;
        const momentRangeFunction = DateUtils.weekMomentRangeFunction(moment);
        return this._snapshot(ref).then((snapshot) => {
            return UserRewardsRepository.userRewardsForPeriod(userId,
                snapshot.val(), momentRangeFunction);
        });
    }

    /**
     * The place where daily user rewards are stored.
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @param {Reference} ref the base ref prefix the result
     * @return {Reference} at the /years/months/days/users location
     */
    static userDailyTotalsRef(userId, moment, ref) {
        const userRef = UserRewardsRepository.userRef(userId, ref);
        const dailiesRef = userRef.child(_totalsPath).child(_dailiesPath);
        return UserRewardsRepository.dayRef(moment, dailiesRef);
    }

    /**
     * The place where daily user rewards are stored.
     *
     * @see UserRewardsRepository.userDailyTotalsRef
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @return {Reference} at the /totals/dailies/years/months/days/users location
     */
    userDailyTotalsRef(userId, moment) {
        return UserRewardsRepository.userDailyTotalsRef(userId, moment, this._ref());
    }

    /**Reference to a specific month, given in the moment, for the user identified.
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment identifying which month in time
     * @return {Reference} to the user's month matching the moment.year()/moment.month()
     */
    userWeeklyTotalsRef(userId, moment) {
        const userRef = UserRewardsRepository.userRef(userId, this._ref());
        const weekRef = userRef.child(_totalsPath).child(_weekliesPath);
        return UserRewardsRepository.weekRef(moment, weekRef);
    }

    /** the place where yearlies are kept.
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @return {Reference}
     */
    userYearlyTotalsRef(userId, moment) {
        const userRef = UserRewardsRepository.userRef(userId, this._ref());
        const yearliesRef = userRef.child(_totalsPath).child(_yearliesPath);
        return UserRewardsRepository.yearRef(moment, yearliesRef);
    }

    /**
     *
     * @param {UserId} userId
     * @return {Reference} /rewards/grandTotals/${userId}/ -> UserRewards.portable()
     */
    userGrandTotalsRef(userId) {
        return this.grandTotalsRef().child(userId.toString());
    }


    /**
     * @return {string} the database path where the grand total rewards are stored
     */
    static grandTotalsPath() {
        return `${UserRewardsRepository.pathName()}/grandTotals`;
    }

    /**
     * @return {Reference} /rewards/grandTotals
     */
    grandTotalsRef() {
        return this._rootRef.child(UserRewardsRepository.grandTotalsPath());
    }

    /**
     *
     * @param {!UserId} userId
     * @param {!Reference} ref
     * @return {!Reference}
     */
    static userRef(userId, ref) {
        return ref.child(_usersPath).child(userId.toString());
    }

    /**
     *  provides the user id from any reference from the rewards/users path.
     *
     * @param {Reference} anyUserRewardsRef
     * @return {UserId}
     */
    static userIdFromRef(anyUserRewardsRef) {
        let ref = anyUserRewardsRef;
        while (ref.parent != null && ref.parent.key != _usersPath) {
            ref = ref.parent;
        }
        console.assert(ref != null, anyUserRewardsRef.toString()
            + " needs a reference to " + _usersPath);
        return new UserId(ref.key);
    }

    /**
     *
     * @param {!moment.Moment} moment
     * @param {!Reference} ref the path to add a child path
     * @return {Reference} /years/{year}/weeks/{isoWeek}/days/{day}
     */
    static dayRef(moment, ref) {
        ref = this.weekRef(moment, ref);
        ref = ref.child(_daysPath);
        ref = ref.child(moment.isoWeekday());
        return ref;
    }

    /**
     *
     * @param {!moment.Moment} moment
     * @param {!Reference} ref the path to add a child path
     * @return {Reference} /years/{year}/weeks/{week}
     */
    static weekRef(moment, ref) {
        ref = this.yearRef(moment, ref);
        ref = ref.child(_weeksPath);
        ref = ref.child(moment.isoWeek());
        return ref;
    }

    /**
     *
     * @param {!moment.Moment} moment
     * @param {!Reference} ref the path to add a child path
     * @return {Reference} /years/{year}/months/{month}
     */
    static yearRef(moment, ref) {
        ref = ref.child(_yearsPath);
        ref = ref.child(moment.isoWeekYear());
        return ref;
    }

    /**Extracts the moment from the day reference path so the path can be used
     * to provide relevant information about the contents.
     *
     * @param {Reference} reference
     * @return {moment.Moment} the date extracted from the reference.
     */
    static momentFromDayRef(reference) {
        let year;
        let week = 0;
        let day = 0;
        let ref = reference;
        if (ref.parent.key == _daysPath) {
            day = Number.parseInt(ref.key);
            ref = ref.parent.parent;
        }
        if (ref.parent.key == _weeksPath) {
            week = Number.parseInt(ref.key);
            ref = ref.parent.parent;
        }
        console.assert(ref.parent.key == _yearsPath, "didn't find years path where expected "
            + reference.toString());
        year = Number.parseInt(ref.key);
        return moment().isoWeekYear(year).isoWeek(week).isoWeekday(day).hour(0).minute(0).second(0);
    }

    /**Sets the rewards given to the children of the user rewards -> scan.
     *
     * It will clober any existing values in place, assumed the be the same.
     *
     * The user rewards points are not updated and are expected to be done
     * by a follow up counter that will sum up the results once persisted.
     *
     * @param {UserRewards} userRewards
     * @param {Scan} scan
     * @param {Reward[]} rewardsForScan
     * @return {Promise.<void>} indicating failure of saving
     */
    setRewardsForScan(userRewards, scan, rewardsForScan) {
        //the identity of the reward is backed into the path
        //save all the details with the reward for clarity and ease of use
        //TODO: the id should be the path
        const rules = {};
        rewardsForScan.forEach((reward) => {
            rules[reward.ruleId()] = reward.portable();
        });
        const userId = userRewards.userId();
        const rulesRef = this.userRewardsSingleScanRulesRef(userId, scan);
        this._pushAudit(rulesRef, "setRewardsForScan");
        return rulesRef.set(rules);
    }

    /**
     *
     * @param {UserId} userId
     * @param {Scan} scan
     * @return {Reference} for /userId/.../scans/y/m/d/scanId/rules
     */
    userRewardsSingleScanRulesRef(userId, scan) {
        const dayScansRef = this.userRewardsDayScansRef(userId, scan);
        const rulesRef = dayScansRef
            .child(scan.id()).child(RuleRepository.pathName());
        return rulesRef;
    }

    /** Reference to the user's scans for a single day.
     *
     * @param {UserId} userId
     * @param {Scan} scan
     * @return {Reference} for /userId/.../scans/y/m/d
     */
    userRewardsDayScansRef(userId, scan) {
        const moment = scan.timestamp();
        const ref = this._ref();
        return UserRewardsRepository.userRewardsDayScansRef(userId, moment, ref);
    }

    /** Counts the number of rules that match the given rule id, for a user,
     * on the day of the given scan.
     *
     *  /rewards/users/.../scans/years/.../{day}/{scanId}/rules/{ruleId}
     *
     * @param {UserId} userId that made the scan
     * @param {Scan} scan made by the user
     * @param {String} ruleId of the rule of interest
     * @return {Promise.<number>}
     */
    ruleCountForUserOnDayOfScan(userId, scan, ruleId) {

        const scansRef = this.userRewardsDayScansRef(userId, scan);
        //tried scansRef.orderByChild("rules").equalTo(ruleId) without luck
        return this._snapshot(scansRef).then((snapshot) => {
            let count = 0;
            snapshot.forEach((scanSnapshot) => {
                //exclude any counted for the given scan
                const scan = scanSnapshot.val();
                const rules = scan[_rulesPath];
                Object.keys(rules).forEach((ruleIdRewarded) => {
                    if (ruleId == ruleIdRewarded) {
                        count++;
                    }
                });
            });
            return count;
        });
    }

    /**
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @param {Reference} ref the root
     * @return {Reference}
     */
    static userRewardsDayScansRef(userId, moment, ref) {
        //no need to use verbose units since the data is homogeneous
        const scansRef = UserRewardsRepository.userRewardsScansRef(userId, ref);
        const dayScansRef = UserRewardsRepository.dayRef(moment, scansRef);
        return dayScansRef;
    }

    /**
     *
     * @param {UserId} userId
     * @param {Reference} ref the root of the path to be generated
     * @return {Reference} to the /users/{userId}/rewards/scans where scans are organized for a user
     */
    static userRewardsScansRef(userId, ref) {
        return UserRewardsRepository.userRef(userId, ref)
            .child("scans");
    }

    /** Sets the totals from the calculator to the day's totals.
     *
     * @param {UserId} userId
     * @param {moment.Moment} dayMoment the date this calculation represents
     * @param {GameCalculator} gameCalculator
     * @return {Promise.<void>} indicating the success of the save
     */
    setUserRewardsForDayFromCalculator(userId, dayMoment, gameCalculator) {
        const reference = this.userDailyTotalsRef(userId, dayMoment);
        return this.setUserRewardsForPeriodFromCalculator(reference, gameCalculator);
    }

    /**
     * Sets the totals from the calculator to the month's totals.
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @param {GameCalculator} gameCalculator
     * @return {Promise.<void>}
     */
    setUserRewardsForWeekFromCalculator(userId, moment, gameCalculator) {
        const reference = this.userWeeklyTotalsRef(userId, moment);
        return this.setUserRewardsForPeriodFromCalculator(reference, gameCalculator);
    }

    /**
     * persists the yearlies.
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @param {GameCalculator} gameCalculator
     * @return {Promise.<void>}
     */
    setUserRewardsForYearFromCalculator(userId, moment, gameCalculator) {
        const reference = this.userYearlyTotalsRef(userId, moment);
        return this.setUserRewardsForPeriodFromCalculator(reference, gameCalculator).then(() => {
            return this.setUserRewardsGrandTotalsFromCalculator(userId, gameCalculator);
        });
    }

    /**
     * All time totals set from the calculator that determined the rewards.
     *
     * @param {UserId} userId
     * @param {GameCalculator} gameCalculator
     * @return {Promise<void>}
     */
    setUserRewardsGrandTotalsFromCalculator(userId, gameCalculator) {
        //FIXME: Grand Totals needs its own calculation when crossing years
        const grandTotalsRef = this.userGrandTotalsRef(userId);
        return this.setUserRewardsForPeriodFromCalculator(grandTotalsRef, gameCalculator);
    }

    /**
     * Sets the totals from the calculator into the totals path of the time period reference
     * given.  Only saves if user rewards is available, otherwise ignores and returns
     * an immediate promise.
     *
     * @param {Reference} reference to the period where the total rewards are to be stored
     * @param {GameCalculator} gameCalculator
     * @return {Promise.<void>} when saved or failed
     */
    setUserRewardsForPeriodFromCalculator(reference, gameCalculator) {
        let promise;
        if (gameCalculator.hasRewards()) {
            const totals = UserRewardsRepository.totals(gameCalculator);
            promise = reference.update(totals);
            this._pushAudit(reference, "update");
        } else {
            promise = Promise.resolve();
        }
        return promise;
    }

    /**Used to produce a small version of user Rewards.
     *
     * @param {GameCalculator} gameCalculator
     * @return {*} properties prepared for storage of totals related to rewards
     */
    static totals(gameCalculator) {
        const points = gameCalculator.points();
        const coins = gameCalculator.coins();
        const values = {};
        values[Reward.prototype.points.name] = points;
        values[Reward.prototype.coins.name] = coins;
        if (gameCalculator.hasMetersToSchool()) {
            values[Reward.prototype.metersToSchool.name] = gameCalculator.metersToSchool();
        }
        if (gameCalculator.hasUserId()) {
            values[Reward.prototype.userId.name] = gameCalculator.userId().toString();
        }
        if (gameCalculator.hasPointsForRules()) {
            values[UserRewards.prototype.pointsForRules.name] = gameCalculator.pointsForRules();
        }
        return values;
    }

    /** Given the root of the scans of a user reward, this will extract the rewards from the
     * heirarchical structure and return all the rewards within the given object.
     *
     * .../scans/-KoB2kVAewmkMrYz_I-6/rules/everyScanCounts/...
     * @param {UserId} userId
     * @param {*} userRewardsScans
     * @return {Reward[]}
     */
    rewardsFromScans(userId, userRewardsScans) {
        const rewards = [];
        Object.keys(userRewardsScans).forEach((scanKey) => {
            const scan = userRewardsScans[scanKey];
            const rules = scan[RuleRepository.pathName()];
            Object.keys(rules).forEach((ruleId) => {
                const rewardProperties = rules[ruleId];
                const reward = Reward.builder(rewardProperties)
                    .ruleId(ruleId)
                    .scanId(scanKey)
                    .userId(userId)
                    .build();
                rewards.push(reward);
            });
        });
        return rewards;
    }

    /**
     * Converts the given time period key to a moment range
     *
     * @callback momentRangeFunction
     * @param {number} timePeriodKey (year,month,day,week,etc)
     * @return {MomentRange}
     */

    /** Given the
     * @param {UserId} userId
     * @param {*} periodWithChildrenRewards having keys representing the
     *  period of time within the period subsequently having properties matching UserRewards.
     * @param {momentRangeFunction} momentRangeFunction
     * @return {UserRewards[]} the user rewards in the order the keys are provided
     */
    static userRewardsForPeriod(userId, periodWithChildrenRewards, momentRangeFunction) {
        const result = [];
        Object.keys(periodWithChildrenRewards).forEach((timePeriodKey) => {
            const totalsProperties = periodWithChildrenRewards[timePeriodKey];
            const userRewardsBuilder = UserRewards.builder(totalsProperties);
            const momentRange = momentRangeFunction(timePeriodKey);
            userRewardsBuilder.timeInterval(momentRange);
            const userRewards = userRewardsBuilder.userId(userId).build();
            result.push(userRewards);
        });
        return result;
    }

    /**
     * @param {DeltaSnapshot|DataSnapshot} snapshot
     * @return {Reward[]}
     */
    rewardsFromScansSnaphsot(snapshot) {
        const userId = UserRewardsRepository.userIdFromRef(snapshot.ref);
        return this.rewardsFromScans(userId, snapshot.val());

    }

}

module.exports = UserRewardsRepository;
