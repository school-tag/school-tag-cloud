const Operator = require("json-rules-engine").Operator;
const Fact = require("json-rules-engine").Fact;
const Rule = require("json-rules-engine").Rule;
const Almanac = require("json-rules-engine").Almanac;
const Scan = require("../../main/scan/scan");
const MomentRuleOperators = require("../../main/rule/moment-rule-operators.js");
const rules = require("../../data/rules/rules-sandbox.json");

const _scanFactName = "scan";

/**
 * Hard coded rules for the default school commute game.
 * This is useful for testing and setting up a game quickly without relying on external resources.
 */
class SchoolGameRules {
    /**
     *
     * @return {Rule[]} All the rules required for game play.
     */
    static allRules() {
        return [
            SchoolGameRules.everyScanCountsRule(),
            SchoolGameRules.morningArrivalOnTimeRule(),
            SchoolGameRules.morningArrivalLateRule(),
            SchoolGameRules.morningArrivalEarlyRule(),
        ];
    }

    /**
     *
     * @return {int} indicating how many points this rule is worth
     */
    static everyScanCountsPoints() {
        return 1;
    }

    /**
     *
     * @return {number} indicating the number of coins earned with every scan
     */
    static everyScanCountsCoins() {
        return 1;
    }

    /**
     * Provides all operators needed to play the school game.
     *
     * @return {Operator[]}
     */
    static schoolGameOperators() {
        return MomentRuleOperators.all();
    }

    /** This rule is useful for validating the rules engine, however, is likely not to be selected
     * for real game play because scanning multiple times repeatedly should not be rewarded.
     *
     * @return {Rule} the rule that matches the UserRewards daily summary as a fact
     */
    static everyScanCountsRule() {
        return rules[SchoolGameRules.everyScanCountsRuleId()];
    }


    /**
     *
     * @return {string} the constant rule id used for testing
     */
    static everyScanCountsRuleId() {
        return "everyScanCounts";
    }

    /** Rewards the player for the first scan ever.
     * The rule looks at all time points, but to avoid being rewarded twice also
     * ensures only a single scan of the day for the rule since that is more transactional than the
     * delayed calculation of the totals.
     *
     * @return {Rule} for the firstScan
     */
    static firstScanRule() {
        return rules[SchoolGameRules.firstScanRuleId()];
    }

    /**
     * @return {string}
     */
    static firstScanRuleId() {
        return "firstScan";
    }

    /**
     *
     * @return {number} of points rewarded to those arriving on time
     */
    static morningArrivalOnTimePoints() {
        return 10;
    }

    /**
     *
     * @return {number} of points rewarded to those arriving late
     */
    static morningArrivalLatePoints() {
        return 5;
    }

    /**
     *
     * @return {number} of points rewarded to those arriving late
     */
    static morningArrivalEarlyPoints() {
        return 15;
    }

    /**
     *
     * @return {string} identifier for the morning arrival rule
     */
    static morningArrivalOnTimeRuleId() {
        return "morningArrivalOnTime";
    }

    /**
     * @return {string} rule id for biking to school
     */
    static arrivedByBike() {
        return "arrivedByBike";
    }

    /**
     *
     * @return {string} when taking the bus
     */
    static arrivedByBus() {
        return "arrivedByBus";
    }

    /**
     *
     * @return {string} for scanning in for walking
     */
    static arrivedByFoot() {
        return "arrivedByFoot";
    }

    /**
     * @return {string} using the phone to scan a walking station
     */
    static scannedWalkStation() {
        return "scannedWalkStation";
    }

    /**
     *
     * @return {string} morningArrivalEarly
     */
    static morningArrivalEarlyRuleId() {
        return "morningArrivalEarly";
    }

    /**
     * @return {string} id for late arrivals
     */
    static morningArrivalLateRuleId() {
        return "morningArrivalLate";
    }

    /**Points are awarded if the time of the scan happens within the target window.
     * The scan is the driver providing the timestamp, the rules must be met:
     * - within the time interval
     * - applies to the day of the scan
     * - only awarded once for the time interval
     *
     * @return {Rule} that matches if scan arrives within arrival window.
     * */
    static morningArrivalOnTimeRule() {
        return rules[SchoolGameRules.morningArrivalOnTimeRuleId()];
    }

    /**
     * When not arriving on time, but still arriving early enough in the day.
     *
     * https://bitbucket.org/school-tag/school-tag-cloud/issues/7
     * @return {Rule}
     */
    static morningArrivalLateRule() {
        return rules[SchoolGameRules.morningArrivalLateRuleId()];
    }

    /**
     * Bonus points for an early arrival.
     *
     * @return {Rule}
     */
    static morningArrivalEarlyRule() {
        return rules[SchoolGameRules.morningArrivalEarlyRuleId()];
    }

    /** Provides a fact that will count the number of times a rule has been achieved for
     * in the day provided.
     *
     * Requires a scan fact as a parameter and a rule id.
     *
     * {
     *     "fact": "userRuleRewardCountForDayOfScan",
     *     "params": {
     *       "ruleId": "morningArrivalEarly"
     *     },
     *     "operator": "equal",
     *     "value": 0
     * }
     *
     * @param {UserRewardsService} userRewardsService to check for existing rewards
     * @return {Fact} with a promise of a value of the number of times the rule has been achieved during the day of a scan.
     */
    static userRuleRewardCountForDayOfScan(userRewardsService) {
        return new Fact(SchoolGameRules.userRuleRewardCountForDayOfScan.name, (params, almanac) => {
            const ruleId = params.ruleId;
            console.assert(ruleId, "ruleId is a required fact");
            return almanac.factValue(_scanFactName).then((scanProperties) => {
                console.assert(scanProperties, "scan is a required fact");
                const scan = Scan.builder(scanProperties).build();
                return userRewardsService.userRuleRewardCountForDayOfScan(scan, ruleId);
            });
        });
    }

    /** Returns a fact that will produce the total points a user has earned for a rule.
     *
     * @param {UserRewardsService} userRewardsService to check for existing rewards
     * @return {Fact} that will be executed
     */
    static totalPointsForRule(userRewardsService) {
        return new Fact(SchoolGameRules.totalPointsForRule.name, (params, almanac) => {
            const ruleId = params.ruleId;
            console.assert(ruleId, "ruleId is a required fact");
            return almanac.factValue(_scanFactName).then((scanProperties) => {
                console.assert(scanProperties, "scan is a required fact");
                const scan = Scan.builder(scanProperties).build();
                return userRewardsService.totalPointsForUserFromScanForRule(scan, ruleId);
            });
        });
    }

    /** A dynamic fact that requires a scan fact in the almanac, takes in a param of a role
     * and ensures the user represented by scan.authUid is in the role.
     *
     * {
     *     "fact": "authUserHasRole",
     *     "params": {
     *       "role": "schoolStationManager"
     *     },
     *     "operator": "equal",
     *     "value": true
     * }
     *
     * @see Scan.authUid
     *
     * @param {AuthService} authService does role checking
     * @return {Fact} true if the scan.authUser is in the role given
     */
    static authUserHasRole(authService) {
        return new Fact(SchoolGameRules.authUserHasRole.name, (params, almanac) => {
            const role = params.role;
            //error because this is a coding error...add it to the school-game-rules.json
            console.assert(role, "role is a required fact identifying the desired role");
            return almanac.factValue(_scanFactName).then((scanProperties) => {
                console.assert(scanProperties, "scan is a required fact");
                const scan = Scan.builder(scanProperties).build();
                const authUid = scan.authUid();
                return authService.authUserHasRole(authUid, role);
            });
        });
    }

    /**
     * @param {UserRewardsService} userRewardsService
     * @param {AuthService} authService (firebase) to retrieve user information
     * @return {Fact[]} for all facts required to apply the rules of the game
     */
    static allFacts(userRewardsService, authService) {
        return [
            SchoolGameRules.totalPointsForRule(userRewardsService),
            SchoolGameRules.userRuleRewardCountForDayOfScan(userRewardsService),
            SchoolGameRules.authUserHasRole(authService),
        ];
    }

    /**
     *
     * @return {string} a constant backpack
     */
    static tagCategoryBackpack() {
        return "backpack";
    }

    /**
     *
     * @return {string} always bike
     */
    static tagCategoryBike() {
        return "bike";
    }

    /**
     * @return {string} indicates the student walked to school
     */
    static tagCategoryWalk() {
        return "walk";
    }

    /**
     * @return {string} indicates the student took the bus
     */
    static tagCategoryBus() {
        return "bus";
    }

    /**
     *
     * @return {string} indicates this is a portable coin that can will receive rewards at the school station.
     */
    static tagCategoryToken() {
        return "token";
    }

    /**
     * @param {string} tagCategory
     * @return {boolean} true if it matches any known category
     */
    static isValidTagCategory(tagCategory) {

        return tagCategory === SchoolGameRules.tagCategoryBackpack() ||
            tagCategory === SchoolGameRules.tagCategoryToken() ||
            tagCategory === SchoolGameRules.tagCategoryBike() ||
            tagCategory === SchoolGameRules.tagCategoryWalk() ||
            tagCategory === SchoolGameRules.tagCategoryStation() ||
            tagCategory === SchoolGameRules.tagCategoryBus();
    }

    /**
     * @return {string} a tag located in the community not owned by a single player
     */
    static tagCategoryStation() {
        return "station";
    }

    /**
     * Clipper transit cards can be used with the school tag station app. Only the id is read, no url is encoded.
     * Category = backpack.
     * @return {string}
     */
    static tagTypeClipper() {
        return "clipper";
    }

    /**
     *
     * @return {string} "tag" indicating the typical NFC sticker, hang tag, key fob, etc
     */
    static tagTypeTag() {
        return "tag";
    }

    /**
     *
     * @return {string} "qrc" indicating it is synonymous to a 'tag', but uses visual QR Code scanning
     */
    static tagTypeQuickReadCode() {
        return "qrc";
    }

    /**
     *
     * @param {string} tagType
     * @return {boolean} true if the tag is one of the known types
     */
    static isValidTagType(tagType) {
        return tagType === SchoolGameRules.tagTypeTag() ||
            tagType === SchoolGameRules.tagTypeQuickReadCode() ||
            tagType === SchoolGameRules.tagTypeClipper();
    }

    /**
     *
     * @return {number} points for walk and bike
     */
    static pointsForGreenTransport() {
        return 20;
    }

    /**
     * Bonus rewards are given by adults to encourage desired behavior.
     * @return {number}
     */
    static pointsForBonusRewards() {
        return 10;
    }

    /**
     * @see functions/config/station-auth.js
     *
     * @return {string} of the name of the role that indicates the user is the dedicated hardware
     * for tagging in at school.
     */
    static schoolStationManagerRoleName() {
        return "schoolStationManager";
    }

    /**
     * @return {string} station.type = "arrival" indicating the student has arrived at school.
     */
    static stationTypeArrival() {
        return "arrival";
    }
}

module.exports = SchoolGameRules;
