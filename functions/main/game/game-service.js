const DateUtils = require("../../main/lang/date-utils.js");
const Scan = require("../../main/scan/scan.js");
const UserId = require("../../main/user/user.js").UserId;
const ScanService = require("../../main/scan/scan-service.js");
const StationService = require("../../main/station/station.service");
const Station = require("../../main/station/station");
const GameCalculator = require("../../main/game/game-calculator.js");
const SchoolGameRules = require("../../main/game/school-game-rules");
const Tag = require("../../main/tag/tag.js");
const TagService = require("../../main/tag/tag-service.js");
const UserRewards = require("../../main/game/user-rewards.js");
const Reward = require("../../main/game/reward.js");
const UserRewardsRepository = require("../../main/game/user-rewards-repository.js");
const UserRewardsService = require("../../main/game/user-rewards-service.js");
const RuleService = require("../../main/rule/rule-service.js");
const Message = require("../../main/message/message.js");
const MessageService = require("../../main/message/message-service.js");
const UserRewardsUtils = require("../../main/game/user-rewards-utils");
const Errors = require("../../main/lang/errors");

/** Manages who gets rewards and when.
 *
 */
class GameService {
    /**
     * @param {UserService} userService
     * @param {RuleService} ruleService
     * @param {ScanService} scanService
     * @param {MessageService} messageService for status updates
     * @param {UserRewardsService} userRewardsService for access and manipulation of rewards
     * @param {MomentService} momentService to create timestamps
     * @param {AuthService} authService for authorization checks
     * @param {StationService} stationService for retrieving station details used with rules
     */
    constructor(userService,
                ruleService,
                scanService,
                messageService,
                userRewardsService,
                momentService,
                authService,
                stationService) {
        this._userService = userService;
        this._ruleService = ruleService;
        this._scanService = scanService;
        this._messageService = messageService;
        this._momentService = momentService;
        this._authService = authService;
        this._stationService = stationService;
        if (userRewardsService) {
            /** @deprecated use service */
            this._userRewardsRepository = userRewardsService._userRewardsRepository;
        }
        this._userRewardsService = userRewardsService;
    }

    /**
     * Provides a simply way for clients to notify of a recent scan of a tag.
     * When a scan is received, the game rules are applied to the scan to see if any rules
     * are satisfied and rewards given.
     *
     * The service waits until all necessary information is gathered by the ScanService:
     * - The user represented by the tag that was scanned
     * - The location of the scanner used
     * - The user that made the scan, if any
     *
     *
     * @param {Scan} scan  a scan that recently arrived in persistence
     * @return {Promise.<Reward[]>} the rewards granted for the scan
     */
    scanReceived(scan) {
        const options = {};
        GameService.tagOwnerOption(scan, options);
        GameService.userCreateOption(scan, options);
        GameService.tagCategoryOption(scan, options);
        GameService.tagTypeOption(scan, options);
        return this.authorizedToCreateOption(scan, options)
            .then(() => {
                return this._scanService.tagForScan(scan, options).then((tag) => {
                    return tag;
                }).then((tag) => {
                    return this._userRewardsService.userIdFromScan(scan, tag).then((userId) => {
                        return userId;
                    }).then((userId) => {
                        //update the time with local time zone
                        const timestamp = this._momentService.toLocal(scan.timestamp());
                        scan = scan.toBuilder().timestamp(timestamp).build();
                        const timeInterval = DateUtils.dayMomentRange(timestamp);
                        const userRewards = UserRewards.builder()
                            .timeInterval(timeInterval)
                            .userId(userId).build();
                        return this.userRewardsForScanAndTag(scan, tag, userRewards).then((rewardsForScan) => {
                            return rewardsForScan;
                        }).then((rewardsForScan) => {
                            return this.sendMessageForRewards(rewardsForScan).then(() => {
                                //no need to return messages.  Interested parties should be triggered
                                return rewardsForScan;
                            });
                        });
                    });
                });
            });
    };


    /**
     * Assigns the tag owner id to the options, if provided, to be used when creating a tag it
     * will associate the new tag to the given user.
     *
     * This does not validate the user exists, which should be done if the tag is being created.
     * @param {Scan} scan
     * @param {object} options
     */
    static tagOwnerOption(scan, options) {
        if (scan.hasUserIdOfTagOwner()) {
            TagService.associateTagToUser(options, scan.userIdOfTagOwner());
        }
    }

    /**
     *  Determines if a user should be created, if authorized to do so, based on the scan.
     *  Token and station tags shouldn't create users.
     *
     * @param {Scan} scan
     * @param {*} options
     */
    static userCreateOption(scan, options) {
        const tagCategory = scan.tagCategory();
        //token is the only type of tag where a new user shouldn't be created and none will be associated
        //station is also, but not yet handled
        if (SchoolGameRules.tagCategoryToken() === tagCategory) {
            TagService.associatedUserShouldNotBeCreated(options, true);
        }
    }

    /**
     * assigns the appropriate category to be used when creating a tag.
     *
     * @param {Scan} scan
     * @param {object} options
     * @throws {Error} when category is not valid
     */
    static tagCategoryOption(scan, options) {
        let tagCategory = SchoolGameRules.tagCategoryBackpack();
        if (scan.hasTagCategory()) {
            tagCategory = scan.tagCategory();
            console.assert(SchoolGameRules.isValidTagCategory(tagCategory),
                `scan ${scan.id()} provided a bad tag category ${tagCategory}`);
        }
        TagService.categoryOption(options, tagCategory);
    }

    /**Assigns the appropriate tag type to the option to be used when tags are created
     *
     * @param {Scan} scan
     * @param {object} options
     */
    static tagTypeOption(scan, options) {
        let tagType = SchoolGameRules.tagTypeTag();
        if (scan.hasTagType()) {
            tagType = scan.tagType();
            console.assert(SchoolGameRules.isValidTagType(tagType),
                `scan ${scan.id()} provided a bad tag type ${tagType}: kk2L`);
        }
        TagService.typeOption(options, tagType);
    }

    /**
     * @param {Scan} scan coming in
     * @param {*} options to be modified with the authorization
     * @return {Promise.<string>} promise of the key of the option when the mutation has happened
     */
    authorizedToCreateOption(scan, options) {
        return this._authService.authUserHasRole(scan.authUid(), SchoolGameRules.schoolStationManagerRoleName())
            .then((authorized) => {
                return TagService.authorizedToCreateOption(options, authorized);
            });
    }

    /** Given the rewards for a single user, this will create messages
     * appropriate for the rewards and post them to the stream for followers to see.
     *
     * @param {Reward[]} rewardsForScan assumed to be coming from the same user
     * @return {Promise.<Promise.<Message>[]>} containing promises of the messages
     */
    sendMessageForRewards(rewardsForScan) {
        const messagePromises = [];
        if (rewardsForScan && rewardsForScan.length > 0) {
            //assuming all rewards belong to same user
            const userId = rewardsForScan[0].userId();
            return this._userService.user(userId)
                .then((user) => {
                    rewardsForScan.forEach((reward) => {
                        const text = UserRewardsUtils.formatRewardMessage(reward, user);
                        const message = Message.builder()
                            .timestamp(reward.timeReceived())
                            .text(text)
                            .aboutUser(user)
                            .aboutPayload(reward)
                            .build();
                        messagePromises.push(this._messageService.send(message));
                    });
                    return messagePromises;
                });
        }
        return Promise.resolve(messagePromises);
    }

    /** Persists the rewards for a scan by finding the tag
     *
     * @param {Scan} scan
     * @param {UserRewards} userRewards
     * @return {Promise.<Reward[]>}
     */
    applyRewardsForScan(scan, userRewards) {
        return this._scanService.tagForScan(scan).then((tag) => {
            return this.userRewardsForScanAndTag(scan, tag, userRewards);
        }, () => {
            //maybe tag just got created...let's create a stub no problem
            const tag = Tag.builder().id(scan.tagId()).build();
            return this.userRewardsForScanAndTag(scan, tag, userRewards);
        });
    }

    /** Given the facts of a single {Scan} this will apply each {Rule} and report achievements
     * by returning the itemized Reward[].
     *
     * If the scan has already been rewarded, as reported by the UserRewards,
     * the resulting rules will be empty.
     *
     *
     * @param {Scan} scan  the scan just received that has not yet been rewarded.
     * @param {Tag} tag associated with the scan
     * @return {Promise.<Reward[]>} containing the details of
     *               how the given scan will affect the game
     *
     * @throws {Assert.AssertionError} when the given scan's day
     *         does not equal the given summary's time component.
     */
    rewardsForScan(scan, tag) {
        const facts = {
            scan: scan.portable(),
            tag: tag.portable(),
        };
        let stationPromise;
        if (scan.hasStation()) {
            //a station id in a scan, but not found, will end in rejection since the system is not setup correctly
            stationPromise = this._stationService.stationById(scan.station());
        } else {
            //an empty station will resolve facts looking for station, but match nothing
            stationPromise = Promise.resolve(Station.builder().build());
        }
        return stationPromise.then((station) => {
            facts.station = station.portable();
            return this._userRewardsService.userIdFromScan(scan, tag).then((userId) => {
                return this._ruleService.run(facts).then((events) => {
                    const rewards = [];
                    events.forEach((event) => {
                        const reward = Reward.builder()
                            .ruleId(event.type)
                            .points(GameService.pointsFromTagOrDefault(tag, event.params.points))
                            .coins(event.params.coins)
                            .metersToSchool(station.metersToSchool())
                            .userId(userId)
                            .scanId(scan.id())
                            .timeReceived(this._momentService.now())
                            .build();
                        // todo:handle if the recipient is the scanner.
                        rewards.push(reward);
                    });
                    return rewards;
                });
            });
        });
    };

    /**
     *
     * @param {Tag} tag that may provide the points
     * @param {number} [defaultPoints] if the tag doesn't provide the points
     * @return {number|undefined}
     */
    static pointsFromTagOrDefault(tag, defaultPoints) {
        return (tag && tag.hasPoints()) ? tag.points() : defaultPoints;
    }

    /**
     * Determines the rewards when the tag and scan are both provided and persists the results.
     *
     * @param {Scan} scan
     * @param {Tag} tag
     * @param {UserRewards} userRewards
     * @return {Promise.<Reward[]>}
     */
    userRewardsForScanAndTag(scan, tag, userRewards) {
        return this.rewardsForScan(scan, tag).then(
            (rewardsForScan) => {
                // saves the given new rewards related to the scan
                return this._userRewardsRepository
                    .setRewardsForScan(userRewards, scan, rewardsForScan).then(() => {
                        return rewardsForScan;
                    });
            }
        );
    }

    /** Given the rewards from the daily summary, this will summarize the statistics for the day
     * and persist.
     * @param {UserId} userId
     * @param {moment.Moment} dayMoment the date of the rewards
     * @param {Reward[]} rewards
     * @return {Promise.<GameCalculator>}
     */
    updateSummaryFromRewards(userId, dayMoment, rewards) {
        //max distance rewarded for the day should be kept, not the sum of all stations reached
        const calculator = GameCalculator.builder().rewards(rewards).calculateMaxDistance().build();
        if (calculator.hasRewards()) {
            return this._userRewardsRepository.setUserRewardsForDayFromCalculator(
                userId, dayMoment, calculator).then(() => {
                return calculator;
            });
        } else {
            return Promise.resolve(calculator);
        }
    }

    /**
     * The totals for the user during the given week.
     *
     * @param {UserId} userId
     * @param {moment.Moment} moment
     * @return {Promise.<UserRewards>}
     */
    userRewardsWeekTotals(userId, moment) {
        return this._userRewardsRepository.userRewardsTotalsForWeek(userId, moment);
    }

    /**
     * @param {UserId} userId
     * @param {moment.Moment} moment for the month to be updated
     * @return {Promise.<GameCalculator>}
     *
     */
    updateWeekTotals(userId, moment) {

        return this._userRewardsRepository.userRewardsDaysForWeek(userId, moment)
            .then((userRewardsForAllDays) => {
                const calculator = GameCalculator.builder().rewards(userRewardsForAllDays).build();
                return this._userRewardsRepository
                    .setUserRewardsForWeekFromCalculator(userId, moment, calculator)
                    .then(() => {
                        return calculator;
                    });
            });
    }

    /**
     * @return {GameServiceBuilder}
     */
    static builder() {
        return new GameServiceBuilder();
    }
}

/**
 *
 */
class GameServiceBuilder {
    /**
     *
     * @param {UserService} userService
     * @return {GameServiceBuilder}
     */
    userService(userService) {
        this._userService = userService;
        return this;
    }

    /**
     *
     * @param {RuleService} ruleService
     * @return {GameServiceBuilder}
     */
    ruleService(ruleService) {
        this._ruleService = ruleService;
        return this;
    }

    /**
     *
     * @param {ScanService} scanService
     * @return {GameServiceBuilder}
     */
    scanService(scanService) {
        this._scanService = scanService;
        return this;
    }

    /**
     *
     * @param {MomentService} momentService
     * @return {GameServiceBuilder}
     */
    momentService(momentService) {
        this._momentService = momentService;
        return this;
    }

    /**
     *
     * @param {MessageService} messageService
     * @return {GameServiceBuilder}
     */
    messageService(messageService) {
        this._messageService = messageService;
        return this;
    }

    /**
     * @param {UserRewardsService} userRewardsService
     * @return {GameServiceBuilder}
     */
    userRewardsService(userRewardsService) {
        this._userRewardsService = userRewardsService;
        return this;
    }

    /**
     * @param {AuthService} authService
     * @return {GameServiceBuilder}
     */
    authService(authService) {
        this._authService = authService;
        return this;
    }

    /**
     *
     * @param {StationService} stationService
     * @return {GameServiceBuilder}
     */
    stationService(stationService) {
        this._stationService = stationService;
        return this;
    }

    /**
     * @return {GameService}
     */
    build() {
        return new GameService(
            this._userService,
            this._ruleService,
            this._scanService,
            this._messageService,
            this._userRewardsService,
            this._momentService,
            this._authService,
            this._stationService);
    }
}

module.exports = GameService;
