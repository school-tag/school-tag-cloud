const Moment = require("moment");
const MomentRange = require("moment-range");
const moment = MomentRange.extendMoment(Moment);

const appRootDir = require("app-root-dir").get();
const Entity = require(appRootDir + "/main/entity/entity.js");
const Reward = require(appRootDir + "/main/game/reward.js");
const DateUtils = require(appRootDir + "/main/lang/date-utils.js");
const UserId = require(appRootDir + "/main/user/user.js").UserId;

/** The summary of points, coins, badges awarded during a specific time period.
 * The time period is identified by the time interval and its user, however, its ID is
 * the most specific time component it can provide.
 *
 * Yearly - year
 * Monthly - monthOfYear
 * Daily - dayOfMonth
 *
 * So the id shouldn't be mingling with other entities out of its time scope
 * without considering the time interval and user id.
 *
 * The actions that provided rewards (scans) are provided only for the most specific
 * time interval the game is tracking (daily).  Other rewards are just summaries
 * that should be the cumulation of all more specific rewards.
 *
 * Scans are associated, via {Reward}, to ensure a player is rewarded only once for an action.
 *
 * @extends {Entity.<UserRewards>}
 */
class UserRewards extends Entity {
    /**
     * @param {UserRewards|*|null} [properties]
     */
    constructor(properties) {
        super(properties);
    }

    /**
     * @return {MomentRange}
     */
    timeInterval() {
        return DateUtils.momentRange(this._timeIntervalProperty());
    }

    /** provides a unique identifier by concatenating user id, if available, and the time interval.
     *
     * @override
     */
    id() {
        return ((this.hasUserId()) ? this.userId() + "-" : "") + this._timeIntervalProperty();
    }

    /**
     * the raw form of time interval
     * @return {string|null}
     * @private
     */
    _timeIntervalProperty() {
        return this._property(UserRewards.prototype.timeInterval.name);
    }

    /** Total points earned during the time interval.
     * The sum of points found in all rewards.
     * @return {int}
     */
    points() {
        //purposely calling the reward interface
        return this._property(Reward.prototype.points.name);
    }

    /**
     *
     * @return {number} total number of coins
     */
    coins() {
        //purposely calling the reward interface
        return this._property(Reward.prototype.coins.name);
    }

    /**
     * @return {boolean} if coins are available...since they may not be provided
     */
    hasCoins() {
        return this._hasProperty(Reward.prototype.coins.name);
    }

    /**
     * @return {number} distance away along the safe route to school
     */
    metersToSchool() {
        return this._property(Reward.prototype.metersToSchool.name);
    }

    /**
     * @return {boolean} true if distance is available..even if zero.
     */
    hasMetersToSchool() {
        return this._hasProperty(Reward.prototype.metersToSchool.name);
    }

    /**Provides total points for each rule.
     *
     * @return {Object.<string, number>} map of ruleId: points
     */
    pointsForRules() {
        return this._property(UserRewards.prototype.pointsForRules.name);
    }

    /**
     *
     * @return {boolean} if user id is set
     */
    hasUserId() {
        return this._hasProperty(UserRewards.prototype.userId.name);
    }

    /**
     *
     * @return {boolean} if time interval is available
     */
    hasTimeInterval() {
        return this._hasProperty(UserRewards.prototype.timeInterval.name);
    }

    /**The user who earned the rewards. If null, then this represents all users.
     *
     * @return {UserId|null}
     */
    userId() {
        if (this.hasUserId()) {
            return new UserId(this._property(UserRewards.prototype.userId.name));
        } else {
            return null;
        }
    }

    /**
     * @param {UserRewards|*|null} [userRewards]
     * @return {UserRewardsBuilder}
     */
    static builder(userRewards) {
        if (!(userRewards instanceof UserRewards)) {
            userRewards = new UserRewards(userRewards);
        }
        return new UserRewardsBuilder(userRewards);
    }
}

// ============== Builder ==============
/**
 * @extends {Entity.<UserRewards>}
 */
class UserRewardsBuilder extends Entity.EntityBuilder {
    /**
     *
     * @param {UserRewards} mutant
     */
    constructor(mutant) {
        super(mutant);
    }

    /**
     *
     * @param {MomentRange|string} momentRangeOrTimeInterval  start to end for this set of rewards
     * @return {UserRewardsBuilder}
     */
    timeInterval(momentRangeOrTimeInterval) {
        const interval = momentRangeOrTimeInterval.toString();
        this._property(UserRewards.prototype.timeInterval.name, interval);
        return this;
    };

    /**
     * Given a single moment, this will set the time interval for the day.
     *
     * @param {moment.Moment} moment
     * @return {UserRewardsBuilder}
     */
    timeIntervalForDay(moment) {
        this.timeInterval(DateUtils.dayMomentRange(moment));
        return this;
    }

    /**Provides total points for each rule.
     *
     * @param {Object.<string, number>} pointsMap of ruleId: points
     * @return {UserRewardsBuilder}
     */
    pointsForRules(pointsMap) {
        return this._property(UserRewards.prototype.pointsForRules.name, pointsMap);
    }

    /**
     * @param {string|UserId} userId
     * @return {UserRewardsBuilder}
     */
    userId(userId) {
        if (userId != null) {
            this._property(UserRewards.prototype.userId.name, userId.toString());
        }
        return this;
    }

    /**
     * @param {number} points
     * @return {UserRewardsBuilder}
     */
    points(points) {
        this._property(Reward.prototype.points.name, points);
        return this;
    }

    /**
     * @param {number} coins given for this reward
     * @return {UserRewardsBuilder}
     */
    coins(coins) {
        if (coins || coins === 0) {
            this._property(Reward.prototype.coins.name, coins);
        }
        return this;
    }

    /**
     * @param {number} meters
     * @return {UserRewardsBuilder}
     */
    metersToSchool(meters) {
        return this._property(Reward.prototype.metersToSchool.name, meters);
    }

    /**
     * @return {UserRewards}
     */
    build() {
        return super.build();
    }
}

module.exports = UserRewards;

