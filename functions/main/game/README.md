# Gamifying School Commutes

The game motivates children to walk/bike/bus to school 
giving rewards (points, badges, coins) when game rules 
are achieved.  The rules are flexible, so a simple one is 
given for an example.

* Check in on time for school

Near Field Communications (NFC) tags are used by the children
to identify themselves at locations.  The scan triggers game
rules and if a rule is achieved then a reward is given
and the child's summaries are updated.

1. Student [scans](../scan) NFC sticker([tag](../tag)) at phone at school
1. Firebase database records the scan in the cloud
1. Firebase function triggers the scan to be handled by the GameService
1. [GameService](game-service.js) uses [RuleService](../rule/rule-service.js) to determine which rules passed
1. [Rewards](reward.js) are given accordingly and stored by [RewardRepository](reward-repository.js) as a [UserRewards](user-rewards.js)
   1. Scan is record with UserRewards for a record of processing
   1. UserRewards represents a time span (daily record in this case)
   1. Time Interval indicates the time range represented by the UserRewards
1. Firebase functions are triggered by the UserRewards being updated will trigger other UserRewards summaries to be updated
1. User's points are updated to all clients listening

