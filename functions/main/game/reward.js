const appRootDir = require("app-root-dir").get();
const console = require("console");
const Entity = require(appRootDir + "/main/entity/entity.js");
const UserId = require(appRootDir + "/main/user/user.js").UserId;
const DateUtils = require(appRootDir + "/main/lang/date-utils.js");
const moment = require("moment");

/**
 *
 * The incentives for any award given for a single worthy action.
 *
 *  Points - Culating points provides ranking for leaderboards
 *  Badges - Awards for specific achievements to wear with pride
 *  Coins - Virtual currency used to purchase game components or real world items
 *
 *  The reward identifies which action (scan) met which rule to achieve this reward.
 *  It also identifies who was given the reward and when.
 *
 * @extends {Entity.<Reward>}
 */
class Reward extends Entity {
    /**
     * @param  {*|null} [properties]  of the portable properties
     * @constructor
     */
    constructor(properties) {
        super(properties);
    }

    /**
     * @param {Reward|Object|*} [reward]
     * @return {RewardBuilder}
     */
    static builder(reward) {
        if (!(reward instanceof Reward)) {
            reward = new Reward(reward);
        }
        return new RewardBuilder(reward);
    };

    /**
     *
     * @return {UserId} identifies who received this reward
     */
    userId() {
        return new UserId(this._property(Reward.prototype.userId.name));
    };

    /** Identifies the entity that recorded the action taken to achieve this reward.
     * The scan indirectly relates to the users related, when, where, etc.
     *
     * @return {string} indicating which action was taken to achieve this reward.
     *
     */
    scanId() {
        return this._property(Reward.prototype.scanId.name);
    };

    /**
     * @return {string} identifies which rule was met to achieve this reward
     */
    ruleId() {
        return this._property(Reward.prototype.ruleId.name);
    };

    /**
     * @return {number} the number of points rewarded for the achievement identified by ruleId
     */
    points() {
        return this._property(Reward.prototype.points.name);
    };

    /**
     * @return {number} of coins earned with this reward.
     */
    coins() {
        return this._property(Reward.prototype.coins.name);
    }

    /**
     * @return {boolean} true if coins are available..even if zero.
     */
    hasCoins() {
        return this._hasProperty(Reward.prototype.coins.name);
    }

    /**
     * @return {number} distance away along the safe route to school
     */
    metersToSchool() {
        return this._property(Reward.prototype.metersToSchool.name);
    }

    /**
     * @return {boolean} true if distance is available..even if zero.
     */
    hasMetersToSchool() {
        return this._hasProperty(Reward.prototype.metersToSchool.name);
    }

    /**The moment the reward was given to the recipient.
     *
     * @return {moment.Moment}
     */
    timeReceived() {
        return DateUtils.timestampMoment(this._props[Reward.prototype.timeReceived.name]);
    }

}

// =============== Builder ================================

/**
 *  @extends {EntityBuilder.<Reward>}
 */
class RewardBuilder extends Entity.EntityBuilder {
    /**
     *
     * @param {Reward|*|null} [mutant]
     * @constructor
     */
    constructor(mutant) {
        super(mutant);
    }

    /**
     * @param {string} scanId
     * @return {RewardBuilder}
     */
    scanId(scanId) {
        return this._property(Reward.prototype.scanId.name, scanId);
    };

    /**
     *
     * @param {UserId|string} userId
     * @return {RewardBuilder}
     */
    userId(userId) {
        return this._property(Reward.prototype.userId.name, userId.toString());
    };

    /**
     *
     * @param {string} ruleId
     * @return {RewardBuilder}
     */
    ruleId(ruleId) {
        return this._property(Reward.prototype.ruleId.name, ruleId);
    };

    /**
     *
     * @param {int} points
     * @return {RewardBuilder}
     */
    points(points) {
        return this._property(Reward.prototype.points.name, points);
    };

    /**
     *
     * @param {number} count the number of coins earned with this reward.
     * @return {RewardBuilder}
     */
    coins(count) {
        if (count || count === 0) {
            this._property(Reward.prototype.coins.name, count);
        }
        return this;
    }

    /**
     * @param {number} meters
     * @return {RewardBuilder}
     */
    metersToSchool(meters) {
        return this._property(Reward.prototype.metersToSchool.name, meters);
    };


    /**
     * @param {string|moment.Moment|Date} momentOrString
     * @return {RewardBuilder}
     */
    timeReceived(momentOrString) {
        const portable = DateUtils.timestampMomentPortable(momentOrString);
        return this._property(Reward.prototype.timeReceived.name, portable);
    }

    /**
     * @override
     * @private
     */
    _validate() {
        console.assert(this._entity._hasProperty(RewardBuilder.prototype.userId.name));
        console.assert(this._entity._hasProperty(RewardBuilder.prototype.ruleId.name));
        console.assert(this._entity._hasProperty(RewardBuilder.prototype.scanId.name));
        console.assert(this._entity._hasProperty(RewardBuilder.prototype.points.name));
    }

    /**
     * @override
     * @private
     */
    _preBuild() {
        this.id(this._entity.userId() + "-" + this._entity.scanId() + "-" + this._entity.ruleId());
    }

    /**
     * @return {Reward}
     */
    build() {
        return super.build();
    }
}

module.exports = Reward;
module.exports.RewardBuilder = RewardBuilder;


