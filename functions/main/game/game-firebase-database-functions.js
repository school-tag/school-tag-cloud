const FirebaseUtils = require("../../main/firebase/firebase-utils.js");
const UserRewardsRepository = require("../../main/game/user-rewards-repository.js");
const UserId = require("../../main/user/user.js").UserId;
const Reward = require("../../main/game/reward.js");
const SchoolGameRules = require("../../main/game/school-game-rules");
const env = require("../../generated/application-env");
const Scan = require("../../main/scan/scan");
const GameCalculator = require("../../main/game/game-calculator");

/**
 * stateless service providing Firebase functions that are triggered by database events
 * for game play.
 *
 * Since these are actually database triggers, there is direct interaction
 * between the repository that specializes in dealing with database data and services
 * which apply business logic. This avoids having the repository depend on the service and
 * avoids the service dealing with database details directly.
 *
 * Hence the use of private repository functions.
 *
 * https://firebase.google.com/docs/functions/database-events
 */
class GameFirebaseDatabaseFunctions {

    /**
     * @param {SystemBootstrap} bootstrap
     */
    constructor(bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     *
     * @see scansCreated
     * @param {DataSnapshot|DeltaSnapshot} snapshot
     * @return {Promise.<Reward[]>}
     */
    scanReceived(snapshot) {
        //the repository knows how to handle snapshots best
        const scan = this.bootstrap.scanRepository.entityFromSnapshot(snapshot);
        //see scansCreated which assigns batch ids and creates the whole chain to avoid race conditions.
        if (scan.hasBatchId()) {
            return Promise.resolve([]);
        }
        return this.bootstrap.gameService.scanReceived(scan).then((rewards) => {
                console.info(rewards.length + " rewards achieved");
                return rewards;
            }
        );
    };

    /**
     * When a scan object is received via a message, rather than writing directly to the datastore.
     * Persists the scan, but processing goes through the standard trigger "oncreate".
     * This scan is authorized as a station manager with all rights so use accordingly.
     * @deprecated now multiple scans are received together. remains here to support legacy station firmware
     * @see scansCreated
     * @param {Scan} scan
     * @return {Promise<Scan>} returns any rewards that may be given for the scan
     */
    scanCreated(scan) {
        const authIdForManagerPromise = this.bootstrap.authService().authUidByEmail(env.variables.stationManagerEmail);
        return authIdForManagerPromise.then((authUid) => {
            const scanBuilder = Scan.builder(scan);
            if (!scan.hasTimestamp()) {
                scanBuilder.timestamp(this.bootstrap.momentService.now());
            }
            const authorizedScan = scanBuilder.authUid(authUid).build();
            return this.bootstrap.scanService.scanCreated(authorizedScan);
        });
    }

    /**
     * @param {DataSnapshot|DeltaSnapshot} snapshot of the scans for a user's single day
     * @return {Promise.<GameCalculator>}
     */
    userDailyPoints(snapshot) {
        const rewards = this.bootstrap.userRewardsService._userRewardsRepository.rewardsFromScansSnaphsot(snapshot);
        const reference = snapshot.ref;
        const dayMoment = UserRewardsRepository.momentFromDayRef(reference);
        const userId = UserRewardsRepository.userIdFromRef(reference);
        return this.bootstrap.gameService.updateSummaryFromRewards(userId, dayMoment, rewards).then(
            (calculator) => {
                console.log(calculator.points() + " points calculated for " + calculator.userId());
                return calculator;
            });
    }

    /** When rules are achieved, followers of the user for that rule
     * should be notified of the achievement.
     *
     * https://trello.com/c/W0nFGT2E
     *
     * @param {DataSnapshot|DeltaSnapshot} snapshot
     * @return {Promise<Promise<NotificationResult>[]>}
     */
    notifyOfRuleReward(snapshot) {
        //TODO: use the repository to extract the details
        const reward = Reward.builder(snapshot.val()).build();
        return this.bootstrap.userRewardsService.notifyFollowersOfReward(reward);
    }

    /**
     *
     * @param {string} value
     * @return {string} the value wrapped like {value}
     */
    static param(value) {
        return "{" + value + "}";
    }

    /**
     * moment that returns the key of the field, rather than a value. to be used in path templates
     *
     * @return {moment.Moment}
     */
    static templateMoment() {
        return {
            // date: () => {
            //     return GameFirebaseDatabaseFunctions.param("day");
            // },
            isoWeekday: () => {
                return GameFirebaseDatabaseFunctions.param("day");
            },
            isoWeek: () => {
                return GameFirebaseDatabaseFunctions.param("week");
            },
            isoWeekYear: () => {
                return GameFirebaseDatabaseFunctions.param("year");
            },
        };
    }

    /**
     * @return {string} providing the template path to the day totals for a user.
     */
    static userRewardsTotalsForDayPathTemplate() {
        return this.pathTemplate(UserRewardsRepository.userDailyTotalsRef);
    }

    /**
     *
     * @param {function} referenceFunction that generates the path given userId,moment,ref
     * @return {string}
     */
    static pathTemplate(referenceFunction) {
        const ref = FirebaseUtils.mockReference().child(UserRewardsRepository.pathName());
        const userId = new UserId(GameFirebaseDatabaseFunctions.param("userId"));
        const moment = GameFirebaseDatabaseFunctions.templateMoment();
        return referenceFunction(userId, moment, ref).toString();
    }

    /**
     *
     * @return {string} the template for user scans
     */
    static userRewardsScansForDayPathTemplate() {
        return this.pathTemplate(UserRewardsRepository.userRewardsDayScansRef);

    }

    /**Given the snapshot for a single day's totals, this will update all related parent totals.
     *
     * Currently just week.
     *
     * @param {DataSnapshot|DeltaSnapshot} snapshot of a single day, for a user, providing rewards
     * @return {Promise.<GameCalculator[]>}
     */
    totalsUpdatedFromDay(snapshot) {
        const reference = snapshot.ref;
        const moment = UserRewardsRepository.momentFromDayRef(reference);
        const userId = UserRewardsRepository.userIdFromRef(reference);
        const gameCalculators = [];
        return this.bootstrap.gameService.updateWeekTotals(userId, moment)
            .then((weekCalculator) => {
                gameCalculators.push(weekCalculator);
                return this.bootstrap.userRewardsService.updateYearTotals(userId, moment)
                    .then((yearCalculator) => {
                        gameCalculators.push(yearCalculator);
                        return gameCalculators;
                    });
            });
    }

    /**
     * Updates the player ranks. Completely re-writes every ranking every time since the ranks could have changed.
     * @return {Promise<PlayerRank[]>[]}
     */
    updateLeaderboard() {
        return this.bootstrap.playerRankService.updateTotalRanks(
            [
                SchoolGameRules.everyScanCountsRuleId(),
                SchoolGameRules.arrivedByBike(),
                SchoolGameRules.arrivedByFoot(),
                SchoolGameRules.arrivedByBus(),
            ]);
    }

    /**
     * Given an array of scans received together in a single message, this will create a new scan for each.
     * Every scan will be considered for reward and those rewards persisted.
     * Every reward will then be totaled and the daily totals for this user will be updated.
     *
     * Relying on the database triggers causes race conditions so calculating daily totals. https://trello.com/c/spokJXgE
     *
     * @param {(Scan|*)[]} scans from the same user, in a batch.
     * @return {Promise<GameCalculator>} with the rewards and totals
     */
    scansCreated(scans) {

        if (scans.length === 0) {
            return Promise.resolve(GameCalculator.builder().build());
        }
        //associate the tag to a school once for the whole batch
        const tagId = Scan.builder(scans[0]).build().tagId();
        this.bootstrap.tagService.associateToSchool(tagId).catch((error) => {
            this.reportTagAssociationError(tagId, error);
        });
        //create arbitrary batch id.  Time is likely good enough given the sequential upload of a single school station
        const firstScan = Scan.builder(scans[0]).build();

        return this.bootstrap.scanService.tagForScan(firstScan).then((tag) => {
            return this.bootstrap.userRewardsService.rewardsForUserForDay(tag.userId(), firstScan.timestamp())
                .then((existingRewards) => {
                    //we have to wait for existing to finish before creating more to avoid race conditions
                    const promises = [Promise.resolve(existingRewards)];
                    //convert each scan into a new reward
                    const batchId = this.bootstrap.momentService.now().toISOString();
                    scans.forEach((scan) => {
                        //ensure scan is class, not just properties
                        scan = Scan.builder(scan).batchId(batchId).build();
                        const promise = this.scanCreated(scan).then((scanWithId) => {
                            return this.bootstrap.gameService.scanReceived(scanWithId);
                        });
                        promises.push(promise);
                    });
                    //return an array of rewards, rather than an array of arrays of rewards
                    //each reward references the origin scan
                    return Promise.all(promises).then((rewards) => {
                        const allRewards = [];
                        let userId;
                        rewards.forEach((rewardsForScan) => {
                            rewardsForScan.forEach((reward) => {
                                allRewards.push(reward);
                                userId = reward.userId();
                            });
                        });
                        //user id indicates if a user is found and rewarded
                        if (userId) {
                            const now = this.bootstrap.momentService.now();
                            return this.bootstrap.gameService.updateSummaryFromRewards(userId, now, allRewards);
                        } else {
                            //no rewards so appropriate to return an empty calculator
                            return Promise.resolve(GameCalculator.builder().build());
                        }
                    });
                });
        });
    }

    /**
     * Called when the tag association fails. Abstracted for testing purposes.
     * @private
     * @param {string} tagId of the tag that failed association
     * @param {string} error explaining the failure
     */
    reportTagAssociationError(tagId, error) {
        //this failure shouldn't stop the more important scans.
        console.error(`failed to associate tag id ${tagId} to a school: `, error);
    }
}


module.exports = GameFirebaseDatabaseFunctions;
