const console = require("console");
const admin = require("firebase-admin");
//env
const env = require("env-var");
const ApplicationEnv = require("../generated/application-env");
const ApplicationVersion = require("../main/system/application-version");

//game
const GameService = require("../main/game/game-service");
const UserRewardsRepository = require("../main/game/user-rewards-repository");
const UserFollowerRepository = require("./game/user-follower-repository");
const UserRewardsService = require("./game/user-rewards-service");
const SchoolGameRuleRepository = require("../main/game/school-game-rules");
const GameWebResources = require("../main/game/game-web-resources");
const PlayerRankService = require("../main/rank/player-rank.service");
const PlayerRank = require("../main/rank/player-rank");

//station
const StationService = require("../main/station/station.service");
const Station = require("../main/station/station");

//scan
const ScanRepository = require("../main/scan/scan-repository");
const ScanService = require("../main/scan/scan-service");
//tag
const TagRepository = require("../main/tag/tag-repository");
const TagService = require("../main/tag/tag-service");
//rule
const Rules = require("../main/rule/rules");
const RuleRepository = require("../main/rule/rule-repository");
const RuleService = require("../main/rule/rule-service");
const Fact = require("json-rules-engine").Fact;

//user
const User = require("../main/user/user");
const UserRepository = require("../main/user/user-repository");
const UserService = require("../main/user/user-service");
const AuthService = require("../main/user/auth-service");

//message
const Message = require("../main/message/message");
const MessageRepository = require("../main/message/message-repository");
const MessageService = require("../main/message/message-service");
//notification

const Notification = require("./notification/notification-result");
const NotificationRecipient = require("./notification/notification-recipient");
const NotificationService = require("./notification/notification-service");

const Repository = require("../main/entity/repository");
const MomentService = require("../main/lang/moment-service");

const PubSub = require("@google-cloud/pubsub");
/**
 * Temporary system bootstrap to glue all of the pieces together.
 *
 * This should be replaced by Dependency Injection,
 * possibly https://www.npmjs.com/package/constitute.
 */
class SystemBootstrap {

    /**
     * Provide those system dependent resources that all others depend upon.
     * @param { Reference} rootReference the root of the path to be used (not necessarily the database root).
     * @param {Audit} auditTemplate giving the who and what
     * @param {Rule[]} rules? of the game if provided, otherwise they will be loaded from the database
     * @param {string} timeZone where the game is taking place, from https://momentjs.com/timezone/
     * @param {Messaging} messaging the firebase messaging service to send notifications
     * @param {admin} admin (firebase) used with user administration
     * @param {PubSub} pubSub for publishing asynchronous messages
     * @private
     */
    constructor(rootReference, auditTemplate, rules, timeZone, messaging, admin, pubSub) {
        const appVersion = SystemBootstrap.applicationVersion();
        this.rootReference = rootReference;
        this.auditTemplate = auditTemplate;
        this.rules = rules;
        this.timeZone = timeZone;
        this.messaging = messaging;
        this.admin = admin;
        this.momentService = new MomentService(timeZone);
        this.gameWebResources = new GameWebResources(appVersion);
        this.userRepository = new UserRepository(rootReference, auditTemplate);
        this.userService = new UserService(this.userRepository, this.userIdentityRepositoryProvider());
        this.messageRepository = new MessageRepository(new Repository(rootReference, "messages", Message, auditTemplate));
        this.messageService = new MessageService(this.messageRepository, this.momentService);
        this.tagRepository = new TagRepository(rootReference, auditTemplate);
        this.tagService = new TagService(this.tagRepository, this.userService, appVersion.environment(), pubSub);
        this.scanRepository = new ScanRepository(rootReference, auditTemplate, this.momentService);
        this.scanService = new ScanService(this.scanRepository, this.tagService);
        this.stationService = new StationService(Repository.builder().entityDomain("stations")
            .auditTemplate(auditTemplate).databaseReference(rootReference).entityType(Station).build());
        this.notificationRepository = Repository.builder()
            .databaseReference(rootReference)
            .entityType(Notification)
            .auditTemplate(auditTemplate)
            .hasFullPathId(true).build();
        this.notificationService = new NotificationService(this.notificationRepository, messaging, this.momentService);

        this.userFollowerRepository = new UserFollowerRepository(
            Repository.builder()
                .hasFullPathId(true)
                .auditTemplate(auditTemplate)
                .databaseReference(rootReference)
                .entityType(NotificationRecipient).build());

        /** @type UserRewardsService*/
        this.userRewardsService = UserRewardsService.builder()
            .use(this.notificationService)
            .and(this.userService)
            .and(new UserRewardsRepository(rootReference, auditTemplate))
            .and(this.scanService)
            .and(this.userFollowerRepository)
            .build();
        this.playerRankRepository = Repository.builder().auditTemplate(auditTemplate)
            .databaseReference(rootReference).entityType(PlayerRank).build();
        this.playerRankService = new PlayerRankService(
            this.playerRankRepository,
            this.userRewardsService,
            this.userService);
        this.ruleRepository = new RuleRepository(rules,
            SystemBootstrap._operators(),
            SystemBootstrap._facts(this.userRewardsService, this.authService()),
            new Repository(rootReference, RuleRepository.pathName(), Rules, auditTemplate));
        this.ruleService = new RuleService(this.ruleRepository);

        /** @type GameService */
        this.gameService = GameService.builder()
            .ruleService(this.ruleService)
            .userRewardsService(this.userRewardsService)
            .messageService(this.messageService)
            .userService(this.userService)
            .scanService(this.scanService)
            .momentService(this.momentService)
            .authService(this.authService())
            .stationService(this.stationService)
            .build();
    }

    /**
     * @return {AuthService} the singleton auth service or undefined if admin is undefined
     */
    authService() {
        if (!this._authService) {
            if (this.admin) {
                this._authService = new AuthService(this.admin.auth());
            }
        }
        return this._authService;
    }

    /**
     * provides a function that creates a new instance of the repository that provides suggested identities for users.
     * By using a function, this allows lazy initialization of something that is not used often.
     * @return {Function} that when called provides a repository of suggested identities.
     */
    userIdentityRepositoryProvider() {
        return () => {
            return Repository.builder()
                .auditTemplate(this.auditTemplate)
                //notice going to the root since this is a fixed path, not relative to tests
                .databaseReference(this.rootReference.root)
                .entityType(User)
                .entityDomain("identities")
                .build();
        };
    }

    /**
     * @return {SystemBootstrapBuilder}
     */
    static builder() {
        return new SystemBootstrapBuilder();
    }

    /** provides all operators needed for game rules.
     * @return {Operator[]}
     * @private
     */
    static _operators() {
        return SchoolGameRuleRepository.schoolGameOperators();
    }

    /**
     *
     * @param {UserRewardsService} userRewardsService
     * @param {AuthService} authService (firebase)
     * @return {Fact[]}
     * @private
     */
    static _facts(userRewardsService, authService) {
        const value = SystemBootstrap.applicationVersion().environment();
        const environmentFact = SystemBootstrap.environmentFact(value);
        const facts = [environmentFact];
        return facts.concat(facts, SchoolGameRuleRepository.allFacts(userRewardsService, authService));
    }

    /**
     * @param {String} value
     * @return {Fact}
     */
    static environmentFact(value) {
        return new Fact(
            "environment",
            value,
            {cache: true});
    }

    /**
     * @return {ApplicationVersion} newly instantiated available after build generation
     */
    static applicationVersion() {
        return new ApplicationVersion(ApplicationEnv.variables);
    }

}

/**
 * use this to create the bootstrap
 */
class SystemBootstrapBuilder {

    /**
     *
     * @param {Rule[]} rules
     * @return {SystemBootstrapBuilder}
     */
    rules(rules) {
        this._rules = rules;
        return this;
    }

    /**
     * @param { Reference} reference
     * @return {SystemBootstrapBuilder}
     */
    rootReference(reference) {
        this._rootReference = reference;
        return this;
    }

    /**
     * https://momentjs.com/docs/#/manipulating/utc-offset/
     * @param {string} timeZone from https://momentjs.com/timezone/
     * @return {SystemBootstrapBuilder}
     */
    timeZone(timeZone) {
        this._timeZone = timeZone;
        return this;
    }

    /**
     *
     * @param {Audit} auditTemplate
     * @return {SystemBootstrapBuilder}
     */
    auditTemplate(auditTemplate) {
        this._auditTemplate = auditTemplate;
        return this;
    }

    /**
     *
     * @param {Messaging} messaging
     * @return {SystemBootstrapBuilder}
     */
    messaging(messaging) {
        this._messaging = messaging;
        return this;
    }

    /**
     * @param {admin} admin (firebase)
     * @return {SystemBootstrapBuilder}
     */
    admin(admin) {
        this._admin = admin;
        return this;
    }

    /**
     * Google Cloud PubSub for publishing/subscriptions.
     * @param {PubSub} pubSub
     * @return {SystemBootstrapBuilder}
     */
    pubSub(pubSub) {
        this._pubSub = pubSub;
        return this;
    }

    /**
     * @return {SystemBootstrap}
     */
    build() {
        console.assert(this._rootReference !== null);
        console.assert(this._auditTemplate !== null);
        return new SystemBootstrap(
            this._rootReference,
            this._auditTemplate,
            this._rules,
            this._timeZone,
            this._messaging,
            this._admin,
            this._pubSub);
    }

}

module.exports = SystemBootstrap;
