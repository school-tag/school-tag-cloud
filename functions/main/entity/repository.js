const Database = require("firebase").database.Database;
const moment = require("moment");
const Reference = require("firebase").database.Reference;
const DataSnapshot = require("firebase").database.DataSnapshot;
const DeltaSnapshot = require("firebase-functions").database.DeltaSnapshot;
const ThenableReference = require("firebase").database.ThenableReference;
const FirebaseError = require("firebase").FirebaseError;
const Entity = require("./entity.js");
const EntityUtils = require("./entity-utils.js");
const EntityProperties = require("./entity.js").EntityProperties;
const FirebaseUtils = require("../firebase/firebase-utils");
const DateUtils = require("../lang/date-utils.js");
const Errors = require("../../main/lang/errors");

module.exports = Repository;

/** General persistence for a specific Entity that must extend this.
 *
 * @param {Reference} databaseReference a Firebase or implementation with the same interface and function
 * @param {string} entityDomain  unique name for the entity used as the root path for the data access API.
 * @param {class} entityType  the class that implements entity...used  to construct entities
 * @param {Audit} auditTemplate provide who and where
 * @param {boolean} hasFullPathId indicates if the id extracted should be the full path or just the key
 * @constructor
 * @template T
 */
function Repository(databaseReference, entityDomain, entityType, auditTemplate, hasFullPathId) {
    console.assert(databaseReference != null, "databaseReference is required");
    console.assert(entityType != null, "entityType is the class the repository manages <T>");
    /**@type Reference*/
    this._rootRef = databaseReference;
    this._entityDomain = entityDomain;
    this._entityType = entityType;
    this._auditTemplate = auditTemplate;
    //ensures false is the default if not provided or not true
    this._hasFullPathId = (hasFullPathId) ? true : false;
    this._fixedPath = false;
}

/** persists the entire object as it is given clobbering everything in its place.
 *
 * @param {Entity} entity to be saved
 * @param {Reference|string} [entityRef] optionally provided, if located elsewhere than the default /entities/entityId
 * @return {Promise.<void>} for confirmation of save success
 *
 */
Repository.prototype.save = function (entity, entityRef) {
    if (this._hasFullPathId) {
        console.assert(entityRef, "path is required for full path id entities");
        if (!(entityRef instanceof Reference)) {
            entityRef = this._ref(entityRef);
        }
    } else {
        const entityId = entity.id();
        console.assert(entityId, "entity.id is null: use push if you need an id generated");
        if (!entityRef) {
            entityRef = this._ref(entityId.toString());
        }
    }
    //purposely not chaining the audit...it shouldn't slow down the important stuff
    this._pushAudit(entityRef, "save");
    return entityRef.set(entity.portable());
};

/**
 *
 * @param {string} path from the root
 * @param {*} value to overwrite the value at the position of the path
 * @return {Promise<void>} indicates when finished
 */
Repository.prototype.set = function(path, value) {
    return this._ref(path).set(value);
};


/**
 *
 * @param {string} path to be appended
 * @return {string} the entity domain + the path given ready to use with repository methods accepting path.
 */
Repository.prototype.pathAppended = function(path) {
    return `${this._entityDomain}/${path}`;
};

/**
 *
 * @param {Entity} entity
 * @param {string} [childPath] when a deeper path is needed
 * @return {ThenableReference.<void>} providing the ID immediately
 */
Repository.prototype.push = function (entity, childPath) {
    const entityId = entity.id();
    console.assert(entityId == null, "entity.id must be null: use save");

    let reference = this._ref();
    if (childPath) {
        reference = reference.child(childPath);
    }
    const pushRef = reference.push(entity.portable());
    //do not chain the audit, just do it and don't affect the flow
    pushRef.then(() => {
        return pushRef.child(EntityProperties.id()).set(pushRef.key);
    }).then(()=>{
        return this._pushAudit(pushRef, "push");
    });
    return pushRef;
};
/** Internal method to provide a reference to the database by the entity key or at the root
 * for general searches.
 *
 * @param  {string|null} [entityId]
 * @return {Reference} keyed by the entity Id or root path if none provided
 * @private
 */
Repository.prototype._ref = function (entityId) {

    let ref;
    if (this._entityDomain) {
        ref = this._rootRef.child(this._entityDomain);
    } else {
        ref = this._rootRef;
    }

    if (entityId) {
        return ref.child(entityId.toString());
    } else {
        return ref;
    }
};

/**
 * @param {Reference} ref of the entity that was modified.
 * @param {string} what describes the action taken
 * @return {ThenableReference.<void>} indicating success
 */
Repository.prototype._pushAudit = function (ref, what) {
    if (this._auditTemplate != null) {
        const auditsRef = FirebaseUtils.parallelEntityRef(this._rootRef, ref, EntityProperties.audits());
        return auditsRef.push(this._auditTemplate.clone(what));
    }
};
/** Called from callbacks to extract the entity in a common manner.
 *
 * @param {DataSnapshot|DeltaSnapshot} snapshot the data found in the database
 * @return {T} the entity extracted from the snapshot
 */
Repository.prototype.entityFromSnapshot = function (snapshot) {
    console.assert(snapshot.exists(), "don't send me snapshots that don't exist and I won't give you a null");
    const properties = snapshot.val();
    if (snapshot.key && (!properties.id || this._hasFullPathId ) ) {
        let id;
        if (this._hasFullPathId) {
            id = snapshot.ref.toString();
        } else {
            id = snapshot.key;
        }
        properties[EntityProperties.id()] = id;
    }
    return this._entityType.builder(properties).build();
};

/**
 * Convenience method for those wishing to work directly with the snapshot
 * resulting from a "once" get method.
 * @private
 * @param {Reference} ref
 * @return {Promise.<DataSnapshot>}
 */
Repository.prototype._snapshot = function (ref) {
    return ref.once(FirebaseUtils.EventType.VALUE);
};
/**
 * Calls the database and handles the response.
 * @param {Reference} ref
 * @return {Promise.<Entity.<T>>} the promise of an entity
 */
Repository.prototype.entityFromRef = function (ref) {
    return this._snapshot(ref).then(
        /**
         * @param  {DataSnapshot} snapshot
         * @return {T}
         */
        (snapshot) => {
            if (snapshot && snapshot.exists() && snapshot.hasChildren()) {
                return this.entityFromSnapshot(snapshot);
            } else {
                return Promise.reject(Errors.of(`${this._entityType.name} is not found for ${ref.toString()}`,
                    this.notFoundCode(), Errors.notFoundStatusCode()));
            }
        }
    );
};


/**
 * The code returned with the error when an entity can't be found.  It is qualified with the entity domain.
 * @return {string}
 */
Repository.prototype.notFoundCode = function () {
    return Errors.notFound(this._entityDomain ? this._entityDomain : this._entityType.prototype.constructor.name);
};

/** Retrieves the entity from persistence matching the given id.
 *
 * @param {string} [id]
 * @return {Promise.<T>} a promise that will provide the entity with the given id,
 *                       if found or the error otherwise
 * @throws {Assert.AssertionError} when the entity was not found.
 */
Repository.prototype.get = function (id) {
    //some ids have the absolute path to the entity. This removes the root
    if (id) {
        id = id.replace(this._ref().toString(), "");
    }
    const ref = this._ref(id);
    return this.entityFromRef(ref);
};

/**
 * @param {Reference|Query} ref
 * @return {Promise.<T[]>}
 */
Repository.prototype.listRef = function (ref) {
    return this._snapshot(ref).then((listSnapshot) => {
        const entities = [];
        listSnapshot.forEach((entitySnapshot) => {
            const entity = this.entityFromSnapshot(entitySnapshot);
            entities.push(entity);
        });
        return entities;
    });
};
/**
 *
 * @param {string} [path] where to find the entities of interest. default looks at entityDomain
 * @return {Promise.<T[]>} returns when the data is available and action will be called
 */
Repository.prototype.list = function (path) {
    const ref = this._ref(path);
    return this.listRef(ref);
};

/**Identifies who modified an entity when and what was done.
 * The id is generated by the system leaving only
 */
class Audit {

    /**
     *
     * where is an encoded string indicating the system that modified:
     * [os]:[device]:[version]
     *
     * examples:
     *
     * android:pixel:7.2
     * node:functions:1.0
     * node:web:1.1
     * mac:cli:3.1
     *
     * Which identifies which version of the code is running.  The closer to production, the more disciplined
     * this value becomes (like version 2.3, etc). The closer to the developer, the less disciplined the value
     * becomes (like a timestamp).
     *
     * @param {string} who the end user who requested this action.
     * @param {string} where the system that modified the entity. see docs javadoc format
     * @param {string} which version of the code.
     */
    constructor(who, where, which) {
        this.who = who;
        this.where = where;
        this.which = which;
    }

    /**
     * @param {string} what enumerated action taken against the system (created,saved,etc)
     * @return {Audit} with what and when updated
     */
    clone(what) {
        const clone = EntityUtils.deepClone(this);
        clone.what = what;
        clone.when = DateUtils.timestampMomentPortable(moment());
        return clone;
    }

}

/**
 * @return {RepositoryBuilder} used to construct repository
 */
Repository.builder = function () {
    return new RepositoryBuilder();
};

/**Use this to build all repositories for ease of extension without errors.*/
class RepositoryBuilder {

    /**
     * @param {Reference} value
     * @return {RepositoryBuilder}
     */
    databaseReference(value) {
        this._databaseReference = value;
        return this;
    }

    /**
     * @param {string} value
     * @return {RepositoryBuilder}
     */
    entityDomain(value) {
        this._entityDomain = value;
        return this;
    }

    /**
     * @param {class} value
     * @return {RepositoryBuilder}
     */
    entityType(value) {
        this._entityType = value;
        return this;
    }

    /**
     * @param {Audit} value
     * @return {RepositoryBuilder}
     */
    auditTemplate(value) {
        this._auditTemplate = value;
        return this;
    }

    /**
     * @param {boolean} value
     * @return {RepositoryBuilder}
     */
    hasFullPathId(value) {
        this._hasFullPathId = value;
        return this;
    }

    /**
     * @return {Repository}
     */
    build() {
        return new Repository(
            this._databaseReference,
            this._entityDomain,
            this._entityType,
            this._auditTemplate,
            this._hasFullPathId
        );
    }
}

module.exports.Audit = Audit;
