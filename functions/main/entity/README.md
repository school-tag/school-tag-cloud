## Entities ##

Entities encapsulate data. They are disconnected are provide no business
 logic, beyond conversation and validation.
 
Entities are immutable providing only accessor (getter) methods using only the property name (no `get`).
Properties are stored in a private field `_props` and should only be retrieved using the accessor methods.
 
All properties are stored in a _portable_ format that is able to understand by other systems. 

Dates, for example, are stored in ISO 8601 format `2017-08-05T15:38:20-07:00` even though the only
accessor may provide a `moment` object for ease of use.  

This allows an entity to be saved using
Firebase to store the _props directly as JSON.

 
## Builders ##

Builders create and mutate Entities by calling the static builder method on the Entity.

```
myEntity.builder().value1(3).build()
```

The builder mutator methods are named as the property and may provide validation.  Exceptions are only encouraged for 
non-recoverable coding errors to communicate requirements to developers.

## Repositories ##

Stateless classes that povide fine grained access to a specific
entity that it serves. Using Firebase, it hides the details
of Firebase (`DataSnapshots` for example) and extracts the portable data
to build and return the entity, usually via a `Promise`.

Repositories also provide queries to retrieve a collection of entities.

```javascript
class PersonRepository{
   get (name){
       this.databaseRef.once('VALUE').then((snapshot) =>{
           return Person.builder(snapshot.data).build();
       })
    }
}
```

## Services ##

Stateless classes invoke business logic on the entities. They intereact with the repositories
to retrieve the data, apply logic and persist again using repositories.

Services have no ties to Firebase or any other external system, but rather 
leave that up to the repositories.

Repositories are generally provided to a single service to ensure access
is controlled in a single place.

## Injection ##

Services and Repositories are managed by the `SystemBootstrap` 
that constructs services with repositories and other services.

```
const firebase = Firebase.init();
const repository1 = new Repository1(firebase.database().root());
const service1 = new Service1(repository1);
const service2 = new Service2(service1);
```

This makes testing easier by allowing mocks.