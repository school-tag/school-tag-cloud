
/**
 * static helpers to be used with general {Entity}.
 */
class EntityUtils {
    /** Converts the given array of entities into a map.
     *
     * @param {Entity[]} entities
     * @param {Function} [idExtractorFunction] if provided will extract the id from the entity.
     * @return {*} portable properties where each element is keyed by its id
     */
    static portableArray(entities, idExtractorFunction) {
        if (idExtractorFunction == null) {
            idExtractorFunction = function (entity) {
                return entity.id();
            };
        }
        const result = {};
        entities.forEach((entity) => {
            result[idExtractorFunction(entity)] = entity.portable();
        });
        return result;
    }

    /**
     * Creates a deep copy of the properties given by converting to JSON and parsing back.
     * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
     *
     * @param {!*} properties
     * @return {!*} copy of the properties
     */
    static deepClone(properties) {
        return JSON.parse(JSON.stringify(properties));
    }
}

module.exports = EntityUtils;
