const EntityUtils = require("./entity-utils.js");

/** Base class for all models that have identity.
 * Usage:
 * - Extend the Entity for your Entity,
 * - Extend the Builder for your Builder
 * - Add static method builder() on your entity.
 *
 *
 * The _props member houses all of the properties in their portable form (ready for json).
 * Date, for example, is stored as its ISO string, not the date object itself.  The getter method
 * would return the date by parsing the property for every access.
 * @template T
 */
class Entity {

    /** Default constructor that accepts the json properties or nothing.
     * Not called directly by anyone but children inheriting the pattern and function.
     *
     * Assigns the given properties to _props which should be used directly by children.
     *
     * @param {object} [properties]   that is the properties or null.
     * @constructor
     */
    constructor(properties) {
        // private naming used to protect direct property access
        if (properties != null) {
            this._props = EntityUtils.deepClone(properties);
        } else {
            this._props = {};
        }
    }

    /** The raw properties ready to provided to other clients (via Firebase).
     *
     * @return {{}|*}
     */
    portable() {
        return this._props;
    };

    /** The unique identifier required for every entity, but often provided by the repository.
     * @return {*} properties
     */
    id() {
        return this._props.id;
    };

    /**
     *
     * @return {boolean}
     */
    hasId() {
        return this._hasProperty(EntityProperties.id());
    };

    /**Returns the property of the entity with the name as the key.
     *
     * Use _hasProperty for optional fields rather than null or undefined checks
     *
     * @param {string} key
     * @return {*|undefined}
     * @protected
     */
    _property(key) {
        return this._props[key];
    };

    /**
     *
     * @param {string} key the property's name
     * @return {boolean} true if a value exists (not null) for the property
     * @protected
     */
    _hasProperty(key) {
        return this._props.hasOwnProperty(key) && this._property(key) != null;
    };

    /** Provides a builder that will mutate this.
     *
     * Use sparingly since it doesn't clone and mutable objects are problems.
     *
     * @return {EntityBuilder.<T>}
     */
    toBuilder() {
        const clone = new this.constructor(EntityUtils.deepClone(this._props));
        return this.constructor.builder(clone);
    };

    // tried to use https://www.npmjs.com/package/builder-builder and failed

    /**
     * @return {string} showing all of the properties
     */
    toString() {
        return JSON.stringify(this._props);
    };
}

// ================== Builder Methods ==========================

// ============ Mutators ===================

/**
 Mutators exist only on the builders to avoid any client mutating the objects directly.
 The properties are stored in the private object _entity which must be provided during construction
 and provided back using the #build method.
 * Child mutators should access _entity directly.
 * @template T
 */
class EntityBuilder {

    /**
     *
     *Base Constructor for Builders of child entities.
     *
     * Implementations should provide the static builder method that will call the specific constructor.
     *
     * @param {T} mutant
     * @constructor
     */
    constructor(mutant) {
        console.assert(mutant != null, "children must always provide an instance of the object");
        /**
         *
         * @type {T}
         * @protected
         */
        this._entity = mutant;
    }

    /** Delivers the Tag object ready to be used after creation/mutation.
     *The second call will deliver a null.
     *
     * @return {T}
     */
    build() {
        console.assert(this._entity != null, "build can only be called once");
        this._validate();
        this._preBuild();
        let result = this._entity;
        this._entity = null;
        return result;
    };

    /**
     * The unique identifier for this entity.
     * @param {string|*} id
     * @return {EntityBuilder.<T>}
     */
    id(id) {
        if (this._entity.hasId()) {
            const existing = this._entity.id();
            console.assert(id.includes(existing), id + " given does not match the existing " + existing);
        } else {
            this._property(EntityProperties.id(), id);
        }
        return this;
    };

    /**
     * General purpose method intended to be used by
     * implementations to set the properties using constant keys and portable properties.
     * @param {string} key
     * @param {*} value
     * @return {EntityBuilder}
     * @protected
     */
    _property(key, value) {
        if (value !== undefined) {
            let properties;
            if (value instanceof Entity) {
                properties = value.portable();
            } else {
                properties = value;
            }
            this._entity._props[key] = properties;
        }
        return this;
    };

    /**
     * called during #build allowing the implementor to provide validation to ensure required
     * arguments are as expected.
     * @protected
     */
    _validate() {
        // override and validate child properties
    };

    /** Optionally allows an implementor to override to prepare
     * properties before building, but after validation.
     * @protected
     */
    _preBuild() {
        // override and prepare for building
    };
}

/**properties found in the general entity.*/
class EntityProperties {
    /** @return {string}*/
    static id() {
        return "id";
    }

    /**Used by the repository to track who, when and what affected an entity.
     * No corresponding property is provided since the system should interact with this information as
     * it is for administrative purposes only.
     * @return {string}*/
    static audits() {
        return "audits";
    }

}

module.exports = Entity;
module.exports.EntityProperties = EntityProperties;
module.exports.EntityBuilder = EntityBuilder;