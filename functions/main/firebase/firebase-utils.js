const Enum = require("enum");

const Reference = require("firebase").database.Reference;

/**
 * enumerations for options of https://firebase.google.com/docs/reference/js/firebase.database.Query#once
 * usage: FirebaseUtils.QueryEventType.VALUE.key
 * @deprecated use EventType
 */
module.exports.QueryEventType = new Enum({
    VALUE: "value",
    CHILD_ADDED: "child_added",
    CHILD_CHANGED: "child_changed",
    CHILD_REMOVED: "child_removed",
    CHILD_MOVED: "child_moved.",
}, {ignoreCase: true, freeze: true});

/**
 * @enum {string}
 * @readonly
 */
module.exports.EventType = {
    VALUE: "value",
    CHILD_ADDED: "child_added",
    CHILD_CHANGED: "child_changed",
    CHILD_REMOVED: "child_removed",
    CHILD_MOVED: "child_moved.",
};

/**
 * Behaves like a Reference, with limited functions.  Mostly used for building paths,
 * but is not connected so it doesn't need the bootstrap of the full Firebase.
 *
 * @param {String} [givenPath] the root path, if needed.  Same as calling .child(givenPath).
 * @return {Reference} with the paths being built.
 */
module.exports.mockReference = function (givenPath) {

    return {
        path: givenPath,
        /**
         *
         * @param {string} pathElement a single element without / or multiple without leading slash
         * @return {Reference}
         */
        child: function (pathElement) {
            const delimeter = "/";
            if (givenPath == null) {
                givenPath = "";
            }
            //whatever the element is, convert it to a string
            pathElement = pathElement.toString();

            if (!pathElement.startsWith(delimeter) && !givenPath.endsWith(delimeter)) {
                givenPath += delimeter;
            }
            givenPath += pathElement;
            const child = exports.mockReference(givenPath);
            child.key = pathElement;
            child.parent = this;
            return child;
        },
        toString: function () {
            return givenPath;
        },
    };
};

/**To avoid too much nested data, this provides a reference to a new given root,
 * but keeps a pareallel path so it may identify the original entity.
 *
 *  Example:
 *  - users
 *  - - abc123
 *  - - - audits
 *  - - - - 567xyz
 *  - - - - - when
 *  - - - - - who
 *  ...
 *
 *  The above makes User heavy with a lot of audits, so instead...
 *  - audits
 *  - - users
 *  - - - abc123
 *  - - - - 567xyz
 *  - - - - - when
 *  - - - - - who
 *
 * @param {Reference} existingRootRef - the reference to the root that will be the base for the new ref
 * @param {Reference} entityRef the existing entity that you wish to parallel
 * @param {String} newRootPathOrElement - the new entity root you wish to insert
 * @return {Reference} to the new parallel entity path
 */
module.exports.parallelEntityRef = function (existingRootRef, entityRef, newRootPathOrElement) {
    const existingPathBeyondRoot = entityRef.toString().replace(existingRootRef.toString(), "");
    return existingRootRef.child(newRootPathOrElement).child(existingPathBeyondRoot);
};

