### User

Any participant in the School Tag game:

* Player - Owns tags and scans them at stations for rewards
* Observer - Receives notifications, scans tags and gives bonus rewards

