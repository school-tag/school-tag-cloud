const Entity = require("../entity/entity.js");
const moment = require("moment");
const DateUtils = require("../lang/date-utils.js");

/**
 * @extends {Entity.<User>}
 */
class User extends Entity {
    /**
     * @param {object} [properties] that is the portable properties or null.
     * @constructor
     */
    constructor(properties) {
        super(properties);
    }

    /**The time this entity was crated.
     *
     * @return {moment.Moment}
     */
    momentCreated() {
        return DateUtils.timestampMoment(this._property(User.prototype.momentCreated.name));
    }

    /**
     * changes the default id type to UserId
     * @override
     * @return {UserId|null}
     */
    id() {
        const id = super.id();
        return (id) ? new UserId(id) : null;
    }

    /**
     * The made up name allowing game followers to recognize a person, without exposing true identity.
     *
     * @return {string}
     */
    nickname() {
        return this._property(User.prototype.nickname.name);
    }

    /**
     * @return {boolean} if nickname is set
     */
    hasNickname() {
        return this._hasProperty(User.prototype.nickname.name);
    }

    /**
     * Small image that represents the user visually.
     *
     * see gs://school-tag-sbox.appspot.com/images/avatars/ for source
     * use https://school-tag-avatars.imgix.net to serve up avatar
     * TODO: create an avatar service that will provide these details for use
     *
     * @return {string} end part of a url path to the image that is located in a common root.
     */
    avatar() {
        return this._property(User.prototype.avatar.name);
    }

    /**
     *
     * @return {boolean} true if an avatar is set
     */
    hasAvatar() {
        return this._hasProperty(User.prototype.avatar.name);
    }

    /**The firebase id provided by the firebase authentication service.
     *
     * @return {string}
     */
    authUid() {
        return this._property(User.prototype.authUid.name);
    }

    /**
     * Not everyone has authenticated and claimed their user.
     *
     * @return {boolean}
     */
    hasAuthUid() {
        return this._hasProperty(User.prototype.authUid.name);
    }

    /**
     * @param {User|*|null} [user]
     * @return {UserBuilder}
     */
    static builder(user) {
        if (!(user instanceof User)) {
            user = new User(user);
        }
        return new UserBuilder(user);
    }
}

/** Represents a game player or observer that may have one or more {Tag} or {Scan} devices.
 *
 * @type {User}
 */
module.exports = User;
module.exports.User = User;

// ============== Builder ================
/**
 *
 * @extends {EntityBuilder.<User>}
 */
class UserBuilder extends Entity.EntityBuilder {
    /**
     *
     * @param {User} mutant
     */
    constructor(mutant) {
        super(mutant);
    }

    /**
     * @param {Moment|string|null} [momentOrString] if not provided, the current time will be used
     * @return {UserBuilder}
     */
    momentCreated(momentOrString) {
        if (momentOrString == null) {
            momentOrString = moment();
        }
        this._entity._props[User.prototype.momentCreated.name] = DateUtils.timestampMomentPortable(momentOrString);
        return this;
    }

    /**
     *
     * @param {string} nickname
     * @return {UserBuilder}
     */
    nickname(nickname) {
        this._property(User.prototype.nickname.name, nickname);
        return this;
    }

    /**
     * @param {string} avatarPath
     * @return {UserBuilder}
     */
    avatar(avatarPath) {
        return this._property(User.prototype.avatar.name, avatarPath);
    }

    /**
     *
     * @param {string} authUserId
     * @return {UserBuilder}
     */
    authUid(authUserId) {
        return this._property(User.prototype.authUid.name, authUserId);
    }

    /**
     * @override
     * @private
     */
    _preBuild() {
        if (!this._entity._hasProperty(User.prototype.momentCreated.name)) {
            this.momentCreated();
        }
    }

    /**
     * @return {User}
     */
    build() {
        return super.build();
    }
}

/** Type safe representation of a user.
 * @deprecated using basic strings is more desirable than the object wrapper
 * */
class UserId {
    /**
     * @param {string} value
     */
    constructor(value) {
        /** @private*/
        console.assert(value != null, "the id value is required");
        this._value = value;
    }

    /**
     * @override
     * @return {string} the value
     */
    toString() {
        return this._value;
    }

    /**
     *
     * @param {UserId} [other]
     * @return {boolean} true if the values of the ids are equal
     */
    equals(other) {
        return other != null
            && other._value != null
            && typeof other == typeof this
            && this._value == other._value;
    }
}

module.exports.UserId = UserId;

/**
 * Interface declaring properties of User.
 */
class UserProperties {
    /**
     */
    constructor() {
        /**
         * @type {string}
         */
        this.id = undefined;
        /**
         * @type {string}
         */
        this.nickname = undefined;
        /**
         * @type {string}
         */
        this.avatar = undefined;
        /**
         * @type {string}
         */
        this.momentCreated = undefined;
        /**
         * @type {string}
         */
        this.authUid = undefined;
    }
}

module.exports.UserProperties=UserProperties;