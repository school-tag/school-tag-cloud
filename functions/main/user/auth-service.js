/**
 * Used to identify authentication and authorization for those authenticated using Firebase.
 *
 * Has no relationship with our User, but works only with the firebase authenticated users.
 *
 * https://firebase.google.com/docs/auth/web/start
 */
class AuthService {

    /**
     * @param {admin.auth.Auth} adminAuth
     */
    constructor(adminAuth) {
        /**Firebase authentication service.
         *
         * @private
         * @type {admin.auth.Auth}
         */
        this._adminAuth = adminAuth;
    }

    /**
     *
     * @param {string} [authUid]
     * @param {string} role that the user
     * @return {Promise<boolean>} true if and only if the user has the expected role
     */
    authUserHasRole(authUid, role) {
        console.assert(role, "role is required");
        if (authUid) {
            return this._adminAuth.getUser(authUid).then((authUser) => {
                //authUser is non-null
                if (authUser.customClaims) {
                    const roleValue = authUser.customClaims[role];
                    return roleValue === true;
                } else {
                    return false;
                }
            }, (error) => {
                console.warn(`${authUid} is being provided, but no matching user which is unexpected: nn23`, error);
                return false;
            });
        } else {
            return Promise.resolve(false);
        }
    }

    /** Simply identifies the user by id when given an email address in the system.
     *
     * @param {string} email address of the auth user being sought.
     * @return {Promise<string>} uid the user (non-null), matching our User.authUid or rejects if not found.
     */
    authUidByEmail(email) {
        return this._adminAuth.getUserByEmail(email).then((userRecord) => {
            return userRecord.uid;
        });
    }
}

module.exports = AuthService;