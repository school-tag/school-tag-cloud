const Database = require("firebase").database.Database;
const DataSnapshot = require("firebase").database.DataSnapshot;
const Promise = require("firebase").Promise;
const FirebaseError = require("firebase").FirebaseError;
const Repository = require("../entity/repository.js");
const User = require("./user.js");
const Audit = require("../entity/repository.js").Audit;
const Errors = require("../../main/lang/errors");

/**Manages fine grained persistence for User.
 *
 * @extends {Repository.<User>}
 */
class UserRepository extends Repository {
    /**
     * @param {Database} database
     * @param {Audit} auditTemplate
     * @constructor
     */
    constructor(database, auditTemplate) {
        super(database, UserRepository.domain(), User, auditTemplate);
    }

    /**
     *
     * @return {string} the restful data path for users
     */
    static domain() {
        return "users";
    }

    /**
     *
     * @return {string} indicating the error when a user is not found.  always the same.
     */
    static userNotFoundCode() {
        return Errors.notFound(UserRepository.domain());
    }

    /**
     * Indicates the user with the auth id is not found.
     *
     * @return {string}
     */
    static userWithAuthIdNotFoundCode() {
        return Errors.notFound(`${UserRepository.domain()}/auth`);
    }

    /**Sets the authUid for the given user.
     *
     * @param {UserId} userId
     * @param {string} authUserId
     * @return {Promise.<void>} indicating success
     */
    setAuthUid(userId, authUserId) {
        return this._ref(userId).child(User.prototype.authUid.name).set(authUserId);
    }

    /**
     *
     * @param {string} authUserId
     * @return {Promise.<User>}
     */
    userForAuthUid(authUserId) {
        const query = this._ref().orderByChild(User.prototype.authUid.name).equalTo(authUserId).limitToFirst(1);
        return this.listRef(query).then((users) => {
            if (users && users.length > 0) {
                return users[0];
            } else {
                return Promise.reject(Errors.of(`no users found with authUserId=${authUserId}`,
                    UserRepository.userWithAuthIdNotFoundCode(),
                    Errors.notFoundStatusCode()));
            }
        });
    }

    /** Loads all users, without limits.  So if the game gets big (> 1000 users) this will need updating.
     *
     * @return {Promise<Object.<string,UserProperties>>}
     */
    allUsersMap() {
        return this._snapshot(this._ref()).then((snapshot)=>{
            return snapshot.val();
        });
    }
}

module.exports = UserRepository;
