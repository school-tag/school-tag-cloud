const UserRepository = require("./user-repository.js");
const User = require("./user.js");
const UserId = require("./user.js").UserId;

/**
 * Manages logic and persistence needs for users.
 */
class UserService {
    /**
     * @param {UserRepository} repository
     * @param {Function} identitySuggestionRepositoryProvider
     */
    constructor(repository, identitySuggestionRepositoryProvider) {
        /**@type {UserRepository}*/
        this._repository = repository;
        this._identitySuggestionRepositoryProvider = identitySuggestionRepositoryProvider;
    }

    /**
     * @return {Promise.<User>} promise of the newly created user with id
     */
    createUser() {
        const user = User.builder().build();
        const promiseWithKey = this._repository.push(user);

        return promiseWithKey.then(() => {
            return user.toBuilder().id(promiseWithKey.key).build();
        }).then((user) => {
            return this.assignSuggestedIdentity(user).then(() => {
                return user;
            });
        });
    }

    /**
     * Handy method when looking for a user by key or nickname.
     * TODO: handle nickname
     * @param {string|UserId} key any string that will uniquely identify a single user
     * @return {Promise.<User>}
     */
    userFromKey(key) {
        let id;
        if (key instanceof UserId) {
            id = key;
        } else {
            id = new UserId(key);
        }
        return this.user(id);
    }

    /** simple accessor
     * @param {UserId} userId
     * @return {Promise.<User>}
     */
    user(userId) {
        return this._repository.get(userId.toString());
    }

    /**
     * @param {string} authUid
     * @return {Promise.<User>}
     */
    userForAuthUid(authUid) {
        return this._repository.userForAuthUid(authUid);
    }

    /**
     *
     * @param {UserId} userId
     * @param {string} authUserId
     * @return {Promise.<void>}
     */
    setAuthUid(userId, authUserId) {
        //perhaps enforce no clobbering?
        return this._repository.setAuthUid(userId, authUserId);
    }

    /**
     * @private
     * @param {User} user
     * @return {Promise.<User>} with assigned identity
     */
    assignSuggestedIdentity(user) {
        return this.identitySuggested(user.id().toString())
            .then((suggestedIdentity) => {
                const userWithIdentity = user.toBuilder()
                    .nickname(suggestedIdentity.nickname())
                    .avatar(suggestedIdentity.avatar())
                    .build();
                return this._repository.save(userWithIdentity).then(() => {
                    return userWithIdentity;
                });
            });
    }

    /**
     * looks in the repository for suggested avatars and nicknames. The identity chosen is associated with the user
     * given for reference, record and to avoid assigning the same identity twice.
     *
     * npm run-script deploy:identities to reset to the countries in data/identities.json
     *
     * Also available at:
     * gs://school-tag-sbox.appspot.com/images/avatars/square-flags/country-avatars.json
     * @private
     * @param {string} userId of the user to be assigned the suggestion
     * @return {Promise.<User>} an identity to be automatically assigned
     */
    identitySuggested(userId) {
        const userIdPropertyName = 'userId';
        const identitySuggestedRepository = this.identitySuggestedRepository();
        //only get those identities without a user assigned already
        const query = identitySuggestedRepository._ref().orderByChild(userIdPropertyName).equalTo(null);
        return identitySuggestedRepository.listRef(query).then((availableIdentities) => {
            const identities = availableIdentities
                    // choose only player tags at level 1 which are identities for new tags
                    .filter( (identity) => identity._props.type === "player")
                    .filter( (identity) => identity._props.level=== 1);
            console.assert(identities.length > 0, "can't find suggested identities");
            //from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
            //could go into a utility
            let min = 0;
            let max = identities.length;
            min = Math.ceil(min);
            max = Math.floor(max);
            const randomIndex = Math.floor(Math.random() * (max - min)) + min;
            return identities[randomIndex];
        }).then((identity) => {
            //assign the user id to the identity chosen to avoid choosing it again
            const userIdPath = `${identity.nickname()}/${userIdPropertyName}`;
            return this.identitySuggestedRepository().set(userIdPath, userId).then(()=>{
                return identity;
            });
        });
    }

    /**
     * @private
     * @return {Repository.<User>}
     */
    identitySuggestedRepository() {
        return this._identitySuggestionRepositoryProvider();
    }

    /** Loads all users, without limits.  So if the game gets big (> 1000 users) this will need updating.
     *
     * @return {Promise<Object.<string,UserProperties>>}
     */
    allUsersMap() {
        return this._repository.allUsersMap();
    }
}

module.exports = UserService;
