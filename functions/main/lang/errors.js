const Builder = require("./builder");

/** field name of status code puposely matching errors.StatusCodeError */
const httpStatusCodeName = "statusCode";

/**
 * Standard Error factory with a code to uniquely identify.
 *
 * Error codes must be staticly declared and unique.  It is encouraged to use a pattern
 * of `${domain}/{adjective}` in a camelCase format.
 */
class Errors {

    /**
     * @param {string} message human readable for the programmer (not the end user)
     * @param {string} code uniquely identifying the error, so a programmer can write dependent logic
     * @param {number} [httpStatusCode] if there is a known mapping of status codes for this type of error.
     * @return {Error} with message and code given
     */
    static of(message, code, httpStatusCode) {
        console.assert(message, "provide a developer explanation of the error");
        const error = new Error(message);
        console.assert(code, "a unique code is required, 4 character random code is recommended");
        error.code = code;
        if (httpStatusCode) {
            error[httpStatusCodeName] = httpStatusCode;
        }
        return error;
    }

    /**
     * The code is a unique string for any error in the system.
     * @param {Error} error
     * @return {string|null} the error code if available, otherwise null
     */
    static code(error) {
        return (error && error.code) ? error.code : null;
    }

    /**
     *
     * @param {Error} error
     * @return {boolean} true if a status code has been assigned
     */
    static hasHttpStatusCode(error) {
        return (error) ? error.hasOwnProperty(httpStatusCodeName) : false;
    }

    /**
     * Checks if the error has the given status code.
     *
     * @param {Error} error
     * @param {number} statusCode
     * @return {boolean} true if the error has exactly the expected status code, false otherwise.
     */
    static hasHttpStatusCodeOf(error, statusCode) {
        return Errors.hasHttpStatusCode(error) && Errors.httpStatusCode(error) === statusCode;
    }

    /**
     *
     * @param {Error} error
     * @return {number} integer...one of http://www.restapitutorial.com/httpstatuscodes.html
     */
    static httpStatusCode(error) {
        return error[httpStatusCodeName];
    }

    /**
     *
     * @return {number} standard code for not found errors
     */
    static notFoundStatusCode() {
        return 404;
    }

    /**
     * @return {number} indicating an action attempted is not possible for the authenticated user
     */
    static forbiddenStatusCode() {
        return 403;
    }

    /**
     *
     * @param {Error} error
     * @return {{message: *, code}}
     */
    static json(error) {
        let json = {};
        if (error) {
            json.message = error["message"];
            json.code = error["code"];
        }
        if (Errors.hasHttpStatusCode(error)) {
            json[httpStatusCodeName] = Errors.httpStatusCode(error);
        }

        return json;
    }

    /**
     * @param {string} prefix qualifying what is not found, not the identifier
     * @return {string} the qualified prefix/notFound string
     */
    static notFound(prefix) {
        return `${prefix}/notFound`;
    }
}

module.exports = Errors;