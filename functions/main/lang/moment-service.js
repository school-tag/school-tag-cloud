const Moment = require("moment-timezone");
const MomentRange = require("moment-range");
const moment = MomentRange.extendMoment(Moment);
const DateUtils = require("../../main/lang/date-utils.js");

/**
 * provides times in a consistent manner across all services.
 *
 * Mostly keeps a consistent time zone matching the school.
 */
class MomentService {

    /**
     *
     * @param {string} timeZone
     */
    constructor(timeZone) {
        //matching https://momentjs.com/docs/#/manipulating/utc-offset/
        this._timeZone = timeZone;
        if (!timeZone) throw Error("timeZone is required");
    }

    /**
     *
     * @return {moment.Moment} in the correct tome zone
     */
    now() {
        return this.toLocal(moment().utc());
    }

    /**
     * @param {moment.Moment} original moment with any time zone, presumably UTC
     * @return {moment.Moment} in the time zone of the game
     */
    toLocal(original) {
        //const offset = moment.tz.zone(this._timeZone).utcOffset(original);
        return DateUtils.timestampMoment(original.clone().tz(this._timeZone).format());
    }

}

module.exports=MomentService;
