/**
 * Used to build any class by storing that which is given
 * and overriding the build method and calling your constructor with the properties in the correct order.
 *
 * This allows partial building and non-ordered calling of methods avoiding confusion when constructors change
 * or passing nulls for optional dependencies.
 *
 *
 * Usage:
 *   static builder() {
 *       return new class extends Builder {
 *           build() {
 *              return new MyClass(this.get(DependencyClass),
 *           }
 *        };
 *    }
 *
 *    const myObject = MyClass.builder().use(new DependencyClass()).build();
 * @template T
 *
 */
class Builder {

    /** depends upon the given
     * @param {*} dependecy
     * @param {string} [name]
     * @return {Builder}
     */
    and(dependecy, name) {
        return this.use(dependecy, name);
    }

    /**
     * depends upon the given.
     * @param {*} dependency
     * @param {string} [name] the name of the field.  the type is used if not provided
     * @return {Builder}
     */
    use(dependency, name) {
        const prototype = Object.getPrototypeOf(dependency);
        if (!name) {
            name = prototype.constructor.name;
        }
        console.assert(!this[name], this[name] + "is about to be clobbered.  provide a name to avoid");
        this[name] = dependency;
        return this;
    }

    /** accessor, typically used by the build method, to retrieve depedencies set
     * by use and and.
     *
     * @param {class|string} nameOrClass
     * @return {*}
     */
    get(nameOrClass) {
        let key;
        if (typeof nameOrClass == "string") {
            key = nameOrClass;
        } else {
            key = nameOrClass.prototype.constructor.name;
        }
        return this[key];
    }

    /**
     * @return {T}
     */
    build() {
        return new Error("override this with your build method");
    }

}

module.exports = Builder;