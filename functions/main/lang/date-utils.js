/**
 * Created by aaronroller on 6/12/17.
 */
const Moment = require("moment");
const MomentRange = require("moment-range");
const moment = MomentRange.extendMoment(Moment);
const console = require("console");

/**
 * Static utility methods useful for manipulating dates in a common way.
 */
class DateUtils {
    /**
     *
     * @param {String|Date|null} value converts the string into a date, if possible.
     * @return {Date|null}
     */
    static asDate(value) {
        let asDate;
        if (value == null) {
            asDate = null;
        } else {
            if (value instanceof Date) {
                asDate = value;
            } else {
                asDate = new Date(value);
            }
            console.assert(!isNaN(asDate.getTime()), value + " is not a valid date");
        }
        return asDate;
    };

    /** Parses the isoString while preserving the original time zones
     *
     * @param  {string} isoTimeInterval
     * @return {MomentRange}
     */
    static momentRange(isoTimeInterval) {
        console.assert(typeof isoTimeInterval == "string", "can only accept string, but was " + typeof isoTimeInterval);
        const parts = isoTimeInterval.split("/");
        const isoStart = parts[0];
        const isoEnd = parts[1];
        const start = moment.parseZone(isoStart);
        const end = moment.parseZone(isoEnd);
        return moment.range(start, end);
    };

    /** Isolates the building of timestamp into a single place.
     *
     * @param {string|moment.Moment|Date|null} dateOrString
     * @return {moment.Moment|null}
     */
    static timestampMoment(dateOrString) {
        return (dateOrString == null) ? null : moment.parseZone(dateOrString, moment.ISO_8601);
    };

    /**
     * @param {moment.Moment|Date|string|null} [momentDateOrString]
     * @return {string|null} the formatted string
     */
    static timestampMomentPortable(momentDateOrString) {
        return (momentDateOrString == null) ? null : DateUtils.timestampMoment(momentDateOrString).format();
    }

    /**
     * formats the moment into a date string, in iso format.
     *
     * @param {moment.Moment|Date|string|null} [momentDateOrString]
     * @return {string|null} in YYYY-MM-DD
     */
    static datePortable(momentDateOrString) {
        return (momentDateOrString === null) ? null : DateUtils.timestampMoment(momentDateOrString).format("YYYY-MM-DD");
    }

    /**
     *
     * @param {moment.Moment} moment the moment representing the day to represent the range.
     * @return {MomentRange} start and end of the day for the date of the moment, in the proper time zone.
     */
    static dayMomentRange(moment) {
        return moment.range(UnitOfTime.day());
    }

    /**
     *
     * @param {moment.Moment} timestamp
     * @return {MomentRange}
     */
    static weekMomentRange(timestamp) {
        const mondayStart = moment(timestamp).startOf(UnitOfTime.week());
        const sundayEnd = moment(timestamp).endOf(UnitOfTime.week());
        return moment.range(mondayStart, sundayEnd);
    }

    /**
     * @name weekdayMomentRange
     * @param {number} weekday the day of the week
     * @return {MomentRange} representing the day
     */
    /** Given the moment, this will return a function that accepts a day that
     * will be assigned to the moment and create the moment day range for that day.
     *
     * @param {moment.Moment} weekMoment
     * @return {weekdayMomentRange}
     */
    static weekdayMomentRangeFunction(weekMoment) {
        return function (weekday) {
            return DateUtils.dayMomentRange(moment(weekMoment)
                .weekday(weekday)
                .hour(0)
                .minute(0)
                .second(0)
                .millisecond(0));
        };
    }

    /** The week number is provided to the function returned from this method
     * and the range for the week is returned.
     *
     * @param {moment.Moment} yearMoment for the year of interest
     * @return {Function}
     */
    static weekMomentRangeFunction(yearMoment) {
        return function (weekOfYear) {
            return DateUtils.weekMomentRange(moment(yearMoment)
                .week(weekOfYear)
                .weekday(DateUtils.DayOfWeek.monday())
                .hour(0)
                .minute(0)
                .second(0)
                .millisecond(0));
        };
    }

    /**
     *
     * @param {Moment|Number} momentOrMonth
     * @return {number} the one indexed version of the month matching common language
     */
    static isoMonth(momentOrMonth) {
        let momentMonth = (Number.isInteger(momentOrMonth)) ? momentOrMonth : momentOrMonth.month();
        return Number.isInteger(momentMonth) ? momentMonth + 1 : momentMonth;
    }

    /**
     * adjusts the iso month, which is normal human speak, converting it to
     * zero based month for moment since apparently computer scientists are still coming up
     * with reasons to make programming difficult and un-fun for the masses.
     *
     * @param {Number} month in iso 1-based terms
     * @return {number} the moment month in zero-based terms
     */
    static momentMonthFromIso(month) {
        return month - 1;
    }

    /**
     * @param {Moment} moment
     * @return {int} the day number of the month.
     */
    static dayOfMonth(moment) {
        return moment.date();
    }

    /**
     *FIXME: moment-range does not support isoWeek.
     *
     * @param {moment.Moment} moment
     * @return {MomentRange} for the entire week that the given moment occurs
     */
    // static weekMomentRange(moment) {
    //     return moment.range('isoWeek');
    // }
}

/**Constants that work with moment and moment range.
 * see moment#UnitOfTime which is not able to be referenced directly
 *
 * https://momentjs.com/docs/#/manipulating/start-of/
 */
class UnitOfTime {
    /**@return {String} day or date*/
    static day() {
        return "day";
    }

    /**@return {string} week with Monday as the first day*/
    static week() {
        return "isoWeek";
    }

    /**@return {string} calendar month*/
    static month() {
        return "month";
    }

    /**@return {string} year */
    static year() {
        return "year";
    }

    /**@return {string} hour */
    static hour() {
        return "hour";
    }
}

/**
 * Number constants used to reference specific days of the week used with moment.dayOfWeek.
 */
class DayOfWeek {
    /**
     * @return {number} representing monday in isoweek
     */
    static monday() {
        return 1;
    }

}

module.exports = DateUtils;
module.exports.UnitOfTime = UnitOfTime;
module.exports.DayOfWeek = DayOfWeek;
