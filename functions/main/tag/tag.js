const appRootDir = require("app-root-dir").get();
const Enum = require("enum");
const console = require("console");
const Entity = require(appRootDir + "/main/entity/entity.js");
const User = require(appRootDir + "/main/user/user.js");
const UserId = require(appRootDir + "/main/user/user.js").UserId;

/** Any NFC/RFID/Barcode scan that can produce a unique identifier.
 * Examples may be clipper card or NFC stickers.
 * A tag is owned by and represents a single user. Although the owner may change over time, this
 * relationship is the current user only for convenience.
 * The user can manage history of tag ownership in those cases.
 *
 * @extends {Entity.<Tag>}
 */
class Tag extends Entity {
    /**
     * @private
     * @param {*} [properties]   that is the properties or null.
     * @constructor
     */
    constructor(properties) {
        super(properties);
    }

    /** The unique identifier for the tag that never changes.
     * @alias id
     * @return {string}
     */
    tagId() {
        return super.id();
    }

    /** Provides the identify of the user who is currently to get credit for using this tag.
     * TODO: keep a history of previous owners
     *
     * @return {UserId}
     */
    userId() {
        return new UserId(this._property(Tag.prototype.userId.name));
    }

    /**
     * @return {boolean} true if an owner id is assigned
     */
    hasUserId() {
        return this._hasProperty(Tag.prototype.userId.name);
    }

    /**
     * purposely general allowing flexibility to be used in multiple ways.
     * Example, bike is one category indiciating the tag is used for biking only. Issue #9
     *
     * @return {string} identifying the tag is used for a specific purpose.
     *
     */
    category() {
        return this._property(Tag.prototype.category.name);
    }

    /**
     *
     * @return {boolean} true if category is assigned, false otherwise
     */
    hasCategory() {
        return this._hasProperty(Tag.prototype.category.name);
    }

    /** Indicates if the tag is a station.  Absent means it is a personal tag.
     *
     * @return {string} of the type, if available
     */
    type() {
        return this._property((Tag.prototype.type.name));
    }

    /**
     * Some tags have points, like tokens or stations.  If provided, these points would be rewarded rather than
     * the default in the rule.
     * @return {number}
     */
    points() {
        return this._property((Tag.prototype.points.name));
    }

    /**
     * @see points
     * @return {boolean}
     */
    hasPoints() {
        return this._hasProperty((Tag.prototype.points.name));
    }

    /**
     * @return {boolean} true if type exists
     */
    hasType() {
        return this._hasProperty(Tag.prototype.type.name);
    }

    /**
     * @return {TagBuilder}
     */
    toBuilder() {
        return super.toBuilder();
    }

    /** Builds all tags to be mutated.
     *
     * @param {Tag} [tag]  the optional existing tag to mutate
     * @return {TagBuilder}
     */
    static builder(tag) {
        if (tag instanceof Tag) {
            return new TagBuilder(tag);
        } else {
            return new TagBuilder(new Tag(tag));
        }
    };
}

module.exports = Tag;

// ================== Builder Methods ==========================

/** Private constructor used to create the new builder and avoid
 * mutable objects.
 *
 * @extends {Entity.EntityBuilder.<Tag>}
 */
class TagBuilder extends Entity.EntityBuilder {
    /**
     *  * Use #builder
     *
     * @param {Tag|*} [mutant]
     */
    constructor(mutant) {
        super(mutant);
    }

    /**
     *
     * @param {UserId} userId
     * @return {TagBuilder}
     */
    userId(userId) {
        const value = (userId instanceof UserId) ? userId.toString() : userId;
        console.assert(typeof value === "string", "userId must be a userId or string, but was " + typeof userId);
        this._property(Tag.prototype.userId.name, value);
        return this;
    }

    /**
     * @param {string} value
     * @return {TagBuilder}
     */
    category(value) {
        this._property(Tag.prototype.category.name, value);
        return this;
    }

    /**
     * @param {string} value
     * @return {TagBuilder}
     */
    type(value) {
        return this._property(Tag.prototype.type.name, value);
    }

    /**
     * @param {number} value for the number of points to rewarded for this tag
     * @return {TagBuilder}
     */
    points(value) {
        return this._property(Tag.prototype.points.name, value);
    }

    /**
     * @override
     * @return {Tag}
     */
    build() {
        return super.build();
    }
}
