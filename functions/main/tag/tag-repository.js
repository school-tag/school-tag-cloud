const Tag = require("../../main/tag/tag.js");
const Repository = require("../../main/entity/repository.js");

/**
 * @extends {Repository<Tag>}
 */
class TagRepository extends Repository {
    /**
     * @param {Reference} reference the reference that matches the functions of Firebase reference.
     * @param {Audit} auditTemplate to record modifications
     * @constructor
     */
    constructor(reference, auditTemplate) {
        super(reference, "tags", Tag, auditTemplate);
    }
}

module.exports = TagRepository;

