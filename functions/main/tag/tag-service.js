const Tag = require("../../main/tag/tag");
const UserId = require("../../main/user/user").UserId;
const Errors = require("../../main/lang/errors");
const {PubSub} = require("@google-cloud/pubsub");
const env = require("env-var");
const associateTagServiceKeyFile = env("npm_package_config_associateTagServiceKeyFile").asString();
const notAuthorizedToCreateNewUserErrorCode = "nsk9";

/** Connected business logic for Tag.
 *
 */
class TagService {


    /**
     * @param {TagRepository} repository  persistence for tags
     * @param {UserService} userService  connected logic for users
     * @param {String} environment used to associate a tag to to a school.
     * @param {PubSub} pubSub for publishing tag association messages
     */
    constructor(repository, userService, environment, pubSub) {
        /**
         * The repository that provides persistence management for all Tags.
         * @private
         * @type TagRepository
         */
        this._repository = repository;
        this._userService = userService;
        this._environment = environment;
        this._pubSub = pubSub;
    }

    /** Given the tagId alone, this will create a tag with the id
     * OR find one that already exists.
     *
     *
     * A new user will be created and associated with the newly created tag.
     *
     * TODO: Merging users if multiple are created
     *
     * @param {string} tagId  will be used to create or find
     * @param {Object} [options]
     * @return {Promise.<Tag>} provides the tag or the error
     */
    tagFoundOrCreated(tagId, options) {
        const service = this;
        return this.tagById(tagId).catch(
            (error) => {
                if (Errors.hasHttpStatusCode(error)) {
                    return service.tagAndUserCreated(tagId, options);
                } else {
                    console.error(`tag ${tagId} had an unhandled error: nns2`, error);
                    return Promise.reject(error);
                }
            });
    }

    /**
     * @param {string} tagId
     * @return {Promise.<Tag>} provides the tag or the error
     */
    tagById(tagId) {
        return this._repository.get(tagId);
    }

    /** Creates a new user and assigns the user ID to the newly created tag.
     *
     * options:
     * 1. assign a default category
     * 2. assign the tag to the user given, instead of creating
     *
     * @see TagService.categoryOption
     * @see TagService.associateTagToUser
     *
     * @param {string} tagId
     * @param {Object} [options] to use when creating entities
     * @return {Promise.<Tag>}
     */
    tagAndUserCreated(tagId, options) {
        const repository = this._repository;
        const associateToUser = TagService.associateTagToUser(options);
        let userPromise;
        //enforce authorization before creating user or tag
        if (TagService.authorizedToCreateOption(options)) {
            if (associateToUser) {
                //TODO: validate the user exists
                const userIdToAssociate = new UserId(associateToUser);
                userPromise = this._userService.user(userIdToAssociate);
            } else {
                if (TagService.associatedUserShouldNotBeCreated(options)) {
                    userPromise = Promise.resolve();
                } else {
                    userPromise = this._userService.createUser();
                }
            }
        } else {
            userPromise = Promise.reject(Errors.of(`Not authorized to create a new user or tag`,
                notAuthorizedToCreateNewUserErrorCode, Errors.forbiddenStatusCode()));
        }
        return userPromise.then((user) => {
            //user may not be provided if no user is to be associated.
            const category = TagService.categoryOption(options);
            const tagBuilder = Tag.builder();
            if (user && user.hasId()) {
                tagBuilder.userId(user.id());
            }
            if (category) {
                tagBuilder.category(category);
            }
            const type = TagService.typeOption(options);
            if (type) {
                tagBuilder.type(type);
            }
            const tag = tagBuilder.id(tagId).build();
            return repository.save(tag).then(() => {
                return tag;
            });
        });
    }

    /**
     * Associates the tag to a school so an observer can view the player's scoreboard when scanning the tag.
     *
     * See https://bitbucket.org/school-tag/school-tag-marketing
     *
     * @param {String} tagId is the nfc uid of the school tag
     * @return {Promise<string>} with the messageId for the pub/sub or the reason for the failure.
     */
    associateToSchool(tagId) {
        const topicName = "projects/school-tag/topics/associate-tag";
        const schoolCode = this._environment;
        const data = {uid: tagId, school: schoolCode};
        //see project README.md project setup to grant access to calling project
        return this._pubSub.topic(topicName).publishJSON(data);
    }

    /** An option, passed in with a scan, to associate a tag to a user.
     * This is a two-way method that will assign or extract the user id.
     *
     * @param {Object} options
     * @param {string} [userId]
     * @return {string|*} userId that is to be associated
     */
    static associateTagToUser(options, userId) {
        return TagService.option(options, userId, TagService.associateTagToUser.name);
    };

    /**Since the default is to create a new user when reading a tag, this will inhibit creating a new user when
     * indicated to do so.
     *
     * @param {Object} options
     * @param {boolean} [shouldNotCreate], if true, indicates a new user will not be created, null or false indicates it will
     * @return {*}
     */
    static associatedUserShouldNotBeCreated(options, shouldNotCreate) {
        return TagService.option(options, shouldNotCreate, TagService.associatedUserShouldNotBeCreated.name);
    }

    /**
     * Dual purpose method to extract the default category, if available or assign the default category
     * if one is given.
     * @param {Object} options where default category is to live
     * @param {string} [category] if provided, this will be assigned to the given object
     * @return {string|*}
     */
    static categoryOption(options, category) {
        return TagService.option(options, category, TagService.categoryOption.name);
    };

    /**
     *
     * @param {object} options
     * @param {string} [type] id (tag,qrc,station, etc)
     * @return {string|*}
     */
    static typeOption(options, type) {
        return TagService.option(options, type, TagService.typeOption.name);
    }

    /**
     *
     * @param {*} options
     * @param {boolean} [authorized] if true, then the tag can be created...otherwise it shouldn't
     * @return {boolean|*}
     */
    static authorizedToCreateOption(options, authorized) {
        return TagService.option(options, authorized, TagService.authorizedToCreateOption.name);
    }

    /**
     *
     * @param {object} options to be mutated or containing the option to be extracted
     * @param {object|string} toBeAssigned value of the option
     * @param {string} key of the option (use the name of the method assigning the option)
     * @return {*} if a key is provided, the options are returned...
     *                  otherwise the value in the options represented by the key
     */
    static option(options, toBeAssigned, key) {
        let result = null;
        if (options) {
            if (toBeAssigned) {
                options[key] = toBeAssigned;
                result = options;
            } else {
                result = options[key];
            }
        }
        return result;
    };
}

module.exports = TagService;
module.exports.notAuthorizedToCreateNewUserErrorCode = notAuthorizedToCreateNewUserErrorCode;