### Tag 

Tags represent a unique identifier that is communicated using NFC, RFID, Bar Code scanning, etc.  
A tag is owned by a [user][1], but a user may have multiple tags.

Example Tags:
* NFC Stickers
* Trasnit Cards (Clipper)

The id of a tag is used directly since it is unique.


[1]: ../user