/**
 * Provides the version of the application by providing the build context.
 */
class ApplicationVersion {

    /**Properties providing the data for the accessors.
     *
     * @see ../../generated/application-env.js
     *
     * @param {*} json
     */
    constructor(json) {
        this._props = json;
    }

    /**
     * References the branch this build represents
     * @return {string}
     */
    branchUrl() {
        return this._props.branchUrl;
    }

    /**
     * References the latest commit that represents this build.
     * @return {string}
     */
    commitUrl() {
        return this._props.commitUrl;
    }

    /**
     *
     * @return {string} http reference to the build that creating this version
     */
    buildUrl() {
        return this._props.buildUrl;
    }

    /**
     * @see buildUrl
     * @return {number} a unique number referencing the build that created this version of the software
     */
    buildNumber() {
        return (this._props.buildNumber != null) ? Number.parseInt(this._props.buildNumber) : null;
    }

    /**
     * @return {string} http reference to request the source of this descriptor
     */
    selfUrl() {
        return this._props.selfUrl;
    }

    /**
     * @return {string} url referencing the root of the user facing web application paired with the cloud
     */
    webApplicationUrl() {
        return this._props.webApplicationUrl;
    }

    /**
     * @return {string} url referencing the root of the web endpoint where the http functions are hosted.
     */
    functionsUrl() {
        return this._props.functionsUrl;
    }

    /**The system this code is targeting: sandbox, playground, demo, wca, etc.
     *
     * @return {string}
     */
    environment() {
        return this._props.environment;
    }

}

module.exports = ApplicationVersion;
