const console = require("console");
const Operator = require("json-rules-engine").Operator;
const moment = require("moment");
const BusinessMoment = require("moment-business");

/**
 *
 */
class MomentRuleOperators {

    /**
     *
     * @return {Operator} that returns true if the moment given is at or after the time of day given
     */
    static afterTimeOfDayInclusive() {
        return operator(afterTimeOfDayInclusive);
    }

    /**
     * @return {Operator} that returns true if the fact (timestamp) is before or at the time of day given
     */
    static beforeTimeOfDayInclusive() {
        return operator(beforeTimeOfDayInclusive);
    }

    /**
     *
     * @return {Operator} that returns true if the moment given after the time of day given
     */
    static afterTimeOfDayExclusive() {
        return operator(afterTimeOfDayExclusive);
    }

    /**
     * @return {Operator} that returns true if the fact (timestamp) is before the time of day given
     */
    static beforeTimeOfDayExclusive() {
        return operator(beforeTimeOfDayExclusive);
    }

    /**
     * Inspects a timestamp looking for a weekday (M-F). Compares the result to the desired value
     * of true or false provided. It looks at only the date portion so ensure the time is in the time
     * zone desired ( moment.parseZone is used).
     *
     * {
     *  fact: "myMoment",
     *  operator: "isWeekDay",
     *  value: true,
     * }
     *
     * @return {Operator} that allows testing of weekdays.
     */
    static isWeekday() {
        return operator(isWeekday);
    }

    /**
     * Easy access to all operators defined in this class.
     *
     * @return {Operator[]}
     */
    static all() {
        return [
            MomentRuleOperators.afterTimeOfDayInclusive(),
            MomentRuleOperators.afterTimeOfDayExclusive(),
            MomentRuleOperators.beforeTimeOfDayInclusive(),
            MomentRuleOperators.beforeTimeOfDayExclusive(),
            MomentRuleOperators.isWeekday()];
    }
}

/** Documentation for the general interface of the functions that provide operations.
 *
 * TODO: support time zones by proviiding the time zone with the timeOfDay, without a date
 * the date from the timestamp could be used to compolete the time of day and provide a
 * corss time zone comparison using moments.
 *
 * TODO: support accepting moments
 *
 @name comparisonFunctionSpecs
 @function
 @param {String} factTimestamp The moment in time that is a face, in ISO format with zone
 @param {String} timeOfDay the time portion of an ISO timestamp, local to the zone of the fact
 */

/**
 *
 * @param {function} operatorFunction the callback required by the operator
 * @return {Operator}
 */
const operator = function (operatorFunction) {
    return new Operator(operatorFunction.name, operatorFunction);
};

/**
 *
 * @param {String} factTimestamp, in ISO Format, when something happened
 * @param {String} timeOfDay the required target time
 * @return {boolean} true if factTimestamp is after or equal to timeOfDay
 */
const afterTimeOfDayInclusive = function (factTimestamp, timeOfDay) {
    return timeOfDayComparison(factTimestamp, timeOfDay, this.name, (factTimeString, timeOfDayString) => {
        return factTimeString >= timeOfDayString;
    });
};

/**
 *
 * @param {string} factTimestamp
 * @param {string} timeOfDay
 * @return {boolean} true if the factTimestamp is before or at the time of day
 */
const beforeTimeOfDayInclusive = function (factTimestamp, timeOfDay) {
    return timeOfDayComparison(factTimestamp, timeOfDay, this.name, (factTimeString, timeOfDayString) => {
        return factTimeString <= timeOfDayString;
    });
};

/**
 *
 * @param {String} factTimestamp, in ISO Format, when something happened
 * @param {String} timeOfDay the required target time
 * @return {boolean} true if factTimestamp is after  to timeOfDay
 */
const afterTimeOfDayExclusive = function (factTimestamp, timeOfDay) {
    return timeOfDayComparison(factTimestamp, timeOfDay, this.name, (factTimeString, timeOfDayString) => {
        return factTimeString > timeOfDayString;
    });
};

/**
 *
 * @param {string} factTimestamp
 * @param {string} timeOfDay
 * @return {boolean} true if the factTimestamp is before  the time of day
 */
const beforeTimeOfDayExclusive = function (factTimestamp, timeOfDay) {
    return timeOfDayComparison(factTimestamp, timeOfDay, this.name, (factTimeString, timeOfDayString) => {
        return factTimeString < timeOfDayString;
    });
};

/**
 * Compares the fact to the timeOfDay by using string comparisons of the time of day given vs the time
 * portion of the fact given.
 *
 * @param {String} factTimestamp a moment with date and time (in ISO format) that is the fact to qualify with  zone
 * @param {String} timeOfDay the time being compared, in local time of the zone of the timestamp
 * @param {String} operatorName method name invoked indicating how to compare
 * @param {Function} comparisonFunction with a left param as the arrival time and right param as the timeOfDay
 * @return {boolean} true if the the given values meet the condition indicated by the operator name
 */
const timeOfDayComparison = function (factTimestamp, timeOfDay, operatorName, comparisonFunction) {
    const factTime = moment.parseZone(factTimestamp).format("HH:mm:ss");
    return comparisonFunction(factTime, timeOfDay);
};

/**
 * see https://github.com/jmeas/moment-business
 * @param {String} factMomentString the timestamp to be tested if it is a weekday, in a parseable format in the local time zone.
 * @param {boolean} expectingWeekday true if weekdays  are sought, false if weekends are sought
 * @return {boolean} true for weekdays, false otherwise
 */
const isWeekday = function (factMomentString, expectingWeekday) {
    if (factMomentString == null) {
        return false;
    }
    const factMoment = moment.parseZone(factMomentString);
    return BusinessMoment.isWeekDay(factMoment) === expectingWeekday;
};

module.exports = MomentRuleOperators;
/**
 * visible for testing
 * @private
 */
module.exports._timeOfDayComparison = timeOfDayComparison;
