const Operator = require("json-rules-engine").Operator;
const Rule = require("json-rules-engine").Rule;
const Fact = require("json-rules-engine").Fact;

const appRootDir = require("app-root-dir").get();
const console = require("console");
const Database = require("firebase").database.Database;
const DataSnapshot = require("firebase").database.DataSnapshot;
const Promise = require("firebase").Promise;
const FirebaseError = require("firebase").FirebaseError;
const Repository = require(appRootDir + "/main/entity/repository.js");
const FirebaseUtils = require(appRootDir + "/main/firebase/firebase-utils");

/**
 * Repository that provides all rules in the system.
 */
class RuleRepository {
    /**
     * @param {Rule[]} defaultRulesArray
     * @param {Operator[]} operators
     * @param {Fact[]} facts that retrieve data from other repositories, services, etc.
     * @param {Repository} repository that overrides the provided components, if available
     */
    constructor(defaultRulesArray, operators, facts, repository) {
        this._operators = operators;
        this._facts = facts;
        if (defaultRulesArray != null) {
            //convert the array to an object for direct rule access
            const rules = {};
            defaultRulesArray.forEach((rule) => {
                rules[rule.event.type] = rule;
            });
            this._rulesPromise = Promise.resolve(rules);
            this._rulesArrayPromise = Promise.resolve(defaultRulesArray);
        } else {
            this._rulesPromise = repository.get();

            //rules engine likes the array
            this._rulesArrayPromise = this._rulesPromise.then((databaseRules) => {
                    this._rulesArray = databaseRules.toArray();
                    return this._rulesArray;
                }
            );
        }

    }

    /**
     * @return {Promise.<Rule[]>} eventually containing all rules
     */
    rules() {
        return this._rulesArrayPromise;
    }

    /** Provides a rule by id, otherwise null if not found...which shouldn't happen often.
     *
     * @param {string} ruleId
     * @return {Promise.<Rule>}
     */
    rule(ruleId) {
        return this._rulesPromise.then((rules) => {
            if (rules.hasOwnProperty(ruleId)) {
                return rules[ruleId];
            } else {
                return Promise.reject(`looking for non-existent rule ${ruleId}: dn39`);
            }
        });
    }

    /**
     * see https://github.com/CacheControl/json-rules-engine/blob/master/docs/engine.md#engineaddoperatorstring-operatorname-function-evaluatefuncfactvalue-jsonvalue
     * See the Operator class in the operator.js of the library package for the most details.
     *
     * @return {Operator[]} the operators referenced in the conditions of the rules
     */
    operators() {
        return this._operators;
    }

    /**
     *
     * @return {Fact[]} providing dynamically resolving facts
     */
    facts() {
        return this._facts;
    }

    /**
     * @return {string} the restful path element
     */
    static pathName() {
        return "rules";
    }
}

module.exports = RuleRepository;
