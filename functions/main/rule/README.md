Rules determine user achievements by inspecting [scans](../scan/README.md)
against a set of rules provided for the game.

See [json-rules-engine](https://www.npmjs.com/package/json-rules-engine) to understand more.

Classes found in this directory process rules generically whereas the [rules of the game](../game/README.md)
provide the logic applied to school tag. 


[Rule Service](rule-service.js) is a service that works with a [repository](rule-repository.js) of 
rules that delegates most of the work
to the json rules engine.  
The rule repository does not provide persistence, but rather provides the data that the rule service uses regardless of the source.

