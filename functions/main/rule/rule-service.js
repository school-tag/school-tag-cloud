const appRootDir = require("app-root-dir").get();
const console = require("console");
const DateUtils = require(appRootDir + "/main/lang/date-utils.js");
const Scan = require(appRootDir + "/main/scan/scan.js");
const RuleRepository = require(appRootDir + "/main/rule/rule-repository.js");
const Reward = require(appRootDir + "/main/game/reward.js");
const UserRewards = require(appRootDir + "/main/game/user-rewards.js");
const Engine = require("json-rules-engine").Engine;
const Fact = require("json-rules-engine").Fact;
const Operator = require("json-rules-engine").Operator;

/**
 *  Applies the rules, given the facts of scans and user summaries,
 * to provide an {Reward} and increase (points, badges and virtual coins).
 *
 * A {Rule} is provided by the {RuleRepository}.
 *
 * @type {RuleService}
 */
class RuleService {
    /**
     *
     * @param {RuleRepository} repository
     * @constructor
     */
    constructor(repository) {
        this._ruleRepository = repository;
        this._enginePromise = repository.rules()
            .then((rules) => {
                /** The rules engine that applies the rule logic to the facts given.
                 * https://github.com/CacheControl/json-rules-engine/blob/master/docs/engine.md
                 *
                 * @member {Engine}
                 * @type {Engine}
                 * @private
                 */
                const engine = new Engine();

                for (let i = 0; i < rules.length; i++) {
                    const rule = rules[i];
                    engine.addRule(rule);
                }

                const operators = repository.operators();
                if (operators !== null) {
                    operators.forEach((operator) => {
                        engine.addOperator(operator.name, operator.cb);
                    });
                }

                const facts = repository.facts();
                if (facts !== null) {
                    facts.forEach((fact) => {
                        if (fact.calculationMethod) {
                            engine.addFact(fact.id, fact.calculationMethod, fact.options);
                        } else {
                            engine.addFact(fact.id, fact.value);
                        }
                    });
                }
                return engine;
            });

    }

    /**
     * Runs the rules engine with the given facts and returns a promise
     * of the events that match the rule.
     *
     * @param {*} facts
     * @return {Promise.<Event[]>}
     */
    run(facts) {
        return this._enginePromise.then((engine) => {
            return engine.run(facts);
        });
    }

}

module.exports = RuleService;
