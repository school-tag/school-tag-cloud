const Rule = require("json-rules-engine").Rule;

const appRootDir = require("app-root-dir").get();
const console = require("console");
const Entity = require(appRootDir + "/main/entity/entity.js");
const EntityBuilder = require(appRootDir + "/main/entity/entity.js").EntityBuilder;

/**
 * Container for rules as they come from the datastore. Since Rule is not an entity
 * this acts as a helper to have all the rules in one place and convert them into a rule.
 * @extends {Entity.<Rules>}
 */
class Rules extends Entity {

    /**
     * @private
     * @param {*} properties
     */
    constructor(properties) {
        super(properties);
    }

    /**The constructor for immutable Rules.
     *
     * @param {Rules} rules
     * @return {RulesBuilder}
     */
    static builder(rules) {
        if (rules instanceof Rules) {
            return new RulesBuilder(rules);
        } else {
            return new RulesBuilder(new Rules(rules));
        }
    }

    /**
     * Provides the rules as an array.
     * @return {Rule[]}
     */
    toArray() {
        const rules = [];
        const properties = this.portable();
        Object.keys(properties).forEach((ruleKey) => {
            const ruleProperties = properties[ruleKey];
            if (ruleProperties instanceof Object) {
                rules.push(new Rule(ruleProperties));
            }
        });
        return rules;
    }
}

/**
 * Builds the Rules entity.
 * @extends {EntityBuilder.<Rules>}
 */
class RulesBuilder extends EntityBuilder {
    /**
     *
     * @param {*} mutant?
     */
    constructor(mutant) {
        super(mutant);
    }

    /**
     *
     * @return {Rules}
     */
    build() {
        return super.build();
    }
}

module.exports = Rules;
module.exports.RulesBuilder = RulesBuilder;