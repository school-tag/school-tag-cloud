const console = require("console");
const School = require("./school.js");
const SchoolRepository = require("./school-repository.js");
const moment = require("moment");

/** Access to school information 
 *
 * @property {SchoolRepository} _repository for persistence management
 */
class SchoolService {
    /**
     *
     * @param {SchoolRepository} repository
     */
    constructor(repository) {
        this._repository = repository;
    }

    /**
     * Access to the only school information available in the system.
     * @return {Promise.<School>} a promise to provide the school information for the system
     */
    school() {
        return this._repository.get();
    };

    /**
     * Access to the only school information available in the system.
     * @param {moment.Moment} original  the original moment given that will remain unmodified
     * @return {Promise.<moment.Moment>} a promise to provide the moment given in the time zone of the school
     */
    momentInLocalTime(original) {
        return this.school().then( (school) => {
            return original.clone().tz(school.timeZone);
        });
    }
}

module.exports = SchoolService;
