const Entity = require("../../main/entity/entity.js");

/**
 * Information about the school where the game is played.
 *
 * @extends {Entity.<School>}
 */
class School extends Entity {
    /** @private
     * @see builder()
     * @param {*} [properties] in portable form
     */
    constructor(properties) {
        super(properties);
    }

    /** The full name of the school.
     *
     * @return {string}
     */
    name() {
        return this._property(School.prototype.name.name);    }

    /**
     * @see https://momentjs.com/timezone/
     *
     * @return {string} the time zone from the momemnt list
     */
    timeZone() {
        return this._property(School.prototype.timeZone.name);
    }

}

/**
 * @type {School}
 */
module.exports = School;

