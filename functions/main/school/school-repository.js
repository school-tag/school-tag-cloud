const Database = require("firebase").database.Database;
const School = require("./school.js");
const Repository = require("../../main/entity/repository.js");
const Audit = require("../../main/entity/repository.js").Audit;

/**
 * @extends {Repository.<School>}
 */
class SchoolRepository extends Repository {
    /**
     * @param  {Database} database
     * @param {Audit} auditTemplate indicating the source
     */
    constructor(database, auditTemplate) {
        super(database, "school", School, auditTemplate);
    }
}

module.exports = SchoolRepository;
