
# Code 

## Files and Directories

* organized in directories by `subject` or `domain`, generally not technology or system component
* `lowercase-with-hyphens` for files and directories alike
* avoid acronyms and abbreviations
* includes `component` in the filename (user-repository.js)
* includes `system` in the filename describing external dependencies (user-firebase-functions.js)

## Component Design

* [entity](entity) Base for all persisted models with ids, named by purpose ([user](user),[tag](tag), etc)
  * Immutable
  * Private properties accessible only by functions
  * Associated builders used for creation/cloning/mutation
* [entity-repository](entity) Class with fine grained methods to help with persistence and searching for a specific `domain`
  * Database is provided matching the Firebase.database interface
* `entity-service` Stateless class providing business logic related to entities using the `repository` for persistence needs

[More details](entity/README.md) about entities


## ECMAScript 6.0 Language

[ECMAScript 6](http://es6-features.org/) and 

### Variable Declaration

* [`const`](http://es6-features.org/#Constants) is used in almost all situations since re-assigning variables can be confusing.
* `let` is used only in situations where re-assignment is required or makes sense.
* `var` is never used.

## Dates
Dates are managed using [moment.js](https://momentjs.com/) library.

The entity property is the ISO String with local time zone `2017-06-08T20:43:59-07:00`.

[moment().parseZone()](https://momentjs.com/docs/#/parsing/parse-zone/) must be used to preserve the time zone when parsing.

[moment().local().format()](https://momentjs.com/docs/#/manipulating/local/) must be used to create the ISO string with TimeZone.

