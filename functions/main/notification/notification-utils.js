/**
 * Static helper for dealing with Notifications.
 *
 * Keep the noise out of the service, which is harder to test.
 *
 */
class NotificationUtils {

    /**
     *
     * @param {Message} message
     * @return {*}
     */
    static messageToNotificationPayload(message) {
        const payload = {};
        const notification = {};
        payload.notification = notification;

        if (message.hasText()) {
            notification.body = message.text();
        }
        if (message.hasIconUrl()) {
            notification.icon = message.iconUrl();
        }
        if (message.hasMoreInfoUrl()) {
            notification.click_action = message.moreInfoUrl();
        }
        if (message.hasTitle()) {
            notification.title = message.title();
        }
        return payload;
    }
}

module.exports = NotificationUtils;
