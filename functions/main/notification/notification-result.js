const Message = require("../../main/message/message.js");
const Entity = require("../../main/entity/entity.js");

/**A record of a message sent to a recipient.
 */
class NotificationResult extends Entity {

    /**
     * @param {*} properties
     */
    constructor(properties) {
        super(properties);
    }

    /**
     * @return {Message}
     */
    message() {
        return Message.builder(this._property(NotificationResult.prototype.message.name)).build();
    }

    /**FCM token for Firebase Cloud Messaging, for example.
     *
     * @return {string} the token used to notify the recipient
     */
    recipientId() {
        return this._property(NotificationResult.prototype.recipientId.name);
    }

    /**
     * @return {string} unique code identifying the type of error
     */
    errorCode() {
        return this._property(NotificationResult.prototype.errorCode.name);
    }

    /**
     * @return {string} explanation of what went wrong
     */
    errorMessage() {
        return this._property(NotificationResult.prototype.errorMessage.name);
    }

    /**
     * @return {boolean} true if an error code is set.
     */
    isErroneous() {
        return this._hasProperty(NotificationResult.prototype.errorCode.name);
    }

    /**
     *
     * @return {string} identifying the successful record
     */
    senderRecordId() {
        return this._property(NotificationResult.prototype.senderRecordId.name);
    }

    /**
     *
     * @return {boolean} true if the message was sent. Indicates there is a sender record id.
     */
    isSuccessful() {
        return this._hasProperty(NotificationResult.prototype.senderRecordId.name);
    }

    /**
     *
     * @param {NotificationResult|*} [notification]
     * @return {NotificationBuilder}
     */
    static builder(notification) {
        if (notification instanceof NotificationResult) {
            return new NotificationBuilder(notification);
        } else {
            return new NotificationBuilder(new NotificationResult(notification));
        }
    }
}

/**
 * @extends {Entity.EntityBuilder<NotificationResult>}
 */
class NotificationBuilder extends Entity.EntityBuilder {

    /**
     *
     * @param {NotificationResult} notification
     */
    constructor(notification) {
        super(notification);
    }

    /**
     * Provided by the sender service, this references the successful message.
     *
     * @param {string} given
     * @return {NotificationBuilder}
     */
    senderRecordId(given) {
        return this._property(NotificationResult.prototype.senderRecordId.name, given);
    }

    /**
     *
     * @param {string} recipientId
     * @return {NotificationBuilder}
     */
    recipientId(recipientId) {
        return this._property(NotificationResult.prototype.recipientId.name, recipientId);
    }

    /**
     * @param {Message} message
     * @return {NotificationBuilder}
     */
    message(message) {
        return this._property(NotificationResult.prototype.message.name, message);
    }

    /**
     *
     * @param {FirebaseError|*} error
     * @return {NotificationBuilder}
     */
    error(error) {
        this._property(NotificationResult.prototype.errorCode.name, error.code);
        return this._property("errorMessage", error.message);
    }

    /**
     * @return {NotificationResult}
     */
    build() {
        return super.build();
    }
}

module.exports = NotificationResult;
module.exports.NotificationBuilder = NotificationBuilder;
