const Entity = require("../../main/entity/entity.js");
const DateUtils = require("../../main/lang/date-utils.js");

/**
 * a specific channel (email, browser notification, text message, etc)
 * that is capable of receiving a notification.
 */
class NotificationRecipient extends Entity {

    /**
     * @param {*} properties
     */
    constructor(properties) {
        super(properties);
    }

    /**
     * The globally unique wya to reach the recipient.  FCM token, phone number, etc.
     * @return {string}
     */
    recipientId() {
        return this._property(NotificationRecipient.prototype.recipientId.name);
    }

    /**
     *
     * @return {boolean} if recipientId will return something
     */
    hasRecipientId() {
        return this._hasProperty(NotificationRecipient.prototype.recipientId.name);
    }
    /**
     * @return {moment.Moment|null}
     */
    subscribedAtTime() {
        return DateUtils.timestampMoment(this._property(NotificationRecipient.prototype.subscribedAtTime.name));
    }

    /**
     *
     * @param {NotificationRecipient} [notificationRecipient]
     * @return {NotificationRecipientBuilder}
     */
    static builder(notificationRecipient) {
        if (notificationRecipient instanceof NotificationRecipient) {
            return new NotificationRecipientBuilder(notificationRecipient);
        } else {
            return new NotificationRecipientBuilder(new NotificationRecipient(notificationRecipient));
        }
    }

}

/**
 * constructs NotificationRecipient
 */
class NotificationRecipientBuilder extends Entity.EntityBuilder {
    /** @param {NotificationRecipient} notificationRecipient*/
    constructor(notificationRecipient) {
        super(notificationRecipient);
    }

    /**
     *
     * @param {string} recipientId
     * @return {NotificationRecipientBuilder}
     */
    recipientId(recipientId) {
        return this._property(NotificationRecipient.prototype.recipientId.name, recipientId);
    }

    /**
     * @param {moment.Moment} moment
     * @return {NotificationRecipientBuilder}
     */
    subscribedAtTime(moment) {
        return this._property(
            NotificationRecipient.prototype.subscribedAtTime.name,
            DateUtils.timestampMomentPortable(moment));
    }

    /**
     * @return {NotificationRecipient}
     */
    build() {
        return super.build();
    }
}

module.exports = NotificationRecipient;
module.exports.NotificationRecipientBuilder = NotificationRecipientBuilder;