const FirebaseError = require("firebase").FirebaseError;
const DateUtils = require("../../main/lang/date-utils");
const Message = require("../../main/message/message");
const NotificationResult = require("./notification-result");
const NotificationUtils = require("./notification-utils");
const Errors = require("../../main/lang/errors");

/**
 * Sends messages to recipients.
 *
 * A useful tutorial for Firebase Messaging:
 *
 *  https://angularfirebase.com/lessons/send-push-notifications-in-angular-with-firebase-cloud-messaging/
 */
class NotificationService {

    /**
     *
     * @param {Repository} notificationRepository
     * @param {Messaging} messaging
     * @param {MomentService} momentService
     */
    constructor(notificationRepository, messaging, momentService) {
        this._momentService = momentService;
        this._notificationRepository = notificationRepository;
        this._messaging = messaging;
    }

    /** Notifies multiple recipients.
     *
     * @see notifyRecipient
     *
     * @param {NotificationRecipient[]} recipients that will be used to notify the recipient
     * @param {Message} message that will be shown
     * @return {Promise<Promise<NotificationResult>[]>} promises for each notification sent or failed.
     */
    notifyRecipients(recipients, message) {
        const recipientIds = [];
        recipients.forEach((recipient) => {
            recipientIds.push(recipient.recipientId());
        });

        const payload = NotificationUtils.messageToNotificationPayload(message);
        return this._messaging.sendToDevice(recipientIds, payload)
            .then((response) => {
                return this._responseToNotifications(message, response);
            });
    }

    /**
     * @private
     * @param {Message} message
     * @param {MessagingDevicesResponse} response
     * @return {Promise<NotificationResult>[]}
     */
    _responseToNotifications(message, response) {
        const promises = [];
        response.results.forEach((result) => {
            const notification = this._notificationFromResult(result, message);
            promises.push(this.saveNotification(notification));
        });
        return promises;
    }

    /**
     * @param {string} messageId path uniquely identifying the message
     * @return {string} relative path to the entity
     */
    idPath(messageId) {
        const moment = this._momentService.now();
        const date = DateUtils.datePortable(moment);
        const path = `/notificationsForDate/${date}/${messageId}`;
        return path;
    }

    /**
     * @private
     * @param {MessagingDeviceResult} result
     * @param {Message} message
     * @return {NotificationResult}
     */
    _notificationFromResult(result, message) {
        const notificationBuilder = NotificationResult.builder();
        if (result.messageId) {
            notificationBuilder.senderRecordId(result.messageId);
            if (result.canonicalRegistrationToken) {
                notificationBuilder.recipientId(result.canonicalRegistrationToken);
            } else {
                console.log(`result didn't identify recipient for ${message.id()}: a93x`);
            }

        } else {
            notificationBuilder.error(result.error);
        }
        return notificationBuilder.message(message).build();
    }

    /**
     *
     * @param {NotificationResult} notification
     * @return {Promise.<NotificationResult>}
     * @rejects {FirebaseError} Sxi3 when the notification is already saved
     */
    saveNotification(notification) {
        if (notification.hasId()) {
            return Promise.reject(Errors.of("id is provided by save", "9ja2"));
        }

        const messageId = notification.message().id();
        if (!messageId) {
            return Promise.reject(Errors.of("message id is required to represent a unique path", "nie9"));
        }

        const path = this.idPath(messageId);
        const thenableReference = this._notificationRepository.push(notification, path);
        return thenableReference
            .then(() => {
                const absolutePath = thenableReference.ref.toString();
                return notification.toBuilder().id(absolutePath).build();
            });
    }

    /**
     * @param {string} idPath which points to the exact notification of interest
     * @return {Promise<NotificationResult>}
     */
    notificationById(idPath) {
        return this._notificationRepository.get(idPath);
    }
}

module.exports = NotificationService;