const Entity = require("../../main/entity/entity");

/**
 * An official device where a player scans a tag.
 */
class Station extends Entity {

    /** @private
     * @see builder()
     * @param {*} [properties] in portable form
     */
    constructor(properties) {
        super(properties);
    }

    /**
     * @return {string} descriptive title for this station recognizable by players, but not necessarily unique
     */
    name() {
        return this._props[Station.prototype.name.name];
    }

    /**
     * Allows categorization of stations.  Handy with rules to check if a "type" of station qualifies, rather than
     * a specific station by name.
     * @return {string} containing the type which is an enum defined by the game
     */
    type() {
        return this._props[Station.prototype.type.name];

    }

    /**
     * @return {number} indicating the distance to the School, in meters, the student will earn if scanned by station.
     */
    metersToSchool() {
        return this._props[Station.prototype.metersToSchool.name];
    }

    /**
     * @param {*} [station] to build upon
     * @return {StationBuilder}
     */
    static builder(station) {
        if (station instanceof Station) {
            return new StationBuilder(station);
        } else {
            return new StationBuilder(new Station(station));
        }
    }
}

/**
 * @type {Station}
 */
module.exports = Station;

/** builder for station*/
class StationBuilder extends Entity.EntityBuilder {
    /**
     * @override
     * @param {*} [mutant]
     */
    constructor(mutant) {
        super(mutant);
    }

    /**
     * @param {string} name
     * @return {StationBuilder}
     */
    name(name) {
        return this._property(Station.prototype.name.name, name);
    }

    /**
     * @param {string} type
     * @return {StationBuilder}
     */
    type(type) {
        return this._property(Station.prototype.type.name, type);
    }

    /**
     * @param {number} meters
     * @return {StationBuilder}
     */
    metersToSchool(meters) {
        return this._property([Station.prototype.metersToSchool.name], meters);
    }
}