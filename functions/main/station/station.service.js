const Station = require("../../main/station/station");


const stationsPath = "/stations";

/**
 * Manages access to station details.
 *
 * Stations are updated directly with the database so no mutation here.
 *
 */
class StationService {
    /**
     * @param {Repository} repository used to store all PlayerRank entities.
     */
    constructor(repository) {
        /**
         * @private
         * @type {Repository}
         */
        this.repository = repository;
    }

    /**
     * @param {string} id as reported by the station in the scan.station.
     *
     * @return {Promise<Station>} the station matching the id or rejection if not found
     */
    stationById(id) {
        return this.repository.get(id);
    }

}

module.exports = StationService;