const console = require("console");
const Entity = require("../../main/entity/entity.js");
const DateUtils = require("../../main/lang/date-utils.js");
const moment = require("moment");

/**
 *Scan is any observation of a {Tag} typically including:
 * who - which person's tag was used
 * where - the location on earth where the scan was made
 * when - the moment the scan was made
 * @see ScanProperties
 * @extends {Entity.<Scan>}
 */
class Scan extends Entity {
    /** @private
     * @see builder()
     * @param {*} [properties] in portable form
     */
    constructor(properties) {
        super(properties);
    }

    /**
     * @param {Scan|Object} [scan]  optionally provided scan, or scan properties
     * @return {ScanBuilder}
     */
    static builder(scan) {
        if (scan instanceof Scan) {
            return new ScanBuilder(scan);
        } else {
            return new ScanBuilder(new Scan(scan));
        }
    }

    /** The unique identifier for the tag that was scanned.
     * @see Tag.id()
     *
     * @return {string}
     */
    tagId() {
        return this._props.tagId;
    }

    /**
     *
     * @return {string|undefined} the authenticated user, if provided
     */
    authUid() {
        return this._property(Scan.prototype.authUid.name);
    }

    /**
     *
     * @return {boolean} true if authUid returns something
     */
    hasAuthUid() {
        return this._hasProperty(Scan.prototype.authUid.name);
    }

    /**If provided, the user id is to be the owner of the tag.
     * @return {string|undefined} userId
     */
    userIdOfTagOwner() {
        return this._property(Scan.prototype.userIdOfTagOwner.name);
    }

    /**
     *
     * @return {boolean} if userIdOfTagOwner will return a user id
     */
    hasUserIdOfTagOwner() {
        return this._hasProperty(Scan.prototype.userIdOfTagOwner.name);
    }

    /**
     * @return {string|undefined} the id of the station, if any
     */
    station() {
        return this._property(Scan.prototype.station.name);
    }

    /**
     *
     * @return {boolean} true if station() will return an id.
     */
    hasStation() {
        return this._hasProperty(Scan.prototype.station.name);
    }

    /**If provided, the user associated with the id is to receive the rewards.
     * @return {string|undefined} userId
     */
    userIdOfRewardRecipient() {
        return this._property(Scan.prototype.userIdOfRewardRecipient.name);
    }

    /**
     *
     * @return {boolean} if userIdOfTagOwner will return a user id
     */
    hasUserIdOfRewardRecipient() {
        return this._hasProperty(Scan.prototype.userIdOfRewardRecipient.name);
    }


    /**
     * Scan self reporting the type of tag being read (tag,qrc, station, etc).
     * This is used to allow users to create tags.
     * @see Tag.type
     * @return {string|undefined}
     */
    tagType() {
        return this._property(Scan.prototype.tagType.name);
    }

    /**
     *
     * @return {boolean} true if tagType will return a type
     */
    hasTagType() {
        return this._hasProperty(Scan.prototype.tagType.name);
    }


    /**
     * Scan self reporting the category of the tag being scanned (backpack, bike, walk, bus, etc).
     * Usesful when creating tags from scans.
     *
     * @return {string|undefined}
     */
    tagCategory() {
        return this._property(Scan.prototype.tagCategory.name);
    }

    /**
     *
     * @return {boolean} true if tagCategory() will return a defined category
     */
    hasTagCategory() {
        return this._hasProperty(Scan.prototype.tagCategory.name);
    }

    /**When the scanner rewards for a specific reason. Unlike tag category which is dedicated for a category,
     * the scan is typically of the backpack tag, but doing an action that deserves reward.
     *
     * @return {string} indicating the scan should receive special reward for a specific reason (walk, bus, etc)
     */
    category() {
        return this._property(Scan.prototype.category.name);
    }

    /**
     * @return {boolean} true if category has a value
     */
    hasCategory() {
        return this._hasProperty(Scan.prototype.category.name);
    }

    /** The time {moment} that the tag happened. If not provided, the current time will be provided.
     *
     * @see hasTimestamp to check if timestamp is available
     *
     * @return {moment.Moment|null}
     */
    timestamp() {
        return DateUtils.timestampMoment(this._props.timestamp);
    }

    /** indicates if a timestamp is available, since it is optional
     * @return {boolean}
     */
    hasTimestamp() {
        return this._props.timestamp != null;
    }

    /** An arbitrary ID that relates this scan to other scans received together.
     * SchoolTag V3 started writing scans to the tag and sending when arriving to school in a batch
     *  (rather than sending directly from the remote stations).
     **
     * @return {string|null}
     */
    batchId() {
        return this._property(Scan.prototype.batchId.name);
    }

    /** Indicates if this scan was part of a batch.
     * @return {boolean}
     */
    hasBatchId() {
        return this._hasProperty(Scan.prototype.batchId.name);
    }


    /**
     * @return {ScanBuilder}
     */
    toBuilder() {
        return super.toBuilder();
    }

}

/**
 * @type {Scan}
 */
module.exports = Scan;

// ================== Builder Methods ==========================
/** Private constructor used to create the new builder and avoid
 * mutable objects.
 *
 * Use #builder
 *
 * @extends {EntityBuilder.<Scan>}
 */
class ScanBuilder extends Entity.EntityBuilder {
    /**
     * @override
     * @param {*} [mutant]
     */
    constructor(mutant) {
        super(mutant);
    }

    /**
     *
     * @param  {string} tagId
     * @return {ScanBuilder}
     */
    tagId(tagId) {
        this._entity._props.tagId = tagId;
        return this;
    };

    /**
     * @param {string} authUserId
     * @return {ScanBuilder}
     */
    authUid(authUserId) {
        return this._property(Scan.prototype.authUid.name, authUserId);
    }

    /**
     * @param {string} userId
     * @return {ScanBuilder}
     */
    userIdOfTagOwner(userId) {
        return this._property(Scan.prototype.userIdOfTagOwner.name, userId);
    }

    /**
     * @param {string} userId
     * @return {ScanBuilder}
     */
    userIdOfRewardRecipient(userId) {
        return this._property(Scan.prototype.userIdOfRewardRecipient.name, userId);
    }

    /**
     * @param {string} station
     * @return {ScanBuilder}
     */
    station(station) {
        return this._property(Scan.prototype.station.name, station);
    }

    /**
     * @param {string} tagCategory
     * @return {ScanBuilder}
     */
    tagCategory(tagCategory) {
        return this._property(Scan.prototype.tagCategory.name, tagCategory);
    }

    /**
     * @param {string} tagType
     * @return {ScanBuilder}
     */
    tagType(tagType) {
        return this._property(Scan.prototype.tagType.name, tagType);
    }

    /**
     *
     * @param {string} category
     * @return {ScanBuilder}
     */
    category(category) {
        return this._property(Scan.prototype.category.name, category);
    }

    /** The time of the scan. If this is not set, then the scan time is assumed to be now.
     * @see DateUtils.asDate()
     * @param {date|moment.Moment|string|null} [dateOrString]
     * @return {ScanBuilder}
     */
    timestamp(dateOrString) {
        this._property(ScanProperties.timestamp(), DateUtils.timestampMoment(dateOrString).format());
        return this;
    };

    /**The id relating this scan to other scans in which it arrived together.
     *
     * @param {string} batchId
     * @return {ScanBuilder}
     */
    batchId(batchId) {
        return this._property(Scan.prototype.batchId.name, batchId);
    }

    /**
     * @override
     */
    _validate() {
        console.assert(this._entity._props.tagId != null, "tagId must not be null");
        if (this._entity.hasTimestamp()) {
            console.assert(this._entity.timestamp().isValid(),
                "Invalid timestamp " + this._entity._props.timestamp);
        }
    }

    /**
     * @override
     * @return {Scan}
     */
    build() {
        return super.build();
    }
}

/** Properties for {Scan}*/
class ScanProperties {
    /** @return {string}*/
    static timestamp() {
        return "timestamp";
    }
}

module.exports.ScanProperties = ScanProperties;