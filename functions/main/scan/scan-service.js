const appRootDir = require("app-root-dir").get();
const console = require("console");
const Scan = require(appRootDir + "/main/scan/scan.js");
const TagService = require(appRootDir + "/main/tag/tag-service.js");
const ScanRepository = require(appRootDir + "/main/scan/scan-repository.js");

/** Applies logic for scanning tags.
 *
 * @property {ScanRepository} _repository for persistence management
 */
class ScanService {
    /**
     *
     * @param {ScanRepository} repository
     * @param {TagService} tagService
     */
    constructor(repository, tagService) {
        this._repository = repository;
        this._tagService = tagService;
    }

    /**
     * Finds or creates a Tag and associated user.
     *
     * @see {Tag} - confirms the tag exists or creates if not
     * @see {User} - the user is related through the tag
     *
     * @param {Scan|string} scanOrId  a scan, or its id, that recently arrived in persistence
     * @param {Object} [options] allowing extension of behavior
     * @return {Promise.<Tag>} a promise to provide the found tag or a new one
     */
    tagForScan(scanOrId, options) {
        let scanPromise;
        if (!(scanOrId instanceof Scan)) {
            scanPromise = this._repository.get(scanOrId);
        } else {
            scanPromise = Promise.resolve(scanOrId);
        }
        return scanPromise.then((scan) => {
            return this._tagService.tagFoundOrCreated(scan.tagId(), options);
        });
    };

    /** Simply pushes the scan into the repository and provides the key created
     * upon success.
     *
     * @param {Scan} scan to be saved
     * @return {Promise.<Scan>} promise with the newly created id assigned to the scan
     */
    scanCreated(scan) {
        const promiseWithKey = this._repository.push(scan);
        //add the id and return immediately.
        return promiseWithKey.then(() => {
            return scan.toBuilder().id(promiseWithKey.key).build();
        });
    }
}

module.exports = ScanService;
