### Scan

A scan is a single action, from a user, that holds a [tag](../tag/README.md)
to a School Tag [Station](../station/README.md) . If the scan qualifies for any rewards, 
the user receives them.

The scan is the primary way players get rewarded, based on the [rules](../rule/README.md).

#### Simulate a Station

Remote stations send scans via a Google Cloud Pub/Sub message.  You can send
a scan from the [console](https://console.cloud.google.com/cloudpubsub/topics/station-scan/publishMessage?prev=topic-details&project=school-tag-sbox):

```json
{ 
  "tagId": "04004022EC4C81",
  "station":"school"
}
```
NOTE: A station with id `school` must be in the database or change the id
to any station that exists.


#### Command Line Write

To test a scan using the firebase cli, one must be authorized to 
write a scan by having admin access to the Firebase database. 

`firebase database:push /scans --data '{ "tagId": "04004022EC4C81", "timestamp":"2019-03-18T05:20:58-07:00" }'`

The firebase cli requires authentication for a push, but the application
restricts writing by providing [database rules](../../../database.rules.json).

View the resulting scans on the [sandbox](https://sandbox.schooltag.org/tag/04004022EC4C81).


WARNING: The scans created by the CLI are insufficient and may cause the app not to work properly. See [bug](https://trello.com/c/w1ekoHN8)

#### School Tag Authorized Rewards

Most rewards are granted by the system requiring a Google account
that is authorized to provide the rewards.  The authorized account is
assigned using [npm run config:auth:station](../../config/station-auth.js).


`firebase database:push /scans --data '{ "tagId": "04004022EC4C81", "timestamp":"2019-03-18T05:20:58-07:00",  "authUid": "jSKhlEM0yHRDW9WoB5R3m7BsuEy1" }'`

Most [game rules](../../data/rules/school-game-rules.json) require the matching role:

```json
{
  "fact": "authUserHasRole",
  "operator": "equal",
  "params": {
    "role": "schoolStationManager"
  },
  "value": true
}
```

NOTE: As an admin of the database, you can write any data so the firebase cli push command will succeed, 
but a push would fail from the application when not an admin.

#### Player Authorized Rewards

Scans can be written by authenticated users to provide rewards for any player.  
This allows parents to provide rewards using their own mobile phone. 

The scan requires an `authUid` key with a value that matches the authenticated user's 
firebase ID. The id can be found using the [firebase CLI](https://firebase.google.com/docs/cli/auth#json).

