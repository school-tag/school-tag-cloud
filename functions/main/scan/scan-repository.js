const Database = require("firebase").database.Database;
const Scan = require("../../main/scan/scan.js");
const Repository = require("../../main/entity/repository.js");
const Audit = require("../../main/entity/repository.js").Audit;
const DateUtils = require("../../main/lang/date-utils.js");
const moment = require("moment");

/**
 * @extends {Repository.<Scan>}
 */
class ScanRepository extends Repository {
    /**
     * @param  {Database} database
     * @param {Audit} auditTemplate indicating the source
     * @param {MomentService} momentService to organize scans by date
     */
    constructor(database, auditTemplate, momentService) {
        super(database, ScanRepository.pathName(momentService), Scan, auditTemplate);
    }

    /** Common place to provide the path to the scans.  Prefixed with local date for
     * better organization and sustainable browsing in Firebase realtime database.
     *
     * @param {MomentService} momentService to organize scans by date
     * @return {string} the name that represents the scan in the url paths*/
    static pathName(momentService) {
        return "scans/" + DateUtils.datePortable(momentService.now());
    }
}

module.exports = ScanRepository;
