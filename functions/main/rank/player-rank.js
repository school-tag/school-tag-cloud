/**
 * A single player's rank, compared to others, in a specific competition.
*/
class PlayerRank {

    /**
     * Set properties directly.
     */
    constructor() {

        /** @type {number}*/
        this.rank = null;

        /** @type {string} */
        this.userId = null;

        /** path to avatar
         * @see User#avatar
         * @type {string} */
        this.avatar = null;

        /** display name for user.
         *
         * @type {string}
         */
        this.nickname = null;

        /**
         * The points earned
         * @type {number}
         */
        this.points = null;
    }

    /**
     *
     * @param {PlayerRank|*} properties
     * @return {{build: function()}}
     */
    static builder(properties) {

        const rank = new PlayerRank();
        Object.keys(properties).forEach((key)=>{
            rank[key] = properties[key];
        });
        return {
            build: ()=>{
                return rank;
            },
        };
    }

    /**
     *
     * @return {PlayerRank} which has all properties ready for storage and transport
     */
    portable() {
        return this;
    }
}

module.exports = PlayerRank;
