const PlayerRank = require("../../main/rank/player-rank");

/**
 * Compares the game totals for players of the game for a single aspect.
 * Total points, total cycling, weekly, weekly cycling, etc. For re-usability, this doesn't care which
 * ranking is being calculated, just taking in a series points related to an id (currently user id - future teams),
 * ordering them to produce the rank integer.
 *
 * First, builds groups of similar points
 *
 *
 */
class PlayerRankCalculator {


    /**
     * This is a mutable class where pointsRewarded can be called and calculate can be called with accuracy, however,
     * full reproduction of results so inefficient for calling repeatedly.
     * @param {function} valueFunction that extracts the value from the entity given.
     */
    constructor(valueFunction) {
        /** @private
         * @type Entity[]*/
        this.all = [];
        this.valueFunction = valueFunction;
    }

    /** If points are available for the user rewards, as retrieved by the value function,
     * this will add the rewards to the list of all for use in the calculate method.
     *
     * @param {Entity} identifiedEntityWithPoints any object that will produce an id
     * value and points as number (like UserRewards.portable()).
     */
    pointsRewarded(identifiedEntityWithPoints) {
        const points = this.valueFunction(identifiedEntityWithPoints);
        if (points) {
            this.all.push(identifiedEntityWithPoints);
        }
    }

    /**
     * Sorts the array of entities given ranking highest points first
     * @return {RankingGroup[]} with ranking groups in order  from best to worst.
     */
    calculate() {
        //highest score first identifies rank easily
        this.all.sort((left, right) => {
            const l = this.valueFunction(left);
            const r = this.valueFunction(right);
            if (l > r) {
                return -1;
            } else if (l < r) {
                return 1;
            } else {
                return 0;
            }
        });
        //a javascript multimap would be great here
        //now provide the rank and handle the ties
        const ranks = [];
        let pointsThatTieCurrentRank = -9999999; //impossible points forces a rank
        let rank = 0;
        this.all.forEach((entity) => {
            //every player increments rank, but ties share the same rank
            const points = this.valueFunction(entity);
            //not all reward sumaries have related values
            if (points) {
                //always increment for every player in the leaderboard
                rank = rank + 1;
                let entitiesWithSameRank;

                if (pointsThatTieCurrentRank !== points) {
                    //create a new ranking group for the next lowest rank
                    entitiesWithSameRank = new RankingGroup(rank, points);
                    ranks.push(entitiesWithSameRank);
                    pointsThatTieCurrentRank = points;
                } else {
                    //get an existing ranking group from the end since this is a tie
                    entitiesWithSameRank = ranks[ranks.length - 1];
                }
                entitiesWithSameRank.entities.push(entity);
            }
        });
        return ranks;
    }


    /**
     * @param {Object.<string, User>} [userMap] when the user details(nickname,avatar) are to be saved with the rankings
     * @return {PlayerRank[]} player ranks ordered from best to worst with ties being ordered indeterminately.
     */
    playerRanks(userMap) {
        const playerRanks = [];
        this.calculate().forEach((rankingGroup) => {
            rankingGroup.entities.forEach((userRewards) => {
                const playerRank = new PlayerRank();
                playerRank.rank = rankingGroup.rank;
                playerRank.points = rankingGroup.points;

                if (userRewards.hasUserId()) {
                    playerRank.userId = userRewards.userId().toString();
                    if (userMap) {
                        const user = userMap[playerRank.userId];
                        if (user) {
                            //confusing, but the map returns user properties, not user entities
                            playerRank.nickname = user.nickname;
                            playerRank.avatar = user.avatar;
                        }
                    }
                }

                playerRanks.push(playerRank);
            });
        });
        return playerRanks;
    }
}

/**
 * Group of entities tied with the same number of points
 */
class RankingGroup {

    /**
     *
     * @param {number} rank starting at 1 and incrementing by 1 so lower is better
     * @param {number} points required to reach the rank
     */
    constructor(rank, points) {
        this.rank = rank;
        this.points = points;
        this.entities = [];
    }
}

module.exports = PlayerRankCalculator;
module.exports.GroupRank = RankingGroup;