const PlayerRankCalculator = require("../../main/rank/player-rank-calculator");
const PlayerRank = require("../../main/rank/player-rank");
const UserRewardsUtils = require("../../main/game/user-rewards-utils");


const ranksPath = "/ranks";
const totalRanksPath = `${ranksPath}/game`;
const rulesTotalRanksPath = `${ranksPath}/rules`;

/**
 * Calculates the ranks for all the competitions and provides access to the stored rank entities.
 *
 */
class PlayerRankService {
    /**
     * @param {Repository} playerRankRepository used to store all PlayerRank entities.
     * @param {UserRewardsService} userRewardsService
     * @param {UserService} userService to gather names and avatars
     */
    constructor(playerRankRepository, userRewardsService, userService) {
        /**
         * @private
         * @type {UserRewardsService}
         */
        this.userRewardsService = userRewardsService;
        /**
         * @private
         * @type {Repository}
         */
        this.playerRankRepository = playerRankRepository;

        /**
         * @type {UserService} to retrieve user details when preparing the leaderboard for quick access
         */
        this.userService = userService;
    }

    /** Inspecting the total game rewards for all users, the rank is calculated for points high to low
     * and stored with information about the user with the intention to display in a tabular format.
     *
     * All ranks are set and replace any previous rankings.
     * @param {string[]} [ruleIds]
     * @return {Array<Promise<PlayerRank[]>>} promises for each of the leaderboards to be persisted
     */
    updateTotalRanks(ruleIds) {
        const userRewardsListPromise = this.userRewardsService.grandTotalsForAll();
        const usersMapPromise = this.userService.allUsersMap();

        return Promise.all([userRewardsListPromise, usersMapPromise]).then((results) => {
            const userRewardsList = results[0];
            const usersMap = results[1];

            const pathValueFunctions = {};
            pathValueFunctions[totalRanksPath] = UserRewardsUtils.pointsValueFunction();
            this.addRulePathValueFunctions(ruleIds, pathValueFunctions);
            return this.calculateAndSaveRanks(userRewardsList, usersMap, pathValueFunctions);
        });
    }

    /**calculates the ranks for the rewards given, looking up the users from the map, and saving
     * the rank groups in the appropriate paths as indicated with the function.
     *
     * @param {UserRewards[]} userRewardsList
     * @param {Object<string,User>} usersMap
     * @param {Object<string,function>} pathValueFunctions
     * @return {Array<Promise<PlayerRank[]>>}
     */
    calculateAndSaveRanks(userRewardsList, usersMap, pathValueFunctions) {
        /** @type Array<Promise<void>>*/
        const rankSavingPromises = [];
        Object.keys(pathValueFunctions).forEach((path) => {
            const valueFunction = pathValueFunctions[path];
            const calculator = new PlayerRankCalculator(valueFunction);
            userRewardsList.forEach((userRewards) => {
                calculator.pointsRewarded(userRewards);
            });
            const playerRanks = calculator.playerRanks(usersMap);
            if (playerRanks.length > 0) {
                const promise = this.playerRankRepository.set(path, playerRanks).then(() => {
                    return playerRanks;
                });
                rankSavingPromises.push(promise);
            }
        });
        return rankSavingPromises;

    }

    /** iterates the rule ids given and create rule id value functions for each, assigning the appropriate path
     * @private
     * @param {String[]} ruleIds
     * @param {Object<string,function>} pathValueFunctions
     */
    addRulePathValueFunctions(ruleIds, pathValueFunctions) {
        //add a path/function for each rule of interest
        if (ruleIds) {
            ruleIds.forEach((ruleId) => {
                pathValueFunctions[`${rulesTotalRanksPath}/${ruleId}`]
                    = UserRewardsUtils.pointsForRuleValueFunction(ruleId);
            });
        }
    }

    /**
     *
     * @return {Promise<PlayerRank>} all the ranks, in order of rank, for the game totals
     */
    totalRanks() {
        return this.playerRankRepository.list(totalRanksPath);
    }

}

module.exports = PlayerRankService;
