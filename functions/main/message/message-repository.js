const appRootDir = require("app-root-dir").get();

const Message = require(appRootDir + "/main/message/message.js");
const Repository = require(appRootDir + "/main/entity/repository.js");

/**
 * Persistence management for Message.
 */
class MessageRepository {

    /**
     * @param {Repository}  repository delegate for persistence management.
     */
    constructor(repository) {
        this._repository = repository;
    }

    /**writes the message to persistence, which creates a new entry every time.
     * This is not idempotent.
     *
     * @param {Message} message
     * @return {ThenableReference.<string>} providing the key immediately as .key or in the promise
     */
    save(message) {
        const push = this._repository.push(message);
        return push.then(() => {
            return push.key;
        });
    }

    /**
     * @param {String} messageId
     * @return {Promise.<T>}
     */
    get(messageId) {
        return this._repository.get(messageId);
    }
}

module.exports = MessageRepository;