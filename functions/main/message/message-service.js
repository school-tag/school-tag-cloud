const ThenableReference = require("firebase").database.ThenableReference;

const Message = require("../../main/message/message.js");
const MessageRepository = require("../../main/message/message-repository.js");

/**
 * manages interactions with messages and streams.
 */
class MessageService {

    /**
     * @param {MessageRepository} messageRepository
     * @param {MomentService} momentService to create localized timestamps.
     */
    constructor(messageRepository, momentService) {
        this._messageRepository = messageRepository;
        this._momentService = momentService;
    }

    /**
     * Simply saves the message to the repository.  Any dependencies (notifications, etc) should be
     * listening for database triggers.  Assigns now timestamp if not already present.
     *
     * @param {Message} message
     * @return {ThenableReference.<Message>} providing the given message with the newly generated id
     */
    send(message) {
        if (!message.hasTimestamp()) {
            message = message.toBuilder().timestamp(this._momentService.now()).build();
        }
        return this._messageRepository.save(message)
            .then((messageId) => {
                const builder = message.toBuilder();
                 return builder.id(messageId).build();
            });
    }

    /**
     * retrieves a message given its id.
     * @param {string} messageId
     * @return {Promise.<Message>}
     */
    messageById(messageId) {
        return this._messageRepository.get(messageId);
    }
}

module.exports = MessageService;