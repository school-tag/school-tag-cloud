const console = require("console");
const moment = require("moment");

const Entity = require("../../main/entity/entity.js");
const EntityBuilder = require("../../main/entity/entity.js");
const DateUtils = require("../../main/lang/date-utils.js");
const UserId = require("../../main/user/user.js").UserId;
const User = require("../../main/user/user.js").User;

/**
 * A chat style message from someone.
 *
 */
class Message extends Entity {

    /**
     *
     * @param {*} properties
     */
    constructor(properties) {
        super(properties);
    }

    /**
     * @return {string} one or two words in Title Case to explain the subject of the text.
     */
    title() {
        return this._property(Message.prototype.title.name);
    }

    /**
     * @return {boolean} true if the title is set
     */
    hasTitle() {
        return this._hasProperty(Message.prototype.title.name);
    }

    /**
     * The simple text in the primary language.
     *
     * @return {string}
     */
    text() {
        return this._property(Message.prototype.text.name);
    }

    /**
     * @return {boolean} true if text is assigned.  Maybe some messages will just have a title or icon.
     */
    hasText() {
        return this._hasProperty(Message.prototype.text.name);
    }

    /**
     * When the message was posted.
     *
     * @return {moment.Moment}
     */
    timestamp() {
        return DateUtils.timestampMoment(this._property(Message.prototype.timestamp.name));
    }

    /**
     *
     * @return {boolean}
     */
    hasTimestamp() {
        return this._hasProperty(Message.prototype.timestamp.name);
    }

    /**
     * Optionally describe who the message is about.  The entire user is copied for full reference.
     * @return {User|null} the only user this message is about
     */
    aboutUser() {
        return User.builder(this._property(Message.prototype.aboutUser.name)).build();
    }

    /**
     *
     * @return {boolean} true if aboutUser will return a user.
     */
    hasAboutUser() {
        return this._hasProperty(Message.prototype.aboutUser.name);
    }

    /**
     * Extra payload
     * @return {*|null}
     */
    aboutPayload() {
        return this._property(Message.prototype.aboutPayload.name);
    }

    /**
     * @return {boolean} true if aboutPayload will return something
     */
    hasAboutPayload() {
        return this._hasProperty(Message.prototype.aboutPayload.name);
    }
    /**
     * @return {string|null} optional https reference to the icon that represents the message.
     */
    iconUrl() {
        return this._property(Message.prototype.iconUrl.name);
    }

    /**
     *
     * @return {boolean} if iconUrl is available.
     */
    hasIconUrl() {
        return this._hasProperty(Message.prototype.iconUrl.name);
    }

    /**
     * @return {string|null} url to send a user for more information
     */
    moreInfoUrl() {
        return this._property(Message.prototype.moreInfoUrl.name);
    }

    /**
     *
     * @return {boolean} true if moreInfoUrl() is available
     */
    hasMoreInfoUrl() {
        return this._hasProperty(Message.prototype.moreInfoUrl.name);
    }

    /**
     * @return {string} unique code that programmers can use to programmatically handle a message response.
     */
    code() {
        return this._property(Message.prototype.code.name);
    }

    /**
     *
     * @return {boolean} true if code() will return a value.
     */
    hasCode() {
        return this._hasProperty(Message.prototype.code.name);
    }

    /**
     *
     * @param {Message} [message] optionally provided for mutation
     * @return {MessageBuilder}
     */
    static builder(message) {
        if (message instanceof Message) {
            return new MessageBuilder(message);
        } else {
            return new MessageBuilder(new Message(message));
        }
    }

}

/**
 * mutator for Message
 * @extends {Entity.EntityBuilder.<Message>}
 */
class MessageBuilder extends Entity.EntityBuilder {

    /**
     * @param {Message} message
     */
    constructor(message) {
        super(message);
    }

    /**
     * @param {string} title
     * @return {MessageBuilder}
     */
    title(title) {
        return this._property(Message.prototype.title.name, title);
    }

    /**the message text in the primary language.
     *
     * @param {string} text
     * @return {MessageBuilder}
     */
    text(text) {
        this._property(Message.prototype.text.name, text);
        return this;
    }

    /**
     * The time of the message.
     * @param {moment.Moment} moment
     * @return {MessageBuilder}
     */
    timestamp(moment) {
        this._property(Message.prototype.timestamp.name, DateUtils.timestampMomentPortable(moment));
        return this;
    }

    /**
     * The only user this message is about.
     *
     * @param {User} user
     * @return {MessageBuilder}
     */
    aboutUser(user) {
        return this._property(Message.prototype.aboutUser.name, user.portable());
    }


    /**
     * @param {*} payload a portable object
     * @return {MessageBuilder}
     */
    aboutPayload(payload) {
        return this._property(Message.prototype.aboutPayload.name, payload);
    }

    /**
     * @param {string} moreInfoUrl
     * @return {MessageBuilder}
     */
    moreInfoUrl(moreInfoUrl) {
        return this._property(Message.prototype.moreInfoUrl.name, moreInfoUrl);
    }

    /**
     *
     * @param {string} iconUrl
     * @return {MessageBuilder}
     */
    iconUrl(iconUrl) {
        return this._property(Message.prototype.iconUrl.name, iconUrl);
    }

    /**
     *
     * @param {string} code
     * @return {MessageBuilder}
     */
    code(code) {
        return this._property(Message.prototype.code.name, code);
    }

    /**
     * @override
     * @return {Message}
     */
    build() {
        return super.build();
    }


}

module.exports = Message;
module.exports.MessageBuilder = MessageBuilder;
