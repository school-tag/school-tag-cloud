module.exports = {
    "env": {
        "es6": true,
        "node": true,
        "mocha": true
    },
    "plugins": [
        "chai-friendly"
    ],
    "rules": {
        "quotes": [1, "double"],
        "no-unused-vars": 1,//needed for imports required for jsdoc
        "max-len": [1, 120],
        "space-before-function-paren": 0,
        "spaced-comment": 0,
        "no-unused-expressions": 0,
        "chai-friendly/no-unused-expressions": 2,
        "eol-last": 1,
        "no-trailing-spaces": 1,
        "padded-blocks": 0,
    },
    "extends": "google"
};