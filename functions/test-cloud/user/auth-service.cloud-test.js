const describe = require("mocha").describe;
const beforeEach = require("mocha").beforeEach;
const it = require("mocha").it;
const expect = require("chai").expect;
const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils.js");

describe("AuthService", function () {
    describe(" when connected to the cloud", function () {
        describe("authUserHasRole", function () {
            it("should get a user not found for a bogus authUid", function () {
                const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
                return bootstrap.authService().authUserHasRole("bogus-authUid", "something").then((hasRole) => {
                    expect(hasRole).to.equal(false);
                });
            });
        });
    });
});

