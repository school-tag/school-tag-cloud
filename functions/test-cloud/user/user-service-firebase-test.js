const describe = require("mocha").describe;
const beforeEach = require("mocha").beforeEach;
const it = require("mocha").it;
const expect = require("chai").expect;
const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils.js");
const UserRepository = require("../../main/user/user-repository");
const UserFixtures = require("../../test/user/user-fixtures");
const Errors = require("../../main/lang/errors");

describe("UserService", function () {
    describe("createUser", function () {
        it("should create a user that can be found", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);

            return bootstrap.userService.createUser().then((user) => {
                expect(user).to.not.equal(null);
                expect(user.momentCreated()).to.not.equal(null);
                return bootstrap.userService.user(user.id().toString()).then((found) => {
                    expect(found.momentCreated().toISOString()).to.equal(user.momentCreated().toISOString());
                    expect(found.id().toString()).to.equal(user.id().toString());
                    //stall until nickname is found
                    if (!found.hasNickname()) {
                        //TODO: listen for nickname to be assigned
                        console.warn("nickname not assigned yet " + found.id());
                    }
                    return found;
                });
            });
        });
        it("should log a warning if unable to assign identity", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const userService = bootstrap.userService;
            const message = "simulating failure";
            userService.assignSuggestedIdentity = () => {
                return Promise.reject(message);
            };
            return userService.createUser().then(() => {
                expect("should have failed").to.equal(null);
            }, (error) => {
                expect(error).to.equal(message);
            });

        });
        it("should be found by key when the user id is a string", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return bootstrap.userService.createUser()
                .then((user) => {
                    return bootstrap.userService.userFromKey(user.id().toString())
                        .then((userFoundByKey) => {
                            expect(userFoundByKey.id().toString()).to.equal(user.id().toString());
                        });
                });
        });
        it("should be found by key when the user id is an id", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return bootstrap.userService.createUser()
                .then((user) => {
                    return bootstrap.userService.userFromKey(user.id())
                        .then((userFoundByKey) => {
                            expect(userFoundByKey.id().toString()).to.equal(user.id().toString());
                        });
                });
        });
    });
    describe("userFromKey", function () {
        it("should indicate an error when a user is not found", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return bootstrap.userService.userFromKey("bogus-user")
                .then(() => {
                    return Promise.resolve("shouldn't be found");
                })
                .catch((error) => {
                    expect(error.code).to.equal(UserRepository.userNotFoundCode());
                    return Promise.resolve();
                });
        });
    });
    describe("identitySuggested", function () {
        it("should provide a different identity with each call", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            //use a userId that identifies the test
            const userId = bootstrap.playerRankRepository._rootRef.toString();
            const userId1 = `${userId}-1`;
            const userId2 = `${userId}-2`;
            const identitySuggestedRepository = bootstrap.userService.identitySuggestedRepository();
            return bootstrap.userService.identitySuggested(userId1).then((identity1) => {
                expect(identity1.avatar()).to.contain("png");
                //identity is returned without user id set, but persistence is confirmed later
                expect(identity1.portable().userId).to.equal(undefined);
                return identity1;
            }).then((identity1) => {
                return identitySuggestedRepository.get(identity1.nickname()).then((identity1Found) => {
                    expect(identity1Found.portable().userId).to.equal(userId1);
                    return identity1;
                }).then((identity1) => {
                    return bootstrap.userService.identitySuggested(userId2).then((identity2) => {
                        expect(identity1.nickname()).to.not.equal(identity2.nickname());
                        expect(identity2.portable().userId).to.equal(undefined);
                        return identity2;
                    }).then((identity2) => {
                        return identitySuggestedRepository.get(identity2.nickname()).then((identity2Found) => {
                            expect(identity2Found.portable().userId).to.equal(userId2);
                        });
                    });
                });
            });
        });
    });
    describe("authUid", function () {
        const authUserId = UserFixtures.authUid1();
        it("should be persistent and loaded with user", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return bootstrap.userService.createUser().then((user) => {
                return user;
            }).then((user) => {
                return bootstrap.userService.setAuthUid(user.id(), authUserId).then(() => {
                    return;
                }).then(() => {
                    return bootstrap.userService.user(user.id()).then((foundUser) => {
                        expect(foundUser.id()).to.deep.equal(user.id());
                        expect(foundUser.authUid()).to.equal(authUserId);
                    });
                });
            });
        });
        it("should be persistent and searchable", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return bootstrap.userService.createUser().then((user) => {
                return user;
            }).then((user) => {
                return bootstrap.userService.setAuthUid(user.id(), authUserId).then(() => {
                    return;
                }).then(() => {
                    return bootstrap.userService.userForAuthUid(authUserId).then((foundUser) => {
                        expect(foundUser.id()).to.deep.equal(user.id());
                    });
                });
            });
        });
        it("should not find a user with different auth id", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return bootstrap.userService.createUser().then((user) => {
                return user;
            }).then((user) => {
                return bootstrap.userService.setAuthUid(user.id(), authUserId).then(() => {
                    return;
                }).then(() => {
                    return bootstrap.userService.userForAuthUid("totally-bogus-id").then((foundUser) => {
                        expect(foundUser.id()).to.equal(null);
                    }, (error) => {
                        expect(Errors.code(error)).to.equal(UserRepository.userWithAuthIdNotFoundCode());
                    });
                });
            });
        });

    });
    describe("allUsersMap", function () {
        it("should find all the users created", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const userPromises = [];
            userPromises.push(bootstrap.userService.createUser());
            userPromises.push(bootstrap.userService.createUser());
            userPromises.push(bootstrap.userService.createUser());
            userPromises.push(bootstrap.userService.createUser());

            return Promise.all(userPromises).then((users) => {
                return bootstrap.userService.allUsersMap().then((usersMap) => {
                    expect(Object.keys(usersMap).length).to.equal(users.length);
                    users.forEach((user) => {
                        const userFromMap = usersMap[user.id()];
                        expect(userFromMap.id).to.equal(user.id().toString());
                        //checking nickname is a problem because that is asynchronously assigned
                        expect(userFromMap.momentCreated).to.equal(user.portable().momentCreated);
                    })
                });
            });
        });
    });
});

