const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const GameCalculator = require("../../main/game/game-calculator");
const UserRewards = require("../../main/game/user-rewards");
const highestReward1 = require("../../test/rank/player-rank.fixtures").highestReward1;
const highestReward2 = require("../../test/rank/player-rank.fixtures").highestReward2;
const middleReward1 = require("../../test/rank/player-rank.fixtures").middleReward1;
const lowestReward1 = require("../../test/rank/player-rank.fixtures").lowReward1;
const lowestReward2 = require("../../test/rank/player-rank.fixtures").lowReward2;
const rewardWithoutPoints = require("../../test/rank/player-rank.fixtures").rewardWithoutPoints;
const lowerReward1 = require("../../test/rank/player-rank.fixtures").lowerReward1;
const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils.js");
const SchoolGameRules = require("../../main/game/school-game-rules");

describe("PlayerRankService ", function () {
    let saveUserRewards = function (bootstrap, rewards) {
        const rewardSavingPromises = [];
        const userRewardsRepository = bootstrap.userRewardsService._userRewardsRepository;
        rewards.forEach((userRewards) => {
            const gameCalculator = GameCalculator.builder().rewards([userRewards]).build();
            rewardSavingPromises.push(userRewardsRepository
                .setUserRewardsGrandTotalsFromCalculator(userRewards.userId(), gameCalculator));
        });
        return Promise.all(rewardSavingPromises);
    };
    describe("with reward fixtures saved", function () {
        it("should find rewards, calculate and save player ranks that can be loaded", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            //notice the rewards are out of order
            const rewards = [lowestReward1, highestReward1, middleReward1, lowestReward2, highestReward2,lowerReward1];
            const userRewardsPromise = saveUserRewards(bootstrap, rewards);
            return userRewardsPromise.then(() => {
                return;
            }).then(() => {
                return bootstrap.playerRankService.updateTotalRanks();
            }).then((playerRankPromises) => {
                expect(playerRankPromises.length).to.equal(1);
                return playerRankPromises[0].then((playerRanks) => {
                    expect(playerRanks).to.not.equal(null);
                    expect(playerRanks.length).to.equal(rewards.length);
                    return bootstrap.playerRankService.totalRanks();
                });
            }).then((playerRanksFound) => {
                expect(playerRanksFound.length).to.equal(rewards.length);
                expect(playerRanksFound[0].points).to.equal(highestReward1.points());
                expect(playerRanksFound[0].userId).to.equal(highestReward1.userId().toString());
            })

        });
    });
    describe("with real users and rewards created", function () {
        it("should calculate the leaderboard and find real users", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const userPromises = [];
            userPromises.push(bootstrap.userService.createUser());
            userPromises.push(bootstrap.userService.createUser());
            userPromises.push(bootstrap.userService.createUser());
            userPromises.push(bootstrap.userService.createUser());
            //use this rule id to test that rule leaderboards can be calculated too
            const ruleId = SchoolGameRules.everyScanCountsRuleId();
            const ruleId2 = SchoolGameRules.arrivedByBike();
            return Promise.all(userPromises).then((users) => {
                let points = 100;
                const rewards = [];
                users.forEach((user) => {
                    points -= 10;
                    //FIXME: move this to the fixtures with points that add up for the best reflection of reality
                    const pointsForRules = {};
                    pointsForRules[ruleId] = points + 1;
                    pointsForRules[ruleId2] = points + 2;
                    const userRewards = UserRewards.builder().points(points).pointsForRules(pointsForRules).userId(user.id()).build();
                    rewards.push(userRewards);
                });
                return saveUserRewards(bootstrap, rewards).then(() => {
                    return;
                }).then(() => {
                    return bootstrap.playerRankService.updateTotalRanks([ruleId, ruleId2]);
                }).then((playerRankPromises) => {
                    //one leaderboard for grand totals, another for every scan counts rule
                    const expectedNumberOfLeaderboards = 3;
                    expect(playerRankPromises.length).to.equal(expectedNumberOfLeaderboards);
                    return playerRankPromises[0].then((playerRanks) => {
                        expect(playerRanks.length).to.equal(users.length);
                        for (let i = 0; i < playerRanks.length; i++) {
                            const user = users[i];
                            const playerRank = playerRanks[i];
                            expect(playerRank.userId).to.equal(user.id().toString());
                            //users upon creation don't have identity assigned yet, but ensure player rank received it
                            expect(playerRank.nickname).to.not.equal(null);
                            expect(playerRank.avatar).to.not.equal(null);
                        }
                        return playerRankPromises;
                    }).then((playerRankPromises) => {
                        return playerRankPromises[1].then((playerRanks) => {
                            expect(playerRanks.length).to.equal(users.length);
                            for (let i = 0; i < playerRanks.length; i++) {
                                const user = users[i];
                                const playerRank = playerRanks[i];
                                expect(playerRank.userId).to.equal(user.id().toString());
                                //users upon creation don't have identity assigned yet,
                                // but ensure player rank received it
                                expect(playerRank.nickname).to.not.equal(null);
                                expect(playerRank.avatar).to.not.equal(null);
                            }
                            return playerRankPromises;
                        });
                    });
                });
            });
        });
    });
});
