const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
var chai = require('chai');
chai.use(require('chai-string'));

const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils.js");
const MessageTestUtils = require("../../test/message/message.fixtures.js");
const Notification = require("../../main/notification/notification-result.js");

describe("NotificationService", function () {
    describe("persistence", function () {
        it("should save and load notifications", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const token = "fcm-token-1";
            const messageId = "/a/b/c";
            const message = MessageTestUtils.message1().toBuilder().id(messageId).build();
            const senderRecordId = "sender-record-id-1"
            const notificationService = bootstrap.notificationService;
            const notification = Notification.builder()
                .message(message)
                .senderRecordId(senderRecordId)
                .recipientId(token)
                .build();
            return notificationService.saveNotification(notification)
                .then((notificationSaved) => {
                    expect(notificationSaved.senderRecordId()).to.equal(senderRecordId);
                    const id = notificationSaved.id();
                    expect(id).to.not.equal(null);
                    expect(id).to.containIgnoreSpaces(messageId);
                    return notificationService.notificationById(id)
                        .then((notificationFound) => {
                            expect(notificationFound.senderRecordId()).to.equal(notification.senderRecordId());
                            expect(notificationFound.id()).to.equal(id);
                        });
                });
        });
    });
});
