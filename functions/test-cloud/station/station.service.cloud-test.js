const describe = require("mocha").describe;
const beforeEach = require("mocha").beforeEach;
const it = require("mocha").it;
const expect = require("chai").expect;
const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils.js");
const Errors = require("../../main/lang/errors");

describe("StationService with firebase", function () {
    describe("stationById", function () {
        it("should return notFound for station unknown", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);

            return bootstrap.stationService.stationById("no-station-should-match-this").then((station) => {
                expect(false).to.equal(true);
            }, (error) => {
                expect(Errors.hasHttpStatusCodeOf(error, Errors.notFoundStatusCode()));
            });
        });
        //we can't create stations so no way to test if one exists without relying on a specific school
    });
});

