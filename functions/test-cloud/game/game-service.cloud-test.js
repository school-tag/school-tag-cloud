const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const UserRewards = require("../../main/game/user-rewards.js");

//test
const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils");
const GameTestUtils = require("../../test/game/game-test-utils");
const SchoolGameRules = require("../../main/game/school-game-rules");
const RewardTestUtils = require("../../test/game/reward-fixtures");
const ScanFixtures = require("../../test/scan/scan-fixtures");
const StationFixtures = require("../../test/station/station.fixtures");
const TagFixtures = require("../../test/tag/tag-fixtures");
const DateFixtures = require("../../test/lang/date-test-utils.js");

describe("GameService when cloud connected", function () {
    describe("scanReceived", function () {
        it("should award points for every scan received", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return GameTestUtils.everyScanShouldBeRewarded(bootstrap);
        });
        it("should not create a new user if not authorized", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return bootstrap.scanService.scanCreated(ScanFixtures.scanWithoutId()).then((scan) => {
                //process the scan to ensure points are given
                return bootstrap.gameService.scanReceived(scan).then((rewards) => {
                    return Promise.reject(`Expected to be rejected lacking authorization ${rewards}`);
                }, (error) => {
                    return Promise.resolve(error);
                });
            });
        });
        it("should create a new tag with the default category", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return GameTestUtils.authorizedScanCreated(bootstrap).then((scan) => {
                //process the scan to ensure points are given
                return bootstrap.gameService.scanReceived(scan).then((rewards) => {
                    return bootstrap.scanService.tagForScan(scan).then((tag) => {
                        expect(tag.hasCategory()).to.equal(true);
                        expect(tag.category()).to.equal(SchoolGameRules.tagCategoryBackpack());
                        expect(tag.hasType()).to.equal(true);
                        expect(tag.type()).to.equal(SchoolGameRules.tagTypeTag());
                    });
                });
            });
        });
        it("should create rewards in local time zone", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return GameTestUtils.authorizedScanCreated(bootstrap, ScanFixtures.scanUtcTomorrow()).then((scan) => {
                return bootstrap.gameService.scanReceived(scan).then((rewards) => {
                   expect(scan.timestamp().format()).to.equal(DateFixtures.timestampUtcTomorrowString());
                   //no code to access persisted scan
                    //consider using databases and hard coded reference to verify writing
                });
            });
        });
        it("should create a new tag with associated user id assigned", function () {

            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            //user must exist before association
            bootstrap.userService.createUser().then((user) => {
                const scan = ScanFixtures.scanWithoutId().toBuilder().userIdOfTagOwner(user.id().toString()).build();
                return bootstrap.scanService.scanCreated(scan).then((scan) => {
                    return bootstrap.gameService.scanReceived(scan).then(() => {
                        return bootstrap.scanService.tagById(scan.tagId()).then((tag) => {
                            expect(tag.userId()).to.deep.equal(user.id());
                        });
                    });
                });

            });
        });
    });
    describe("rewardsForScan", function () {
        it("should fail when searching for a missing station", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const scan = ScanFixtures.scan1WithStation1();
            const tag = TagFixtures.tag1();
            return bootstrap.gameService.rewardsForScan(scan, tag).then((rewards) => {
                return Promise.reject("Expected to not find the station");
            }, (error) => {
                return Promise.resolve(error);
            });
        });
        it("should include distance provided by the station", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const station = StationFixtures.station1WithType1Distance1();
            return bootstrap.stationService.repository.save(station).then(() => {
                return GameTestUtils.authorizedScanCreated(bootstrap).then((scan) => {
                    //notice the scan is not being updated with the station, which is not necessary in this case
                    const scanWithStation = scan.toBuilder().station(station.id()).build();
                    return bootstrap.gameService.scanReceived(scanWithStation).then((rewards) => {
                        expect(rewards.length).to.equal(1);
                        const reward = rewards[0];
                        expect(reward.metersToSchool()).to.equal(station.metersToSchool());
                    });
                });
            });
        });
        it("should create a new tag with the default category", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return GameTestUtils.authorizedScanCreated(bootstrap).then((scan) => {
                //process the scan to ensure points are given
                return bootstrap.gameService.scanReceived(scan).then((rewards) => {
                    return bootstrap.scanService.tagForScan(scan).then((tag) => {
                        expect(tag.hasCategory()).to.equal(true);
                        expect(tag.category()).to.equal(SchoolGameRules.tagCategoryBackpack());
                        expect(tag.hasType()).to.equal(true);
                        expect(tag.type()).to.equal(SchoolGameRules.tagTypeTag());
                    });
                });
            });
        });
        it("should create a new tag with associated user id assigned", function () {

            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            //user must exist before association
            bootstrap.userService.createUser().then((user) => {
                const scan = ScanFixtures.scanWithoutId().toBuilder().userIdOfTagOwner(user.id().toString()).build();
                return bootstrap.scanService.scanCreated(scan).then((scan) => {
                    return bootstrap.gameService.scanReceived(scan).then(() => {
                        return bootstrap.scanService.tagById(scan.tagId()).then((tag) => {
                            expect(tag.userId()).to.deep.equal(user.id());
                        });
                    });
                });

            });
        });
    });

    describe("sendMessageForRewards", function () {
        it("should return no messages for no rewards", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return bootstrap.gameService.sendMessageForRewards([]).then((messagesPromises) => {
                expect(messagesPromises).to.not.equal(null);
                expect(messagesPromises).to.have.lengthOf(0);
            });
        });
        it("should return error for a user not found", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const reward = RewardTestUtils.reward1();
            const rewards = [reward];
            return bootstrap.gameService.sendMessageForRewards(rewards)
                .then((messagesPromises) => {
                    return Promise.reject(new Error("not expecting the user to be found."));
                }, (error) => {
                    expect(error.message).to.contain(reward.userId().toString());
                    return Promise.resolve();
                });
        });
        it("should return a message promise for a single reward", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            return bootstrap.userService.createUser().then((user) => {
                const ruleId2 = "rule-2";
                const reward1 = RewardTestUtils.builder1WithMinimal().userId(user.id()).points(33).build();
                const reward2 = RewardTestUtils.builder1WithMinimal()
                    .userId(user.id()).ruleId(ruleId2).points(44).build();
                const rewards = [reward1, reward2];
                return bootstrap.gameService.sendMessageForRewards(rewards).then((messagesPromises) => {
                    expect(messagesPromises).to.not.equal(null);
                    return Promise.all(messagesPromises).then((messages) => {
                        expect(messages).to.have.lengthOf(2);
                        const message1 = messages[0];
                        //unhandled rules show rule id, but this is not a requirement
                        expect(message1.text()).to.contain(reward1.ruleId());
                        expect(message1.aboutUser().id()).to.deep.equal(user.id());
                        const message2 = messages[1];
                        expect(message2.text()).to.contain(reward2.points());
                        expect(message2.aboutUser().id()).to.deep.equal(user.id());

                    });
                });
            });

        });
    });
});
