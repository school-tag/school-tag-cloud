const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils");
const GameFirebaseHttpFunctions = require("../../main/game/game-firebase-http-functions.js");
const Errors = require("../../main/lang/errors");
const UserRepository = require("../../main/user/user-repository");
const NotificationRecipient = require("../../main/notification/notification-recipient");
const NotificationResult = require("../../main/notification/notification-result");
const MessageFixtures = require("../../test/message/message.fixtures");

describe("GameFirebaseHttpFunctions", function () {
    describe("followUserHttpFunction", function () {
        const expectedRecipientId = "abc134";
        const expectedUserId = "fake-user";
        const request = {
            body: {
                following: expectedUserId,
                notificationRecipient: {
                    recipientId: expectedRecipientId,
                },
            },
        };

        it("should return a 200 when succesful", function (done) {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const functions = new GameFirebaseHttpFunctions(bootstrap);
            let followUserCalled = false;
            functions.followUser = function (userId, recipient) {
                expect(userId).to.equal(expectedUserId);
                expect(recipient.recipientId()).to.equal(expectedRecipientId);
                followUserCalled = true;
                return Promise.resolve(MessageFixtures.message1());
            }
            const request = {
                body: {
                    following: expectedUserId,
                    notificationRecipient: {
                        recipientId: expectedRecipientId,
                    },
                },
            };
            const response = {
                status: function (status) {
                    expect(status).to.equal(200);
                    expect(followUserCalled).to.equal(true);
                    done();
                },
            };
            functions.followUserHttpFunction(request, response);
        });
        it("should return a 400 when error has no status coe", function (done) {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const functions = new GameFirebaseHttpFunctions(bootstrap);
            functions.followUser = function () {
                return Promise.reject(new Error("error without code"));
            };
            const response = {
                status: function (status) {
                    expect(status).to.equal(400);
                    done();
                },
            };
            functions.followUserHttpFunction(request, response)
        });
        it("should return the error code when the error reports one", function (done) {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const functions = new GameFirebaseHttpFunctions(bootstrap);
            const status = 999;
            functions.followUser = function () {
                return Promise.reject(Errors.of("error with status", "ank2", status));
            };
            const response = {
                status: function (status) {
                    expect(status).to.equal(status);
                    done();
                },
            };
            functions.followUserHttpFunction(request, response)
        });
    });
    describe("followUser", function () {
        it("should return 404 for user not found", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const functions = new GameFirebaseHttpFunctions(bootstrap);
            return functions.followUser("not-an-existing-user", null)
                .then(() => {
                    return Promise.reject("shouldn't be found");
                })
                .catch((error) => {
                    expect(error.code).to.equal(UserRepository.userNotFoundCode());
                    expect(Errors.httpStatusCode(error)).to.equal(404);
                    return Promise.resolve();
                });
        });
        it("should return 400 for a missing token", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const functions = new GameFirebaseHttpFunctions(bootstrap);
            return bootstrap.userService.createUser()
                .then((user) => {
                    const recipient = NotificationRecipient.builder().build();
                    return functions.followUser(user.id().toString(), recipient)
                        .then(() => {
                            return Promise.reject("missing token");
                        })
                        .catch((error) => {
                            expect(error.code).to.equal("k3dc");
                            expect(Errors.httpStatusCode(error)).to.equal(400);
                            return Promise.resolve();
                        });
                });
        });
        it("should return 403 for an invalid token", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const functions = new GameFirebaseHttpFunctions(bootstrap);
            return bootstrap.userService.createUser()
                .then((user) => {
                    functions.validateRecipientSubscription = function () {
                        const notificationResult = NotificationResult.builder()
                            .error(Errors.of("testing rejection","an3d")).build();
                        return Promise.resolve( notificationResult);
                    }
                    const recipient = NotificationRecipient.builder().recipientId("token-doesn't-matter").build();
                    return functions.followUser(user.id().toString(), recipient)
                        .then(() => {
                            return Promise.reject("shouldn't be valid");
                        })
                        .catch((error) => {
                            expect(Errors.hasHttpStatusCode(error));
                            expect(Errors.httpStatusCode(error)).to.equal(403);
                            return Promise.resolve();
                        });
                });
        });
        it("should return a 400 for not providing a user", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const functions = new GameFirebaseHttpFunctions(bootstrap);
            return functions.followUser(null, null)
                .then(() => {
                    return Promise.reject("missing token");
                })
                .catch((error) => {
                    expect(error.code).to.equal("kdn2");
                    expect(Errors.httpStatusCode(error)).to.equal(400);
                    return Promise.resolve();
                });
        });
        it("should return a message for sending a token", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const functions = new GameFirebaseHttpFunctions(bootstrap);
            return bootstrap.userService.createUser()
                .then((user) => {
                    const recipient = NotificationRecipient.builder().recipientId("test-tokens-always-work").build();
                    return functions.followUser(user.id().toString(), recipient)
                        .then((message) => {
                            expect(message.code()).to.equal("cn2d");
                        });
                });
        });
    });
});

