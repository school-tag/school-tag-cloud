//test
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));

const DateTestUtils = require("../../test/lang/date-test-utils.js");
const ScanFixtures = require("../../test/scan/scan-fixtures.js");
const TagFixtures = require("../../test/tag/tag-fixtures");
const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils.js");

const DateUtils = require("../../main/lang/date-utils.js");
const GameFirebaseDatabaseFunctions = require("../../main/game/game-firebase-database-functions.js");
const UserRewards = require("../../main/game/user-rewards.js");
const UserId = require("../../main/user/user.js").UserId;
const Scan = require("../../main/scan/scan");
const GameCalculator = require("../../main/game/game-calculator.js");
const SchoolGameRules = require("../../main/game/school-game-rules");

/** @type SystemBootstrap*/
let bootstrap;
/** @type GameFirebaseDatabaseFunctions*/
let functions;

let tieLaces = function () {
    bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
    functions = new GameFirebaseDatabaseFunctions(bootstrap);
};

let tieLacesWithNoRules = function () {
    bootstrap = FirebaseCloudTestUtils.bootstrapWithRules(this, []);
    functions = new GameFirebaseDatabaseFunctions(bootstrap);
};
/**
 *
 * @param {int} expectedRewardCount
 * @return {Promise<unknown>}
 */
function assertScanReceived(expectedRewardCount) {
    const scan = ScanFixtures.scanWithoutId();
    return bootstrap.tagService.tagAndUserCreated(scan.tagId(), TagFixtures.optionsAuthorizedToCreate()).then(() => {
        return bootstrap.scanService.scanCreated(scan).then((scan) => {
            const scanRef = bootstrap.scanRepository._ref(scan.id());
            return bootstrap.scanRepository._snapshot(scanRef).then((snapshot) => {
                return functions.scanReceived(snapshot).then((rewards) => {
                    expect(rewards).to.not.equal(null);
                    expect(rewards).to.have.lengthOf(expectedRewardCount);
                });
            });
        });
    });
}
describe("GameFirebaseDatabaseFunctions", function () {
    describe("scanReceived", function () {
        it("should award points for every scan received", function () {
            //tie the laces for the bootstrap within the method so title is available.
            tieLaces.call(this);
            return assertScanReceived(1);
        });
        it("should award no points for when no rules matched", function () {
            //no rules, no rewards
            tieLacesWithNoRules.call(this);
            return assertScanReceived(0);
        });
        it("should not reward for scans in a batch", function () {
            //batches create the rewards so this will avoid the database trigger from going wild
            tieLaces.call(this);
            const scan = ScanFixtures.scanWithoutId().toBuilder().batchId("test-batch").build();
            return bootstrap.tagService.tagAndUserCreated(scan.tagId(), TagFixtures.optionsAuthorizedToCreate()).then(() => {
                return bootstrap.scanService.scanCreated(scan).then((scan) => {
                    const scanRef = bootstrap.scanRepository._ref(scan.id());
                    return bootstrap.scanRepository._snapshot(scanRef).then((snapshot) => {
                        return functions.scanReceived(snapshot).then((rewards) => {
                            //no rewards because batch scans are ignored here
                            expect(rewards).to.have.lengthOf(0);
                        });
                    });
                });
            });
        });
    });
    describe("scanCreated", function () {
        it("should create a new scan", function () {
            //tie the laces for the bootstrap within the method so title is available.
            tieLaces.call(this);
            const scan = ScanFixtures.scanWithoutId();
            expect(scan.hasTimestamp()).to.equal(true);
            return bootstrap.tagService.tagAndUserCreated(scan.tagId(), TagFixtures.optionsAuthorizedToCreate())
                .then(() => {
                    return functions.scanCreated(scan).then((scanCreated) => {
                        expect(scanCreated.id()).to.not.equal(null);
                        expect(scanCreated.authUid()).to.exist;
                        //timestamp can be modified if not provided so ensure it is not modified when provided
                        expect(scanCreated.timestamp().isSame(scan.timestamp())).to.equal(true);
                    });
                });
        });
        it("should assign a timestamp if not provided", function () {
            //tie the laces for the bootstrap within the method so title is available.
            tieLaces.call(this);
            const scan = Scan.builder().tagId(TagFixtures.tag1Id()).build();
            expect(scan.hasTimestamp()).to.equal(false);
            return bootstrap.tagService.tagAndUserCreated(scan.tagId(), TagFixtures.optionsAuthorizedToCreate())
                .then(() => {
                    return functions.scanCreated(scan).then((scanCreated) => {
                        expect(scanCreated.timestamp()).to.not.equal(null);
                        expect(scanCreated.timestamp().utcOffset()).to.equal(bootstrap.momentService.now().utcOffset());
                    });
                });
        });
    });
    describe("scansCreated", function () {
        const scan1 = ScanFixtures.scanWithoutId();
        const scan2 = ScanFixtures.scan2WithoutId();
        const scans = [scan1, scan2];
        it("should create multiple scans", function () {
            tieLaces.call(this);
            return bootstrap.tagService.tagAndUserCreated(scan1.tagId(), TagFixtures.optionsAuthorizedToCreate())
                .then(() => {
                    return functions.scansCreated(scans).then((calculator) => {
                        expect(calculator.points()).to.equal(scans.length * SchoolGameRules.everyScanCountsPoints()); //every scan counts ... twice
                        const rewards = calculator.rewards();
                        expect(rewards.length).to.equal(scans.length);
                        //not the best test, but no other correlation between scan and reward besides scanId not avail.
                        expect(rewards[0].ruleId()).to.equal(SchoolGameRules.everyScanCountsRuleId());
                        expect(rewards[1].ruleId()).to.equal(SchoolGameRules.everyScanCountsRuleId());
                        //confirm both scans created with same batch id
                        return bootstrap.scanRepository.list().then((scansFound) => {
                            expect(scansFound.length).to.equal(scans.length);
                            let batchId;
                            scansFound.forEach((scan) => {
                                if (batchId) {
                                    expect(scan.batchId()).to.equal(batchId);
                                } else {
                                    batchId = scan.batchId();
                                    expect(batchId).to.not.be.undefined;
                                }
                            });
                        });
                    });
                });
        });
        it("should find no rewards with no rules", function () {
            tieLacesWithNoRules.call(this);
            return bootstrap.tagService.tagAndUserCreated(scan1.tagId(), TagFixtures.optionsAuthorizedToCreate())
                .then(() => {
                    return functions.scansCreated(scans).then((calculator) => {
                        expect(calculator.points()).to.equal(0);
                    });
                });
        });
        it("should handle just properties", function () {
            tieLaces.call(this);
            const scan1 = ScanFixtures.scanWithoutId();
            const scans = [scan1.portable()];
            return bootstrap.tagService.tagAndUserCreated(scan1.tagId(), TagFixtures.optionsAuthorizedToCreate())
                .then(() => {
                    return functions.scansCreated(scans).then((calculator) => {
                        expect(calculator.rewards().length).to.equal(scans.length);
                    });
                });
        });
        it("should consider existing rewards", function () {
            tieLaces.call(this);
            const scan1 = ScanFixtures.scanWithoutId();
            const scan2 = ScanFixtures.scan2WithoutId();
            return bootstrap.tagService.tagAndUserCreated(scan1.tagId(), TagFixtures.optionsAuthorizedToCreate())
                .then(() => {
                    return functions.scansCreated([scan1]).then((calculator) => {
                        expect(calculator.rewards().length).to.equal(1);
                        //do another scan and expect to get back both rewards
                        return functions.scansCreated([scan2]).then((calculator) => {
                            const rewards = calculator.rewards();
                            const expectedRewardCount = 2;
                            expect(rewards.length).to.equal(expectedRewardCount);
                            expect(rewards[0].scanId()).to.not.equal(rewards[1].scanId());
                            const expectedPoints = SchoolGameRules.everyScanCountsPoints()*expectedRewardCount;
                            expect(calculator.points()).to.equal(expectedPoints);
                        });
                    });
                });
        });
    });
    describe("userDailyPoints", function () {
        it("should total the points for a scan received", function () {
            tieLaces.call(this);
            const scan = ScanFixtures.scanWithoutId();
            return bootstrap.scanService.scanCreated(scan).then((scan) => {
                const options = TagFixtures.optionsAuthorizedToCreate();
                return bootstrap.tagService.tagAndUserCreated(scan.tagId(), options).then(() => {
                    return bootstrap.gameService.scanReceived(scan).then((rewards) => {
                        return bootstrap.tagService.tagFoundOrCreated(scan.tagId()).then((tag) => {
                            const ref = bootstrap.userRewardsService._userRewardsRepository
                                .userRewardsDayScansRef(tag.userId(), scan);
                            return bootstrap.userRewardsService._userRewardsRepository._snapshot(ref)
                                .then((snapshot) => {
                                    return functions.userDailyPoints(snapshot).then(
                                        (calculator) => {
                                            expect(calculator.points()).equal(SchoolGameRules.everyScanCountsPoints());
                                            expect(calculator.coins()).equals(SchoolGameRules.everyScanCountsCoins());
                                            //confirm points are persisted.
                                        });
                                });
                        });
                    });
                });
            });
        });
    });
    describe("totalsUpdatedFromDay", function () {
        let shouldUpdateTotalsForDay = function (moment) {
            const userId = new UserId("foo");
            //these points are set, not part of any rule achievement
            const expectedPoints = 10;
            const expectedCoins = 2;
            const userRewards = UserRewards.builder()
                .points(expectedPoints)
                .coins(expectedCoins)
                .timeInterval(DateUtils.dayMomentRange(moment)).build();
            const calculator1 = GameCalculator.builder().rewards([userRewards]).build();
            //a little setup...
            return bootstrap.userRewardsService._userRewardsRepository
                .setUserRewardsForDayFromCalculator(userId, moment, calculator1).then(() => {
                    const ref = bootstrap.userRewardsService._userRewardsRepository.userDailyTotalsRef(userId, moment);
                    //the goal of the test.
                    return functions.totalsUpdatedFromDay(ref).then((calculators) => {
                        const weekCalculator = calculators[0];
                        expect(weekCalculator.points()).to.equal(expectedPoints);
                        expect(weekCalculator.coins()).to.equal(expectedCoins);
                        //now ask the service.
                        return bootstrap.gameService.userRewardsWeekTotals(userId, moment).then((weekTotals) => {
                            expect(weekTotals.points()).to.equal(expectedPoints);
                            expect(weekTotals.coins()).to.equal(expectedCoins);
                            return weekTotals;
                        }).then(() => {
                            return bootstrap.userRewardsService.userRewardsYearTotals(userId, moment)
                                .then((yearTotals) => {
                                    expect(yearTotals.points()).to.equal(expectedPoints);
                                    expect(yearTotals.coins()).to.equal(expectedCoins);
                                    return yearTotals;
                                }).then(() => {
                                    return bootstrap.userRewardsService.userRewardsGrandTotals(userId).then((grandTotals) => {
                                        expect(grandTotals.points()).to.equal(expectedPoints);
                                        expect(grandTotals.coins()).to.equal(expectedCoins);
                                    }).then(() => {
                                        return bootstrap.userRewardsService.grandTotalsForAll().then((grandTotalsList) => {
                                            expect(grandTotalsList.length).to.equal(1);
                                            const grandTotals = grandTotalsList[0];
                                            expect(grandTotals.points()).to.equal(expectedPoints);
                                            expect(grandTotals.coins()).to.equal(expectedCoins);
                                        });
                                    });
                                });
                        });
                    });
                });
        };
        it("should update day's parent totals", function () {
            tieLaces.call(this);
            const moment = DateTestUtils.timestamp1();
            return shouldUpdateTotalsForDay(moment);
        });
        /** https://bitbucket.org/school-tag/school-tag-cloud/issues/4 */
        it("should update day's parent totals on sundays", function () {
            tieLaces.call(this);
            return shouldUpdateTotalsForDay(DateTestUtils.timestampSunday1());
        });
    });
    describe("userRewardsTotalsForDayPathTemplate", function () {
        it("should produce the path to the users totals", function () {
            const expected = "/rewards/users/{userId}/totals/dailies/years/{year}/weeks/{week}/days/{day}";
            expect(GameFirebaseDatabaseFunctions.userRewardsTotalsForDayPathTemplate()).to.equal(expected);
        });
    });
    describe("userRewardsScansForDayPathTemplate", function () {
        it("should produce the path to the users totals", function () {
            const expected = "/rewards/users/{userId}/scans/years/{year}/weeks/{week}/days/{day}";
            expect(GameFirebaseDatabaseFunctions.userRewardsScansForDayPathTemplate()).to.equal(expected);
        });
    });

});
