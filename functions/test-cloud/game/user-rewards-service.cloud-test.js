const console = require("console");
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));

const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils");
const ScanTestUtils = require("../../test/scan/scan-fixtures");
const SchoolGameRules = require("../../main/game/school-game-rules");
const NotificationRecipient = require("../../main/notification/notification-recipient");
const UserId = require("../../main/user/user").UserId;
const RewardTestUtils = require("../../test/game/reward-fixtures");
const GameTestUtils = require("../../test/game/game-test-utils");
const Scan = require("../../main/scan/scan.js");
const TagFixtures = require("../../test/tag/tag-fixtures");
const TagService = require("../../main/tag/tag-service");
const UserFixtures = require("../../test/user/user-fixtures");
const Errors = require("../../main/lang/errors");
const ScanFixtures = require("../../test/scan/scan-fixtures.js");
const GameCalculator = require("../../main/game/game-calculator");

describe("UserRewardsService, connected to the cloud,", function () {
    describe("ruleCount", function () {
        it("should be zero for no rules yet rewarded", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const scan = ScanTestUtils.scan1();
            //must create the scan with authorization approved first
            return bootstrap.scanService.tagForScan(scan, TagService.authorizedToCreateOption({}, true)).then(() => {
                return bootstrap.userRewardsService.userRuleRewardCountForDayOfScan(scan,
                    "rule-id-nonexistent").then((count) => {
                    expect(count).to.equal(0);
                });
            });
        });
    });
    it("should be 1 for every scan counts rewarded", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        return GameTestUtils.authorizedScanCreated(bootstrap).then((scan) => {
            return bootstrap.gameService.scanReceived(scan).then((rewards) => {
                expect(rewards).to.have.lengthOf(1);
                const expectedRuleId = SchoolGameRules.everyScanCountsRuleId();
                expect(rewards[0].ruleId()).to.equal(expectedRuleId);
                return bootstrap.userRewardsService.userRuleRewardCountForDayOfScan(scan, expectedRuleId)
                    .then((count) => {
                        expect(count).to.equal(1);
                        return bootstrap.userRewardsService.userRuleRewardCountForDayOfScan(scan, "other-rule")
                            .then((count) => {
                                expect(count).to.equal(0);
                            });
                    });
            });
        });
    });
});
describe("rewardsForUserForDay", function () {
    it("should be empty by default", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        const now = bootstrap.momentService.now();
        return bootstrap.userRewardsService.rewardsForUserForDay("user", now).then((rewards) => {
            expect(rewards).to.have.lengthOf(0);
        });
    });
    it("should have a single reward for a single scan", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        return GameTestUtils.authorizedScanCreated(bootstrap).then((scan) => {
            return bootstrap.gameService.scanReceived(scan).then((rewardsFromScanReived) => {
                expect(rewardsFromScanReived).to.have.lengthOf(1);
                const reward1 = rewardsFromScanReived[0];
                const userId = reward1.userId();
                const moment = scan.timestamp();
                return bootstrap.userRewardsService.rewardsForUserForDay(userId, moment).then((rewards) => {
                    expect(rewards).to.have.lengthOf(1);
                });
            });
        });
    });
});

describe("totalPointsForUserFromScanForRule", function () {
    it("should be zero with no rewards yet", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        const scan = ScanFixtures.scan1();
        return bootstrap.scanService.tagForScan(scan, TagService.authorizedToCreateOption({}, true)).then(() => {
            const ruleId = "doesNotMatter";
            return bootstrap.userRewardsService.totalPointsForUserFromScanForRule(scan, ruleId).then((points) => {
                expect(points).to.equal(0);
            });
        });
    });
    it("should be have points for the rewarded rule and no points for other rules", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);

        const reward1 = RewardTestUtils.reward1();
        const userId = reward1.userId();
        const ruleId = reward1.ruleId();
        const calculator = GameCalculator.builder().rewards([reward1]).build();
        const userRewardsRepository = bootstrap.userRewardsService._userRewardsRepository;
        return userRewardsRepository.setUserRewardsForYearFromCalculator(userId, bootstrap.momentService.now(), calculator).then(() => {
            return bootstrap.userRewardsService.totalPointsForUserForRule(userId, ruleId)
                .then((points) => {
                    expect(points).to.equal(reward1.points());
                    return bootstrap.userRewardsService.totalPointsForUserForRule(userId, "not-the-rule")
                        .then((points) => {
                            expect(points).to.equal(0);
                        });
                });
        });
    });
    it("should get the user id from the scan", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        return GameTestUtils.authorizedScanCreated(bootstrap).then((scan) => {
            return bootstrap.gameService.scanReceived(scan).then((rewards) => {
                expect(rewards).to.have.lengthOf(1);
                const reward = rewards[0];
                const expectedUserId = reward.userId();
                const expectedRuleId = reward.ruleId();
                const userRewardsService = bootstrap.userRewardsService;
                const expectedPoints = 13;
                //mock out the function to ensure correct interface
                //rely on sister test to ensure rewards query
                userRewardsService.totalPointsForUserForRule = (userId, ruleId) => {
                    expect(userId.value).to.equal(expectedUserId.value);
                    expect(ruleId).to.equal(expectedRuleId);
                    return expectedPoints;
                };
                return userRewardsService.totalPointsForUserFromScanForRule(scan, expectedRuleId)
                    .then((points) => {
                        expect(points).to.equal(expectedPoints);
                    });
            });
        });
    });
});
describe("userIdFromScan", function () {
    const tag = TagFixtures.tag1();
    const scan = ScanFixtures.scan1();
    const scanWithRewardRecipient = ScanFixtures.scan1().toBuilder().userIdOfRewardRecipient("other-user").build();
    const tagWithoutUser = TagFixtures.withId().build();

    it("should use the user id from the tag if provided", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        return bootstrap.userRewardsService.userIdFromScan(scan, tag).then((userId) => {
            expect(userId).to.deep.equal(tag.userId());
        });
    });
    it("should use the user id from the scan if reward recipient provided", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        return bootstrap.userRewardsService.userIdFromScan(scanWithRewardRecipient, tagWithoutUser)
            .then((userId) => {
                expect(userId.toString()).to.equal(scanWithRewardRecipient.userIdOfRewardRecipient());
            });
    });
    it("should use the user id from the tag even if reward recipient provided", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        return bootstrap.userRewardsService.userIdFromScan(scanWithRewardRecipient, tag).then((userId) => {
            expect(userId).to.deep.equal(tag.userId());
        });
    });
    it("should fail if the tag and scan don't provide user id", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        return bootstrap.userRewardsService.userIdFromScan(scan, tagWithoutUser).then((userId) => {
            expect(userId).to.equal(null);
        }, (error) => {
            expect(Errors.httpStatusCode(error)).to.equal(400);
        });
    });
    it("should should get the user from the auth id if tag doesn't provide", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        return bootstrap.userService.createUser().then((user) => {
            const authUid = UserFixtures.authUid1();
            return bootstrap.userService.setAuthUid(user.id(), authUid).then(() => {
                /** @type {Scan} */
                const scanWithAuthUser = Scan.builder().tagId("no-matter").authUid(authUid).build();
                return bootstrap.userRewardsService.userIdFromScan(scanWithAuthUser, tagWithoutUser).then((userId) => {
                    expect(userId).to.deep.equal(user.id());
                });
            });
        });
    });
});
describe("sendNotificationsForRewards", function () {
    it("should send no notifications for no subscribers", function () {
        let bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        return bootstrap.userService.createUser().then((user) => {
            const reward = RewardTestUtils.builder1WithMinimal().userId(user.id()).build();
            return bootstrap.userRewardsService.notifyFollowersOfReward(reward)
                .then((notificationPromises) => {
                    expect(notificationPromises).to.have.length(0);
                });
        });
    });
    it("should send one notification for one subscriber", function () {
        let bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        return bootstrap.userService.createUser().then((user) => {
            const recipient = NotificationRecipient.builder()
                .recipientId("game-service-one-follower")
                .subscribedAtTime(bootstrap.momentService.now()).build();
            return bootstrap.userRewardsService.followUser(user.id(), recipient).then(() => {
                const reward = RewardTestUtils.builder1WithMinimal().userId(user.id()).build();
                return bootstrap.userRewardsService.notifyFollowersOfReward(reward)
                    .then((notificationPromises) => {
                        expect(notificationPromises).to.have.length(1);
                        const promise = notificationPromises[0];
                        return promise.then((notificationResult) => {
                            expect(notificationResult.recipientId()).to.equal(recipient.recipientId());
                        });
                    });
            });
        });
    });
    it("should have error for user not found", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        const reward1 = RewardTestUtils.reward1();
        return bootstrap.userRewardsService.notifyFollowersOfReward(reward1)
            .then(() => {
                return Promise.reject("expecting error")
            }, (error) => {
                expect(error.message).to.contain(reward1.userId().toString());
                return Promise.resolve();
            });
    });
    describe("userRewardsForDay", function () {
        it("should not be available until rewards are processed", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const scan = ScanFixtures.scan1();
            return bootstrap.userRewardsService.userRewardsForDay(UserFixtures.user1Id(), scan.timestamp())
                .then((userRewards) => {

                }).catch((error) => {
                    expect(error.message).to.contain("not found");
                });
        });
    });
});
describe("followUser", function () {
    it("should be able to follow a user", function () {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
        const recipient = NotificationRecipient.builder()
            .recipientId("recipient-id-90tT")
            .subscribedAtTime(bootstrap.momentService.now()).build();
        const userId = new UserId("follow-user-18kO");
        return bootstrap.userRewardsService.followUser(userId, recipient).then(() => {
            return bootstrap.userRewardsService.recipientsFollowingUser(userId).then((recipients) => {
                expect(recipients.length).to.equal(1);
                const foundRecipient = recipients[0];
                expect(foundRecipient.recipientId()).to.equal(recipient.recipientId());
            });
        });
    });
});


