const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;

const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils.js");
const MessageService = require("../../main/message/message-service.js");
const Message = require("../../main/message/message.js");
const MessageFixtures = require("../../test/message/message.fixtures.js");

describe("MessageService", function () {
    describe("send", function () {
        it("should create a message that can be retrieved", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const message = MessageFixtures.message1();
            expect(message.hasTimestamp()).to.equal(true);
            const messageService = bootstrap.messageService;
            return messageService.send(message).then((messageWithId)=>{
                expect(messageWithId).to.not.equal(null);
                const messageId = messageWithId.id();
                expect(messageId).to.not.equal(null);
                expect(message.timestamp().toISOString()).to.equal(MessageFixtures.timestamp1().toISOString());
                return messageService.messageById(messageId).then((found)=>{
                    expect(found).to.not.equal(null);
                    expect(found.id()).to.equal(messageId);
                    expect(found.text()).to.equal(message.text());
                });
            });
        });
        it("should add a timestamp if not provided", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const message = Message.builder().text("testing timestamp").build();
            expect(message.hasTimestamp()).to.equal(false);
            const messageService = bootstrap.messageService;
            return messageService.send(message).then((persistedMessage)=>{
                expect(message.hasTimestamp(),"original not modified").to.equal(false);
                expect(persistedMessage.hasTimestamp()).to.equal(true);
                return messageService.messageById(persistedMessage.id()).then((found)=>{
                    expect(found.hasTimestamp()).to.equal(true);
                });
            });
        });
    });
});
