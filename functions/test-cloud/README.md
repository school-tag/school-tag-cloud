## Cloud Testing - Before Deploy

Tests that validate cloud connected resources (using firebase), __before deployment__ to the cloud.
Integration tests must run in an isolated environment that do not depend on any existing
resources on the cloud, hence no tests that can depend upon each other.

Integration tests typically produce data in different entities during the same test 
so a test can validate the dependnecy on another is working.  

These tests can take some time to complete because remote calls are being made.  Timeouts should be set to a few seconds.

Read more about [testing levels][1].


[1]: https://en.wikipedia.org/wiki/Software_testing#Testing_levels
