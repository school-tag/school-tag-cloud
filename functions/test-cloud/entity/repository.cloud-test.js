const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;
const chai = require("chai");
chai.use(require("chai-string"));
const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils.js");
const Entity = require("../../main/entity/entity.js");
const Repository = require("../../main/entity/repository.js");
const TestEntity = require("../../test/entity/test-entity.js");

describe("Repository", function () {
    const entity = TestEntity.builder().id("repository-cloud-1").build();
    let repository = function (test) {
        const bootstrap = FirebaseCloudTestUtils.bootstrap(test);
        const repository = Repository.builder()
            .auditTemplate(bootstrap.auditTemplate)
            .databaseReference(bootstrap.rootReference)
            .entityDomain("repositories")
            .entityType(TestEntity).build();
        return repository;
    };
    describe("list", function () {
        it("path to nowhere should not call action with no entities saved", function () {
            const repo = repository(this.test);
            return repo.list("/path/to/nowhere").then((entities) => {
                expect(entities.length).to.equal(0);
                return Promise.resolve();
            });
        });
        it("one entity should be listed", function () {
            const repo = repository(this.test);
            return repo.save(entity).then(() => {
                return repo.list().then((entities) => {
                    expect(entities.length).to.equal(1);
                    const entityFound = entities[0];
                    expect(entityFound.id()).to.equal(entity.id());
                    return Promise.resolve();
                });
            });
        });
        it("two entities should be listed", function () {
            const repo = repository(this.test);
            return repo.save(entity)
                .then(() => {
                    return repo.save(TestEntity.builder().id("another-usesless-id").build())
                        .then((entities) => {
                            return repo.list().then((entities) => {
                                expect(entities.length).to.equal(2);
                            });
                        });
                });
        });

    });
});
