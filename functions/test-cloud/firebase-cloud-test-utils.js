const SystemBootstrap = require("../main/bootstrap.js");
const GameTestUtils = require("../test/game/game-test-utils.js");
const FirebaseTestUtils = require("../test/firebase/firebase-test-utils.js");
const DateFixtures = require("../test/lang/date-test-utils");
const admin = require("firebase-admin");
const {PubSub} = require("@google-cloud/pubsub");

/**
 * ties together the game and firebase for complete integration testing using basic rules.
 */
class FirebaseCloudTestUtils {

    /**Provides default rules of every scan counts.
     * @param {Test} test being executed
     * @return {SystemBootstrap} the bootstrap providing the runtime reference to everything
     */
    static bootstrap(test) {
        const rules = [GameTestUtils.everyScanCountsRule()];
        return FirebaseCloudTestUtils.bootstrapWithRules(test, rules);
    }

    /**
     * @param {Test} [test] being executed
     * @return {SystemBootstrap} the bootstrap providing the runtime reference to everything
     */
    static bootstrapWithDatabaseRules(test) {
        return FirebaseCloudTestUtils.bootstrapWithRules(test, null);
    }

    /**
     * @param {Test} test being executed
     * @param {Rule[]} rules? passed in to the bootstrap to be used. Null uses database rules
     * @param {Messaging} [messaging] the optional messaging mock to
     *                      simulate responses different than the default all successes
     * @return {SystemBootstrap} the bootstrap providing the runtime reference to everything
     */
    static bootstrapWithRules(test, rules, messaging) {
        //give a unique root to ensure no server side triggers will happen and creations are new
        const firebaseTestUtils = FirebaseTestUtils.instance();
        const testRef = firebaseTestUtils.testReference(test);
        const pubSub = new PubSub();

        //provide a do-nothing mock.
        if (!messaging) {
            messaging = {
                sendToDevice: function (fcm, payload) {
                    console.log(`mock messaging is sending to ${fcm} with payload ${payload}: 229s`);
                    const response = {};
                    response.results = [];
                    fcm.forEach((recipientId) => {
                        const result = {
                            messageId: "mock-notification-" + response.results.length,
                            canonicalRegistrationToken: recipientId,
                        };
                        response.results.push(result);
                    });
                    return Promise.resolve(response);
                },
            };
        }
        return SystemBootstrap.builder()
            .rootReference(testRef)
            .auditTemplate(firebaseTestUtils.auditTemplate)
            .messaging(messaging)
            .timeZone(DateFixtures.timeZone())
            .rules(rules)
            .admin(admin)
            .pubSub(pubSub)
            .build();
    }
}

module.exports = FirebaseCloudTestUtils;
