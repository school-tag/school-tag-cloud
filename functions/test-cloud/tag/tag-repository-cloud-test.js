const appRootDir = require("app-root-dir").get();
const TagTestUtils = require(appRootDir + "/test/tag/tag-fixtures.js");
const FirebaseCloudTestUtils = require(appRootDir + "/test-cloud/firebase-cloud-test-utils.js");
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;


describe("TagFirebaseRepository", function () {
    describe("save", function () {
        it("should save without error ", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const tag = TagTestUtils.tag1();
            return bootstrap.tagRepository.save(tag).then(function () {
                return bootstrap.tagRepository.get(tag.tagId()).then((tagReturned) => {
                        expect(tagReturned).to.not.equal(null);
                        expect(tagReturned.tagId()).to.equal(tag.tagId());
                    }
                );
            });
        });
        it("should indicate not found", function (done) {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            bootstrap.tagRepository.get("junk").catch((error) => {
                done();
            });
        });
    });
});


