const describe = require("mocha").describe;
const it = require("mocha").it;
const before = require("mocha").before;
const expect = require("chai").expect;

const appRootDir = require("app-root-dir").get();
const Tag = require(appRootDir + "/main/tag/tag.js");
const TagService = require(appRootDir + "/main/tag/tag-service.js");
const TagTestUtils = require(appRootDir + "/test/tag/tag-fixtures.js");
const FirebaseCloudTestUtils = require(appRootDir + "/test-cloud/firebase-cloud-test-utils.js");

describe("TagService", function () {
    describe("tagAndUserCreated", function () {
        it("should call tag repository and user service", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const tagId = "tagAndUserCreated-tag";
            return bootstrap.tagService.tagAndUserCreated(tagId, TagTestUtils.optionsAuthorizedToCreate()).then((tag) => {
                expect(tag).not.to.equal(null);
                expect(tag.id()).to.equal(tagId, "tag id is different");
                expect(tag.userId()).to.not.equal(null);
                expect(tag.hasCategory()).to.equal(false);
                expect(tag.hasType()).to.equal(false);
                return bootstrap.userService.user(tag.userId()).then((user) => {
                    expect(user).to.not.equal(null);
                });
            });
        });
        it("should not create a user when requested not to", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const tagId = "tag-without-user";
            const options = TagTestUtils.optionsAuthorizedToCreate();
            TagService.associatedUserShouldNotBeCreated(options, true);
            return bootstrap.tagService.tagAndUserCreated(tagId, options).then((tag) => {
                expect(tag).not.to.equal(null);
                expect(tag.id()).to.equal(tagId, "tag id is different");
                expect(tag.hasUserId()).to.equal(false);
                expect(tag.hasCategory()).to.equal(false);
                expect(tag.hasType()).to.equal(false);
            });
        });
        it("should associate the tag to the given user", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const tagId = "user-assigned-to-tag";
            const options = TagTestUtils.optionsAuthorizedToCreate();
            //must first create a user when associating, it won't create one
            return bootstrap.userService.createUser().then((user) => {
                const userId = user.id().toString();
                TagService.associateTagToUser(options, userId);
                return bootstrap.tagService.tagAndUserCreated(tagId, options).then((tag) => {
                    expect(tag.id()).to.equal(tagId, "tag id is different");
                    expect(tag.userId().toString()).to.equal(userId);
                    expect(tag.hasType()).to.equal(false);
                    expect(tag.hasCategory()).to.equal(false);
                });
            });
        });
        it("should create with type and category options", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const tagId = "tagAndUserCreated-category-type";
            const options = TagTestUtils.optionsAuthorizedToCreate();
            const type = 'yellow';
            TagService.typeOption(options, type);
            const category = 'blue';
            TagService.categoryOption(options, category);
            return bootstrap.tagService.tagAndUserCreated(tagId, options).then((tag) => {
                expect(tag.hasCategory()).to.equal(true);
                expect(tag.hasType()).to.equal(true);
                expect(tag.type()).to.equal(type);
                expect(tag.category()).to.equal(category);
            });
        });
    });

    describe("tagFoundOrCreated", function () {
        it("should find an existing tag", function () {
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const tag = TagTestUtils.tag1();
            const tagService = bootstrap.tagService;
            return tagService.tagFoundOrCreated(tag.id(), TagTestUtils.optionsAuthorizedToCreate()).then((found) => {
                expect(found.id()).to.equal(tag.id());
                return tagService.tagFoundOrCreated(tag.id()).then((found2) => {
                    expect(found2.id()).to.equal(found.id());
                    expect(found2.userId().toString()).to.equal(found.userId().toString());
                });
            });
        });
    });
    describe("associateToSchool", function () {
        it.skip("should successfully send message", function () {
            //credentials use gcloud auth - see package.json/ci:gcloud:auth:sandbox
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);
            const tagId = TagTestUtils.tag1Id();
            return bootstrap.tagService.associateToSchool(tagId);
            //not a very robust test.  lack of failure is success.
            // it validates necessary credentials
            //checking school-tag for the value would require cleanup and potential pollution of a production env
            //its not worth setting up a sandbox for such a limited application
        });
    });
})