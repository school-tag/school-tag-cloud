const Scan = require("../../main/scan/scan.js");
const FirebaseCloudTestUtils = require("../../test-cloud/firebase-cloud-test-utils.js");
const describe = require("mocha").describe;
const it = require("mocha").it;
const expect = require("chai").expect;

describe("ScanFirebaseRepository", function () {
    describe("save", function () {
        it("should save and provide id ", function () {
            const scan = Scan.builder().tagId("abc132").timestamp(new Date()).build();
            const bootstrap = FirebaseCloudTestUtils.bootstrap(this.test);

            // push provides the generated key reference right away
            const scanRef = bootstrap.scanRepository.push(scan);
            const generatedId = scanRef.getKey();
            expect(generatedId).to.not.equal(null);

            return scanRef.then(function () {
                return bootstrap.scanRepository.get(generatedId).then(
                    function (scanReturned) {
                        expect(scanReturned).to.not.equal(null);
                        expect(scanReturned.id()).to.equal(generatedId);
                        expect(scanReturned.tagId()).to.equal(scan.tagId());
                    }
                );
            }).then(()=>{
                //id should be saved by the push in the repository
                return bootstrap.scanRepository.get(generatedId).then((scanFound)=>{
                    expect(scanFound.id()).to.equal(generatedId);
                });
            });
        });
    });
});
